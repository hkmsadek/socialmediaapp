// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'InfoModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InfoModel _$InfoModelFromJson(Map<String, dynamic> json) {
  return InfoModel(
    json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    json['friend'] == null
        ? null
        : Friend.fromJson(json['friend'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$InfoModelToJson(InfoModel instance) => <String, dynamic>{
      'user': instance.user,
      'friend': instance.friend,
    };

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['abouts'] == null
        ? null
        : Abouts.fromJson(json['abouts'] as Map<String, dynamic>),
    json['firstName'],
    json['lastName'],
    json['nickName'],
    json['email'],
    json['userName'],
    json['profilePic'],
    json['coverPic'],
    json['gender'],
    json['birthDay'],
    json['phone'],
    json['status'],
  )..id = json['id'];
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'nickName': instance.nickName,
      'email': instance.email,
      'userName': instance.userName,
      'profilePic': instance.profilePic,
      'coverPic': instance.coverPic,
      'gender': instance.gender,
      'birthDay': instance.birthDay,
      'phone': instance.phone,
      'status': instance.status,
      'abouts': instance.abouts,
    };

Abouts _$AboutsFromJson(Map<String, dynamic> json) {
  return Abouts(
    json['workData'] == null
        ? null
        : WorkData.fromJson(json['workData'] as Map<String, dynamic>),
    json['educationData'] == null
        ? null
        : EducationData.fromJson(json['educationData'] as Map<String, dynamic>),
    json['locationData'] == null
        ? null
        : LocationData.fromJson(json['locationData'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AboutsToJson(Abouts instance) => <String, dynamic>{
      'workData': instance.workData,
      'educationData': instance.educationData,
      'locationData': instance.locationData,
    };

WorkData _$WorkDataFromJson(Map<String, dynamic> json) {
  return WorkData(
    (json['work'] as List)
        ?.map(
            (e) => e == null ? null : Work.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['pro_skills'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$WorkDataToJson(WorkData instance) => <String, dynamic>{
      'work': instance.work,
      'pro_skills': instance.proSkills,
    };

Work _$WorkFromJson(Map<String, dynamic> json) {
  return Work(
    json['company'],
    json['position'],
    json['city'],
    json['joinDate'],
    json['leaveDate'],
    json['currentlyWorking'],
    json['description'],
    json['privacy'],
    json['isEdit'],
  );
}

Map<String, dynamic> _$WorkToJson(Work instance) => <String, dynamic>{
      'company': instance.company,
      'position': instance.position,
      'city': instance.city,
      'joinDate': instance.joinDate,
      'leaveDate': instance.leaveDate,
      'currentlyWorking': instance.currentlyWorking,
      'description': instance.description,
      'privacy': instance.privacy,
      'isEdit': instance.isEdit,
    };

EducationData _$EducationDataFromJson(Map<String, dynamic> json) {
  return EducationData(
    (json['collages'] as List)
        ?.map((e) =>
            e == null ? null : Collages.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['high_schools'] as List)
        ?.map((e) =>
            e == null ? null : HighSchools.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$EducationDataToJson(EducationData instance) =>
    <String, dynamic>{
      'collages': instance.collages,
      'high_schools': instance.highSchools,
    };

Collages _$CollagesFromJson(Map<String, dynamic> json) {
  return Collages(
    json['collage'],
    json['degree'],
    json['city'],
    json['startDate'],
    json['endDate'],
    json['description'],
    json['privacy'],
  );
}

Map<String, dynamic> _$CollagesToJson(Collages instance) => <String, dynamic>{
      'collage': instance.collage,
      'degree': instance.degree,
      'city': instance.city,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'description': instance.description,
      'privacy': instance.privacy,
    };

HighSchools _$HighSchoolsFromJson(Map<String, dynamic> json) {
  return HighSchools(
    json['collage'],
    json['city'],
    json['startDate'],
    json['endDate'],
    json['description'],
    json['privacy'],
  );
}

Map<String, dynamic> _$HighSchoolsToJson(HighSchools instance) =>
    <String, dynamic>{
      'collage': instance.collage,
      'city': instance.city,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'description': instance.description,
      'privacy': instance.privacy,
    };

LocationData _$LocationDataFromJson(Map<String, dynamic> json) {
  return LocationData(
    json['currentCity'] == null
        ? null
        : CurrentCity.fromJson(json['currentCity'] as Map<String, dynamic>),
    json['homeTown'] == null
        ? null
        : HomeTown.fromJson(json['homeTown'] as Map<String, dynamic>),
    (json['otherPlaces'] as List)
        ?.map((e) =>
            e == null ? null : OtherPlaces.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$LocationDataToJson(LocationData instance) =>
    <String, dynamic>{
      'currentCity': instance.currentCity,
      'homeTown': instance.homeTown,
      'otherPlaces': instance.otherPlaces,
    };

CurrentCity _$CurrentCityFromJson(Map<String, dynamic> json) {
  return CurrentCity(
    json['city'],
    json['movedDate'],
    json['description'],
    json['isEdit'],
  );
}

Map<String, dynamic> _$CurrentCityToJson(CurrentCity instance) =>
    <String, dynamic>{
      'city': instance.city,
      'movedDate': instance.movedDate,
      'description': instance.description,
      'isEdit': instance.isEdit,
    };

HomeTown _$HomeTownFromJson(Map<String, dynamic> json) {
  return HomeTown(
    json['homeTown'],
    json['description'],
    json['isEdit'],
  );
}

Map<String, dynamic> _$HomeTownToJson(HomeTown instance) => <String, dynamic>{
      'homeTown': instance.homeTown,
      'description': instance.description,
      'isEdit': instance.isEdit,
    };

OtherPlaces _$OtherPlacesFromJson(Map<String, dynamic> json) {
  return OtherPlaces(
    json['city'],
    json['description'],
    json['isEdit'],
    json['movedDate'],
    json['leftDate'],
  );
}

Map<String, dynamic> _$OtherPlacesToJson(OtherPlaces instance) =>
    <String, dynamic>{
      'city': instance.city,
      'description': instance.description,
      'isEdit': instance.isEdit,
      'movedDate': instance.movedDate,
      'leftDate': instance.leftDate,
    };

Friend _$FriendFromJson(Map<String, dynamic> json) {
  return Friend(
    json['id'],
    json['request_sender_id'],
    json['request_receiver_id'],
    json['status'],
    json['isFriend'],
  );
}

Map<String, dynamic> _$FriendToJson(Friend instance) => <String, dynamic>{
      'id': instance.id,
      'request_sender_id': instance.request_sender_id,
      'request_receiver_id': instance.request_receiver_id,
      'status': instance.status,
      'isFriend': instance.isFriend,
    };
