import 'package:json_annotation/json_annotation.dart';

part 'InfoModel.g.dart';

@JsonSerializable()
class InfoModel {
  User user;
  Friend friend;
  InfoModel(this.user, this.friend);
  factory InfoModel.fromJson(Map<String, dynamic> json) =>
      _$InfoModelFromJson(json);
  Map<String, dynamic> toJson() => _$InfoModelToJson(this);
}

@JsonSerializable()
class User {
  var id;
  var firstName;
  var lastName;
  var nickName;
  var email;
  var userName;
  var profilePic;
  var coverPic;
  var gender;
  var birthDay;
  var phone;
  var status;
  Abouts abouts;
  User(
    this.abouts,
    this.firstName,
    this.lastName,
    this.nickName,
    this.email,
    this.userName,
    this.profilePic,
    this.coverPic,
    this.gender,
    this.birthDay,
    this.phone,
    this.status,
  );
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class Abouts {
  WorkData workData;
  EducationData educationData;
  LocationData locationData;
  Abouts(this.workData, this.educationData, this.locationData);
  factory Abouts.fromJson(Map<String, dynamic> json) => _$AboutsFromJson(json);
  Map<String, dynamic> toJson() => _$AboutsToJson(this);
}

@JsonSerializable()
class WorkData {
  List<Work> work;
  @JsonKey(name: "pro_skills")
  List<String> proSkills;
  WorkData(this.work, this.proSkills);
  factory WorkData.fromJson(Map<String, dynamic> json) =>
      _$WorkDataFromJson(json);
  Map<String, dynamic> toJson() => _$WorkDataToJson(this);
}

@JsonSerializable()
class Work {
  var company;
  var position;
  var city;
  var joinDate;
  var leaveDate;
  var currentlyWorking;
  var description;
  var privacy;
  var isEdit;
  Work(this.company, this.position, this.city, this.joinDate, this.leaveDate,
      this.currentlyWorking, this.description, this.privacy, this.isEdit);
  factory Work.fromJson(Map<String, dynamic> json) => _$WorkFromJson(json);
  Map<String, dynamic> toJson() => _$WorkToJson(this);
}

@JsonSerializable()
class EducationData {
  List<Collages> collages;
  @JsonKey(name: "high_schools")
  List<HighSchools> highSchools;
  EducationData(this.collages, this.highSchools);
  factory EducationData.fromJson(Map<String, dynamic> json) =>
      _$EducationDataFromJson(json);
  Map<String, dynamic> toJson() => _$EducationDataToJson(this);
}

@JsonSerializable()
class Collages {
  var collage;
  var degree;
  var city;
  var startDate;
  var endDate;
  var description;
  var privacy;
  Collages(this.collage, this.degree, this.city, this.startDate, this.endDate,
      this.description, this.privacy);
  factory Collages.fromJson(Map<String, dynamic> json) =>
      _$CollagesFromJson(json);
  Map<String, dynamic> toJson() => _$CollagesToJson(this);
}

@JsonSerializable()
class HighSchools {
  var collage;
  var city;
  var startDate;
  var endDate;
  var description;
  var privacy;
  HighSchools(this.collage, this.city, this.startDate, this.endDate,
      this.description, this.privacy);
  factory HighSchools.fromJson(Map<String, dynamic> json) =>
      _$HighSchoolsFromJson(json);
  Map<String, dynamic> toJson() => _$HighSchoolsToJson(this);
}

@JsonSerializable()
class LocationData {
  CurrentCity currentCity;
  HomeTown homeTown;
  List<OtherPlaces> otherPlaces;
  LocationData(this.currentCity, this.homeTown, this.otherPlaces);
  factory LocationData.fromJson(Map<String, dynamic> json) =>
      _$LocationDataFromJson(json);
  Map<String, dynamic> toJson() => _$LocationDataToJson(this);
}

@JsonSerializable()
class CurrentCity {
  var city;
  var movedDate;
  var description;
  var isEdit;
  CurrentCity(this.city, this.movedDate, this.description, this.isEdit);
  factory CurrentCity.fromJson(Map<String, dynamic> json) =>
      _$CurrentCityFromJson(json);
  Map<String, dynamic> toJson() => _$CurrentCityToJson(this);
}

@JsonSerializable()
class HomeTown {
  var homeTown;
  var description;
  var isEdit;
  HomeTown(this.homeTown, this.description, this.isEdit);
  factory HomeTown.fromJson(Map<String, dynamic> json) =>
      _$HomeTownFromJson(json);
  Map<String, dynamic> toJson() => _$HomeTownToJson(this);
}

@JsonSerializable()
class OtherPlaces {
  var city;
  var description;
  var isEdit;
  var movedDate;
  var leftDate;
  OtherPlaces(
      this.city, this.description, this.isEdit, this.movedDate, this.leftDate);
  factory OtherPlaces.fromJson(Map<String, dynamic> json) =>
      _$OtherPlacesFromJson(json);
  Map<String, dynamic> toJson() => _$OtherPlacesToJson(this);
}

@JsonSerializable()
class Friend {
  var id;
  var request_sender_id;
  var request_receiver_id;
  var status;
  var isFriend;
  Friend(
    this.id,
    this.request_sender_id,
    this.request_receiver_id,
    this.status,
    this.isFriend,
  );
  factory Friend.fromJson(Map<String, dynamic> json) => _$FriendFromJson(json);
  Map<String, dynamic> toJson() => _$FriendToJson(this);
}
