import 'package:json_annotation/json_annotation.dart';

part 'GroupMemberModel.g.dart';

@JsonSerializable()
class GroupMemberModel {
  List<Members> members;

  GroupMemberModel(this.members);
  factory GroupMemberModel.fromJson(Map<String, dynamic> json) =>
      _$GroupMemberModelFromJson(json);
  Map<String, dynamic> toJson() => _$GroupMemberModelToJson(this);
}

@JsonSerializable()
class Members {
  var id;
  var user_role;
  Member members;

  Members(
    this.id,
    this.user_role,
    this.members,
  );
  factory Members.fromJson(Map<String, dynamic> json) =>
      _$MembersFromJson(json);
  Map<String, dynamic> toJson() => _$MembersToJson(this);
}

@JsonSerializable()
class Member {
  var id;
  var userName;
  var firstName;
  var lastName;
  var profilePic;

  Member(
    this.id,
    this.userName,
    this.firstName,
    this.lastName,
    this.profilePic,
  );
  factory Member.fromJson(Map<String, dynamic> json) => _$MemberFromJson(json);
  Map<String, dynamic> toJson() => _$MemberToJson(this);
}
