// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GroupMemberModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GroupMemberModel _$GroupMemberModelFromJson(Map<String, dynamic> json) {
  return GroupMemberModel(
    (json['members'] as List)
        ?.map((e) =>
            e == null ? null : Members.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$GroupMemberModelToJson(GroupMemberModel instance) =>
    <String, dynamic>{
      'members': instance.members,
    };

Members _$MembersFromJson(Map<String, dynamic> json) {
  return Members(
    json['id'],
    json['user_role'],
    json['members'] == null
        ? null
        : Member.fromJson(json['members'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MembersToJson(Members instance) => <String, dynamic>{
      'id': instance.id,
      'user_role': instance.user_role,
      'members': instance.members,
    };

Member _$MemberFromJson(Map<String, dynamic> json) {
  return Member(
    json['id'],
    json['userName'],
    json['firstName'],
    json['lastName'],
    json['profilePic'],
  );
}

Map<String, dynamic> _$MemberToJson(Member instance) => <String, dynamic>{
      'id': instance.id,
      'userName': instance.userName,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'profilePic': instance.profilePic,
    };
