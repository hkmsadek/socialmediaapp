// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FeedDetailsModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedDetailsModel _$FeedDetailsModelFromJson(Map<String, dynamic> json) {
  return FeedDetailsModel(
    json['feed'] == null
        ? null
        : Feed.fromJson(json['feed'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$FeedDetailsModelToJson(FeedDetailsModel instance) =>
    <String, dynamic>{
      'feed': instance.feed,
    };

Feed _$FeedFromJson(Map<String, dynamic> json) {
  return Feed(
    json['id'],
    json['user_id'],
    json['activity_text'],
    json['activity_type'],
    json['privacy'],
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
    json['__meta__'] == null
        ? null
        : Meta.fromJson(json['__meta__'] as Map<String, dynamic>),
    json['created_at'],
    json['like'],
  );
}

Map<String, dynamic> _$FeedToJson(Feed instance) => <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'activity_text': instance.activityText,
      'activity_type': instance.activityType,
      'privacy': instance.privacy,
      'data': instance.data,
      '__meta__': instance.meta,
      'created_at': instance.createdAt,
      'like': instance.like,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    json['status'],
    (json['images'] as List)
        ?.map((e) =>
            e == null ? null : Images.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['statusUser_Name'],
    json['statusUser_profilePic'],
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'status': instance.status,
      'images': instance.images,
      'statusUser_Name': instance.statusUserName,
      'statusUser_profilePic': instance.statusUserProfilePic,
    };

Images _$ImagesFromJson(Map<String, dynamic> json) {
  return Images(
    json['id'],
    json['url'],
  );
}

Map<String, dynamic> _$ImagesToJson(Images instance) => <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
    };

Meta _$MetaFromJson(Map<String, dynamic> json) {
  return Meta(
    json['totalComments_count'],
    json['totalLikes_count'],
    json['totalShares_count'],
  );
}

Map<String, dynamic> _$MetaToJson(Meta instance) => <String, dynamic>{
      'totalComments_count': instance.totalCommentsCount,
      'totalLikes_count': instance.totalLikesCount,
      'totalShares_count': instance.totalSharesCount,
    };
