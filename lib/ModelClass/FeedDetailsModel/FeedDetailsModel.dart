import 'package:json_annotation/json_annotation.dart';

part 'FeedDetailsModel.g.dart';

@JsonSerializable()
class FeedDetailsModel {
  Feed feed;
  FeedDetailsModel(this.feed);
  factory FeedDetailsModel.fromJson(Map<String, dynamic> json) =>
      _$FeedDetailsModelFromJson(json);
  Map<String, dynamic> toJson() => _$FeedDetailsModelToJson(this);
}

@JsonSerializable()
class Feed {
  var id;
  @JsonKey(name: "user_id")
  dynamic userId;
  @JsonKey(name: "activity_text")
  dynamic activityText;
  @JsonKey(name: "activity_type")
  dynamic activityType;
  var privacy;
  Data data;
  @JsonKey(name: "__meta__")
  final Meta meta;
  @JsonKey(name: "created_at")
  dynamic createdAt;
  var like;
  Feed(this.id, this.userId, this.activityText, this.activityType, this.privacy,
      this.data, this.meta, this.createdAt, this.like);
  factory Feed.fromJson(Map<String, dynamic> json) => _$FeedFromJson(json);
  Map<String, dynamic> toJson() => _$FeedToJson(this);
}

@JsonSerializable()
class Data {
  var status;
  List<Images> images;
  @JsonKey(name: "statusUser_Name")
  dynamic statusUserName;
  @JsonKey(name: "statusUser_profilePic")
  dynamic statusUserProfilePic;
  Data(
      this.status, this.images, this.statusUserName, this.statusUserProfilePic);
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Images {
  var id;
  var url;

  Images(
    this.id,
    this.url,
  );
  factory Images.fromJson(Map<String, dynamic> json) => _$ImagesFromJson(json);
  Map<String, dynamic> toJson() => _$ImagesToJson(this);
}

@JsonSerializable()
class Meta {
  @JsonKey(name: "totalComments_count")
  dynamic totalCommentsCount;
  @JsonKey(name: "totalLikes_count")
  dynamic totalLikesCount;
  @JsonKey(name: "totalShares_count")
  dynamic totalSharesCount;

  Meta(
    this.totalCommentsCount,
    this.totalLikesCount,
    this.totalSharesCount,
  );
  factory Meta.fromJson(Map<String, dynamic> json) => _$MetaFromJson(json);
  Map<String, dynamic> toJson() => _$MetaToJson(this);
}
