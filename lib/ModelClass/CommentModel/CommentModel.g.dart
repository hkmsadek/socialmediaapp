// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CommentModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentModel _$CommentModelFromJson(Map<String, dynamic> json) {
  return CommentModel(
    (json['statusComment'] as List)
        ?.map((e) => e == null
            ? null
            : StatusComment.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CommentModelToJson(CommentModel instance) =>
    <String, dynamic>{
      'statusComment': instance.statusComment,
    };

StatusComment _$StatusCommentFromJson(Map<String, dynamic> json) {
  return StatusComment(
    json['id'],
    json['user_id'],
    json['status_id'],
    json['comment'],
    json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    json['created_at'],
    json['like'],
    json['__meta__'] == null
        ? null
        : Meta.fromJson(json['__meta__'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$StatusCommentToJson(StatusComment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'status_id': instance.statusId,
      'comment': instance.comment,
      'user': instance.user,
      'created_at': instance.createdAt,
      'like': instance.like,
      '__meta__': instance.meta,
    };

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['id'],
    json['firstName'],
    json['lastName'],
    json['profilePic'],
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'profilePic': instance.profilePic,
    };

Meta _$MetaFromJson(Map<String, dynamic> json) {
  return Meta(
    json['totalLikes_count'],
    json['totalReplies_count'],
  );
}

Map<String, dynamic> _$MetaToJson(Meta instance) => <String, dynamic>{
      'totalLikes_count': instance.totalLikesCount,
      'totalReplies_count': instance.totalRepliesCount,
    };
