import 'package:json_annotation/json_annotation.dart';

part 'CommentModel.g.dart';

@JsonSerializable()
class CommentModel {
  List<StatusComment> statusComment;
  CommentModel(this.statusComment);
  factory CommentModel.fromJson(Map<String, dynamic> json) =>
      _$CommentModelFromJson(json);
  Map<String, dynamic> toJson() => _$CommentModelToJson(this);
}

@JsonSerializable()
class StatusComment {
  var id;
  @JsonKey(name: "user_id")
  dynamic userId;
  @JsonKey(name: "status_id")
  dynamic statusId;
  var comment;
  User user;
  @JsonKey(name: "created_at")
  dynamic createdAt;
  var like;
  @JsonKey(name: "__meta__")
  final Meta meta;
  StatusComment(
    this.id,
    this.userId,
    this.statusId,
    this.comment,
    this.user,
    this.createdAt,
    this.like,
    this.meta,
  );
  factory StatusComment.fromJson(Map<String, dynamic> json) =>
      _$StatusCommentFromJson(json);
  Map<String, dynamic> toJson() => _$StatusCommentToJson(this);
}

@JsonSerializable()
class User {
  var id;
  var firstName;
  var lastName;
  var profilePic;
  User(
    this.id,
    this.firstName,
    this.lastName,
    this.profilePic,
  );
  factory User.fromJson(Map<String, dynamic> json) =>
      _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class Meta {
  @JsonKey(name: "totalLikes_count")
  dynamic totalLikesCount;
  @JsonKey(name: "totalReplies_count")
  dynamic totalRepliesCount;

  Meta(
    this.totalLikesCount,
    this.totalRepliesCount,
  );
  factory Meta.fromJson(Map<String, dynamic> json) => _$MetaFromJson(json);
  Map<String, dynamic> toJson() => _$MetaToJson(this);
}