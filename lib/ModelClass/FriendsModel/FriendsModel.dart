import 'package:json_annotation/json_annotation.dart';

part 'FriendsModel.g.dart';

@JsonSerializable()
class FriendsModel {
  List<Friends> friends;

  FriendsModel(this.friends);
  factory FriendsModel.fromJson(Map<String, dynamic> json) =>
      _$FriendsModelFromJson(json);
  Map<String, dynamic> toJson() => _$FriendsModelToJson(this);
}

@JsonSerializable()
class Friends {
  var id;
  var request_sender_id;
  var request_receiver_id;
  var status;
  var isFriend;
  Sender sender;

  Friends(
    this.id,
    this.request_sender_id,
    this.request_receiver_id,
    this.status,
    this.isFriend,
    this.sender,
  );
  factory Friends.fromJson(Map<String, dynamic> json) =>
      _$FriendsFromJson(json);
  Map<String, dynamic> toJson() => _$FriendsToJson(this);
}

@JsonSerializable()
class Sender {
  var id;
  var userName;
  var firstName;
  var lastName;
  var profilePic;

  Sender(
    this.id,
    this.userName,
    this.firstName,
    this.lastName,
    this.profilePic,
  );
  factory Sender.fromJson(Map<String, dynamic> json) => _$SenderFromJson(json);
  Map<String, dynamic> toJson() => _$SenderToJson(this);
}
