// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'conversationModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConversationModel _$ConversationModelFromJson(Map<String, dynamic> json) {
  return ConversationModel(
    (json['chats'] as List)
        ?.map(
            (e) => e == null ? null : Chats.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ConversationModelToJson(ConversationModel instance) =>
    <String, dynamic>{
      'chats': instance.chats,
    };

Chats _$ChatsFromJson(Map<String, dynamic> json) {
  return Chats(
    json['id'],
    json['conversation_id'],
    json['created_at'],
    json['seen'],
    json['message_sender'],
    json['message_receiver'],
    json['message'],
    json['deleted'],
    json['file'],
  );
}

Map<String, dynamic> _$ChatsToJson(Chats instance) => <String, dynamic>{
      'id': instance.id,
      'conversation_id': instance.conId,
      'created_at': instance.createdAt,
      'message': instance.message,
      'file': instance.file,
      'seen': instance.seen,
      'deleted': instance.deleted,
      'message_sender': instance.msgSender,
      'message_receiver': instance.msgReceiver,
    };
