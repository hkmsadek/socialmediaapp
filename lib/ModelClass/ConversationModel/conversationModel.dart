import 'package:json_annotation/json_annotation.dart';

part 'conversationModel.g.dart';

@JsonSerializable()
class ConversationModel {
  List<Chats> chats;

  ConversationModel(this.chats);
  factory ConversationModel.fromJson(Map<String, dynamic> json) =>
      _$ConversationModelFromJson(json);
  Map<String, dynamic> toJson() => _$ConversationModelToJson(this);
}

@JsonSerializable()
class Chats {
  var id;
  @JsonKey(name: "conversation_id")
  dynamic conId;
  @JsonKey(name: "created_at")
  dynamic createdAt;
  var message;
  var file;
  var seen;
  var deleted;
  @JsonKey(name: "message_sender")
  dynamic msgSender;
  @JsonKey(name: "message_receiver")
  dynamic msgReceiver;

  Chats(this.id, this.conId, this.createdAt, this.seen, this.msgSender, this.msgReceiver, this.message, this.deleted, this.file);
  factory Chats.fromJson(Map<String, dynamic> json) =>
      _$ChatsFromJson(json);
  Map<String, dynamic> toJson() => _$ChatsToJson(this);
}
