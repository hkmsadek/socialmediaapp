import 'package:json_annotation/json_annotation.dart';

part 'FeedModel.g.dart';

@JsonSerializable()
class FeedModel {
  List<Feed> feed;
  FeedModel(this.feed);
  factory FeedModel.fromJson(Map<String, dynamic> json) =>
      _$FeedModelFromJson(json);
  Map<String, dynamic> toJson() => _$FeedModelToJson(this);
}

@JsonSerializable()
class Feed {
  var id;
  @JsonKey(name: "user_id")
  dynamic userId;
  @JsonKey(name: "activity_text")
  dynamic activityText;
  @JsonKey(name: "activity_type")
  dynamic activityType;
  var privacy;
  Data data;
  @JsonKey(name: "__meta__")
  final Meta meta;
  @JsonKey(name: "created_at")
  dynamic createdAt;
  FeedUser feedUser;
  GroupInfo groupInfo;
  PageData pageData;
  ShareData shareData;
  var like;
  var groupPostLike;
  var isAdd;
  Feed(
      this.id,
      this.userId,
      this.activityText,
      this.activityType,
      this.privacy,
      this.data,
      this.meta,
      this.createdAt,
      this.feedUser,
      this.groupInfo,
      this.pageData,
      this.shareData,
      this.like,
      this.isAdd,
      this.groupPostLike);
  factory Feed.fromJson(Map<String, dynamic> json) => _$FeedFromJson(json);
  Map<String, dynamic> toJson() => _$FeedToJson(this);
}

@JsonSerializable()
class Data {
  var status;
  List<Images> images;
  Share share;
  FeedOwner feedOwner;
  @JsonKey(name: "statusUser_Name")
  dynamic statusUserName;
  @JsonKey(name: "statusUser_userName")
  dynamic statusUserUserName;
  @JsonKey(name: "statusUser_profilePic")
  dynamic statusUserProfilePic;
  Data(this.status, this.images, this.share, this.feedOwner,
      this.statusUserName, this.statusUserUserName, this.statusUserProfilePic);
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Images {
  var id;
  var url;

  Images(
    this.id,
    this.url,
  );
  factory Images.fromJson(Map<String, dynamic> json) => _$ImagesFromJson(json);
  Map<String, dynamic> toJson() => _$ImagesToJson(this);
}

@JsonSerializable()
class Share {
  @JsonKey(name: "activity_text")
  dynamic activityText;

  Share(
    this.activityText,
  );
  factory Share.fromJson(Map<String, dynamic> json) => _$ShareFromJson(json);
  Map<String, dynamic> toJson() => _$ShareToJson(this);
}

@JsonSerializable()
class FeedUser {
  var id;
  var firstName;
  var lastName;
  var profilePic;

  FeedUser(
    this.id,
    this.firstName,
    this.lastName,
    this.profilePic,
  );
  factory FeedUser.fromJson(Map<String, dynamic> json) =>
      _$FeedUserFromJson(json);
  Map<String, dynamic> toJson() => _$FeedUserToJson(this);
}

@JsonSerializable()
class GroupInfo {
  var id;
  var name;
  var description;
  var privacy;
  var logo;
  var banner;

  GroupInfo(
    this.id,
    this.name,
    this.description,
    this.privacy,
    this.logo,
    this.banner,
  );
  factory GroupInfo.fromJson(Map<String, dynamic> json) =>
      _$GroupInfoFromJson(json);
  Map<String, dynamic> toJson() => _$GroupInfoToJson(this);
}

@JsonSerializable()
class ShareData {
  var id;
  @JsonKey(name: "user_id")
  dynamic userId;
  @JsonKey(name: "activity_text")
  dynamic activityText;
  @JsonKey(name: "activity_id")
  dynamic activityId;
  var privacy;
  Data data;
  dynamic createdAt;
  PageData pageData;

  ShareData(
    this.id,
    this.userId,
    this.activityText,
    this.privacy,
    this.activityId,
    this.data,
    this.createdAt,
    this.pageData,
  );
  factory ShareData.fromJson(Map<String, dynamic> json) =>
      _$ShareDataFromJson(json);
  Map<String, dynamic> toJson() => _$ShareDataToJson(this);
}

@JsonSerializable()
class PageData {
  var id;
  var logo;
  var name;

  PageData(
    this.id,
    this.logo,
    this.name,
  );
  factory PageData.fromJson(Map<String, dynamic> json) =>
      _$PageDataFromJson(json);
  Map<String, dynamic> toJson() => _$PageDataToJson(this);
}

@JsonSerializable()
class FeedOwner {
  var id;
  var firstName;
  var lastName;
  var profilePic;

  FeedOwner(
    this.id,
    this.firstName,
    this.lastName,
    this.profilePic,
  );
  factory FeedOwner.fromJson(Map<String, dynamic> json) =>
      _$FeedOwnerFromJson(json);
  Map<String, dynamic> toJson() => _$FeedOwnerToJson(this);
}

@JsonSerializable()
class Meta {
  @JsonKey(name: "totalComments_count")
  dynamic totalCommentsCount;
  @JsonKey(name: "totalLikes_count")
  dynamic totalLikesCount;
  @JsonKey(name: "totalShares_count")
  dynamic totalSharesCount;
  @JsonKey(name: "totalGroupPostComments_count")
  dynamic totalGroupPostCommentsCount;
  @JsonKey(name: "totalGroupPostLikes_count")
  dynamic totalGroupPostLikesCount;

  Meta(
    this.totalCommentsCount,
    this.totalLikesCount,
    this.totalSharesCount,
    this.totalGroupPostCommentsCount,
    this.totalGroupPostLikesCount,
  );
  factory Meta.fromJson(Map<String, dynamic> json) => _$MetaFromJson(json);
  Map<String, dynamic> toJson() => _$MetaToJson(this);
}
