// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FeedModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedModel _$FeedModelFromJson(Map<String, dynamic> json) {
  return FeedModel(
    (json['feed'] as List)
        ?.map(
            (e) => e == null ? null : Feed.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$FeedModelToJson(FeedModel instance) => <String, dynamic>{
      'feed': instance.feed,
    };

Feed _$FeedFromJson(Map<String, dynamic> json) {
  return Feed(
    json['id'],
    json['user_id'],
    json['activity_text'],
    json['activity_type'],
    json['privacy'],
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
    json['__meta__'] == null
        ? null
        : Meta.fromJson(json['__meta__'] as Map<String, dynamic>),
    json['created_at'],
    json['feedUser'] == null
        ? null
        : FeedUser.fromJson(json['feedUser'] as Map<String, dynamic>),
    json['groupInfo'] == null
        ? null
        : GroupInfo.fromJson(json['groupInfo'] as Map<String, dynamic>),
    json['pageData'] == null
        ? null
        : PageData.fromJson(json['pageData'] as Map<String, dynamic>),
    json['shareData'] == null
        ? null
        : ShareData.fromJson(json['shareData'] as Map<String, dynamic>),
    json['like'],
    json['isAdd'],
    json['groupPostLike'],
  );
}

Map<String, dynamic> _$FeedToJson(Feed instance) => <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'activity_text': instance.activityText,
      'activity_type': instance.activityType,
      'privacy': instance.privacy,
      'data': instance.data,
      '__meta__': instance.meta,
      'created_at': instance.createdAt,
      'feedUser': instance.feedUser,
      'groupInfo': instance.groupInfo,
      'pageData': instance.pageData,
      'shareData': instance.shareData,
      'like': instance.like,
      'groupPostLike': instance.groupPostLike,
      'isAdd': instance.isAdd,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    json['status'],
    (json['images'] as List)
        ?.map((e) =>
            e == null ? null : Images.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['share'] == null
        ? null
        : Share.fromJson(json['share'] as Map<String, dynamic>),
    json['feedOwner'] == null
        ? null
        : FeedOwner.fromJson(json['feedOwner'] as Map<String, dynamic>),
    json['statusUser_Name'],
    json['statusUser_userName'],
    json['statusUser_profilePic'],
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'status': instance.status,
      'images': instance.images,
      'share': instance.share,
      'feedOwner': instance.feedOwner,
      'statusUser_Name': instance.statusUserName,
      'statusUser_userName': instance.statusUserUserName,
      'statusUser_profilePic': instance.statusUserProfilePic,
    };

Images _$ImagesFromJson(Map<String, dynamic> json) {
  return Images(
    json['id'],
    json['url'],
  );
}

Map<String, dynamic> _$ImagesToJson(Images instance) => <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
    };

Share _$ShareFromJson(Map<String, dynamic> json) {
  return Share(
    json['activity_text'],
  );
}

Map<String, dynamic> _$ShareToJson(Share instance) => <String, dynamic>{
      'activity_text': instance.activityText,
    };

FeedUser _$FeedUserFromJson(Map<String, dynamic> json) {
  return FeedUser(
    json['id'],
    json['firstName'],
    json['lastName'],
    json['profilePic'],
  );
}

Map<String, dynamic> _$FeedUserToJson(FeedUser instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'profilePic': instance.profilePic,
    };

GroupInfo _$GroupInfoFromJson(Map<String, dynamic> json) {
  return GroupInfo(
    json['id'],
    json['name'],
    json['description'],
    json['privacy'],
    json['logo'],
    json['banner'],
  );
}

Map<String, dynamic> _$GroupInfoToJson(GroupInfo instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'privacy': instance.privacy,
      'logo': instance.logo,
      'banner': instance.banner,
    };

ShareData _$ShareDataFromJson(Map<String, dynamic> json) {
  return ShareData(
    json['id'],
    json['user_id'],
    json['activity_text'],
    json['privacy'],
    json['activity_id'],
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
    json['createdAt'],
    json['pageData'] == null
        ? null
        : PageData.fromJson(json['pageData'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ShareDataToJson(ShareData instance) => <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'activity_text': instance.activityText,
      'activity_id': instance.activityId,
      'privacy': instance.privacy,
      'data': instance.data,
      'createdAt': instance.createdAt,
      'pageData': instance.pageData,
    };

PageData _$PageDataFromJson(Map<String, dynamic> json) {
  return PageData(
    json['id'],
    json['logo'],
    json['name'],
  );
}

Map<String, dynamic> _$PageDataToJson(PageData instance) => <String, dynamic>{
      'id': instance.id,
      'logo': instance.logo,
      'name': instance.name,
    };

FeedOwner _$FeedOwnerFromJson(Map<String, dynamic> json) {
  return FeedOwner(
    json['id'],
    json['firstName'],
    json['lastName'],
    json['profilePic'],
  );
}

Map<String, dynamic> _$FeedOwnerToJson(FeedOwner instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'profilePic': instance.profilePic,
    };

Meta _$MetaFromJson(Map<String, dynamic> json) {
  return Meta(
    json['totalComments_count'],
    json['totalLikes_count'],
    json['totalShares_count'],
    json['totalGroupPostComments_count'],
    json['totalGroupPostLikes_count'],
  );
}

Map<String, dynamic> _$MetaToJson(Meta instance) => <String, dynamic>{
      'totalComments_count': instance.totalCommentsCount,
      'totalLikes_count': instance.totalLikesCount,
      'totalShares_count': instance.totalSharesCount,
      'totalGroupPostComments_count': instance.totalGroupPostCommentsCount,
      'totalGroupPostLikes_count': instance.totalGroupPostLikesCount,
    };
