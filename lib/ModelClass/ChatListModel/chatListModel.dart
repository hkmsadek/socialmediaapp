import 'package:json_annotation/json_annotation.dart';

part 'chatListModel.g.dart';

@JsonSerializable()
class ChatListModel {
  List<Lists> lists;

  ChatListModel(this.lists);
  factory ChatListModel.fromJson(Map<String, dynamic> json) =>
      _$ChatListModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatListModelToJson(this);
}

@JsonSerializable()
class Lists {
  var id;
  var sender;
  var receiver;
  var conType;
  var username;
  var firstName;
  var lastName;
  var profilePic;
  var jobTitle;
  var dayJob;
  @JsonKey(name: "created_at")
  dynamic createdAt;
  var userType;
  var isOnline;
  var message;
  var seen;
  @JsonKey(name: "message_sender")
  dynamic messageSender;

  Lists(
      this.id,
      this.sender,
      this.receiver,
      this.createdAt,
      this.conType,
      this.dayJob,
      this.firstName,
      this.isOnline,
      this.jobTitle,
      this.lastName,
      this.message,
      this.messageSender,
      this.profilePic,
      this.seen,
      this.username,
      this.userType);
  factory Lists.fromJson(Map<String, dynamic> json) => _$ListsFromJson(json);
  Map<String, dynamic> toJson() => _$ListsToJson(this);
}
