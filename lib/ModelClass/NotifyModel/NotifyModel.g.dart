// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NotifyModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotifyModel _$NotifyModelFromJson(Map<String, dynamic> json) {
  return NotifyModel(
    (json['notifications'] as List)
        ?.map((e) => e == null
            ? null
            : AllNotification.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$NotifyModelToJson(NotifyModel instance) =>
    <String, dynamic>{
      'notifications': instance.notifications,
    };

AllNotification _$AllNotificationFromJson(Map<String, dynamic> json) {
  return AllNotification(
    json['id'],
    json['userName'],
    json['url'],
    json['notification_text'],
    json['seen'],
    json['created_at'],
  );
}

Map<String, dynamic> _$AllNotificationToJson(AllNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userName': instance.userName,
      'url': instance.url,
      'notification_text': instance.notification_text,
      'seen': instance.seen,
      'created_at': instance.created_at,
    };
