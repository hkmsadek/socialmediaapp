import 'package:json_annotation/json_annotation.dart';

part 'NotifyModel.g.dart';

@JsonSerializable()
class NotifyModel {
  List<AllNotification> notifications;
  NotifyModel(this.notifications);
  factory NotifyModel.fromJson(Map<String, dynamic> json) => _$NotifyModelFromJson(json);
  Map<String, dynamic> toJson() => _$NotifyModelToJson(this);
}

@JsonSerializable()
class AllNotification {
  var id;
  var userName;
  var url;
  var notification_text;
  var seen;
  var created_at;

  AllNotification(this.id, this.userName, this.url, this.notification_text, this.seen,
      this.created_at);
  factory AllNotification.fromJson(Map<String, dynamic> json) => _$AllNotificationFromJson(json);
  Map<String, dynamic> toJson() => _$AllNotificationToJson(this);
}