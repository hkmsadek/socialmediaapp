import 'package:json_annotation/json_annotation.dart';

part 'ReplyModel.g.dart';

@JsonSerializable()
class ReplyModel {
  List<StatusReply> statusReply;
  ReplyModel(this.statusReply);
  factory ReplyModel.fromJson(Map<String, dynamic> json) =>
      _$ReplyModelFromJson(json);
  Map<String, dynamic> toJson() => _$ReplyModelToJson(this);
}

@JsonSerializable()
class StatusReply {
  var id;
  @JsonKey(name: "user_id")
  dynamic userId;
  @JsonKey(name: "status_id")
  dynamic statusId;
  @JsonKey(name: "reply_text")
  dynamic replyText;
  var like;
  User user;
  @JsonKey(name: "created_at")
  dynamic createdAt;
  @JsonKey(name: "__meta__")
  final Meta meta;
  StatusReply(
    this.id,
    this.userId,
    this.statusId,
    this.replyText,
    this.like,
    this.user,
    this.createdAt,
    this.meta,
  );
  factory StatusReply.fromJson(Map<String, dynamic> json) =>
      _$StatusReplyFromJson(json);
  Map<String, dynamic> toJson() => _$StatusReplyToJson(this);
}

@JsonSerializable()
class User {
  var id;
  var firstName;
  var lastName;
  var profilePic;
  User(
    this.id,
    this.firstName,
    this.lastName,
    this.profilePic,
  );
  factory User.fromJson(Map<String, dynamic> json) =>
      _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class Meta {
  @JsonKey(name: "totalLikes_count")
  dynamic totalLikesCount;
  @JsonKey(name: "totalReplies_count")
  dynamic totalRepliesCount;

  Meta(
    this.totalLikesCount,
    this.totalRepliesCount,
  );
  factory Meta.fromJson(Map<String, dynamic> json) => _$MetaFromJson(json);
  Map<String, dynamic> toJson() => _$MetaToJson(this);
}