// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ReplyModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReplyModel _$ReplyModelFromJson(Map<String, dynamic> json) {
  return ReplyModel(
    (json['statusReply'] as List)
        ?.map((e) =>
            e == null ? null : StatusReply.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ReplyModelToJson(ReplyModel instance) =>
    <String, dynamic>{
      'statusReply': instance.statusReply,
    };

StatusReply _$StatusReplyFromJson(Map<String, dynamic> json) {
  return StatusReply(
    json['id'],
    json['user_id'],
    json['status_id'],
    json['reply_text'],
    json['like'],
    json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    json['created_at'],
    json['__meta__'] == null
        ? null
        : Meta.fromJson(json['__meta__'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$StatusReplyToJson(StatusReply instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'status_id': instance.statusId,
      'reply_text': instance.replyText,
      'like': instance.like,
      'user': instance.user,
      'created_at': instance.createdAt,
      '__meta__': instance.meta,
    };

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['id'],
    json['firstName'],
    json['lastName'],
    json['profilePic'],
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'profilePic': instance.profilePic,
    };

Meta _$MetaFromJson(Map<String, dynamic> json) {
  return Meta(
    json['totalLikes_count'],
    json['totalReplies_count'],
  );
}

Map<String, dynamic> _$MetaToJson(Meta instance) => <String, dynamic>{
      'totalLikes_count': instance.totalLikesCount,
      'totalReplies_count': instance.totalRepliesCount,
    };
