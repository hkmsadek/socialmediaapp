// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FriendReqModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FriendReqModel _$FriendReqModelFromJson(Map<String, dynamic> json) {
  return FriendReqModel(
    (json['requests'] as List)
        ?.map((e) =>
            e == null ? null : Requsets.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$FriendReqModelToJson(FriendReqModel instance) =>
    <String, dynamic>{
      'requests': instance.requests,
    };

Requsets _$RequsetsFromJson(Map<String, dynamic> json) {
  return Requsets(
    json['id'],
    json['request_sender_id'],
    json['request_receiver_id'],
    json['status'],
    json['isFriend'],
    json['sender'] == null
        ? null
        : Sender.fromJson(json['sender'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RequsetsToJson(Requsets instance) => <String, dynamic>{
      'id': instance.id,
      'request_sender_id': instance.request_sender_id,
      'request_receiver_id': instance.request_receiver_id,
      'status': instance.status,
      'isFriend': instance.isFriend,
      'sender': instance.sender,
    };

Sender _$SenderFromJson(Map<String, dynamic> json) {
  return Sender(
    json['id'],
    json['userName'],
    json['firstName'],
    json['lastName'],
    json['profilePic'],
  );
}

Map<String, dynamic> _$SenderToJson(Sender instance) => <String, dynamic>{
      'id': instance.id,
      'userName': instance.userName,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'profilePic': instance.profilePic,
    };
