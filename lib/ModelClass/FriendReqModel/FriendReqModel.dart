import 'package:json_annotation/json_annotation.dart';

part 'FriendReqModel.g.dart';

@JsonSerializable()
class FriendReqModel {
  List<Requsets> requests;

  FriendReqModel(this.requests);
  factory FriendReqModel.fromJson(Map<String, dynamic> json) =>
      _$FriendReqModelFromJson(json);
  Map<String, dynamic> toJson() => _$FriendReqModelToJson(this);
}

@JsonSerializable()
class Requsets {
  var id;
  var request_sender_id;
  var request_receiver_id;
  var status;
  var isFriend;
  Sender sender;

  Requsets(
    this.id,
    this.request_sender_id,
    this.request_receiver_id,
    this.status,
    this.isFriend,
    this.sender,
  );
  factory Requsets.fromJson(Map<String, dynamic> json) =>
      _$RequsetsFromJson(json);
  Map<String, dynamic> toJson() => _$RequsetsToJson(this);
}

@JsonSerializable()
class Sender {
  var id;
  var userName;
  var firstName;
  var lastName;
  var profilePic;

  Sender(
    this.id,
    this.userName,
    this.firstName,
    this.lastName,
    this.profilePic,
  );
  factory Sender.fromJson(Map<String, dynamic> json) =>
      _$SenderFromJson(json);
  Map<String, dynamic> toJson() => _$SenderToJson(this);
}
