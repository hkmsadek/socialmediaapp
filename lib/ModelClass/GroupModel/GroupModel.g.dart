// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GroupModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GroupModel _$GroupModelFromJson(Map<String, dynamic> json) {
  return GroupModel(
    json['id'],
    json['groupId'],
    json['userId'],
    json['userRole'],
    json['createdAt'],
    json['updatedAt'],
    json['group'] == null
        ? null
        : Groups.fromJson(json['group'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$GroupModelToJson(GroupModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'groupId': instance.groupId,
      'userId': instance.userId,
      'userRole': instance.userRole,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'group': instance.group,
    };

Groups _$GroupsFromJson(Map<String, dynamic> json) {
  return Groups(
    json['id'],
    json['name'],
    json['logo'],
  );
}

Map<String, dynamic> _$GroupsToJson(Groups instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'logo': instance.logo,
    };
