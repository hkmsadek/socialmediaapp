import 'package:json_annotation/json_annotation.dart';

part 'GroupModel.g.dart';

@JsonSerializable()
class GroupModel {
  var id;
  var groupId;
  var userId;
  var userRole;
  var createdAt;
  var updatedAt;
  Groups group;

  GroupModel(
    this.id,
    this.groupId,
    this.userId,
    this.userRole,
    this.createdAt,
    this.updatedAt,
    this.group,
  );
  factory GroupModel.fromJson(Map<String, dynamic> json) =>
      _$GroupModelFromJson(json);
  Map<String, dynamic> toJson() => _$GroupModelToJson(this);
}

@JsonSerializable()
class Groups {
  var id;
  var name;
  var logo;

  Groups(
    this.id,
    this.name,
    this.logo,
  );

  factory Groups.fromJson(Map<String, dynamic> json) => _$GroupsFromJson(json);
  Map<String, dynamic> toJson() => _$GroupsToJson(this);
}
