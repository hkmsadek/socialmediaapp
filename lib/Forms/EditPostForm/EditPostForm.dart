import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:web_socket_channel/io.dart';

import '../../main.dart';

class EditPostForm extends StatefulWidget {
  final feedList;
  final userData;
  EditPostForm(this.feedList, this.userData);

  @override
  _EditPostFormState createState() => _EditPostFormState();
}

class _EditPostFormState extends State<EditPostForm> {
  String status = "Public", post = "";
  TextEditingController msgController = new TextEditingController();
  bool loading = false;
  List img = [];

  @override
  void initState() {
    setState(() {
      msgController.text = widget.feedList.activityText;
      post = widget.feedList.activityText;
      status = widget.feedList.privacy;

      for (int i = 0; i < widget.feedList.data.images.length; i++) {
        img.add({
          'id': widget.feedList.data.images[i].id,
          'user_id': widget.feedList.userId,
          'url': widget.feedList.data.images[i].url,
          'created_at': widget.feedList.createdAt,
          'updated_at': widget.feedList.createdAt,
        });
      }
    });
    super.initState();
  }

  void _statusModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                  leading: new Icon(Icons.public),
                  title: new Text('Public',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  trailing: status == "Public"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "Public";
                    }),
                    Navigator.pop(context)
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.lock_outline),
                  title: new Text('Only Me',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  trailing: status == "Only Me"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "Only Me";
                    }),
                    Navigator.pop(context)
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.group),
                  title: new Text('Friends',
                      style: TextStyle(fontWeight: FontWeight.normal)),
                  trailing: status == "3"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "3";
                    }),
                    Navigator.pop(context)
                  },
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
      Center(
        child: Row(
          children: <Widget>[
            Container(
              width: 50,
              height: 50,
              margin: EdgeInsets.only(top: 0, left: 20),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/user.png"),
                      fit: BoxFit.cover),
                  border: Border.all(color: Colors.grey, width: 0.1),
                  borderRadius: BorderRadius.circular(15)),
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(top: 0, left: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              "${widget.userData['firstName']} ${widget.userData['lastName']}",
                              maxLines: 1,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            _statusModalBottomSheet(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 0, right: 5, left: 10),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: 0.3, color: Colors.black54),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(left: 5),
                                  child: Icon(
                                    status == "Public"
                                        ? Icons.public
                                        : status == "Only Me"
                                            ? Icons.lock_outline
                                            : Icons.group,
                                    size: 12,
                                    color: Colors.black54,
                                  ),
                                ),
                                Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Text(
                                      status == "Public"
                                          ? "Public"
                                          : status == "Only Me"
                                              ? "Only Me"
                                              : "Friends",
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w300,
                                          fontFamily: "Oswald"),
                                    )),
                                Icon(
                                  Icons.arrow_drop_down,
                                  size: 25,
                                  color: Colors.black54,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      Container(
        //height: 150,
        padding: EdgeInsets.all(0),
        margin: EdgeInsets.only(top: 25, left: 20, bottom: 5, right: 20),
        decoration: BoxDecoration(
            borderRadius: new BorderRadius.only(
                topLeft: Radius.circular(5.0),
                topRight: Radius.circular(5.0),
                bottomLeft: Radius.circular(5.0),
                bottomRight: Radius.circular(5.0)),
            //color: Colors.grey[100],
            border: Border.all(width: 0.2, color: Colors.grey)),
        child: Row(
          children: <Widget>[
            Flexible(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: 130.0,
                ),
                child: new SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  //reverse: true,
                  child: Container(
                    //height: 100,
                    child: new TextField(
                      maxLines: null,
                      autofocus: false,
                      style: TextStyle(
                        color: Colors.black87,
                        fontFamily: 'Oswald',
                      ),
                      controller: msgController,
                      decoration: InputDecoration(
                        alignLabelWithHint: true,
                        hintText: "What do you want to say?",
                        hintStyle: TextStyle(
                            color: Colors.black54,
                            fontSize: 15,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.w300),
                        contentPadding:
                            EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        border: InputBorder.none,
                      ),
                      onChanged: (value) {
                        setState(() {
                          post = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(left: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                //snackBar(context);
                loading == false ? statusEdit() : null;
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 20, top: 20),
                child: Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(left: 20, right: 20, top: 0),
                    decoration: BoxDecoration(
                        color: loading == false ? mainColor : Colors.grey[100],
                        border: Border.all(color: Colors.grey, width: 0.5),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Text(
                      loading == false ? "Edit" : "Please wait...",
                      style: TextStyle(
                          color:
                              loading == false ? Colors.white : Colors.black26,
                          fontSize: 15,
                          fontFamily: 'BebasNeue',
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    )),
              ),
            ),
          ],
        ),
      ),
    ]));
  }

  Future statusEdit() async {
    setState(() {
      loading = true;
    });
    var data = {
      'id': widget.feedList.id,
      'user_id': widget.feedList.userId,
      'activity_id': widget.feedList.id,
      'activity_text': post,
      'activity_type': "Status",
      "privacy": status,
      'data': {
        'status': post,
        'comments': [],
        'images': img,
        'statusUser_Name': widget.feedList.data.statusUserName,
        'statusUser_userName': widget.feedList.data.statusUserUserName,
        'statusUser_profilePic': widget.feedList.data.statusUserProfilePic,
        'link': "",
        'link_meta': {},
      },
      'created_at': widget.feedList.createdAt,
      'updated_at': widget.feedList.createdAt,
      'isEdit': true,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/add');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    } else {
      _showMessage("Something went wrong!", 1);
    }

    setState(() {
      loading = false;
    });
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
