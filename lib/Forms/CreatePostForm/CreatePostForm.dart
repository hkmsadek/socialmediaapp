import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:multi_media_picker/multi_media_picker.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';
import 'package:social_app_fb/main.dart';
import 'package:web_socket_channel/io.dart';

class CreatePostForm extends StatefulWidget {
  final userData;
  final number;
  CreatePostForm(this.userData, this.number);
  @override
  _CreatePostFormState createState() => _CreatePostFormState();
}

class _CreatePostFormState extends State<CreatePostForm> {
  String post = '', chk = "";
  String interests = "";
  String status = "Public";
  int _start = 3, line = 1, _postStart = 2;
  bool isLoading = true;
  var catList, metaData, link;
  bool isOpen = false;
  List<String> selectedCategory = [];
  var selectedCat;
  int maxImageNo = 5;
  List allImages = [];
  List images = [];
  List imagesBase64 = [];
  List<File> resultList;
  var img;
  bool selectSingleImage = false;
  bool isSubmit = false;
  bool isImageLoading = false;

  void _statusModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                  leading: new Icon(Icons.public),
                  title: new Text('Public',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  trailing: status == "Public"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "Public";
                    }),
                    Navigator.pop(context)
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.lock_outline),
                  title: new Text('Only Me',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  trailing: status == "Only Me"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "Only Me";
                    }),
                    Navigator.pop(context)
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.group),
                  title: new Text('Friends',
                      style: TextStyle(fontWeight: FontWeight.normal)),
                  trailing: status == "3"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "3";
                    }),
                    Navigator.pop(context)
                  },
                ),
              ],
            ),
          );
        });
  }

  initMultiPickUp() async {
    setState(() {
      allImages = [];
      //isImageLoading = true;
    });
    //List<File> resultList;
    resultList = await MultiMediaPicker.pickImages(
        source: ImageSource.gallery, maxHeight: 480, maxWidth: 640);
    setState(() {
      //resultList = imgs;
      if (resultList != null) {
        //isSubmit = true;
        //isImageLoading = false;
      }
    });
  }

  uploadImages(images) async {
    if (images != null) {
      for (int i = 0; i < images.length; i++) {
        //File file = new File(resultList[i].toString());
        List<int> imageBytes = images[i].readAsBytesSync();
        String image = base64.encode(imageBytes);
        image = 'data:image/png;base64,' + image;
        var data3 = {'image': image};
        print(data3);
        var res1 = await CallApi().postData1(data3, 'status/upload/images');
        var body1 = json.decode(res1.body);
        print("image success");
        print(body1);
        setState(() {
          allImages.add(body1['uploadFile_id']);
        });
      }
    }else{
      images = [];
    }

    statusUpload();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
          Widget>[
        Center(
          child: Row(
            children: <Widget>[
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.only(top: 0, left: 20),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/user.png"),
                        fit: BoxFit.cover),
                    border: Border.all(color: Colors.grey, width: 0.1),
                    borderRadius: BorderRadius.circular(15)),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(top: 0, left: 10),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                "${widget.userData['firstName']} ${widget.userData['lastName']}",
                                maxLines: 1,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ],
                        )),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              _statusModalBottomSheet(context);
                            },
                            child: Container(
                              margin:
                                  EdgeInsets.only(top: 0, right: 5, left: 10),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 0.3, color: Colors.black54),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Icon(
                                      status == "Public"
                                          ? Icons.public
                                          : status == "Only Me"
                                              ? Icons.lock_outline
                                              : Icons.group,
                                      size: 12,
                                      color: Colors.black54,
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(left: 5),
                                      child: Text(
                                        status == "Public"
                                            ? "Public"
                                            : status == "Only Me"
                                                ? "Only Me"
                                                : "Friends",
                                        style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w300,
                                            fontFamily: "Oswald"),
                                      )),
                                  Icon(
                                    Icons.arrow_drop_down,
                                    size: 25,
                                    color: Colors.black54,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // Text(interests == "" ? "" : "-",
                          //     style: TextStyle(color: header, fontSize: 25)),
                          // Container(
                          //     margin: EdgeInsets.only(left: 5),
                          //     child: Text(
                          //       interests == "" ? "" : "$interests",
                          //       style: TextStyle(
                          //           color: mainColor,
                          //           fontSize: 12,
                          //           fontWeight: FontWeight.w400,
                          //           fontFamily: "Oswald"),
                          //     )),
                          // interests == ""
                          //     ? Container()
                          //     : GestureDetector(
                          //         onTap: () {
                          //           setState(() {
                          //             interests = "";
                          //           });
                          //         },
                          //         child: Container(
                          //             margin: EdgeInsets.only(left: 3),
                          //             child: Icon(Icons.clear,
                          //                 size: 18, color: Colors.black45)),
                          //       )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          //height: 150,
          padding: EdgeInsets.all(0),
          margin: EdgeInsets.only(top: 25, left: 20, bottom: 5, right: 20),
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  topRight: Radius.circular(5.0),
                  bottomLeft: Radius.circular(5.0),
                  bottomRight: Radius.circular(5.0)),
              //color: Colors.grey[100],
              border: Border.all(width: 0.2, color: Colors.grey)),
          child: Row(
            children: <Widget>[
              Flexible(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: 130.0,
                  ),
                  child: new SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    //reverse: true,
                    child: Container(
                      //height: 100,
                      child: new TextField(
                        maxLines: null,
                        autofocus: false,
                        style: TextStyle(
                          color: Colors.black87,
                          fontFamily: 'Oswald',
                        ),
                        //controller: msgController,
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          hintText: "What do you want to say?",
                          hintStyle: TextStyle(
                              color: Colors.black54,
                              fontSize: 15,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w300),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                          border: InputBorder.none,
                        ),
                        onChanged: (value) {
                          setState(() {
                            post = value;
                          });
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        // Container(
        //     width: 50,
        //     margin: EdgeInsets.only(top: 0, left: 25, right: 25, bottom: 10),
        //     decoration: BoxDecoration(
        //         borderRadius: BorderRadius.all(Radius.circular(15.0)),
        //         color: mainColor,
        //         boxShadow: [
        //           BoxShadow(
        //             blurRadius: 1.0,
        //             color: mainColor,
        //             //offset: Offset(6.0, 7.0),
        //           ),
        //         ],
        //         border: Border.all(width: 0.5, color: mainColor))),
        Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                onTap: initMultiPickUp,
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Icon(
                          Icons.add_a_photo,
                          size: 20,
                          color: Colors.black45,
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text(
                            "Add Pictures",
                            style: TextStyle(color: Colors.black45),
                          )),
                      isImageLoading == false
                          ? Container()
                          : Container(
                              height: 30,
                              margin: EdgeInsets.only(bottom: 20),
                              child: SpinKitCircle(color: Colors.black26)),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  //snackBar(context);
                  isImageLoading == false ? uploadImages(resultList) : null;
                },
                child: Container(
                  margin: EdgeInsets.only(bottom: 20, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.only(left: 20, right: 20, top: 0),
                          decoration: BoxDecoration(
                              color: isImageLoading == false
                                  ? mainColor
                                  : Colors.grey[100],
                              border:
                                  Border.all(color: Colors.grey, width: 0.5),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: Text(
                            isImageLoading == false ? "Post" : "Please wait...",
                            style: TextStyle(
                                color: isImageLoading == false
                                    ? Colors.white
                                    : Colors.black26,
                                fontSize: 15,
                                fontFamily: 'BebasNeue',
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),

        resultList == null
            ? Container()
            : Container(
                //height: 100,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                margin: EdgeInsets.only(top: 0, bottom: 0, left: 15, right: 15),
                child: Column(
                    children: List.generate(resultList.length, (index) {
                  return Container(
                    height: 220,
                    //width: 150,
                    alignment: Alignment.topRight,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        border: Border.all(color: Colors.grey, width: 0.3),
                        borderRadius: BorderRadius.circular(5),
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: FileImage(resultList[index]))),
                    margin:
                        EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          resultList.removeAt(index);
                          //images = img;
                          print(resultList.length);
                        });
                      },
                      child: Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: Colors.black87.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(5)),
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                          )),
                    ),
                  );
                })),
              ),
      ]),
    );
  }

  Future statusUpload() async {
    setState(() {
      isImageLoading = true;
    });
    var data = {
      'user_id': widget.userData['id'],
      'activity_text': post,
      'activity_type': "Status",
      'privacy': status,
      'link_meta': {},
      'link': '',
      'images': allImages,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/add');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 201) {
      Navigator.pop(context);
      if (widget.number == 1) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      } else {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ProfileScreen()));
      }
    } else {
      _showMessage("Something went wrong!", 1);
    }

    setState(() {
      isImageLoading = false;
    });
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
