import 'package:flutter/material.dart';
import 'package:social_app_fb/main.dart';

class ReplyList extends StatefulWidget {
  final replyList;
  final userData;

  ReplyList(this.replyList, this.userData);

  @override
  _ReplyListState createState() => _ReplyListState();
}

class _ReplyListState extends State<ReplyList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          children: List.generate(widget.replyList.length, (index) {
        return Container(
          padding: EdgeInsets.only(top: 0, bottom: 5),
          margin: EdgeInsets.only(top: 0, bottom: 0, left: 20, right: 10),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(right: 0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ////// <<<<< Name start >>>>> //////
                            Container(
                              //color: Colors.yellow,
                              margin: EdgeInsets.only(
                                  left: 40, right: 0, bottom: 0, top: 20),
                              padding: EdgeInsets.only(right: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< pic start >>>>> //////
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                    padding: EdgeInsets.all(1.0),
                                    child: CircleAvatar(
                                      radius: 12.0,
                                      backgroundColor: Colors.white,
                                      backgroundImage: AssetImage(
                                          "assets/images/prabal.jpg"),
                                    ),
                                    decoration: new BoxDecoration(
                                      color: Colors.grey[300], // border color
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                  ////// <<<<< pic end >>>>> //////

                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ////// <<<<< Name start >>>>> //////
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Expanded(
                                                child: Container(
                                                  child: Text(
                                                    "${widget.replyList[index]['fName']} ${widget.replyList[index]['lName']}",
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        color: mainColor,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                              ),
                                              widget.userData['id'] !=
                                                      widget.replyList[index]
                                                          ['uId']
                                                  ? Container()
                                                  : GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          // if (idx == -1) {
                                                          //   _statusModalBottomSheet(
                                                          //       context,
                                                          //       index,
                                                          //       widget
                                                          //           .userData,
                                                          //       widget.repList[
                                                          //           index]);
                                                          //   reply = widget
                                                          //               .repList[
                                                          //           index]
                                                          //       ['replyTxt'];
                                                          //   editingController
                                                          //       .text = widget
                                                          //               .repList[
                                                          //           index]
                                                          //       ['replyTxt'];
                                                          //   //idx = index;
                                                          // }
                                                        });
                                                      },
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 15),
                                                          // color: Colors.blue,
                                                          child: Icon(
                                                            Icons.more_horiz,
                                                            color:
                                                                Colors.black54,
                                                          )),
                                                    ),
                                            ],
                                          ),

                                          ////// <<<<< Name end >>>>> //////
                                          Container(
                                              margin: EdgeInsets.only(
                                                left: 0,
                                                right: 0,
                                                top: 5,
                                              ),
                                              child: Column(
                                                children: <Widget>[
                                                  ////// <<<<< Comment start >>>>> //////
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 0),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    child: Container(
                                                      child: Text(
                                                        "${widget.replyList[index]['replyTxt']}",
                                                        textAlign:
                                                            TextAlign.justify,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 12,
                                                            fontFamily:
                                                                "Oswald",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400),
                                                      ),
                                                    ),
                                                  ),

                                                  ////// <<<<< Comment end >>>>> //////
                                                ],
                                              )),
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 0, top: 5),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 0),
                                                  child: Text("6h ago",
                                                      style: TextStyle(
                                                          fontFamily: 'Oswald',
                                                          fontWeight:
                                                              FontWeight.w300,
                                                          color: Colors.black54,
                                                          fontSize: 10)),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 15),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Container(
                                                        padding:
                                                            EdgeInsets.all(3.0),
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            if (widget.replyList[
                                                                        index][
                                                                    'myLike'] !=
                                                                null) {
                                                              setState(() {
                                                                widget.replyList[
                                                                        index][
                                                                    'myLike'] = null;
                                                                widget.replyList[
                                                                        index][
                                                                    'totalLike']--;
                                                              });
                                                              // replayLikePressed(
                                                              //     index, 0);
                                                            } else {
                                                              setState(() {
                                                                widget.replyList[
                                                                        index][
                                                                    'myLike'] = 1;
                                                                widget.replyList[
                                                                        index][
                                                                    'totalLike']++;
                                                              });
                                                              // replayLikePressed(
                                                              //     index, 1);
                                                            }
                                                          },
                                                          child: Icon(
                                                            widget.replyList[
                                                                            index]
                                                                        [
                                                                        'myLike'] ==
                                                                    null
                                                                ? Icons
                                                                    .favorite_border
                                                                : Icons
                                                                    .favorite,
                                                            size: 16,
                                                            color: widget.replyList[
                                                                            index]
                                                                        [
                                                                        'myLike'] ==
                                                                    null
                                                                ? Colors.black54
                                                                : Colors
                                                                    .redAccent,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 3),
                                                        child: Text(
                                                            "${widget.replyList[index]['totalLike']}",
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    'Oswald',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300,
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 10)),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Divider()
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      })),
    );
  }
}
