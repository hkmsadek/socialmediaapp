import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:social_app_fb/API/api.dart';

import '../../main.dart';

class AllRequestCard extends StatefulWidget {
  final requsets;
  AllRequestCard(this.requsets);

  @override
  _AllRequestCardState createState() => _AllRequestCardState();
}

class _AllRequestCardState extends State<AllRequestCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SliverPadding(
        padding: EdgeInsets.only(bottom: 25),
        sliver: SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => FriendProfileNewPage()));
              },

              ////// <<<<< Main Data >>>>> //////
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      // boxShadow: [
                      //   BoxShadow(
                      //     blurRadius: 1.0,
                      //     color: Colors.black26,
                      //   ),
                      // ],
                    ),
                    margin: EdgeInsets.only(
                        top: 2.5, bottom: 2.5, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Container(
                                  height: 40,
                                  margin: EdgeInsets.only(right: 10),
                                  padding: EdgeInsets.all(1.0),
                                  child: widget.requsets[index].sender
                                                  .profilePic ==
                                              "" ||
                                          widget.requsets[index].sender
                                                  .profilePic ==
                                              null
                                      ? Image.asset('assets/images/user.png')
                                      : CachedNetworkImage(
                                          imageUrl: widget.requsets[index]
                                              .sender.profilePic,
                                          placeholder: (context, url) =>
                                              CircularProgressIndicator(),
                                          errorWidget: (context, url, error) =>
                                              Image.asset(
                                            "assets/images/user.png",
                                            height: 40,
                                          ),

                                          // NetworkImage(
                                          //     widget.friend[index].profilePic
                                        ),
                                  decoration: new BoxDecoration(
                                    //color: Colors.grey[300],
                                    shape: BoxShape.circle,
                                  ),
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        widget.requsets[index].sender
                                                .firstName +
                                            " " +
                                            widget.requsets[index].sender
                                                .lastName,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w500),
                                      ),

                                      // ////// <<<<< Mutual Friends >>>>> //////
                                      // Container(
                                      //   margin: EdgeInsets.only(top: 3),
                                      //   child: Text(
                                      //     index == 0
                                      //         ? "2 mutual friends"
                                      //         : index == 1
                                      //             ? "5 mutual friends"
                                      //             : "9 mutual friends",
                                      //     maxLines: 1,
                                      //     overflow: TextOverflow.ellipsis,
                                      //     style: TextStyle(
                                      //         fontFamily: 'Oswald',
                                      //         fontWeight: FontWeight.w400,
                                      //         fontSize: 11,
                                      //         color: Colors.black45),
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        ////// <<<<< Message Button >>>>> //////
                        GestureDetector(
                          onTap: () {
                            acceptReq(widget.requsets[index]);
                          },
                          child: Container(
                              margin: EdgeInsets.only(right: 20),
                              padding: EdgeInsets.only(
                                  left: 4, right: 4, top: 4, bottom: 4),
                              decoration: BoxDecoration(
                                  color: mainColor.withOpacity(0.8),
                                  borderRadius: BorderRadius.circular(100)),
                              child: Icon(
                                Icons.done,
                                color: Colors.white,
                                size: 13,
                              )),
                        ),
                        GestureDetector(
                          onTap: () {
                            deleteReq(widget.requsets[index]);
                          },
                          child: Container(
                              margin: EdgeInsets.only(right: 20),
                              padding: EdgeInsets.only(
                                  left: 4, right: 4, top: 4, bottom: 4),
                              decoration: BoxDecoration(
                                  color: Colors.grey.withOpacity(0.3),
                                  borderRadius: BorderRadius.circular(100)),
                              child: Icon(
                                Icons.close,
                                color: Colors.black54,
                                size: 13,
                              )),
                        ),
                      ],
                    ),
                  ),
                  Divider()
                ],
              ),
            );
          }, childCount: widget.requsets.length),
        ),
      ),
    );
  }

  Future acceptReq(req) async {
    var data = {
      'request_sender_id': req.request_sender_id,
    };

    print(data);

    var res =
        await CallApi().postData1(data, 'friend/request/accept/${req.id}');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
      // Navigator.push(
      //     context, MaterialPageRoute(builder: (context) => MyHomePage(1)));
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  Future deleteReq(req) async {
    var data = {
      'request_sender_id': req.request_sender_id,
    };

    print(data);

    var res = await CallApi().postData1(data, 'friend/request/cancel/${req.id}');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
