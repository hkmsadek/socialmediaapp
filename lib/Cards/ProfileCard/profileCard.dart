import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';
import 'dart:async';

class ProfileCard extends StatefulWidget {
  final userData;
  ProfileCard(this.userData);
  @override
  ProfileCardState createState() => ProfileCardState();
}

class ProfileCardState extends State<ProfileCard> {
  SharedPreferences sharedPreferences;
  String theme = "",
      image = "",
      fName = "",
      lName = "",
      verified = "",
      businessInfoProvided = "";
  Timer _timer;
  int _start = 3, shopProvided = 0;
  bool loading = true;
  bool _isVerified = false;
  bool _isPending = false;
  bool _isBusiness = false;

  @override
  void initState() {
    print(widget.userData);

    setState(() {
      image = "${widget.userData['profilePic']}";
      fName = "${widget.userData['firstName']}";
      lName = "${widget.userData['lastName']}";
    });

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ProfileScreen()));
        },

        ////// <<<<< Main Data >>>>> //////
        child: Container(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 5.0,
                color: Colors.grey[300],
                //offset: Offset(3.0, 4.0),
              ),
            ],
            //border: Border.all(width: 1.0, color: Colors.grey[300]),
          ),
          margin: EdgeInsets.only(top: 2.5, bottom: 2.5, left: 0, right: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 20, right: 20, top: 0),
                  padding: EdgeInsets.only(right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      ////// <<<<< Profile picture >>>>> //////
                      Stack(
                        children: <Widget>[
                          ////// <<<<< Picture >>>>> //////
                          Container(
                            height: 50,
                            width: 50,
                            margin: EdgeInsets.only(right: 10),
                            padding: EdgeInsets.all(1.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: CachedNetworkImage(
                                imageUrl: image,
                                placeholder: (context, url) =>
                                    Center(child: Text("wait...")),
                                errorWidget: (context, url, error) =>
                                    Image.asset(
                                  "assets/images/user.png",
                                  //height: 40,
                                ),
                                fit: BoxFit.cover,

                                // NetworkImage(
                                //     widget.friend[index].profilePic
                              ),
                            ),
                            decoration: new BoxDecoration(
                              //color: Colors.grey[300],
                              shape: BoxShape.circle,
                            ),
                          ),

                          ////// <<<<< Online Green Dot >>>>> //////
                          Container(
                            margin: EdgeInsets.only(left: 40, top: 5),
                            padding: EdgeInsets.all(1.0),
                            child: CircleAvatar(
                              radius: 5.0,
                              backgroundColor: Colors.greenAccent,
                            ),
                            decoration: new BoxDecoration(
                              color: Colors.greenAccent, // border color
                              shape: BoxShape.circle,
                            ),
                          ),
                        ],
                      ),

                      ////// <<<<< User Name >>>>> //////
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              fName != "" && lName != ""
                                  ? '$fName ' + '$lName'
                                  : '',
                              style: TextStyle(
                                  fontSize: 19,
                                  color: Colors.black,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w400),
                            ),

                            ////// <<<<< View Profile Option >>>>> //////
                            Container(
                              margin: EdgeInsets.only(top: 3),
                              child: Text(
                                "View and edit profile",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12,
                                    color: Colors.black54),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(),
            ],
          ),
        ),
      ),
    );
  }
}
