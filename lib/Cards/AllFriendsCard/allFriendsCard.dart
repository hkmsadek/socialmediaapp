import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/ProfileFriendScreen/ProfileFriendScreen.dart';

import 'dart:async';

import '../../main.dart';

class AllFriendsCard extends StatefulWidget {
  final friendList;
  AllFriendsCard(this.friendList);

  @override
  _AllFriendsCardState createState() => _AllFriendsCardState();
}

class _AllFriendsCardState extends State<AllFriendsCard> {
  bool loading = true;
  bool unfriends = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SliverPadding(
        padding: EdgeInsets.only(bottom: 25),
        sliver: SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProfileFriendScreen(
                            widget.friendList[index].sender.userName)));
              },

              ////// <<<<< Main Data >>>>> //////
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      // boxShadow: [
                      //   BoxShadow(
                      //     blurRadius: 1.0,
                      //     color: Colors.black26,
                      //   ),
                      // ],
                    ),
                    margin: EdgeInsets.only(
                        top: 2.5, bottom: 2.5, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Container(
                                  height: 40,
                                  margin: EdgeInsets.only(right: 10),
                                  padding: EdgeInsets.all(1.0),
                                  child: widget.friendList[index].sender
                                                  .profilePic ==
                                              "" ||
                                          widget.friendList[index].sender
                                                  .profilePic ==
                                              null
                                      ? Image.asset('assets/images/user.png')
                                      : CachedNetworkImage(
                                          imageUrl: widget.friendList[index]
                                              .sender.profilePic,
                                          placeholder: (context, url) =>
                                              CircularProgressIndicator(),
                                          errorWidget: (context, url, error) =>
                                              Image.asset(
                                            "assets/images/user.png",
                                            height: 40,
                                          ),

                                          // NetworkImage(
                                          //     widget.friend[index].profilePic
                                        ),
                                  decoration: new BoxDecoration(
                                    //color: Colors.grey[300],
                                    shape: BoxShape.circle,
                                  ),
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        widget.friendList[index].sender
                                                .firstName +
                                            " " +
                                            widget.friendList[index].sender
                                                .lastName,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w500),
                                      ),

                                      ////// <<<<< Mutual Friends >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(top: 3),
                                        child: Text(
                                          unfriends ? "Add Friend" : "Friend",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 11,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        ////// <<<<< Message Button >>>>> //////
                        GestureDetector(
                          onTap: () {
                            showBottomSheet(context, widget.friendList[index]);
                          },
                          child: Container(
                              margin: EdgeInsets.only(right: 15),
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, top: 10, bottom: 10),
                              child: Icon(
                                Icons.more_vert,
                                color: Colors.black38,
                                size: 17,
                              )),
                        ),
                      ],
                    ),
                  ),
                  Divider()
                ],
              ),
            );
          }, childCount: widget.friendList.length),
        ),
      ),
    );
  }

  void showBottomSheet(context, friendList) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return Wrap(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              ProfileFriendScreen(friendList.sender.userName)));
                },
                child: Container(
                  margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(Icons.perm_identity,
                                    size: 17, color: Colors.black54)),
                            Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "View Profile",
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),

                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(Icons.chevron_right,
                              size: 17, color: Colors.black54)),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.chat_bubble_outline,
                                  size: 17, color: Colors.black54)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Send Message",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
              unfriends
                  ? Container()
                  : GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                        unfriend(friendList);
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                      margin: EdgeInsets.only(right: 10),
                                      child: Stack(
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 2, top: 2),
                                            child: Icon(Icons.perm_identity,
                                                size: 12,
                                                color: Colors.redAccent),
                                          ),
                                          Icon(Icons.block,
                                              size: 17,
                                              color: Colors.redAccent),
                                        ],
                                      )),
                                  Text.rich(
                                    TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Unfriend",
                                            style: TextStyle(
                                                color: Colors.redAccent,
                                                fontSize: 14,
                                                fontFamily: "Oswald",
                                                fontWeight: FontWeight.w400)),

                                        // can add more TextSpans here...
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(Icons.chevron_right,
                                    size: 17, color: Colors.black54)),
                          ],
                        ),
                      ),
                    ),
              Container(
                margin:
                    EdgeInsets.only(left: 15, right: 15, top: 30, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.block,
                                  size: 17, color: Colors.black54)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: unfriends ? "Follow" : "Unfollow",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
            ],
          );
        });
  }

  Future unfriend(req) async {
    var data = {
      'request_sender_id': req.request_sender_id,
    };

    print(data);

    var res =
        await CallApi().postData1(data, 'friend/request/unfriend/${req.id}');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
      setState(() {
        unfriends = true;
      });
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
