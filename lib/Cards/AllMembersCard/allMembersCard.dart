import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/ChattingPage/chattingPage.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/ModelClass/GroupMemberModel/GroupMemberModel.dart';
import 'dart:async';
import '../../main.dart';

class AllMembersCard extends StatefulWidget {
  final id;
  final isAdmin;
  AllMembersCard(this.id, this.isAdmin);

  @override
  _AllMembersCardState createState() => _AllMembersCardState();
}

class _AllMembersCardState extends State<AllMembersCard> {
  bool loading = true;
  var memberList;
  List allAdmin = [];
  List allMember = [];

  @override
  void initState() {
    super.initState();
    loadMemberList();
  }

  Future loadMemberList() async {
    setState(() {
      loading = true;
    });
    var reqresponse =
        await CallApi().getData('group/members?group_id=${widget.id}');
    var reqcontent = reqresponse.body;
    final req = json.decode(reqcontent);
    //var memdata = GroupMemberModel.fromJson(req);

    setState(() {
      memberList = req["members"];
      for (int i = 0; i < memberList.length; i++) {
        if (memberList[i]['user_role'] == "Super Admin" ||
            memberList[i]['user_role'] == "Admin") {
          allAdmin.add({
            'id': memberList[i]['id'],
            'uID': memberList[i]['members']['id'],
            'fname': memberList[i]['members']['firstName'],
            'lname': memberList[i]['members']['lastName'],
            'lname': memberList[i]['members']['profilePic'],
            'role': memberList[i]['user_role'],
          });
        } else {
          allMember.add({
            'id': memberList[i]['id'],
            'uID': memberList[i]['members']['id'],
            'fname': memberList[i]['members']['firstName'],
            'lname': memberList[i]['members']['lastName'],
            'lname': memberList[i]['members']['profilePic'],
            'role': memberList[i]['user_role'],
          });
        }
      }
      loading = false;
    });
    print(allAdmin.length);
    print(allMember.length);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: loading
          ? LoaderScreen()
          : Column(
              children: List.generate(memberList.length, (index) {
              return Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => FriendProfileNewPage()));
                    },

                    ////// <<<<< Main Data >>>>> //////
                    child: Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        // boxShadow: [
                        //   BoxShadow(
                        //     blurRadius: 1.0,
                        //     color: Colors.black38,
                        //   ),
                        // ],
                      ),
                      margin:
                          EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 20, right: 20, top: 0),
                              padding: EdgeInsets.only(right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< Profile picture >>>>> //////
                                  Stack(
                                    children: <Widget>[
                                      ////// <<<<< Picture >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(right: 10),
                                        padding: EdgeInsets.all(1.0),
                                        child: CircleAvatar(
                                          radius: 20.0,
                                          backgroundColor: Colors.white,
                                          backgroundImage: AssetImage(
                                              'assets/images/user.png'),
                                        ),
                                        decoration: new BoxDecoration(
                                          color: Colors.grey[300],
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                    ],
                                  ),

                                  ////// <<<<< User Name >>>>> //////
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "${memberList[index]['members']['firstName']} ${memberList[index]['members']['lastName']}",
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black54,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w400),
                                        ),

                                        ////// <<<<< Mutual Friends >>>>> //////
                                        Container(
                                          margin: EdgeInsets.only(top: 3),
                                          child: Text(
                                            "${memberList[index]['user_role']}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w400,
                                                fontSize: 11,
                                                color: Colors.black45),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),

                          ////// <<<<< Message Button >>>>> //////
                          Row(
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => ChattingPage()));
                                },
                                child: Container(
                                    margin: EdgeInsets.only(right: 0),
                                    padding: EdgeInsets.only(
                                        left: 10, right: 10, top: 5, bottom: 5),
                                    decoration: BoxDecoration(
                                        color: mainColor,
                                        borderRadius: BorderRadius.circular(15),
                                        border: Border.all(
                                            color: mainColor, width: 0.5)),
                                    child: Text("Message",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w400,
                                            color: Colors.white,
                                            fontSize: 12))),
                              ),
                              widget.isAdmin
                                  ? GestureDetector(
                                      onTap: () {
                                        showBottomSheet(context, index);
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.all(15.0),
                                        child: Icon(Icons.more_vert,
                                            color: Colors.grey, size: 18),
                                      ),
                                    )
                                  : Container()
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Divider()
                ],
              );
            })),
    );
  }

  void showBottomSheet(context, index) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return Wrap(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(
                                  memberList[index]['user_role'] ==
                                              "Super Admin" ||
                                          memberList[index]['user_role'] ==
                                              "Admin"
                                      ? Icons.cancel
                                      : Icons.account_box,
                                  size: 17,
                                  color: Colors.black54)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: memberList[index]['user_role'] ==
                                                "Super Admin" ||
                                            memberList[index]['user_role'] ==
                                                "Admin"
                                        ? "Remove as Admin"
                                        : "Make Admin",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Container(
                  margin:
                      EdgeInsets.only(left: 15, right: 15, top: 30, bottom: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(Icons.close,
                                    size: 17, color: Colors.redAccent)),
                            Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "Remove",
                                      style: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),

                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(Icons.chevron_right,
                              size: 17, color: Colors.black54)),
                    ],
                  ),
                ),
              ),
            ],
          );
        });
  }
}
