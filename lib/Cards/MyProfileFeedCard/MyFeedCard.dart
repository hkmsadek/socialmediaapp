import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/CommentPage/CommentPage.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';
import 'package:social_app_fb/MainScreens/StatusDetailsPage/StatusDetailsPage.dart';

import '../../main.dart';

List comPostCount = [];

class MyFeedCard extends StatefulWidget {
  final loading;
  final index;
  final userData;
  final feedList;
  MyFeedCard(this.loading, this.index, this.userData, this.feedList);

  @override
  _MyFeedCardState createState() => _MyFeedCardState();
}

class _MyFeedCardState extends State<MyFeedCard> {
  int no, likeNum = 0;
  int l = 0;
  int like = 0, likeStore = 0;
  String date = "";

  @override
  void initState() {
    setState(() {
      like = widget.feedList.meta.totalLikesCount;
      likeStore = widget.feedList.meta.totalLikesCount;

      DateTime dateTime = DateTime.parse(widget.feedList.createdAt);
      date = DateFormat.yMMMd().format(dateTime);
    });
    super.initState();
  }

  void _statusModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                // Text('React to this post',
                //       style: TextStyle(fontWeight: FontWeight.normal)),
                new ListTile(
                  leading: new Icon(Icons.edit),
                  title: new Text('Edit',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => CreatePost()))
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  title: new Text('Delete',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.redAccent,
                          fontFamily: "Oswald")),
                  onTap: () => {Navigator.pop(context), _showDeleteDialog()},
                ),
              ],
            ),
          );
        });
  }

  Future<Null> _showDeleteDialog() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          "Want to delete the post?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future statusDelete() async {
    var data = {
      'user_id': widget.userData['id'],
      'status_id': widget.feedList.id,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/delete');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 201) {
      Navigator.pop(context);
      setState(() {
        profileFeedList.removeAt(widget.index);
      });
      return _showMessage("Deleted Successfully!", 2);
    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    StatusDetailsPage(widget.index, widget.userData,widget.feedList.id)));
      },
      child: Container(
        child: widget.loading == false
            ? Container(
                padding: EdgeInsets.only(top: 0, bottom: 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  //border: Border.all(width: 0.8, color: Colors.grey[300]),
                  // boxShadow: [
                  //   BoxShadow(
                  //     blurRadius: 0.0,
                  //     color: Colors.black26,
                  //     //offset: Offset(6.0, 7.0),
                  //   ),
                  // ],
                ),
                margin: EdgeInsets.only(top: 5, bottom: 5, left: 0, right: 0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            //color: Colors.red,
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 15),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Picture start >>>>> //////
                                Container(
                                  margin: EdgeInsets.only(right: 10),
                                  //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                  padding: EdgeInsets.all(1.0),
                                  child: CircleAvatar(
                                    radius: 20.0,
                                    backgroundColor: Colors.white,
                                    backgroundImage:
                                        AssetImage('assets/images/prabal.jpg'),
                                  ),
                                  decoration: new BoxDecoration(
                                    color: Colors.grey[300], // border color
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                ////// <<<<< Picture end >>>>> //////

                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    ////// <<<<< Name start >>>>> //////
                                    Text(
                                      "${widget.feedList.data.statusUserName}",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.black54,
                                          fontFamily: 'Oswald',
                                          fontWeight: FontWeight.w500),
                                    ),
                                    ////// <<<<< Name start >>>>> //////

                                    ////// <<<<< Time start >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(top: 3),
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            date,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w400,
                                                fontSize: 11,
                                                color: Colors.black45),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 5),
                                              child: Icon(
                                                Icons.public,
                                                color: Colors.black45,
                                                size: 12,
                                              ))
                                        ],
                                      ),
                                    ),
                                    ////// <<<<< Time end >>>>> //////
                                  ],
                                ),
                              ],
                            ),
                          ),
                          ////// <<<<< More start >>>>> //////
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _statusModalBottomSheet(context);
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(5),
                                margin: EdgeInsets.only(right: 15),
                                child: Icon(
                                  Icons.more_horiz,
                                  color: Colors.black45,
                                )),
                          ),
                          ////// <<<<< More end >>>>> //////
                        ],
                      ),
                    ),

                    ////// <<<<< Post start >>>>> //////
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20, top: 20),
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        "${widget.feedList.activityText}",
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            color: Colors.black54, fontWeight: FontWeight.w400),
                      ),
                    ),
                    // Container(
                    //     margin: EdgeInsets.only(left: 20, right: 20, top: 20),
                    //     child: widget.index == 0
                    //         ? Container(
                    //             width: MediaQuery.of(context).size.width,
                    //             child: Text(
                    //               "Honesty is the best policy",
                    //               textAlign: TextAlign.justify,
                    //               style: TextStyle(
                    //                   color: Colors.black54,
                    //                   fontWeight: FontWeight.w400),
                    //             ),
                    //           )
                    //         : widget.index == 1
                    //             ? Container(
                    //                 child: Column(
                    //                   children: <Widget>[
                    //                     Container(
                    //                       alignment: Alignment.centerLeft,
                    //                       child: Text(
                    //                         "Tour to nearest hill",
                    //                         textAlign: TextAlign.justify,
                    //                         style: TextStyle(
                    //                             color: Colors.black54,
                    //                             fontWeight: FontWeight.w400),
                    //                       ),
                    //                     ),
                    //                     Container(
                    //                         //color: Colors.red,
                    //                         height: 200,
                    //                         padding: const EdgeInsets.all(0.0),
                    //                         margin: EdgeInsets.only(top: 10),
                    //                         decoration: BoxDecoration(
                    //                             borderRadius:
                    //                                 BorderRadius.circular(5),
                    //                             image: DecorationImage(
                    //                                 image: AssetImage(
                    //                                     "assets/images/f7.jpg"),
                    //                                 fit: BoxFit.cover)),
                    //                         child: null),
                    //                   ],
                    //                 ),
                    //               )
                    //             : Container(
                    //                 child: Column(
                    //                   children: <Widget>[
                    //                     Container(
                    //                       alignment: Alignment.centerLeft,
                    //                       child: Text(
                    //                         "Bike and Car Riding",
                    //                         textAlign: TextAlign.justify,
                    //                         style: TextStyle(
                    //                             color: Colors.black54,
                    //                             fontWeight: FontWeight.w400),
                    //                       ),
                    //                     ),
                    //                     Container(
                    //                       child: Column(
                    //                         children: <Widget>[
                    //                           Container(
                    //                             child: Row(
                    //                               children: <Widget>[
                    //                                 Expanded(
                    //                                   child: Container(
                    //                                       //color: Colors.red,
                    //                                       height: 150,
                    //                                       //width: 150,
                    //                                       padding:
                    //                                           const EdgeInsets
                    //                                               .all(0.0),
                    //                                       margin:
                    //                                           EdgeInsets
                    //                                               .only(
                    //                                                   top: 10,
                    //                                                   right: 5),
                    //                                       decoration: BoxDecoration(
                    //                                           borderRadius:
                    //                                               BorderRadius
                    //                                                   .circular(
                    //                                                       5),
                    //                                           image: DecorationImage(
                    //                                               image: AssetImage(
                    //                                                   "assets/images/bike1.jpg"),
                    //                                               fit: BoxFit
                    //                                                   .cover)),
                    //                                       child: null),
                    //                                 ),
                    //                                 Expanded(
                    //                                   child: Container(
                    //                                       //color: Colors.red,
                    //                                       height: 150,
                    //                                       // width: 150,
                    //                                       padding:
                    //                                           const EdgeInsets
                    //                                               .all(0.0),
                    //                                       margin:
                    //                                           EdgeInsets
                    //                                               .only(
                    //                                                   top: 10,
                    //                                                   left: 5),
                    //                                       decoration: BoxDecoration(
                    //                                           borderRadius:
                    //                                               BorderRadius
                    //                                                   .circular(
                    //                                                       5),
                    //                                           image: DecorationImage(
                    //                                               image: AssetImage(
                    //                                                   "assets/images/car3.jpeg"),
                    //                                               fit: BoxFit
                    //                                                   .cover)),
                    //                                       child: null),
                    //                                 ),
                    //                               ],
                    //                             ),
                    //                           ),
                    //                           Container(
                    //                             child: Row(
                    //                               children: <Widget>[
                    //                                 Expanded(
                    //                                   child: Container(
                    //                                       //color: Colors.red,
                    //                                       height: 150,
                    //                                       //width: 150,
                    //                                       padding:
                    //                                           const EdgeInsets
                    //                                               .all(0.0),
                    //                                       margin:
                    //                                           EdgeInsets
                    //                                               .only(
                    //                                                   top: 10,
                    //                                                   right: 5),
                    //                                       decoration: BoxDecoration(
                    //                                           borderRadius:
                    //                                               BorderRadius
                    //                                                   .circular(
                    //                                                       5),
                    //                                           image: DecorationImage(
                    //                                               image: AssetImage(
                    //                                                   "assets/images/car6.jpg"),
                    //                                               fit: BoxFit
                    //                                                   .cover)),
                    //                                       child: null),
                    //                                 ),
                    //                                 Expanded(
                    //                                   child: Container(
                    //                                       //color: Colors.red,
                    //                                       height: 150,
                    //                                       // width: 150,
                    //                                       padding:
                    //                                           const EdgeInsets
                    //                                               .all(0.0),
                    //                                       margin:
                    //                                           EdgeInsets
                    //                                               .only(
                    //                                                   top: 10,
                    //                                                   left: 5),
                    //                                       decoration: BoxDecoration(
                    //                                           borderRadius:
                    //                                               BorderRadius
                    //                                                   .circular(
                    //                                                       5),
                    //                                           image: DecorationImage(
                    //                                               image: AssetImage(
                    //                                                   "assets/images/bike3.jpeg"),
                    //                                               fit: BoxFit
                    //                                                   .cover)),
                    //                                       child: Container(
                    //                                         height: 150,
                    //                                         // width: 150,
                    //                                         padding:
                    //                                             const EdgeInsets
                    //                                                 .all(0.0),
                    //                                         margin:
                    //                                             EdgeInsets.only(
                    //                                                 top: 0,
                    //                                                 left: 0),
                    //                                         decoration:
                    //                                             BoxDecoration(
                    //                                           color: Colors
                    //                                               .black
                    //                                               .withOpacity(
                    //                                                   0.6),
                    //                                           borderRadius:
                    //                                               BorderRadius
                    //                                                   .circular(
                    //                                                       5),
                    //                                         ),
                    //                                         child: Center(
                    //                                             child: Container(
                    //                                                 child: Text(
                    //                                           "+5",
                    //                                           style: TextStyle(
                    //                                               color: Colors
                    //                                                   .white,
                    //                                               fontSize: 22),
                    //                                         ))),
                    //                                       )),
                    //                                 ),
                    //                               ],
                    //                             ),
                    //                           ),
                    //                         ],
                    //                       ),
                    //                     ),
                    //                   ],
                    //                 ),
                    //               )),
                    ////// <<<<< Post end >>>>> //////
                    Container(
                        margin: EdgeInsets.only(
                            left: 20, right: 20, bottom: 0, top: 10),
                        child: Divider(
                          color: Colors.grey[300],
                        )),
                    Container(
                      margin: EdgeInsets.only(left: 20, top: 0),
                      padding: EdgeInsets.all(0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          ////// <<<<< Like start >>>>> //////
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                l++;
                                if (like % 2 == 0) {
                                  like--;
                                  if (like < 0) {
                                    like = 0;
                                  }
                                } else {
                                  like++;
                                }
                                print(l);
                                print(like);
                              });
                            },
                            child: Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(3.0),
                                    child: Icon(
                                      like != likeStore
                                          ? Icons.favorite
                                          : Icons.favorite_border,
                                      size: 20,
                                      color: like != likeStore
                                          ? Colors.redAccent
                                          : Colors.black54,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 3),
                                    child: Text("$like",
                                        style: TextStyle(
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300,
                                            color: Colors.black54,
                                            fontSize: 12)),
                                  )
                                ],
                              ),
                            ),
                          ),
                          ////// <<<<< Like end >>>>> //////

                          ////// <<<<< Comment start >>>>> //////
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (context) => CommentPage(
                              //             widget.userData, widget.index)));
                            },
                            child: Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(left: 15),
                                    padding: EdgeInsets.all(3.0),
                                    child: Icon(
                                      Icons.chat_bubble_outline,
                                      size: 20,
                                      color: Colors.black54,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 3),
                                    child: Text(
                                        "${postComCount[widget.index]['count']}",
                                        style: TextStyle(
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300,
                                            color: Colors.black54,
                                            fontSize: 12)),
                                  )
                                ],
                              ),
                            ),
                          ),
                          ////// <<<<< Comment end >>>>> //////

                          ////// <<<<< Share start >>>>> //////
                          GestureDetector(
                            onTap: () {
                              _shareModalBottomSheet(
                                  context, widget.index, widget.userData);
                            },
                            child: Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(left: 15),
                                    padding: EdgeInsets.all(3.0),
                                    child: Icon(
                                      Icons.share,
                                      size: 20,
                                      color: Colors.black54,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 3),
                                    child: Text(
                                        "${widget.feedList.meta.totalSharesCount}",
                                        style: TextStyle(
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300,
                                            color: Colors.black54,
                                            fontSize: 12)),
                                  )
                                ],
                              ),
                            ),
                          ),
                          ////// <<<<< Share end >>>>> //////
                        ],
                      ),
                    ),
                    Container(
                        height: 10,
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(5)),
                        margin: EdgeInsets.only(
                            left: 0, right: 0, bottom: 0, top: 7),
                        child: null),
                  ],
                ),
              )
            : Container(
                padding: EdgeInsets.only(top: 20, bottom: 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  //border: Border.all(width: 0.8, color: Colors.grey[300]),
                  // boxShadow: [
                  //   BoxShadow(
                  //     blurRadius: 1.0,
                  //     color: Colors.black38,
                  //     //offset: Offset(6.0, 7.0),
                  //   ),
                  // ],
                ),
                margin: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          //color: Colors.red,
                          margin: EdgeInsets.only(left: 10, right: 10, top: 0),
                          padding: EdgeInsets.only(right: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                padding: EdgeInsets.all(1.0),
                                child: Shimmer.fromColors(
                                  baseColor: Colors.grey[100],
                                  highlightColor: Colors.grey[200],
                                  child: CircleAvatar(
                                    radius: 20.0,
                                    //backgroundColor: Colors.white,
                                  ),
                                ),
                                decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Shimmer.fromColors(
                                    baseColor: Colors.grey[100],
                                    highlightColor: Colors.grey[200],
                                    child: Container(
                                      width: 100,
                                      height: 22,
                                      child: Container(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 3),
                                    child: Shimmer.fromColors(
                                      baseColor: Colors.grey[100],
                                      highlightColor: Colors.grey[200],
                                      child: Container(
                                        width: 50,
                                        height: 12,
                                        child: Container(
                                          color: Colors.black,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            margin: EdgeInsets.only(
                                left: 20, right: 20, top: 20, bottom: 0),
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey[100],
                              highlightColor: Colors.grey[200],
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 10,
                                child: Container(
                                  color: Colors.black,
                                ),
                              ),
                            )),
                        Container(
                            margin: EdgeInsets.only(
                                left: 20, right: 20, top: 2, bottom: 5),
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey[100],
                              highlightColor: Colors.grey[200],
                              child: Container(
                                width: MediaQuery.of(context).size.width - 100,
                                height: 10,
                                child: Container(
                                  color: Colors.black,
                                ),
                              ),
                            )),
                      ],
                    ),
                    Container(
                        height: 10,
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(5)),
                        margin: EdgeInsets.only(
                            left: 0, right: 0, bottom: 0, top: 7),
                        child: null),
                  ],
                ),
              ),
      ),
    );
  }

  void _shareModalBottomSheet(context, int index, var userData) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Container(
                //height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.share,
                              color: Colors.black54,
                              size: 16,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 5),
                              child: Text(
                                "Share this post",
                                style: TextStyle(fontFamily: "Oswald"),
                              ),
                            ),
                          ],
                        )),
                    Divider(),
                    Container(
                      child: Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              width: 50,
                              height: 50,
                              margin: EdgeInsets.only(top: 0, left: 20),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/prabal.jpg"),
                                      fit: BoxFit.cover),
                                  border: Border.all(
                                      color: Colors.grey, width: 0.1),
                                  borderRadius: BorderRadius.circular(15)),
                            ),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                      alignment: Alignment.centerLeft,
                                      margin:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(
                                              "${userData['firstName']} ${userData['lastName']}",
                                              maxLines: 1,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14,
                                                  fontFamily: 'Oswald',
                                                  fontWeight:
                                                      FontWeight.normal),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            //_securityModalBottomSheet(context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 0, right: 5, left: 10),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 0.3,
                                                    color: Colors.black54),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Icon(
                                                    Icons.public,
                                                    size: 12,
                                                    color: Colors.black54,
                                                  ),
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Public",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                          fontFamily: "Oswald"),
                                                    )),
                                                Icon(
                                                  Icons.arrow_drop_down,
                                                  size: 25,
                                                  color: Colors.black54,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //height: 150,
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.only(
                          top: 20, left: 20, bottom: 5, right: 20),
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.only(
                            topLeft: Radius.circular(5.0),
                            topRight: Radius.circular(5.0),
                            bottomLeft: Radius.circular(5.0),
                            bottomRight: Radius.circular(5.0)),
                        //color: Colors.grey[100],
                        //border: Border.all(width: 0.2, color: Colors.grey)
                      ),
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(minHeight: 100.0, maxHeight: 100.0),
                        child: new SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          //reverse: true,
                          child: Container(
                            //height: 100,
                            child: new TextField(
                              maxLines: null,
                              autofocus: false,
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: 'Oswald',
                              ),
                              //controller: msgController,
                              decoration: InputDecoration(
                                alignLabelWithHint: true,
                                hintText: "Write something...",
                                hintStyle: TextStyle(
                                    color: Colors.black54,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w300),
                                contentPadding:
                                    EdgeInsets.fromLTRB(0.0, 10.0, 20.0, 10.0),
                                border: InputBorder.none,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  //sharePost = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              //snackBar(context);
                              //isSubmit == false ? statusShare(feeds.id) : null;
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 20, top: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.only(
                                          left: 20, right: 20, top: 0),
                                      decoration: BoxDecoration(
                                          color: mainColor,
                                          border: Border.all(
                                              color: Colors.grey, width: 0.5),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: Text(
                                        "Share",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontFamily: 'BebasNeue',
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      //height: MediaQuery.of(context).size.height / 2.05,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        //border: Border.all(width: 0.8, color: Colors.grey[300]),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 16.0,
                            color: Colors.grey[300],
                            //offset: Offset(3.0, 4.0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(
                          top: 5, bottom: 15, left: 10, right: 10),
                      child: Container(
                        //color: Colors.yellow,
                        margin:
                            EdgeInsets.only(left: 20, right: 20, bottom: 10),
                        padding: EdgeInsets.only(right: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< pic start >>>>> //////
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    padding: EdgeInsets.all(1.0),
                                    child: CircleAvatar(
                                      radius: 20.0,
                                      backgroundColor: Colors.white,
                                      backgroundImage:
                                          AssetImage("assets/images/man2.jpg"),
                                    ),
                                    decoration: new BoxDecoration(
                                      color: Colors.grey[300], // border color
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                  ////// <<<<< pic end >>>>> //////

                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ////// <<<<< Name & Interest start >>>>> //////
                                          Container(
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    "John Smith",
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        color: Colors.black,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                          ////// <<<<< Name & Interest end >>>>> //////

                                          ////// <<<<< time job start >>>>> //////
                                          Container(
                                            margin: EdgeInsets.only(top: 3),
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    "Aug 7 at 5:34 PM",
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 11,
                                                        color: Colors.black54),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 3),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Icon(
                                                          Icons.public,
                                                          size: 12,
                                                          color: Colors.black54,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          ////// <<<<< time job end >>>>> //////
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                                margin:
                                    EdgeInsets.only(left: 0, right: 0, top: 20),
                                child: index == 0
                                    ? Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Text(
                                          "Honesty is the best policy",
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              color: Colors.black54,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      )
                                    : index == 1
                                        ? Container(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                    "Tour to nearest hill",
                                                    textAlign:
                                                        TextAlign.justify,
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                                Container(
                                                    //color: Colors.red,
                                                    height: 200,
                                                    padding:
                                                        const EdgeInsets.all(
                                                            0.0),
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        image: DecorationImage(
                                                            image: AssetImage(
                                                                "assets/images/f7.jpg"),
                                                            fit: BoxFit.cover)),
                                                    child: null),
                                              ],
                                            ),
                                          )
                                        : Container(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                    "Bike and Car Riding",
                                                    textAlign:
                                                        TextAlign.justify,
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                                Container(
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 150,
                                                                  //width: 150,
                                                                  padding:
                                                                      const EdgeInsets.all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          right:
                                                                              5),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              "assets/images/bike1.jpg"),
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                  child: null),
                                                            ),
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 150,
                                                                  // width: 150,
                                                                  padding:
                                                                      const EdgeInsets.all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          left:
                                                                              5),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              "assets/images/car3.jpeg"),
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                  child: null),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 150,
                                                                  //width: 150,
                                                                  padding:
                                                                      const EdgeInsets.all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          right:
                                                                              5),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              "assets/images/car6.jpg"),
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                  child: null),
                                                            ),
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 150,
                                                                  // width: 150,
                                                                  padding:
                                                                      const EdgeInsets.all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          left:
                                                                              5),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              "assets/images/bike3.jpeg"),
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                  child:
                                                                      Container(
                                                                    height: 150,
                                                                    // width: 150,
                                                                    padding:
                                                                        const EdgeInsets.all(
                                                                            0.0),
                                                                    margin: EdgeInsets.only(
                                                                        top: 0,
                                                                        left:
                                                                            0),
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      color: Colors
                                                                          .black
                                                                          .withOpacity(
                                                                              0.6),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                    ),
                                                                    child: Center(
                                                                        child: Container(
                                                                            child: Text(
                                                                      "+5",
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              22),
                                                                    ))),
                                                                  )),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )),
                          ],
                        ),
                      ),
                    )
                  ],
                )),
          );
        });
  }
}
