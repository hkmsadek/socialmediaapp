import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:shimmer/shimmer.dart';
import 'package:social_app_fb/MainScreens/ProfileFriendScreen/ProfileFriendScreen.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';

import '../../main.dart';

class SearchCard extends StatefulWidget {
  final searchResult;
  SearchCard(this.searchResult);
  @override
  SearchCardState createState() => SearchCardState();
}

class SearchCardState extends State<SearchCard> {
  int _current = 0;
  int _isBack = 0;
  String result = '', img = "";
  bool _isChecked = false;
  var userData;
  SharedPreferences sharedPreferences;
  String theme = "";
  Timer _timer;
  int _start = 3;
  bool loading = true;
  TextEditingController src = new TextEditingController();

  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.searchResult['userName'] == userData['userName']
            ? Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProfileScreen()),
              )
            : Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ProfileFriendScreen(widget.searchResult['userName'])),
              );
      },
      child: Container(
        padding: EdgeInsets.only(top: 10, bottom: 10, right: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          //border: Border.all(width: 0.8, color: Colors.grey[300]),
          boxShadow: [
            BoxShadow(
              blurRadius: 1.0,
              color: Colors.black38,
              //offset: Offset(6.0, 7.0),
            ),
          ],
        ),
        margin: EdgeInsets.only(top: 2.5, bottom: 2.5, left: 20, right: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Container(
                //color: Colors.red,
                margin: EdgeInsets.only(left: 15, right: 20, top: 0),
                padding: EdgeInsets.only(right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      width: 40,
                      height: 40,
                      padding: EdgeInsets.all(1.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: CachedNetworkImage(
                            imageUrl: widget.searchResult['type'] == "user"
                                ? "${widget.searchResult['user']['profilePic']}"
                                : "${widget.searchResult['logo']}",
                            placeholder: (context, url) =>
                                Container(child: CircularProgressIndicator()),
                            errorWidget: (context, url, error) =>
                                Image.asset("assets/images/user.png"),
                            fit: BoxFit.cover),
                      ),
                      decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "${widget.searchResult['firstName']} ${widget.searchResult['lastName']}",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                // setState(() {
                //   friendname.removeAt(widget.index);
                // });
              },
              child: Container(
                margin: EdgeInsets.only(right: 5),
                padding: EdgeInsets.all(5),
                child: Icon(Icons.message, color: mainColor, size: 18),
              ),
            )
          ],
        ),
      ),
    );
  }
}
