import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:social_app_fb/API/api.dart';
import 'dart:async';

import '../../main.dart';

class AllFriendInviteCard extends StatefulWidget {
  final id;
  AllFriendInviteCard(this.id);

  @override
  AllFriendInviteCardState createState() => AllFriendInviteCardState();
}

class AllFriendInviteCardState extends State<AllFriendInviteCard> {
  String result = '';
  Timer _timer;
  int _start = 3;
  bool loading = false;
  bool memberLoading = false;
  TextEditingController src = new TextEditingController();
  List frndName = [];
  var userData;

  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
  }

  Future getMember() async {
    setState(() {
      loading = true;
    });
    var data = {
      'search_query': result,
    };

    print(data);

    var res = await CallApi()
        .getData('search/group/member/${widget.id}?search_query=$result');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      frndName.clear();
      setState(() {
        for (int i = 0; i < body['searchResult'].length; i++) {
          frndName.add({
            'id': body['searchResult'][i]['id'],
            'user_id': body['searchResult'][i]['user_id'],
            'fname': "${body['searchResult'][i]['firstName']}",
            'lname': "${body['searchResult'][i]['lastName']}",
            'pic': "${body['searchResult'][i]['profilePic']}",
          });
        }
      });
    } else {
      _showMessage("Something went wrong!", 1);
    }

    setState(() {
      loading = false;
    });
  }

  Future addMember(userId, index) async {
    setState(() {
      frndName[index]['loader'] = true;
    });
    var data = {
      'member_id': userId,
      'user_id': userData['id'],
    };

    print(data);

    var res = await CallApi().postData1(data, 'group/add/members/${widget.id}');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      _showMessage("Added successfully!", 2);
      setState(() {
        for (int i = 0; i < body['searchResult'].length; i++) {
          frndName.removeAt(index);
        }
      });
    } else {
      _showMessage("Something went wrong!", 1);
    }

    setState(() {
      frndName[index]['loader'] = false;
    });
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 5, top: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(left: 10, right: 10),
                    margin: EdgeInsets.only(left: 20, right: 20, top: 5),
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.7),
                        border: Border.all(color: Colors.grey, width: 0.2),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.search,
                          color: Colors.black26,
                          size: 16,
                        ),
                        Flexible(
                          child: Container(
                            child: TextField(
                              controller: src,
                              keyboardType: TextInputType.text,
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: 'Oswald',
                              ),
                              decoration: InputDecoration(
                                hintText: "Enter name...",
                                hintStyle: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w300),
                                //labelStyle: TextStyle(color: Colors.white70),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                                border: InputBorder.none,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  result = value;
                                });
                                getMember();
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5),
                          child: Icon(
                            Icons.chevron_right,
                            color: mainColor.withOpacity(0.6),
                            size: 20,
                          ),
                        ),
                      ],
                    )),
                Container(
                  margin: EdgeInsets.only(top: 5, bottom: 15),
                  child: Column(
                      children: List.generate(frndName.length, (index) {
                    return GestureDetector(
                      onTap: () {
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => FriendProfileNewPage()));
                      },
                      child: loading == false
                          ? Container(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    margin: EdgeInsets.only(
                                        top: 0, bottom: 0, left: 0, right: 0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            //color: Colors.red,
                                            margin: EdgeInsets.only(
                                                left: 20, right: 20, top: 0),
                                            padding: EdgeInsets.only(right: 10),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Stack(
                                                  children: <Widget>[
                                                    Container(
                                                      height: 40,
                                                      width: 40,
                                                      margin: EdgeInsets.only(
                                                          right: 10),
                                                      padding:
                                                          EdgeInsets.all(1.0),
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(100),
                                                        child:
                                                            CachedNetworkImage(
                                                          imageUrl:
                                                              frndName[index]
                                                                  ['pic'],
                                                          placeholder: (context,
                                                                  url) =>
                                                              Center(
                                                                  child: Text(
                                                                      "wait...")),
                                                          errorWidget: (context,
                                                                  url, error) =>
                                                              Image.asset(
                                                            "assets/images/user.png",
                                                            //height: 40,
                                                          ),
                                                          fit: BoxFit.cover,

                                                          // NetworkImage(
                                                          //     widget.friend[index].profilePic
                                                        ),
                                                      ),
                                                      decoration:
                                                          new BoxDecoration(
                                                        //color: Colors.grey[300],
                                                        shape: BoxShape.circle,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 28, top: 2),
                                                      //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                      padding:
                                                          EdgeInsets.all(1.0),
                                                      child: CircleAvatar(
                                                        radius: 4.0,
                                                        backgroundColor:
                                                            Colors.greenAccent,
                                                        //backgroundImage: AssetImage('assets/user.png'),
                                                      ),
                                                      decoration:
                                                          new BoxDecoration(
                                                        color: Colors
                                                            .greenAccent, // border color
                                                        shape: BoxShape.circle,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        "${frndName[index]['fname']} ${frndName[index]['lname']}",
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            color: Colors.black,
                                                            fontFamily:
                                                                'Oswald',
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400),
                                                      ),
                                                      // Container(
                                                      //   margin: EdgeInsets.only(
                                                      //       top: 3),
                                                      //   child: Text(
                                                      //     index == 0
                                                      //         ? "6 mutual friends"
                                                      //         : index == 1
                                                      //             ? "16 mutual friends"
                                                      //             : "34 mutual friends",
                                                      //     maxLines: 1,
                                                      //     overflow: TextOverflow
                                                      //         .ellipsis,
                                                      //     style: TextStyle(
                                                      //         fontFamily:
                                                      //             'Oswald',
                                                      //         fontWeight:
                                                      //             FontWeight
                                                      //                 .w400,
                                                      //         fontSize: 11,
                                                      //         color: Colors
                                                      //             .black54),
                                                      //   ),
                                                      // ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            addMember(
                                                frndName[index]['user_id'],
                                                index);
                                          },
                                          child: Container(
                                              margin:
                                                  EdgeInsets.only(right: 20),
                                              padding: EdgeInsets.only(
                                                  left: 10,
                                                  right: 10,
                                                  top: 5,
                                                  bottom: 5),
                                              decoration: BoxDecoration(
                                                  color: mainColor,
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                  border: Border.all(
                                                      color: mainColor,
                                                      width: 0.5)),
                                              child: Text("Add +",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Colors.white,
                                                      fontSize: 12))),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider()
                                ],
                              ),
                            )
                          : Container(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15),
                                      //border: Border.all(width: 0.8, color: Colors.grey[300]),
                                      // boxShadow: [
                                      //   BoxShadow(
                                      //     blurRadius: 1.0,
                                      //     color: Colors.black38,
                                      //     //offset: Offset(6.0, 7.0),
                                      //   ),
                                      // ],
                                    ),
                                    margin: EdgeInsets.only(
                                        top: 0, bottom: 0, left: 20, right: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            //color: Colors.red,
                                            margin: EdgeInsets.only(
                                                left: 0, right: 0, top: 0),
                                            padding: EdgeInsets.only(right: 10),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                  padding: EdgeInsets.all(1.0),
                                                  child: Shimmer.fromColors(
                                                    baseColor: Colors.grey[300],
                                                    highlightColor:
                                                        Colors.grey[200],
                                                    child: CircleAvatar(
                                                      radius: 20.0,
                                                      backgroundColor:
                                                          Colors.white,
                                                    ),
                                                  ),
                                                  decoration: new BoxDecoration(
                                                    shape: BoxShape.circle,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Shimmer.fromColors(
                                                        baseColor:
                                                            Colors.grey[300],
                                                        highlightColor:
                                                            Colors.grey[200],
                                                        child: Container(
                                                          width: 150,
                                                          height: 22,
                                                          child: Container(
                                                            color: Colors.black,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 3),
                                                        child:
                                                            Shimmer.fromColors(
                                                          baseColor:
                                                              Colors.grey[300],
                                                          highlightColor:
                                                              Colors.grey[200],
                                                          child: Container(
                                                            width: 90,
                                                            height: 12,
                                                            child: Container(
                                                              color:
                                                                  Colors.black,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider()
                                ],
                              ),
                            ),
                    );
                  })),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
