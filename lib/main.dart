import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';
import 'package:social_app_fb/MainScreens/StatusDetailsPage/StatusDetailsPage.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:flutter/foundation.dart';
import 'API/api.dart';
import 'MainScreens/ConfirmEmailPage/ConfirmEmailPage.dart';
import 'MainScreens/LoginRegisterPage/LoginRegisterPage.dart';

int stId = 0;
List work = [];
List fwork = [];
List skill = [];
List fskill = [];
List education = [];
List feducation = [];
List school = [];
List fschool = [];
List locationCur = [];
List flocationCur = [];
List locationHome = [];
List flocationHome = [];
List locationOther = [];
List flocationOther = [];
List allInfo = [];
List fallInfo = [];
List profileFeedList = [];
List fprofileFeedList = [];
List lastMsg = [];
List lastSeen = [];
List feedList = [];
int selectedPage = 0;
int frndNum = 0;
int chatUnseenNum = 0;
int notifyNum = 0;
bool isLoggedIn = false;
List<String> user = [];
List<String> name = [
  "John Smith",
  "David Ryan",
  "Simon Wright",
  "Mike Johnson",
  "Daniel Smith"
];

void main() => runApp(MyApp());

Color mainColor = Color(0xFF1577EC);
Color back_new = Color(0xFFF3E1D7);
Color back = Color(0xFFF6F6F6);
var deviceToken;
List albumList = [];
final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isLoggedIn = false;
  var userData;
  var email;
  var initData;
  int req = 0, chat = 0, notice = 0, comm = 0, repp = 0, stID;

  void initState() {
    //removeUser();
    firebaseCheck();
    _getUserInfo();
    _firebaseMessaging.getToken().then((token) async {
      print("Notification token");
      print(token);

      setState(() {
        deviceToken = token;
      });
    });

    super.initState();
  }

  Future<void> removeUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove("token");
    localStorage.remove("user");
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var userJson = localStorage.getString('user');

    if (token != null) {
      setState(() {
        _isLoggedIn = true;
      });
      getInitData(token);
    }

    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
        email = userData['email'];
      });
    }
  }

  Future<void> getInitData(token) async {
    var res = await CallApi().getData2('initData');
    var body = json.decode(res.body);

    setState(() {
      initData = body['user'];
      notifyNum = body['totalNotification'];
    });
    print("initData");
    print(initData);
  }

  void firebaseCheck() {
    _firebaseMessaging.getToken().then((token) {
      print("Notification token");
      print(token);
    });
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        setState(() {
          if (message['data']['type'] == "request" ||
              message['data']['type'] == "friend-request-accept") {
            frndNum += 1;
            notifyNum += 1;
          } else if (message['data']['type'] == "feed-like" ||
              message['data']['type'] == "feed-comment" ||
              message['data']['type'] == "feed-reply" ||
              message['data']['type'] == "feed-comment-like" ||
              message['data']['type'] == "feed-reply-like" ||
              message['data']['type'] == "feed-share") {
            notifyNum += 1;
          } else if (message['data']['msg'] != "" ||
              message['data']['msg'] != null) {
            chatUnseenNum += 1;
          }
        });
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        //pageRoute(message);
        setState(() async {
          if (message['data']['type'] == "request" ||
              message['data']['type'] == "friend-request-accept") {
            req = 1;
          } else if (message['data']['type'] == "feed-like" ||
              message['data']['type'] == "feed-comment" ||
              message['data']['type'] == "feed-reply" ||
              message['data']['type'] == "feed-comment-like" ||
              message['data']['type'] == "feed-reply-like" ||
              message['data']['type'] == "feed-share") {
            notice = 1;
            SharedPreferences localStorage =
                await SharedPreferences.getInstance();
            localStorage.setString('stid', message['data']['id']);
            stId = message['data']['id'];
          } else if (message['data']['msg'] != "" ||
              message['data']['msg'] != null) {
            //loadChatList(message['data']['con_id']);
          }
        });
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        setState(() {
          if (message['data']['type'] == "request" ||
              message['data']['type'] == "friend-request-accept") {
            frndNum += 1;
            notifyNum += 1;
          } else if (message['data']['type'] == "feed-like" ||
              message['data']['type'] == "feed-comment" ||
              message['data']['type'] == "feed-reply" ||
              message['data']['type'] == "feed-comment-like" ||
              message['data']['type'] == "feed-reply-like" ||
              message['data']['type'] == "feed-share") {
            notifyNum += 1;
          } else if (message['data']['msg'] != "" ||
              message['data']['msg'] != null) {
            chatUnseenNum += 1;
          }
        });
        //pageRoute(message);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Social App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: _isLoggedIn
          ? initData == null
              ? LoaderScreen()
              : initData['status'] == "Inactive"
                  ? ConfirmEmailPage(email)
                  : notice == 1
                      ? StatusDetailsPage(0, userData, feedList)
                      : HomePage()
          : LoginRegisterPage(),
    );
  }
}
