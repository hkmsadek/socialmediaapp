import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class FeedLoader extends StatefulWidget {
  @override
  _FeedLoaderState createState() => _FeedLoaderState();
}

class _FeedLoaderState extends State<FeedLoader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20, bottom: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        //border: Border.all(width: 0.8, color: Colors.grey[300]),
        // boxShadow: [
        //   BoxShadow(
        //     blurRadius: 1.0,
        //     color: Colors.black38,
        //     //offset: Offset(6.0, 7.0),
        //   ),
        // ],
      ),
      margin: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                //color: Colors.red,
                margin: EdgeInsets.only(left: 10, right: 10, top: 0),
                padding: EdgeInsets.only(right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                      padding: EdgeInsets.all(1.0),
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[100],
                        highlightColor: Colors.grey[200],
                        child: CircleAvatar(
                          radius: 20.0,
                          //backgroundColor: Colors.white,
                        ),
                      ),
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Shimmer.fromColors(
                          baseColor: Colors.grey[100],
                          highlightColor: Colors.grey[200],
                          child: Container(
                            width: 100,
                            height: 22,
                            child: Container(
                              color: Colors.black,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 3),
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[100],
                            highlightColor: Colors.grey[200],
                            child: Container(
                              width: 50,
                              height: 12,
                              child: Container(
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[100],
                    highlightColor: Colors.grey[200],
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      child: Container(
                        color: Colors.black,
                      ),
                    ),
                  )),
              Container(
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 2, bottom: 5),
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[100],
                    highlightColor: Colors.grey[200],
                    child: Container(
                      width: MediaQuery.of(context).size.width - 100,
                      height: 10,
                      child: Container(
                        color: Colors.black,
                      ),
                    ),
                  )),
            ],
          ),
          Container(
              height: 10,
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(5)),
              margin: EdgeInsets.only(left: 0, right: 0, bottom: 0, top: 7),
              child: null),
        ],
      ),
    );
  }
}
