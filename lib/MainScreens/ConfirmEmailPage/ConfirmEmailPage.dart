import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';

import '../../main.dart';

class ConfirmEmailPage extends StatefulWidget {
  final email;
  ConfirmEmailPage(this.email);

  @override
  _ConfirmEmailPageState createState() => _ConfirmEmailPageState();
}

class _ConfirmEmailPageState extends State<ConfirmEmailPage> {
  TextEditingController codeController = new TextEditingController();
  bool isSubmit = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Container(
            child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(top: 0, bottom: 10, left: 30),
                        padding: EdgeInsets.only(top: 1, bottom: 1),
                        child: Text(
                          "Confirm Email",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 22,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w500),
                        )),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(
                          top: 10, left: 30, right: 30, bottom: 20),
                      child: RichText(
                        textAlign: TextAlign.justify,
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                              text: "Let us know that email ",
                              style: TextStyle(
                                  //decoration: TextDecoration.underline,
                                  fontSize: 15,
                                  color: Colors.black,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w500),
                            ),
                            TextSpan(
                              text: "(${widget.email}) ",
                              style: TextStyle(
                                  //decoration: TextDecoration.underline,
                                  fontSize: 15,
                                  color: mainColor,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w500),
                            ),
                            TextSpan(
                              text:
                                  "belongs to you.We have send an activation email to ${widget.email}",
                              style: TextStyle(
                                  //decoration: TextDecoration.underline,
                                  fontSize: 15,
                                  color: Colors.black,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 5, top: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.all(0),
                              margin:
                                  EdgeInsets.only(left: 30, right: 30, top: 0),
                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.7),
                                  border: Border.all(
                                      color: Colors.grey, width: 0.2),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: TextField(
                                controller: codeController,
                                keyboardType: TextInputType.text,
                                style: TextStyle(
                                  color: Colors.black87,
                                  fontFamily: 'Oswald',
                                ),
                                decoration: InputDecoration(
                                  hintText: "Code",
                                  hintStyle: TextStyle(
                                      color: Colors.black38,
                                      fontSize: 15,
                                      fontFamily: 'Oswald',
                                      fontWeight: FontWeight.w300),
                                  //labelStyle: TextStyle(color: Colors.white70),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                                  border: InputBorder.none,
                                ),
                              )),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        sendCode();
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          alignment: Alignment.center,
                          padding:
                              EdgeInsets.only(left: 10, top: 15, bottom: 15),
                          margin: EdgeInsets.only(
                              left: 30, right: 30, top: 5, bottom: 20),
                          decoration: BoxDecoration(
                              color: isSubmit
                                  ? Colors.grey.withOpacity(0.5)
                                  : mainColor,
                              border:
                                  Border.all(color: Colors.grey, width: 0.2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: Text(
                            isSubmit ? "Please wait..." : "Continue",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w500),
                          )),
                    ),
                    GestureDetector(
                      onTap: () {
                        sendEmail();
                      },
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(
                                    left: 30, right: 30, top: 0, bottom: 20),
                                child: Text(
                                  "Resend Email",
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 15,
                                      fontFamily: 'Oswald',
                                      fontWeight: FontWeight.w500),
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ));
  }

  Future<void> sendCode() async {
    if (codeController.text.isEmpty) {
      _showMessage("Code field cannot be blank!", 1);
    } else {
      setState(() {
        isSubmit = true;
      });

      var data = {'code': codeController.text};

      print(data);

      var res = await CallApi().postData1(data, 'user/account/activate');
      var body = json.decode(res.body);
      print(body);

      if (res.statusCode == 200) {
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('user', json.encode(body['user']));

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomePage()));
        _showMessage("Email confirmed successfully!", 2);
      } else {
        _showMessage(body['messages'], 1);
      }

      setState(() {
        isSubmit = false;
      });
    }
  }

  Future<void> sendEmail() async {
    var data = {'email': widget.email};

    print(data);

    var res =
        await CallApi().postData1(data, 'user/account/activate/resend-email');
    //var body = json.decode(res.body);
    print(res.body);

    if (res.statusCode == 200) {
      // SharedPreferences localStorage = await SharedPreferences.getInstance();
      // localStorage.setString('user', json.encode(body['user']));
      _showMessage("Email sent successfully!", 2);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
