import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Cards/AllFriendsCard/allFriendsCard.dart';
import 'package:social_app_fb/Cards/AllRequestCard/AllRequestCard.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/ModelClass/FriendReqModel/FriendReqModel.dart';
import 'package:social_app_fb/main.dart';

class FriendRequestPage extends StatefulWidget {
  @override
  _FriendRequestPageState createState() => _FriendRequestPageState();
}

class _FriendRequestPageState extends State<FriendRequestPage> {
  String start = "";
  bool loading = true;
  var reqList;

  @override
  void initState() {
    loadRequests();
    super.initState();
  }

  Future loadRequests() async {
    setState(() {
      loading = true;
    });

    var reqresponse =
        await CallApi().getData('friend/get/all/request?last_id=$start');
    var reqcontent = reqresponse.body;
    final req = json.decode(reqcontent);
    var reqdata = FriendReqModel.fromJson(req);
    //print(reqdata);

    setState(() {
      reqList = reqdata;
      //page2 = 1;
    });

    //await Future.delayed(Duration(seconds: 3));

    setState(() {
      loading = false;
    });
    // print("reqList.pending.length");
    // print(reqList.pending.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? LoaderScreen()
          : Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/white.jpg"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: null,
                ),
                Container(
                  margin: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
                  child: CustomScrollView(
                    physics: BouncingScrollPhysics(),
                    slivers: <Widget>[
                      ////// <<<<< All Friend Option >>>>> //////
                      SliverToBoxAdapter(
                        child: Column(
                          children: <Widget>[
                            ////// <<<<< Friend Number >>>>> //////
                            Container(
                              margin:
                                  EdgeInsets.only(top: 12, left: 20, bottom: 7),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                      margin:
                                          EdgeInsets.only(right: 0, bottom: 3),
                                      child: Icon(Icons.group,
                                          size: 17, color: Colors.black54)),
                                  Container(
                                      margin: EdgeInsets.only(bottom: 3),
                                      child: Text(
                                        " Requests ",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontSize: 13,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w400),
                                      )),
                                  Container(
                                      child: Text(
                                    "${reqList.requests.length}",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 18,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w400),
                                  )),
                                ],
                              ),
                            ),

                            ////// <<<<< Divider 5 >>>>> //////
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 40,
                                  margin: EdgeInsets.only(
                                      top: 0, left: 20, bottom: 12),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      color: Colors.black54,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 3.0,
                                          color: Colors.black54,
                                          //offset: Offset(6.0, 7.0),
                                        ),
                                      ],
                                      border: Border.all(
                                          width: 0.5, color: Colors.black54)),
                                ),
                              ],
                            ),
                            reqList.requests.length == 0
                                ? Row(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(left: 20),
                                        child: Text(
                                          "No Friend Requests!",
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ],
                                  )
                                : Container()
                          ],
                        ),
                      ),

                      ////// <<<<< All Members Card >>>>> //////
                      AllRequestCard(reqList.requests),
                    ],
                  ),
                ),
              ],
            ),
    );
  }
}
