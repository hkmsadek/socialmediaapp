import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
//import 'package:image_cropper/image_cropper.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Cards/MyProfileFeedCard/MyProfileFeedCard.dart';
import 'package:social_app_fb/Loader/FeedLoader/FeedLoader.dart';
import 'package:social_app_fb/MainScreens/AboutPage/AboutPage.dart';
import 'package:social_app_fb/MainScreens/CommentPage/CommentPage.dart';
import 'package:social_app_fb/MainScreens/CreatePostPage/CreatePostPage.dart';
import 'package:social_app_fb/MainScreens/EditPostPage/EditPostPage.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/FriendsPage/FriendsPage.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/MainScreens/PhotosPage/PhotoPage.dart';
import 'package:social_app_fb/MainScreens/StatusDetailsPage/StatusDetailsPage.dart';
import 'package:social_app_fb/ModelClass/FeedModel/FeedModel.dart';
import 'package:social_app_fb/ModelClass/InfoModel/InfoModel.dart';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:social_app_fb/main.dart';

List profileComCount = [];

class ProfileScreen extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

enum PhotoCrop {
  free,
  picked,
  cropped,
}

class _ProfilePageState extends State<ProfileScreen> {
  SharedPreferences sharedPreferences;
  String theme = "";
  String date = "", sharePost = "";
  Timer _timer;
  int _start = 3;
  bool loading = true;
  PhotoCrop state;
  File bannerFile;
  File imageFile;
  var userData, basicInfo;
  bool timeline = true;
  bool about = false;
  bool friends = false;
  bool photos = false;
  var postData, postList;
  bool _isLoading = true;
  int lastID = 1;
  List feedList = [];
  ScrollController _controller = new ScrollController();
  TextEditingController shareController = new TextEditingController();

  @override
  void initState() {
    setState(() {
      profileComCount.clear();
      profileFeedList.clear();
      work.clear();
      education.clear();
      school.clear();
      skill.clear();
      locationCur.clear();
      locationHome.clear();
      locationOther.clear();
    });
    _getUserInfo();
    state = PhotoCrop.free;
    // WidgetsBinding.instance.addPostFrameCallback((_) async {
    //   await showDialogBox();
    // });
    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels == 0) {
          setState(() {
            print("top");
          });
        }
        // you are at top position

        else {
          setState(() {
            print("bottom");
            loadPosts(lastID);

            print("lastID 1");
            print(lastID);
          });
        }
        // you are at bottom position
      }
    });
    super.initState();
  }

  Future loadPosts(int number) async {
    var postresponse = await CallApi()
        .getData('profile/details/${userData['userName']}?tab=timeline');

    setState(() {
      var postcontent = postresponse.body;
      final posts = json.decode(postcontent);
      var postdata = FeedModel.fromJson(posts);
      postList = postdata;

      for (int i = 0; i < postList.feed.length; i++) {
        //print(postList.feed[i].id);

        profileFeedList.add(postList.feed[i]);
        print(profileFeedList[i].meta.totalCommentsCount);
        profileComCount
            .add({'count': postList.feed[i].meta.totalCommentsCount});
        lastID = postList.feed[i].id;
        print(lastID);
      }

      print(feedList);
      print(profileComCount);
    });

    setState(() {
      _isLoading = false;
    });
    //print(page1);
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
      getBasicInfo(userData['userName']);
    }

    loadPosts(1);
  }

  Future getBasicInfo(userName) async {
    //await Future.delayed(Duration(seconds: 3));
    var postresponse = await CallApi().getData2('profile/' + userName);
    var postcontent = postresponse.body;
    final body = json.decode(postcontent);
    var postdata = InfoModel.fromJson(body);

    setState(() {
      basicInfo = postdata;
    });

    for (int i = 0; i < basicInfo.user.abouts.workData.work.length; i++) {
      setState(() {
        work.add({
          'company': basicInfo.user.abouts.workData.work[i].company,
          'position': basicInfo.user.abouts.workData.work[i].position,
          'city': basicInfo.user.abouts.workData.work[i].city,
          'joinDate': basicInfo.user.abouts.workData.work[i].joinDate,
          'leaveDate': basicInfo.user.abouts.workData.work[i].leaveDate,
          'currentlyWorking':
              basicInfo.user.abouts.workData.work[i].currentlyWorking,
          'description': basicInfo.user.abouts.workData.work[i].description,
          'privacy': basicInfo.user.abouts.workData.work[i].privacy,
          'isEdit': basicInfo.user.abouts.workData.work[i].isEdit,
        });
      });
    }

    for (int i = 0;
        i < basicInfo.user.abouts.educationData.collages.length;
        i++) {
      setState(() {
        education.add({
          'collage': basicInfo.user.abouts.educationData.collages[i].collage,
          'degree': basicInfo.user.abouts.educationData.collages[i].degree,
          'city': basicInfo.user.abouts.educationData.collages[i].city,
          'startDate':
              basicInfo.user.abouts.educationData.collages[i].startDate,
          'endDate': basicInfo.user.abouts.educationData.collages[i].endDate,
          'description':
              basicInfo.user.abouts.educationData.collages[i].description,
          'privacy': basicInfo.user.abouts.educationData.collages[i].privacy,
        });
      });
    }

    for (int i = 0;
        i < basicInfo.user.abouts.educationData.highSchools.length;
        i++) {
      setState(() {
        school.add({
          'collage': basicInfo.user.abouts.educationData.highSchools[i].collage,
          'city': basicInfo.user.abouts.educationData.highSchools[i].city,
          'startDate':
              basicInfo.user.abouts.educationData.highSchools[i].startDate,
          'endDate': basicInfo.user.abouts.educationData.highSchools[i].endDate,
          'description':
              basicInfo.user.abouts.educationData.highSchools[i].description,
          'privacy': basicInfo.user.abouts.educationData.highSchools[i].privacy,
        });
      });
    }

    locationCur.add({
      'city': basicInfo.user.abouts.locationData.currentCity.city,
      'movedDate': basicInfo.user.abouts.locationData.currentCity.movedDate,
      'description': basicInfo.user.abouts.locationData.currentCity.description,
      'isEdit': basicInfo.user.abouts.locationData.currentCity.isEdit,
    });

    locationHome.add({
      'homeTown': basicInfo.user.abouts.locationData.homeTown.homeTown,
      'description': basicInfo.user.abouts.locationData.homeTown.description,
      'isEdit': basicInfo.user.abouts.locationData.homeTown.isEdit,
    });

    for (int i = 0;
        i < basicInfo.user.abouts.locationData.otherPlaces.length;
        i++) {
      setState(() {
        locationOther.add({
          'city': basicInfo.user.abouts.locationData.otherPlaces[i].city,
          'movedDate':
              basicInfo.user.abouts.locationData.otherPlaces[i].movedDate,
          'leftDate':
              basicInfo.user.abouts.locationData.otherPlaces[i].leftDate,
          'description':
              basicInfo.user.abouts.locationData.otherPlaces[i].description,
          'isEdit': basicInfo.user.abouts.locationData.otherPlaces[i].isEdit,
        });
      });
    }

    for (int i = 0; i < basicInfo.user.abouts.workData.proSkills.length; i++) {
      setState(() {
        skill.add(basicInfo.user.abouts.workData.proSkills[i]);
      });
    }

    print("skill");
    print(skill);
  }

  _pickImage() async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    uploadImage();
  }

  _pickBanner(ImageSource src) async {
    bannerFile = await ImagePicker.pickImage(source: src);
    uploadCover();
  }

  // Future<Null> _cropImage() async {
  //   File croppedFile = await ImageCropper.cropImage(
  //     sourcePath: imageFile.path,
  //     // toolbarTitle: 'Cropper',
  //     // toolbarColor: Colors.black.withOpacity(0.5),
  //     // toolbarWidgetColor: Colors.white,
  //   );
  //   if (croppedFile != null) {
  //     imageFile = croppedFile;
  //     setState(() {
  //       // state = PhotoCrop.free;
  //       state = PhotoCrop.cropped;
  //     });
  //   }
  // }

  Future uploadImage() async {
    List<int> imageBytes = imageFile.readAsBytesSync();
    String dp = base64.encode(imageBytes);
    dp = 'data:image/png;base64,' + dp;

    var data = {'type': "profilePicture", 'image': dp};
    print(data);

    var res1 = await CallApi().postData1(data, 'upload/profile');
    var body1 = json.decode(res1.body);
    print(body1);

    // setState(() {
    //   state1 = PhotoCrop.free;
    // });

    if (res1.statusCode == 200) {
      _showMessage("Profile picture uploaded successfully!", 2);
    } else {
      _showMessage("Failed to upload profile picture!", 1);
    }
  }

  Future uploadCover() async {
    List<int> imageBytes = bannerFile.readAsBytesSync();
    String dp = base64.encode(imageBytes);
    dp = 'data:image/png;base64,' + dp;

    var data = {'type': "coverPicture", 'image': dp};
    print(data);

    var res1 = await CallApi().postData1(data, 'upload/profile');
    var body1 = json.decode(res1.body);
    print(body1);

    // setState(() {
    //   state1 = PhotoCrop.free;
    // });

    if (res1.statusCode == 200) {
      _showMessage("Cover picture uploaded successfully!", 2);
    } else {
      _showMessage("Failed to upload cover picture!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }

  Future<Null> showDialogBox() async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            content: SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  Container(
                    color: Colors.transparent,
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(20),
                        margin: EdgeInsets.only(top: 35),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.only(top: 30),
                                child: Text(
                                  "Complete your profile info",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontFamily: 'Oswald',
                                      fontWeight: FontWeight.w400),
                                )),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: new LinearPercentIndicator(
                                animation: true,
                                animationDuration: 1000,
                                lineHeight: 15.0,
                                // leading: new Text("left content"),
                                trailing: Text(
                                  " 45.0%",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 10,
                                      fontFamily: 'Oswald',
                                      fontWeight: FontWeight.w400),
                                ),
                                backgroundColor: back,
                                percent: 0.45,
                                // center: Text("20.0%",style: TextStyle(
                                //     color: Colors.black,
                                //     fontSize: 10,
                                //     fontFamily: 'Oswald',
                                //     fontWeight: FontWeight.w400),),
                                //linearStrokeCap: LinearStrokeCap.butt,
                                progressColor: mainColor.withOpacity(0.7),
                              ),
                            ),
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          Navigator.of(context).pop();
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      AboutPage()));
                                        });
                                      },
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.only(
                                              left: 0,
                                              right: 0,
                                              top: 20,
                                              bottom: 0),
                                          decoration: BoxDecoration(
                                              color: mainColor,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Text(
                                            "Go to 'About' to complete the profile",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12,
                                              fontFamily: 'BebasNeue',
                                            ),
                                            textAlign: TextAlign.center,
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                    left: 0, top: 15, bottom: 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    FeedPage()));
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              "Skip to 'Feed'",
                                              style: TextStyle(
                                                  //decoration: TextDecoration.underline,
                                                  color: mainColor,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                            Icon(Icons.chevron_right,
                                                color: mainColor, size: 16)
                                          ],
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Container(
                                        child: Text(
                                          "Cancel",
                                          style: TextStyle(
                                              //decoration: TextDecoration.underline,
                                              color: Colors.black54,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.center,
                    //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                    padding: EdgeInsets.all(5.0),
                    child: CircleAvatar(
                      radius: 30.0,
                      backgroundColor: Colors.transparent,
                      backgroundImage: AssetImage('assets/images/prabal.jpg'),
                    ),
                    decoration: new BoxDecoration(
                      color: Colors.white, // border color
                      shape: BoxShape.circle,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black54),
          //automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          title: Container(
            margin: EdgeInsets.only(top: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 5, right: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      //color: Colors.black.withOpacity(0.5),
                    ),
                    child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Profile",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 20,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.normal),
                        )),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[],
        ),
        body: userData == null
            ? LoaderScreen()
            : Stack(
                children: <Widget>[
                  Container(
                    child: CustomScrollView(
                      physics: BouncingScrollPhysics(),
                      slivers: <Widget>[
                        SliverToBoxAdapter(
                            child: SingleChildScrollView(
                          child: Container(
                              child: Column(
                            children: <Widget>[
                              SafeArea(
                                child: Stack(
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        bannerFile != null
                                            ? Container(
                                                height: 150,
                                                padding:
                                                    const EdgeInsets.all(0.0),
                                                decoration: BoxDecoration(
                                                    //color: header,

                                                    image: DecorationImage(
                                                        image: FileImage(
                                                            bannerFile),
                                                        fit: BoxFit.cover)),
                                                child: null)
                                            : Container(
                                                height: 150,
                                                padding:
                                                    const EdgeInsets.all(0.0),
                                                decoration: BoxDecoration(
                                                    //color: header,

                                                    image: DecorationImage(
                                                        image: AssetImage(
                                                            'assets/images/cover.jpg'),
                                                        fit: BoxFit.cover)),
                                                child: null),
                                        Container(
                                            height: 150,
                                            padding: const EdgeInsets.all(0.0),
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.black.withOpacity(0.2),
                                            ),
                                            child: null),
                                        GestureDetector(
                                          onTap: () {
                                            _pickBanner(ImageSource.gallery);
                                          },
                                          child: Container(
                                            height: 135,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                                right: 0, top: 15),
                                            alignment: Alignment.topRight,
                                            child: Container(
                                                decoration: BoxDecoration(
                                                    color: back,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5)),
                                                padding: EdgeInsets.all(4),
                                                margin:
                                                    EdgeInsets.only(right: 15),
                                                child: Icon(
                                                  Icons.photo_camera,
                                                  color: Colors.black
                                                      .withOpacity(0.5),
                                                  size: 20,
                                                )),
                                          ),
                                        ),
                                        Stack(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              margin: EdgeInsets.only(
                                                  right: 0, top: 110),
                                              alignment: Alignment.bottomCenter,
                                              child: Container(
                                                height: 50,
                                                padding:
                                                    const EdgeInsets.all(0.0),
                                                decoration: BoxDecoration(
                                                  color: Colors.black
                                                      .withOpacity(0.5),
                                                ),
                                              ),
                                            ),
                                            Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                padding: EdgeInsets.all(5),
                                                margin: EdgeInsets.only(
                                                    left: 125, top: 115),
                                                child: Text(
                                                  "${userData['firstName']} ${userData['lastName']}",
                                                  textAlign: TextAlign.start,
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: "Oswald",
                                                      color: Colors.white),
                                                )),
                                          ],
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 155, left: 125),
                                            padding: EdgeInsets.all(5),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Container(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      Icon(Icons.edit,
                                                          color: mainColor,
                                                          size: 15),
                                                      SizedBox(width: 5),
                                                      Text(
                                                        "Edit Profile",
                                                        style: TextStyle(
                                                            color: mainColor,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontFamily:
                                                                "Oswald"),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            )),
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        _pickImage();
                                      },
                                      child: Container(
                                        child: Stack(
                                          children: <Widget>[
                                            state == PhotoCrop.free
                                                ? Container(
                                                    margin: EdgeInsets.only(
                                                        left: 20, top: 100),
                                                    //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                    padding:
                                                        EdgeInsets.all(5.0),
                                                    child: CircleAvatar(
                                                      radius: 45.0,
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      backgroundImage: AssetImage(
                                                          'assets/images/user.png'),
                                                    ),
                                                    decoration:
                                                        new BoxDecoration(
                                                      color: Colors.grey[
                                                          300], // border color
                                                      shape: BoxShape.circle,
                                                    ),
                                                  )
                                                : (state == PhotoCrop.picked ||
                                                        state ==
                                                            PhotoCrop.cropped)
                                                    ? Container(
                                                        margin: EdgeInsets.only(
                                                            left: 20, top: 100),
                                                        //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                        padding:
                                                            EdgeInsets.all(5.0),
                                                        child: CircleAvatar(
                                                            radius: 45.0,
                                                            backgroundColor:
                                                                Colors
                                                                    .transparent,
                                                            backgroundImage:
                                                                FileImage(
                                                                    imageFile)),
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: Colors.grey[
                                                              300], // border color
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                      )
                                                    : Container(
                                                        margin: EdgeInsets.only(
                                                            left: 20, top: 100),
                                                        //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                        padding:
                                                            EdgeInsets.all(5.0),
                                                        child: CircleAvatar(
                                                          radius: 45.0,
                                                          backgroundColor:
                                                              Colors
                                                                  .transparent,
                                                          backgroundImage:
                                                              AssetImage(
                                                                  'assets/images/user.png'),
                                                        ),
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: Colors.grey[
                                                              300], // border color
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                      ),
                                            Container(
                                                decoration: BoxDecoration(
                                                    color: back,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25),
                                                    border: Border.all(
                                                        color: Colors.grey
                                                            .withOpacity(0.5))),
                                                padding: EdgeInsets.all(5),
                                                margin: EdgeInsets.only(
                                                    left: 85, top: 170),
                                                child: Icon(
                                                  Icons.photo_camera,
                                                  color: Colors.black
                                                      .withOpacity(0.5),
                                                  size: 20,
                                                )),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              state == PhotoCrop.picked
                                  ? Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.only(right: 0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                GestureDetector(
                                                  onTap: () {
                                                    //_cropImage();
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        //shape: BoxShape.circle,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                        color: Colors.grey
                                                            .withOpacity(0.2)),
                                                    padding: EdgeInsets.only(
                                                        top: 5,
                                                        bottom: 5,
                                                        left: 10,
                                                        right: 10),
                                                    margin: EdgeInsets.only(
                                                        right: 5,
                                                        top: 20,
                                                        bottom: 10),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Icon(Icons.crop,
                                                            size: 15,
                                                            color:
                                                                Colors.black87),
                                                        Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 5),
                                                            child: Text("Crop",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black54,
                                                                    fontFamily:
                                                                        "Oswald"))),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    setState(() {
                                                      state = PhotoCrop.cropped;
                                                      uploadImage();
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        //shape: BoxShape.circle,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                        color: mainColor),
                                                    padding: EdgeInsets.only(
                                                        top: 5,
                                                        bottom: 5,
                                                        left: 10,
                                                        right: 10),
                                                    margin: EdgeInsets.only(
                                                        left: 5,
                                                        top: 20,
                                                        bottom: 10),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Icon(Icons.done,
                                                            size: 15,
                                                            color:
                                                                Colors.white),
                                                        Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 5),
                                                            child: Text("Done",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontFamily:
                                                                        "Oswald"))),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  : Container(),

                              Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          showDialogBox();
                                          // setState(() {
                                          //   timeline = true;
                                          //   about = false;
                                          //   friends = false;
                                          //   photos = false;
                                          // });
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 15, top: 15, right: 5),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              color: timeline
                                                  ? mainColor.withOpacity(0.8)
                                                  : back.withOpacity(0.3),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: timeline
                                                      ? mainColor
                                                      : Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.show_chart,
                                                color: timeline
                                                    ? Colors.white
                                                    : Colors.black,
                                                size: 15,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  "Timeline",
                                                  style: TextStyle(
                                                      color: timeline
                                                          ? Colors.white
                                                          : Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          // setState(() {
                                          //   timeline = false;
                                          //   about = true;
                                          //   friends = false;
                                          //   photos = false;
                                          // });
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      AboutPage()));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5, top: 15),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              color: about
                                                  ? mainColor.withOpacity(0.8)
                                                  : back.withOpacity(0.3),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: about
                                                      ? mainColor
                                                      : Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.info_outline,
                                                color: about
                                                    ? Colors.white
                                                    : Colors.black,
                                                size: 15,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  "About",
                                                  style: TextStyle(
                                                      color: about
                                                          ? Colors.white
                                                          : Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      FriendsPage(userData[
                                                          'userName'])));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 15, top: 15),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              color: back.withOpacity(0.3),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.group,
                                                color: Colors.black,
                                                size: 17,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  "Friends",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      PhotoPage()));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 15, top: 10, right: 5),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              color: back.withOpacity(0.3),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.photo,
                                                color: Colors.black,
                                                size: 15,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  "Photos",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 5, right: 5, top: 10),
                                        padding: EdgeInsets.all(5.0),
                                        decoration: new BoxDecoration(
                                            color: back.withOpacity(0.3),
                                            border: Border.all(
                                                width: 0.5,
                                                color: Colors.white),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(
                                              Icons.event_available,
                                              color: Colors.black,
                                              size: 15,
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 5),
                                              child: Text(
                                                "Events",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: "Oswald",
                                                    fontWeight: FontWeight.w300,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 5, right: 15, top: 10),
                                        padding: EdgeInsets.all(5.0),
                                        decoration: new BoxDecoration(
                                            color: back.withOpacity(0.3),
                                            border: Border.all(
                                                width: 0.5,
                                                color: Colors.white),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(
                                              Icons.more_horiz,
                                              color: Colors.black,
                                              size: 17,
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 5),
                                              child: Text(
                                                "More",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: "Oswald",
                                                    fontWeight: FontWeight.w300,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              // Container(
                              //     width: 50,
                              //     margin: EdgeInsets.only(
                              //         top: 25, left: 25, right: 25, bottom: 0),
                              //     decoration: BoxDecoration(
                              //         borderRadius:
                              //             BorderRadius.all(Radius.circular(15.0)),
                              //         color: mainColor,
                              //         boxShadow: [
                              //           BoxShadow(
                              //             blurRadius: 1.0,
                              //             color: mainColor,
                              //             //offset: Offset(6.0, 7.0),
                              //           ),
                              //         ],
                              //         border:
                              //             Border.all(width: 0.5, color: mainColor))),

                              work.length == 0
                                  ? Container()
                                  : Container(
                                      margin: EdgeInsets.only(top: 20),
                                      child: Column(
                                          children: List.generate(work.length,
                                              (index) {
                                        return Container(
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(Icons.work,
                                                      size: 17,
                                                      color: Colors.black54)),
                                              Expanded(
                                                child: Text.rich(
                                                  TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text: work[index][
                                                                      'currentlyWorking'] ==
                                                                  false
                                                              ? "Former ${work[index]['position']} at"
                                                              : "${work[index]['position']} at",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              " ${work[index]['company']}",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                      // can add more TextSpans here...
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })),
                                    ),

                              education.length == 0
                                  ? Container()
                                  : Container(
                                      child: Column(
                                          children: List.generate(
                                              education.length, (index) {
                                        return Container(
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(Icons.book,
                                                      size: 17,
                                                      color: Colors.black54)),
                                              Expanded(
                                                child: Text.rich(
                                                  TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text: "Studied ",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              "${education[index]['degree']} at",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              " ${education[index]['collage']}",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                      // can add more TextSpans here...
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })),
                                    ),
                              school.length == 0
                                  ? Container()
                                  : Container(
                                      child: Column(
                                          children: List.generate(school.length,
                                              (index) {
                                        return Container(
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(Icons.book,
                                                      size: 17,
                                                      color: Colors.black54)),
                                              Expanded(
                                                child: Text.rich(
                                                  TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text: "Went to",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              " ${school[index]['collage']}",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                      // can add more TextSpans here...
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })),
                                    ),
                              locationCur == null
                                  ? Container()
                                  : Container(
                                      child: Column(
                                          children: List.generate(
                                              locationCur.length, (index) {
                                        return Container(
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(Icons.location_on,
                                                      size: 17,
                                                      color: Colors.black54)),
                                              Expanded(
                                                child: Text.rich(
                                                  TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text: "Lives in ",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              "${locationCur[index]['city']}",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                      // can add more TextSpans here...
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })),
                                    ),

                              Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                //color: sub_white,
                                child: Container(
                                  //color: Colors.white,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                          width: 50,
                                          margin: EdgeInsets.only(
                                              top: 20,
                                              left: 25,
                                              right: 25,
                                              bottom: 10),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(15.0)),
                                              color: mainColor,
                                              boxShadow: [
                                                BoxShadow(
                                                  blurRadius: 1.0,
                                                  color: mainColor,
                                                  //offset: Offset(6.0, 7.0),
                                                ),
                                              ],
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: mainColor))),
                                      Column(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.centerLeft,
                                              margin: EdgeInsets.only(
                                                  top: 15, left: 20),
                                              child: Text(
                                                "Mutual Friends",
                                                style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: 20,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.normal),
                                              )),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                width: 30,
                                                margin: EdgeInsets.only(
                                                    top: 10, left: 20),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                15.0)),
                                                    color: Colors.black54,
                                                    boxShadow: [
                                                      BoxShadow(
                                                        blurRadius: 3.0,
                                                        color: Colors.black54,
                                                        //offset: Offset(6.0, 7.0),
                                                      ),
                                                    ],
                                                    border: Border.all(
                                                        width: 0.5,
                                                        color: Colors.black54)),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  margin: EdgeInsets.only(
                                                      top: 12,
                                                      left: 20,
                                                      bottom: 0),
                                                  child: Text(
                                                    "3 friends",
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        color: Colors.black45,
                                                        fontSize: 13,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  )),
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              FriendsPage(userData[
                                                                  'userName'])));
                                                },
                                                child: Container(
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    margin: EdgeInsets.only(
                                                        top: 12,
                                                        right: 20,
                                                        bottom: 0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Text(
                                                          "See all",
                                                          textAlign:
                                                              TextAlign.start,
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black45,
                                                              fontSize: 13,
                                                              fontFamily:
                                                                  'Oswald',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 3),
                                                          child: Icon(
                                                              Icons
                                                                  .chevron_right,
                                                              color: Colors
                                                                  .black45,
                                                              size: 17),
                                                        )
                                                      ],
                                                    )),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                                top: 10, left: 10),
                                            child: Wrap(
                                              //mainAxisAlignment: MainAxisAlignment.start,
                                              children: List.generate(
                                                  loading ? 1 : 3, (int index) {
                                                return GestureDetector(
                                                  onTap: () {
                                                    // Navigator.push(
                                                    //   context,
                                                    //   MaterialPageRoute(
                                                    //       builder: (context) => HotelSearchPage()),
                                                    // );
                                                  },
                                                  child: Container(
                                                    child: Column(
                                                      children: <Widget>[
                                                        new Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10,
                                                                  right: 10,
                                                                  top: 5,
                                                                  bottom: 10),
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 0),
                                                          height: 50,
                                                          width: 50,
                                                          decoration:
                                                              BoxDecoration(
                                                            image:
                                                                DecorationImage(
                                                              image: index == 0
                                                                  ? AssetImage(
                                                                      'assets/images/man.png')
                                                                  : index == 1
                                                                      ? AssetImage(
                                                                          'assets/images/man4.jpg')
                                                                      : AssetImage(
                                                                          'assets/images/man2.png'),
                                                              fit: BoxFit.cover,
                                                            ),
                                                            borderRadius:
                                                                BorderRadius.all(
                                                                    Radius.circular(
                                                                        100.0)),
                                                            color: Colors.white,
                                                            boxShadow: [
                                                              BoxShadow(
                                                                blurRadius: 2.0,
                                                                color: Colors
                                                                    .black
                                                                    .withOpacity(
                                                                        .5),
                                                                //offset: Offset(6.0, 7.0),
                                                              ),
                                                            ],
                                                            // border: Border.all(width: 0.2, color: Colors.grey)
                                                          ),
                                                        ),
                                                        Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 0,
                                                                    left: 0,
                                                                    bottom: 5),
                                                            child: Text(
                                                              index == 0
                                                                  ? "John Louis"
                                                                  : index == 1
                                                                      ? "David King"
                                                                      : "Daniel Ryan",
                                                              maxLines: 2,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize: 10,
                                                                  fontFamily:
                                                                      'Oswald',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400),
                                                            )),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              }),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )),
                        )),
                        SliverToBoxAdapter(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  width: 50,
                                  margin: EdgeInsets.only(
                                      top: 10, left: 25, right: 25, bottom: 20),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      color: mainColor,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 1.0,
                                          color: mainColor,
                                          //offset: Offset(6.0, 7.0),
                                        ),
                                      ],
                                      border: Border.all(
                                          width: 0.5, color: mainColor))),
                            ],
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: Column(
                            children: <Widget>[
                              Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 0, left: 20),
                                  child: Text(
                                    "Post",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.normal),
                                  )),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: 30,
                                    margin: EdgeInsets.only(
                                        top: 10, left: 20, bottom: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(15.0)),
                                        color: Colors.black54,
                                        boxShadow: [
                                          BoxShadow(
                                            blurRadius: 3.0,
                                            color: Colors.black54,
                                            //offset: Offset(6.0, 7.0),
                                          ),
                                        ],
                                        border: Border.all(
                                            width: 0.5, color: Colors.black54)),
                                  ),
                                ],
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              CreatePostPage(userData, 2)));
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      top: 0, left: 5, right: 5),
                                  padding: EdgeInsets.only(top: 0, bottom: 0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    // boxShadow: [
                                    //   BoxShadow(
                                    //     blurRadius: 0.2,
                                    //     color: Colors.black38,
                                    //     //offset: Offset(6.0, 7.0),
                                    //   ),
                                    // ],
                                  ),
                                  child: Container(
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          child: Row(
                                            children: <Widget>[
                                              ////// <<<<< Profile >>>>> //////
                                              GestureDetector(
                                                onTap: () {
                                                  // Navigator.push(
                                                  //     context,
                                                  //     MaterialPageRoute(
                                                  //         builder: (context) =>
                                                  //             //FriendsProfilePage("prabal23")),
                                                  //             MyProfilePage(userData)));
                                                },
                                                child: Container(
                                                  child: Stack(
                                                    children: <Widget>[
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            right: 0, left: 15),
                                                        padding:
                                                            EdgeInsets.all(1.0),
                                                        child: CircleAvatar(
                                                          radius: 21.0,
                                                          backgroundColor:
                                                              Colors
                                                                  .transparent,
                                                          backgroundImage:
                                                              AssetImage(
                                                                  'assets/images/user.png'),
                                                        ),
                                                        // decoration: new BoxDecoration(
                                                        //   color: Colors.grey[300],
                                                        //   shape: BoxShape.circle,
                                                        // ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 45),
                                                        padding:
                                                            EdgeInsets.all(1.0),
                                                        child: CircleAvatar(
                                                          radius: 5.0,
                                                          backgroundColor:
                                                              Colors
                                                                  .greenAccent,
                                                        ),
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: Colors
                                                              .greenAccent,
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),

                                              ////// <<<<< Status/Photo field >>>>> //////
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, top: 4),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    //mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      Container(
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 5,
                                                                  right: 5),
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 0,
                                                                  right: 10,
                                                                  top: 5),
                                                          // decoration: BoxDecoration(
                                                          //     color: Colors.grey[100],
                                                          //     border: Border.all(
                                                          //         color: Colors.grey[200],
                                                          //         width: 0.5),
                                                          //     borderRadius: BorderRadius.all(
                                                          //         Radius.circular(25))),
                                                          child: TextField(
                                                            enabled: false,
                                                            //controller: phoneController,
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  'Oswald',
                                                            ),
                                                            decoration:
                                                                InputDecoration(
                                                              hintText:
                                                                  "What's in your mind? ${userData['firstName']}",
                                                              hintStyle: TextStyle(
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize: 15,
                                                                  fontFamily:
                                                                      'Oswald',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              //labelStyle: TextStyle(color: Colors.white70),
                                                              contentPadding:
                                                                  EdgeInsets
                                                                      .fromLTRB(
                                                                          10.0,
                                                                          1,
                                                                          20.0,
                                                                          1),
                                                              border:
                                                                  InputBorder
                                                                      .none,
                                                            ),
                                                          )),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                left: 15,
                                                right: 15,
                                                top: 0,
                                                bottom: 0),
                                            child: Divider()),
                                        SingleChildScrollView(
                                          physics: BouncingScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 15),
                                            decoration: BoxDecoration(
                                                //border:Border.all(color:Colors.grey, width:0.3)
                                                ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 15,
                                                      right: 5,
                                                      top: 5,
                                                      bottom: 0),
                                                  padding: EdgeInsets.all(5.0),
                                                  decoration: new BoxDecoration(
                                                      // color: Colors.grey
                                                      //     .withOpacity(0.1),
                                                      border: Border.all(
                                                          width: 0.5,
                                                          color: Colors.white),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.photo,
                                                        color: Colors.redAccent
                                                            .withOpacity(0.7),
                                                        size: 15,
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                          "Photo/Video",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 14),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                    height: 20,
                                                    child: VerticalDivider()),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 5,
                                                      right: 5,
                                                      top: 5,
                                                      bottom: 0),
                                                  padding: EdgeInsets.all(5.0),
                                                  decoration: new BoxDecoration(
                                                      // color: Colors.grey
                                                      //     .withOpacity(0.1),
                                                      border: Border.all(
                                                          width: 0.5,
                                                          color: Colors.white),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.mood,
                                                        color:
                                                            Color(0xFFECBF00),
                                                        size: 17,
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                          "Feeling/Activity",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 14),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                    height: 20,
                                                    child: VerticalDivider()),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 5,
                                                      right: 15,
                                                      top: 5,
                                                      bottom: 0),
                                                  padding: EdgeInsets.all(5.0),
                                                  decoration: new BoxDecoration(
                                                      // color: Colors.grey
                                                      //     .withOpacity(0.1),
                                                      border: Border.all(
                                                          width: 0.5,
                                                          color: Colors.white),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.more_horiz,
                                                        color: Colors.black,
                                                        size: 17,
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                          "More",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 14),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        // Container(
                                        //   child: Row(
                                        //     mainAxisAlignment: MainAxisAlignment.end,
                                        //     children: <Widget>[
                                        //       Container(
                                        //         margin: EdgeInsets.only(
                                        //             left: 15,
                                        //             top: 35,
                                        //             right: 15,
                                        //             bottom: 15),
                                        //         padding: EdgeInsets.all(5.0),
                                        //         decoration: new BoxDecoration(
                                        //             color: mainColor.withOpacity(0.8),
                                        //             border: Border.all(
                                        //                 width: 0.5, color: mainColor),
                                        //             borderRadius: BorderRadius.all(
                                        //                 Radius.circular(5))),
                                        //         child: Row(
                                        //           mainAxisAlignment:
                                        //               MainAxisAlignment.center,
                                        //           children: <Widget>[
                                        //             Container(
                                        //               margin: EdgeInsets.only(left: 5),
                                        //               child: Text(
                                        //                 "Create Post",
                                        //                 style: TextStyle(
                                        //                     color: Colors.white,
                                        //                     fontFamily: "Oswald",
                                        //                     fontWeight: FontWeight.w300,
                                        //                     fontSize: 14),
                                        //               ),
                                        //             ),
                                        //           ],
                                        //         ),
                                        //       ),
                                        //     ],
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                  width: 50,
                                  margin: EdgeInsets.only(
                                      top: 20, left: 25, right: 25, bottom: 0),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      color: mainColor,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 1.0,
                                          color: mainColor,
                                          //offset: Offset(6.0, 7.0),
                                        ),
                                      ],
                                      border: Border.all(
                                          width: 0.5, color: mainColor))),
                              Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 15, left: 20),
                                  child: Text(
                                    "Timeline",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.normal),
                                  )),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: 30,
                                    margin: EdgeInsets.only(
                                        top: 10, left: 20, bottom: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(15.0)),
                                        color: Colors.black54,
                                        boxShadow: [
                                          BoxShadow(
                                            blurRadius: 3.0,
                                            color: Colors.black54,
                                            //offset: Offset(6.0, 7.0),
                                          ),
                                        ],
                                        border: Border.all(
                                            width: 0.5, color: Colors.black54)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                            DateTime dateTime = DateTime.parse(
                                profileFeedList[index].createdAt);
                            date = DateFormat.yMMMd().format(dateTime);
                            List img = [];

                            if (profileFeedList[index].data.images != null) {
                              for (int i = 0;
                                  i < profileFeedList[index].data.images.length;
                                  i++) {
                                img.add({
                                  'id':
                                      profileFeedList[index].data.images[i].id,
                                  'url':
                                      profileFeedList[index].data.images[i].url,
                                });
                              }
                            }
                            return Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                StatusDetailsPage(
                                                    index,
                                                    userData,
                                                    profileFeedList[index]
                                                        .id)));
                                  },
                                  child: Container(
                                    child: _isLoading == false
                                        ? Container(
                                            padding: EdgeInsets.only(
                                                top: 0, bottom: 0),
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              //border: Border.all(width: 0.8, color: Colors.grey[300]),
                                              // boxShadow: [
                                              //   BoxShadow(
                                              //     blurRadius: 0.0,
                                              //     color: Colors.black26,
                                              //     //offset: Offset(6.0, 7.0),
                                              //   ),
                                              // ],
                                            ),
                                            margin: EdgeInsets.only(
                                                top: 5,
                                                bottom: 5,
                                                left: 0,
                                                right: 0),
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: Container(
                                                          //color: Colors.red,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 20,
                                                                  right: 20,
                                                                  top: 15),
                                                          padding:
                                                              EdgeInsets.only(
                                                                  right: 10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              ////// <<<<< Picture start >>>>> //////
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        right:
                                                                            10),
                                                                //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            1.0),
                                                                child:
                                                                    CircleAvatar(
                                                                  radius: 20.0,
                                                                  backgroundColor:
                                                                      Colors
                                                                          .white,
                                                                  backgroundImage:
                                                                      AssetImage(
                                                                          'assets/images/user.png'),
                                                                ),
                                                                decoration:
                                                                    new BoxDecoration(
                                                                  color: Colors
                                                                          .grey[
                                                                      300], // border color
                                                                  shape: BoxShape
                                                                      .circle,
                                                                ),
                                                              ),
                                                              ////// <<<<< Picture end >>>>> //////

                                                              Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: <
                                                                      Widget>[
                                                                    ////// <<<<< Name start >>>>> //////

                                                                    Text(
                                                                      profileFeedList[index].activityType ==
                                                                              "Share"
                                                                          ? "${profileFeedList[index].data.feedOwner.firstName} ${profileFeedList[index].data.feedOwner.lastName} shared a post"
                                                                          : "${profileFeedList[index].data.statusUserName}",
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15,
                                                                          color: Colors
                                                                              .black54,
                                                                          fontFamily:
                                                                              'Oswald',
                                                                          fontWeight:
                                                                              FontWeight.w500),
                                                                    ),
                                                                    ////// <<<<< Name start >>>>> //////

                                                                    ////// <<<<< Time start >>>>> //////
                                                                    Container(
                                                                      margin: EdgeInsets
                                                                          .only(
                                                                              top: 3),
                                                                      child:
                                                                          Row(
                                                                        children: <
                                                                            Widget>[
                                                                          Text(
                                                                            date,
                                                                            maxLines:
                                                                                1,
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                            style: TextStyle(
                                                                                fontFamily: 'Oswald',
                                                                                fontWeight: FontWeight.w400,
                                                                                fontSize: 11,
                                                                                color: Colors.black45),
                                                                          ),
                                                                          Container(
                                                                              margin: EdgeInsets.only(left: 5),
                                                                              child: Icon(
                                                                                Icons.public,
                                                                                color: Colors.black45,
                                                                                size: 12,
                                                                              ))
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    ////// <<<<< Time end >>>>> //////
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      ////// <<<<< More start >>>>> //////
                                                      profileFeedList[index]
                                                                  .userId !=
                                                              userData['id']
                                                          ? Container()
                                                          : GestureDetector(
                                                              onTap: () {
                                                                setState(() {
                                                                  _statusModalBottomSheet(
                                                                      context,
                                                                      index,
                                                                      profileFeedList[
                                                                          index]);
                                                                });
                                                              },
                                                              child: Container(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              5),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          right:
                                                                              15),
                                                                  child: Icon(
                                                                    Icons
                                                                        .more_horiz,
                                                                    color: Colors
                                                                        .black45,
                                                                  )),
                                                            ),
                                                      ////// <<<<< More end >>>>> //////
                                                    ],
                                                  ),
                                                ),

                                                ////// <<<<< Post start >>>>> //////

                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                        top: 20),
                                                    child: Container(
                                                      child: Column(
                                                        children: <Widget>[
                                                          profileFeedList[index]
                                                                      .activityText ==
                                                                  ""
                                                              ? Container()
                                                              : Container(
                                                                  width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                                  child: Text(
                                                                    "${profileFeedList[index].activityText}",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .justify,
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .black87,
                                                                        fontWeight:
                                                                            FontWeight.w400),
                                                                  ),
                                                                ),
                                                          profileFeedList[index]
                                                                      .activityType !=
                                                                  "Share"
                                                              ? Container()
                                                              : Container(
                                                                  child: Column(
                                                                    children: <
                                                                        Widget>[
                                                                      Container(
                                                                        child:
                                                                            Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceBetween,
                                                                          children: <
                                                                              Widget>[
                                                                            Expanded(
                                                                              child: Container(
                                                                                //color: Colors.red,
                                                                                margin: EdgeInsets.only(left: 20, right: 0, top: 15),
                                                                                padding: EdgeInsets.only(right: 10),
                                                                                child: Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    ////// <<<<< Picture start >>>>> //////
                                                                                    Container(
                                                                                      margin: EdgeInsets.only(right: 10),
                                                                                      //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                                                      padding: EdgeInsets.all(1.0),
                                                                                      child: CircleAvatar(
                                                                                        radius: 20.0,
                                                                                        backgroundColor: Colors.white,
                                                                                        backgroundImage: AssetImage('assets/images/user.png'),
                                                                                      ),
                                                                                      decoration: new BoxDecoration(
                                                                                        color: Colors.grey[300], // border color
                                                                                        shape: BoxShape.circle,
                                                                                      ),
                                                                                    ),
                                                                                    ////// <<<<< Picture end >>>>> //////

                                                                                    Expanded(
                                                                                      child: Column(
                                                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                                                        children: <Widget>[
                                                                                          ////// <<<<< Name start >>>>> //////

                                                                                          Text(
                                                                                            "${profileFeedList[index].feedUser.firstName} ${profileFeedList[index].feedUser.lastName}",
                                                                                            maxLines: 1,
                                                                                            overflow: TextOverflow.ellipsis,
                                                                                            style: TextStyle(fontSize: 15, color: Colors.black54, fontFamily: 'Oswald', fontWeight: FontWeight.w500),
                                                                                          ),
                                                                                          ////// <<<<< Name start >>>>> //////

                                                                                          ////// <<<<< Time start >>>>> //////
                                                                                          Container(
                                                                                            margin: EdgeInsets.only(top: 3),
                                                                                            child: Row(
                                                                                              children: <Widget>[
                                                                                                Text(
                                                                                                  date,
                                                                                                  maxLines: 1,
                                                                                                  overflow: TextOverflow.ellipsis,
                                                                                                  style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w400, fontSize: 11, color: Colors.black45),
                                                                                                ),
                                                                                                Container(
                                                                                                    margin: EdgeInsets.only(left: 5),
                                                                                                    child: Icon(
                                                                                                      Icons.public,
                                                                                                      color: Colors.black45,
                                                                                                      size: 12,
                                                                                                    ))
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                          ////// <<<<< Time end >>>>> //////
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      profileFeedList[index].data.share.activityText ==
                                                                              ""
                                                                          ? Container()
                                                                          : Container(
                                                                              width: MediaQuery.of(context).size.width,
                                                                              margin: EdgeInsets.only(left: 20, top: 20),
                                                                              child: Text(
                                                                                "${profileFeedList[index].data.share.activityText}",
                                                                                textAlign: TextAlign.justify,
                                                                                style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w400),
                                                                              ),
                                                                            ),
                                                                      img.length ==
                                                                              0
                                                                          ? Container()
                                                                          : Container(
                                                                              margin: EdgeInsets.only(left: 20),
                                                                              child: Column(
                                                                                children: <Widget>[
                                                                                  img.length == 1
                                                                                      ? Row(
                                                                                          children: <Widget>[
                                                                                            Expanded(
                                                                                              child: Container(
                                                                                                  //color: Colors.red,
                                                                                                  height: 200,
                                                                                                  //width: 150,
                                                                                                  padding: const EdgeInsets.all(0.0),
                                                                                                  margin: EdgeInsets.only(top: 10, right: 5, left: 0),
                                                                                                  //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                                  child: ClipRRect(
                                                                                                    borderRadius: BorderRadius.circular(5),
                                                                                                    child: CachedNetworkImage(
                                                                                                      imageUrl: img[0]['url'],
                                                                                                      placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                      errorWidget: (context, url, error) => Image.asset(
                                                                                                        "assets/images/placeholder_cover.jpg",
                                                                                                        fit: BoxFit.cover,
                                                                                                        //height: 40,
                                                                                                      ),
                                                                                                      fit: BoxFit.cover,
                                                                                                    ),
                                                                                                  )),
                                                                                            ),
                                                                                          ],
                                                                                        )
                                                                                      : Column(
                                                                                          children: <Widget>[
                                                                                            Container(
                                                                                              child: Row(
                                                                                                children: <Widget>[
                                                                                                  Expanded(
                                                                                                    child: Container(
                                                                                                        //color: Colors.red,
                                                                                                        height: 150,
                                                                                                        //width: 150,
                                                                                                        padding: const EdgeInsets.all(0.0),
                                                                                                        margin: EdgeInsets.only(top: 10, right: 5),
                                                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                                        child: ClipRRect(
                                                                                                          borderRadius: BorderRadius.circular(5),
                                                                                                          child: CachedNetworkImage(
                                                                                                            imageUrl: img[0]['url'],
                                                                                                            placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                            errorWidget: (context, url, error) => Image.asset(
                                                                                                              "assets/images/placeholder_cover.jpg",
                                                                                                              fit: BoxFit.cover,
                                                                                                              //height: 40,
                                                                                                            ),
                                                                                                            fit: BoxFit.cover,
                                                                                                          ),
                                                                                                        )),
                                                                                                  ),
                                                                                                  Expanded(
                                                                                                    child: Container(
                                                                                                        //color: Colors.red,
                                                                                                        height: 150,
                                                                                                        // width: 150,
                                                                                                        padding: const EdgeInsets.all(0.0),
                                                                                                        margin: EdgeInsets.only(top: 10, left: 5),
                                                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car3.jpeg"), fit: BoxFit.cover)),
                                                                                                        child: ClipRRect(
                                                                                                          borderRadius: BorderRadius.circular(5),
                                                                                                          child: CachedNetworkImage(
                                                                                                            imageUrl: img[1]['url'],
                                                                                                            placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                            errorWidget: (context, url, error) => Image.asset(
                                                                                                              "assets/images/placeholder_cover.jpg",
                                                                                                              fit: BoxFit.cover,
                                                                                                              //height: 40,
                                                                                                            ),
                                                                                                            fit: BoxFit.cover,
                                                                                                          ),
                                                                                                        )),
                                                                                                  ),
                                                                                                ],
                                                                                              ),
                                                                                            ),
                                                                                            img.length == 2
                                                                                                ? Container()
                                                                                                : Container(
                                                                                                    child: Row(
                                                                                                      children: <Widget>[
                                                                                                        Expanded(
                                                                                                          child: Container(
                                                                                                              //color: Colors.red,
                                                                                                              height: img.length == 3 ? 200 : 150,
                                                                                                              //width: 150,
                                                                                                              padding: const EdgeInsets.all(0.0),
                                                                                                              margin: EdgeInsets.only(top: 10, right: img.length == 3 ? 0 : 5),
                                                                                                              //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car6.jpg"), fit: BoxFit.cover)),
                                                                                                              child: ClipRRect(
                                                                                                                borderRadius: BorderRadius.circular(5),
                                                                                                                child: CachedNetworkImage(
                                                                                                                  imageUrl: img[2]['url'],
                                                                                                                  placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                                  errorWidget: (context, url, error) => Image.asset(
                                                                                                                    "assets/images/placeholder_cover.jpg",
                                                                                                                    fit: BoxFit.cover,
                                                                                                                    //height: 40,
                                                                                                                  ),
                                                                                                                  fit: BoxFit.cover,
                                                                                                                ),
                                                                                                              )),
                                                                                                        ),
                                                                                                        img.length < 4
                                                                                                            ? Container()
                                                                                                            : Expanded(
                                                                                                                child: Container(
                                                                                                                    //color: Colors.red,
                                                                                                                    height: 150,
                                                                                                                    // width: 150,
                                                                                                                    padding: const EdgeInsets.all(0.0),
                                                                                                                    margin: EdgeInsets.only(top: 10, left: 5),
                                                                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/placeholder_cover.jpg"), fit: BoxFit.cover)),
                                                                                                                    child: Container(
                                                                                                                      child: Stack(
                                                                                                                        children: <Widget>[
                                                                                                                          Container(
                                                                                                                            height: 150,
                                                                                                                            child: ClipRRect(
                                                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                                                              child: CachedNetworkImage(
                                                                                                                                imageUrl: img[3]['url'],
                                                                                                                                placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                                                errorWidget: (context, url, error) => Image.asset(
                                                                                                                                  "assets/images/placeholder_cover.jpg",
                                                                                                                                  fit: BoxFit.cover,
                                                                                                                                  //height: 40,
                                                                                                                                ),
                                                                                                                                fit: BoxFit.cover,
                                                                                                                              ),
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                          Container(
                                                                                                                            height: 150,
                                                                                                                            // width: 150,
                                                                                                                            padding: const EdgeInsets.all(0.0),
                                                                                                                            margin: EdgeInsets.only(top: 0, left: 0),
                                                                                                                            decoration: BoxDecoration(
                                                                                                                              color: img.length > 4 ? Colors.black.withOpacity(0.6) : Colors.transparent,
                                                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                          img.length > 4
                                                                                                                              ? Center(
                                                                                                                                  child: Container(
                                                                                                                                      child: Text(
                                                                                                                                  "+${img.length - 3}",
                                                                                                                                  style: TextStyle(color: Colors.white, fontSize: 22),
                                                                                                                                )))
                                                                                                                              : Container(),
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    )),
                                                                                                              ),
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                          ],
                                                                                        ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                    ],
                                                                  ),
                                                                ),
                                                          img.length == 0
                                                              ? Container()
                                                              : Container(
                                                                  child: Column(
                                                                    children: <
                                                                        Widget>[
                                                                      img.length ==
                                                                              1
                                                                          ? Row(
                                                                              children: <Widget>[
                                                                                Expanded(
                                                                                  child: Container(
                                                                                      //color: Colors.red,
                                                                                      height: 200,
                                                                                      //width: 150,
                                                                                      padding: const EdgeInsets.all(0.0),
                                                                                      margin: EdgeInsets.only(top: 10, right: 5),
                                                                                      //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                      child: ClipRRect(
                                                                                        borderRadius: BorderRadius.circular(5),
                                                                                        child: CachedNetworkImage(
                                                                                          imageUrl: img[0]['url'],
                                                                                          placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                          errorWidget: (context, url, error) => Image.asset(
                                                                                            "assets/images/placeholder_cover.jpg",
                                                                                            fit: BoxFit.cover,
                                                                                            //height: 40,
                                                                                          ),
                                                                                          fit: BoxFit.cover,
                                                                                        ),
                                                                                      )),
                                                                                ),
                                                                              ],
                                                                            )
                                                                          : Column(
                                                                              children: <Widget>[
                                                                                Container(
                                                                                  child: Row(
                                                                                    children: <Widget>[
                                                                                      Expanded(
                                                                                        child: Container(
                                                                                            //color: Colors.red,
                                                                                            height: 150,
                                                                                            //width: 150,
                                                                                            padding: const EdgeInsets.all(0.0),
                                                                                            margin: EdgeInsets.only(top: 10, right: 5),
                                                                                            //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                            child: ClipRRect(
                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                              child: CachedNetworkImage(
                                                                                                imageUrl: img[0]['url'],
                                                                                                placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                errorWidget: (context, url, error) => Image.asset(
                                                                                                  "assets/images/placeholder_cover.jpg",
                                                                                                  fit: BoxFit.cover,
                                                                                                  //height: 40,
                                                                                                ),
                                                                                                fit: BoxFit.cover,
                                                                                              ),
                                                                                            )),
                                                                                      ),
                                                                                      Expanded(
                                                                                        child: Container(
                                                                                            //color: Colors.red,
                                                                                            height: 150,
                                                                                            // width: 150,
                                                                                            padding: const EdgeInsets.all(0.0),
                                                                                            margin: EdgeInsets.only(top: 10, left: 5),
                                                                                            //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car3.jpeg"), fit: BoxFit.cover)),
                                                                                            child: ClipRRect(
                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                              child: CachedNetworkImage(
                                                                                                imageUrl: img[1]['url'],
                                                                                                placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                errorWidget: (context, url, error) => Image.asset(
                                                                                                  "assets/images/placeholder_cover.jpg",
                                                                                                  fit: BoxFit.cover,
                                                                                                  //height: 40,
                                                                                                ),
                                                                                                fit: BoxFit.cover,
                                                                                              ),
                                                                                            )),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                img.length == 2
                                                                                    ? Container()
                                                                                    : Container(
                                                                                        child: Row(
                                                                                          children: <Widget>[
                                                                                            Expanded(
                                                                                              child: Container(
                                                                                                  //color: Colors.red,
                                                                                                  height: img.length == 3 ? 200 : 150,
                                                                                                  //width: 150,
                                                                                                  padding: const EdgeInsets.all(0.0),
                                                                                                  margin: EdgeInsets.only(top: 10, right: img.length == 3 ? 0 : 5),
                                                                                                  //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car6.jpg"), fit: BoxFit.cover)),
                                                                                                  child: ClipRRect(
                                                                                                    borderRadius: BorderRadius.circular(5),
                                                                                                    child: CachedNetworkImage(
                                                                                                      imageUrl: img[2]['url'],
                                                                                                      placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                      errorWidget: (context, url, error) => Image.asset(
                                                                                                        "assets/images/placeholder_cover.jpg",
                                                                                                        fit: BoxFit.cover,
                                                                                                        //height: 40,
                                                                                                      ),
                                                                                                      fit: BoxFit.cover,
                                                                                                    ),
                                                                                                  )),
                                                                                            ),
                                                                                            img.length < 4
                                                                                                ? Container()
                                                                                                : Expanded(
                                                                                                    child: Container(
                                                                                                        //color: Colors.red,
                                                                                                        height: 150,
                                                                                                        // width: 150,
                                                                                                        padding: const EdgeInsets.all(0.0),
                                                                                                        margin: EdgeInsets.only(top: 10, left: 5),
                                                                                                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/placeholder_cover.jpg"), fit: BoxFit.cover)),
                                                                                                        child: Container(
                                                                                                          child: Stack(
                                                                                                            children: <Widget>[
                                                                                                              Container(
                                                                                                                height: 150,
                                                                                                                child: ClipRRect(
                                                                                                                  borderRadius: BorderRadius.circular(5),
                                                                                                                  child: CachedNetworkImage(
                                                                                                                    imageUrl: img[3]['url'],
                                                                                                                    placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                                    errorWidget: (context, url, error) => Image.asset(
                                                                                                                      "assets/images/placeholder_cover.jpg",
                                                                                                                      fit: BoxFit.cover,
                                                                                                                      //height: 40,
                                                                                                                    ),
                                                                                                                    fit: BoxFit.cover,
                                                                                                                  ),
                                                                                                                ),
                                                                                                              ),
                                                                                                              Container(
                                                                                                                height: 150,
                                                                                                                // width: 150,
                                                                                                                padding: const EdgeInsets.all(0.0),
                                                                                                                margin: EdgeInsets.only(top: 0, left: 0),
                                                                                                                decoration: BoxDecoration(
                                                                                                                  color: img.length > 4 ? Colors.black.withOpacity(0.6) : Colors.transparent,
                                                                                                                  borderRadius: BorderRadius.circular(5),
                                                                                                                ),
                                                                                                              ),
                                                                                                              img.length > 4
                                                                                                                  ? Center(
                                                                                                                      child: Container(
                                                                                                                          child: Text(
                                                                                                                      "+${img.length - 3}",
                                                                                                                      style: TextStyle(color: Colors.white, fontSize: 22),
                                                                                                                    )))
                                                                                                                  : Container(),
                                                                                                            ],
                                                                                                          ),
                                                                                                        )),
                                                                                                  ),
                                                                                          ],
                                                                                        ),
                                                                                      ),
                                                                              ],
                                                                            ),
                                                                    ],
                                                                  ),
                                                                ),
                                                        ],
                                                      ),
                                                    )),
                                                ////// <<<<< Post end >>>>> //////
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 20,
                                                        right: 20,
                                                        bottom: 0,
                                                        top: 10),
                                                    child: Divider(
                                                      color: Colors.grey[300],
                                                    )),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 20, top: 0),
                                                  padding: EdgeInsets.all(0),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      ////// <<<<< Like start >>>>> //////
                                                      GestureDetector(
                                                        onTap: () {
                                                          setState(() {
                                                            if (profileFeedList[
                                                                        index]
                                                                    .like ==
                                                                null) {
                                                              profileFeedList[
                                                                      index]
                                                                  .meta
                                                                  .totalLikesCount++;
                                                              profileFeedList[
                                                                      index]
                                                                  .like = 1;
                                                              likeUnlike(
                                                                  index,
                                                                  1,
                                                                  profileFeedList[
                                                                          index]
                                                                      .id);
                                                            } else {
                                                              profileFeedList[
                                                                      index]
                                                                  .meta
                                                                  .totalLikesCount--;
                                                              profileFeedList[
                                                                      index]
                                                                  .like = null;
                                                              likeUnlike(
                                                                  index,
                                                                  2,
                                                                  profileFeedList[
                                                                          index]
                                                                      .id);
                                                            }
                                                          });
                                                        },
                                                        child: Container(
                                                          child: Row(
                                                            children: <Widget>[
                                                              Container(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            3.0),
                                                                child: Icon(
                                                                  profileFeedList[index]
                                                                              .like !=
                                                                          null
                                                                      ? Icons
                                                                          .favorite
                                                                      : Icons
                                                                          .favorite_border,
                                                                  size: 20,
                                                                  color: profileFeedList[index]
                                                                              .like !=
                                                                          null
                                                                      ? Colors
                                                                          .redAccent
                                                                      : Colors
                                                                          .black54,
                                                                ),
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            3),
                                                                child: Text(
                                                                    "${profileFeedList[index].meta.totalLikesCount}",
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'Oswald',
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w300,
                                                                        color: Colors
                                                                            .black54,
                                                                        fontSize:
                                                                            12)),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      ////// <<<<< Like end >>>>> //////

                                                      ////// <<<<< Comment start >>>>> //////
                                                      GestureDetector(
                                                        onTap: () {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) => CommentPage(
                                                                      userData,
                                                                      index,
                                                                      profileFeedList[
                                                                          index])));
                                                        },
                                                        child: Container(
                                                          child: Row(
                                                            children: <Widget>[
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            15),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            3.0),
                                                                child: Icon(
                                                                  Icons
                                                                      .chat_bubble_outline,
                                                                  size: 20,
                                                                  color: Colors
                                                                      .black54,
                                                                ),
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            3),
                                                                child: Text(
                                                                    "${profileComCount[index]['count']}",
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'Oswald',
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w300,
                                                                        color: Colors
                                                                            .black54,
                                                                        fontSize:
                                                                            12)),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      ////// <<<<< Comment end >>>>> //////

                                                      ////// <<<<< Share start >>>>> //////
                                                      GestureDetector(
                                                        onTap: () {
                                                          _shareModalBottomSheet(
                                                              context,
                                                              index,
                                                              userData,
                                                              profileFeedList[
                                                                  index]);
                                                        },
                                                        child: Container(
                                                          child: Row(
                                                            children: <Widget>[
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            15),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            3.0),
                                                                child: Icon(
                                                                  Icons.share,
                                                                  size: 20,
                                                                  color: Colors
                                                                      .black54,
                                                                ),
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            3),
                                                                child: Text(
                                                                    "${profileFeedList[index].meta.totalSharesCount}",
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'Oswald',
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w300,
                                                                        color: Colors
                                                                            .black54,
                                                                        fontSize:
                                                                            12)),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      ////// <<<<< Share end >>>>> //////
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                    height: 10,
                                                    decoration: BoxDecoration(
                                                        color: Colors.grey
                                                            .withOpacity(0.1),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5)),
                                                    margin: EdgeInsets.only(
                                                        left: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                        top: 7),
                                                    child: null),
                                              ],
                                            ),
                                          )
                                        : FeedLoader(),
                                  ),
                                ));
                          }, childCount: profileFeedList.length),
                        ),
                      ],
                    ),
                  ),
                ],
              ));
  }

  Future likeUnlike(index, status, id) async {
    var data = {
      'user_id': userData['id'],
      'status_id': id,
      'type': status,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/add/like');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      //Navigator.pop(context);

    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }

  void _shareModalBottomSheet(context, int index, var userData, feedList) {
    DateTime dateTime = DateTime.parse(feedList.createdAt);
    String date1 = DateFormat.yMMMd().format(dateTime);

    List img = [];

    for (int i = 0; i < feedList.data.images.length; i++) {
      img.add({
        'id': feedList.data.images[i].id,
        'url': feedList.data.images[i].url,
      });
    }
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Container(
                //height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.share,
                              color: Colors.black54,
                              size: 16,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 5),
                              child: Text(
                                "Share this post",
                                style: TextStyle(fontFamily: "Oswald"),
                              ),
                            ),
                          ],
                        )),
                    Divider(),
                    Container(
                      child: Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              width: 50,
                              height: 50,
                              margin: EdgeInsets.only(top: 0, left: 20),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/prabal.jpg"),
                                      fit: BoxFit.cover),
                                  border: Border.all(
                                      color: Colors.grey, width: 0.1),
                                  borderRadius: BorderRadius.circular(15)),
                            ),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                      alignment: Alignment.centerLeft,
                                      margin:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(
                                              "${userData['firstName']} ${userData['lastName']}",
                                              maxLines: 1,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14,
                                                  fontFamily: 'Oswald',
                                                  fontWeight:
                                                      FontWeight.normal),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            //_securityModalBottomSheet(context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 0, right: 5, left: 10),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 0.3,
                                                    color: Colors.black54),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Icon(
                                                    Icons.public,
                                                    size: 12,
                                                    color: Colors.black54,
                                                  ),
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Public",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                          fontFamily: "Oswald"),
                                                    )),
                                                Icon(
                                                  Icons.arrow_drop_down,
                                                  size: 25,
                                                  color: Colors.black54,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //height: 150,
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.only(
                          top: 20, left: 20, bottom: 5, right: 20),
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.only(
                            topLeft: Radius.circular(5.0),
                            topRight: Radius.circular(5.0),
                            bottomLeft: Radius.circular(5.0),
                            bottomRight: Radius.circular(5.0)),
                        //color: Colors.grey[100],
                        //border: Border.all(width: 0.2, color: Colors.grey)
                      ),
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(minHeight: 100.0, maxHeight: 100.0),
                        child: new SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          //reverse: true,
                          child: Container(
                            //height: 100,
                            child: new TextField(
                              maxLines: null,
                              autofocus: false,
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: 'Oswald',
                              ),
                              controller: shareController,
                              decoration: InputDecoration(
                                alignLabelWithHint: true,
                                hintText: "Write something...",
                                hintStyle: TextStyle(
                                    color: Colors.black54,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w300),
                                contentPadding:
                                    EdgeInsets.fromLTRB(0.0, 10.0, 20.0, 10.0),
                                border: InputBorder.none,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  sharePost = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              //snackBar(context);
                              loading == false
                                  ? statusShare(feedList, img)
                                  : null;
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 20, top: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.only(
                                          left: 20, right: 20, top: 0),
                                      decoration: BoxDecoration(
                                          color: mainColor,
                                          border: Border.all(
                                              color: Colors.grey, width: 0.5),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: Text(
                                        "Share",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontFamily: 'BebasNeue',
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      //height: MediaQuery.of(context).size.height / 2.05,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        //border: Border.all(width: 0.8, color: Colors.grey[300]),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 16.0,
                            color: Colors.grey[300],
                            //offset: Offset(3.0, 4.0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(
                          top: 5, bottom: 15, left: 10, right: 10),
                      child: Container(
                        //color: Colors.yellow,
                        margin:
                            EdgeInsets.only(left: 20, right: 20, bottom: 10),
                        padding: EdgeInsets.only(right: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< pic start >>>>> //////
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    padding: EdgeInsets.all(1.0),
                                    child: CircleAvatar(
                                      radius: 20.0,
                                      backgroundColor: Colors.white,
                                      backgroundImage: AssetImage(
                                          "assets/images/prabal.jpg"),
                                    ),
                                    decoration: new BoxDecoration(
                                      color: Colors.grey[300], // border color
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                  ////// <<<<< pic end >>>>> //////

                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ////// <<<<< Name & Interest start >>>>> //////
                                          Container(
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    feedList
                                                        .data.statusUserName,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        color: Colors.black,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                          ////// <<<<< Name & Interest end >>>>> //////

                                          ////// <<<<< time job start >>>>> //////
                                          Container(
                                            margin: EdgeInsets.only(top: 3),
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    date1,
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 11,
                                                        color: Colors.black54),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 3),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Icon(
                                                          Icons.public,
                                                          size: 12,
                                                          color: Colors.black54,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          ////// <<<<< time job end >>>>> //////
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                                margin:
                                    EdgeInsets.only(left: 0, right: 0, top: 20),
                                child: Container(
                                  child: Column(
                                    children: <Widget>[
                                      feedList.activityText == ""
                                          ? Container()
                                          : Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: Text(
                                                "${feedList.activityText}",
                                                textAlign: TextAlign.justify,
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontWeight:
                                                        FontWeight.w400),
                                              ),
                                            ),
                                      img.length == 0
                                          ? Container()
                                          : Container(
                                              child: Column(
                                                children: <Widget>[
                                                  img.length == 1
                                                      ? Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 200,
                                                                  //width: 150,
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          right:
                                                                              0),
                                                                  //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                  child:
                                                                      ClipRRect(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(5),
                                                                    child:
                                                                        CachedNetworkImage(
                                                                      imageUrl:
                                                                          img[0]
                                                                              [
                                                                              'url'],
                                                                      placeholder: (context,
                                                                              url) =>
                                                                          SpinKitCircle(
                                                                              color: Colors.grey.withOpacity(0.5)),
                                                                      errorWidget: (context,
                                                                              url,
                                                                              error) =>
                                                                          Image
                                                                              .asset(
                                                                        "assets/images/placeholder_cover.jpg",
                                                                        fit: BoxFit
                                                                            .cover,
                                                                        //height: 40,
                                                                      ),
                                                                      fit: BoxFit
                                                                          .cover,
                                                                    ),
                                                                  )),
                                                            ),
                                                          ],
                                                        )
                                                      : Column(
                                                          children: <Widget>[
                                                            Container(
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                    child: Container(
                                                                        //color: Colors.red,
                                                                        height: 150,
                                                                        //width: 150,
                                                                        padding: const EdgeInsets.all(0.0),
                                                                        margin: EdgeInsets.only(top: 10, right: 5),
                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                        child: ClipRRect(
                                                                          borderRadius:
                                                                              BorderRadius.circular(5),
                                                                          child:
                                                                              CachedNetworkImage(
                                                                            imageUrl:
                                                                                img[0]['url'],
                                                                            placeholder: (context, url) =>
                                                                                SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                            errorWidget: (context, url, error) =>
                                                                                Image.asset(
                                                                              "assets/images/placeholder_cover.jpg",
                                                                              fit: BoxFit.cover,
                                                                              //height: 40,
                                                                            ),
                                                                            fit:
                                                                                BoxFit.cover,
                                                                          ),
                                                                        )),
                                                                  ),
                                                                  Expanded(
                                                                    child: Container(
                                                                        //color: Colors.red,
                                                                        height: 150,
                                                                        // width: 150,
                                                                        padding: const EdgeInsets.all(0.0),
                                                                        margin: EdgeInsets.only(top: 10, left: 5),
                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car3.jpeg"), fit: BoxFit.cover)),
                                                                        child: ClipRRect(
                                                                          borderRadius:
                                                                              BorderRadius.circular(5),
                                                                          child:
                                                                              CachedNetworkImage(
                                                                            imageUrl:
                                                                                img[1]['url'],
                                                                            placeholder: (context, url) =>
                                                                                SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                            errorWidget: (context, url, error) =>
                                                                                Image.asset(
                                                                              "assets/images/placeholder_cover.jpg",
                                                                              fit: BoxFit.cover,
                                                                              //height: 40,
                                                                            ),
                                                                            fit:
                                                                                BoxFit.cover,
                                                                          ),
                                                                        )),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            img.length == 2
                                                                ? Container()
                                                                : Container(
                                                                    child: Row(
                                                                      children: <
                                                                          Widget>[
                                                                        Expanded(
                                                                          child: Container(
                                                                              //color: Colors.red,
                                                                              height: img.length == 3 ? 200 : 150,
                                                                              //width: 150,
                                                                              padding: const EdgeInsets.all(0.0),
                                                                              margin: EdgeInsets.only(top: 10, right: img.length == 3 ? 0 : 5),
                                                                              //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car6.jpg"), fit: BoxFit.cover)),
                                                                              child: ClipRRect(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                child: CachedNetworkImage(
                                                                                  imageUrl: img[2]['url'],
                                                                                  placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                  errorWidget: (context, url, error) => Image.asset(
                                                                                    "assets/images/placeholder_cover.jpg",
                                                                                    fit: BoxFit.cover,
                                                                                    //height: 40,
                                                                                  ),
                                                                                  fit: BoxFit.cover,
                                                                                ),
                                                                              )),
                                                                        ),
                                                                        img.length <
                                                                                4
                                                                            ? Container()
                                                                            : Expanded(
                                                                                child: Container(
                                                                                    //color: Colors.red,
                                                                                    height: 150,
                                                                                    // width: 150,
                                                                                    padding: const EdgeInsets.all(0.0),
                                                                                    margin: EdgeInsets.only(top: 10, left: 5),
                                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/placeholder_cover.jpg"), fit: BoxFit.cover)),
                                                                                    child: Container(
                                                                                      child: Stack(
                                                                                        children: <Widget>[
                                                                                          Container(
                                                                                            height: 150,
                                                                                            child: ClipRRect(
                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                              child: CachedNetworkImage(
                                                                                                imageUrl: img[3]['url'],
                                                                                                placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                errorWidget: (context, url, error) => Image.asset(
                                                                                                  "assets/images/placeholder_cover.jpg",
                                                                                                  fit: BoxFit.cover,
                                                                                                  //height: 40,
                                                                                                ),
                                                                                                fit: BoxFit.cover,
                                                                                              ),
                                                                                            ),
                                                                                          ),
                                                                                          Container(
                                                                                            height: 150,
                                                                                            // width: 150,
                                                                                            padding: const EdgeInsets.all(0.0),
                                                                                            margin: EdgeInsets.only(top: 0, left: 0),
                                                                                            decoration: BoxDecoration(
                                                                                              color: img.length > 4 ? Colors.black.withOpacity(0.6) : Colors.transparent,
                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                            ),
                                                                                          ),
                                                                                          img.length > 4
                                                                                              ? Center(
                                                                                                  child: Container(
                                                                                                      child: Text(
                                                                                                  "+${img.length - 3}",
                                                                                                  style: TextStyle(color: Colors.white, fontSize: 22),
                                                                                                )))
                                                                                              : Container(),
                                                                                        ],
                                                                                      ),
                                                                                    )),
                                                                              ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                          ],
                                                        ),
                                                ],
                                              ),
                                            ),
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      ),
                    )
                  ],
                )),
          );
        });
  }

  Future statusShare(feedList, img) async {
    setState(() {
      loading = true;
    });
    var data = {
      'id': feedList.id,
      'user_id': feedList.userId,
      'activity_id': feedList.id,
      'activity_text': sharePost,
      'activity_type': "Share",
      "privacy": feedList.privacy,
      'data': {
        'status': feedList.activityText,
        'comments': [],
        'images': img,
        'statusUser_Name': feedList.data.statusUserName,
        'statusUser_userName': feedList.data.statusUserUserName,
        'statusUser_profilePic': feedList.data.statusUserProfilePic,
        'link': "",
        'link_meta': {},
      },
      'created_at': feedList.createdAt,
      'updated_at': feedList.createdAt,
    };

    print(data);

    var res = await CallApi().postData1(data, 'share/add');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 201) {
      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    } else {
      _showMessage("Something went wrong!", 1);
    }

    setState(() {
      loading = false;
    });
  }

  void _statusModalBottomSheet(context, index, feedList) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                // Text('React to this post',
                //       style: TextStyle(fontWeight: FontWeight.normal)),
                feedList.activityText != ""
                    ? new ListTile(
                        leading: new Icon(Icons.edit),
                        title: new Text('Edit',
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontFamily: "Oswald")),
                        onTap: () => {
                          Navigator.pop(context),
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      EditPostPage(feedList, userData)))
                        },
                      )
                    : Container(),
                new ListTile(
                  leading: new Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  title: new Text('Delete',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.redAccent,
                          fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    _showDeleteDialog(index, feedList)
                  },
                ),
              ],
            ),
          );
        });
  }

  Future<Null> _showDeleteDialog(index, feedList) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          "Want to delete the post?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                              statusDelete(index, feedList);
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future statusDelete(index, feedLists) async {
    var data = {
      'user_id': userData['id'],
      'status_id': feedLists.id,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/delete');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      //Navigator.pop(context);
      setState(() {
        feedList.removeAt(index);
      });
      return _showMessage("Deleted Successfully!", 2);
    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }
}
