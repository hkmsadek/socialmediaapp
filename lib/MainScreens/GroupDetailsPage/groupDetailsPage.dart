import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shimmer/shimmer.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/CreatePostPage/CreatePostPage.dart';
import 'package:social_app_fb/MainScreens/EventsPage/EventsPage.dart';
import 'package:social_app_fb/MainScreens/GroupAboutPage/GroupAboutPage.dart';
import 'package:social_app_fb/MainScreens/GroupAlbumPage/GroupAlbumPage.dart';
import 'package:social_app_fb/MainScreens/GroupMemberPage/groupMemberPage.dart';
import 'package:social_app_fb/MainScreens/GroupPhotosPage/GroupPhotosPage.dart';
import 'package:social_app_fb/MainScreens/InviteGroupMemberPage/inviteGroupMemberPage.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/MainScreens/PendingPostsPage/PendingPostsPage.dart';
import 'package:social_app_fb/ModelClass/GroupMemberModel/GroupMemberModel.dart';
import '../../main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GroupDetailsPage extends StatefulWidget {
  final id;
  GroupDetailsPage(this.id);

  @override
  _GroupDetailsPageState createState() => _GroupDetailsPageState();
}

class _GroupDetailsPageState extends State<GroupDetailsPage> {
  SharedPreferences sharedPreferences;
  String theme = "";
  String pic = "", creator = "", banner = "", logo = "";
  Timer _timer;
  int _start = 3;
  bool loading = true;
  var userData, groupDetails, pendingPostCount, memberList;
  var file, file1;
  File logoImage, bannerImage;
  bool isAdmin = false;
  bool isLogo = false;
  bool isBanner = false;
  bool isFollow = false;

  @override
  void initState() {
    _getUserInfo();
    sharedPrefcheck();
    timerCheck();
    super.initState();
  }

  void sharedPrefcheck() async {
    sharedPreferences = await SharedPreferences.getInstance();

    setState(() {
      theme = sharedPreferences.getString("theme");
    });
    //print(theme);
  }

  void timerCheck() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            timer.cancel();
            setState(() {
              loading = false;
            });
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
        pic = userData['profilePic'];
      });
      print(userData);

      loadRequests();
    }
  }

  Future loadRequests() async {
    setState(() {
      loading = true;
    });

    var reqresponse =
        await CallApi().getData2('group/get/details/${widget.id}');
    var reqcontent = reqresponse.body;
    final req = json.decode(reqcontent);
    //var reqdata = GroupModel.fromJson(req);
    //print(reqdata);

    setState(() {
      groupDetails = req;

      if (userData['id'] == groupDetails['admins']['user_id']) {
        isFollow = true;
      }

      if (userData['id'] == groupDetails['admins']['user_id'] &&
          (groupDetails['admins']['user_role'] == "Super Admin" ||
              groupDetails['admins']['user_role'] == "Admin")) {
        isAdmin = true;
      }
    });

    loadPendingPosts();

    setState(() {
      loading = false;
    });
  }

  Future loadPendingPosts() async {
    var reqresponse =
        await CallApi().getData1('group/feed/get/pending/${widget.id}/count');
    var reqcontent = reqresponse.body;
    final req = json.decode(reqcontent);
    //var reqdata = GroupModel.fromJson(req);
    print(req['count']);

    setState(() {
      pendingPostCount = req['count'];
    });

    loadMemberList();
  }

  Future loadMemberList() async {
    var reqresponse =
        await CallApi().getData('group/members?group_id=${widget.id}');
    var reqcontent = reqresponse.body;
    final req = json.decode(reqcontent);
    var memdata = GroupMemberModel.fromJson(req);

    setState(() {
      memberList = memdata.members;
    });
    print(memberList.length);
  }

  void showBottomSheet(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return Wrap(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.assignment_turned_in,
                                  size: 17, color: mainColor)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Following",
                                    style: TextStyle(
                                        color: mainColor,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.person,
                                  size: 17, color: Colors.black54)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Manage Requests",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.share,
                                  size: 17, color: Colors.black54)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Share",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(left: 2, top: 2),
                                    child: Icon(Icons.perm_identity,
                                        size: 12, color: Colors.redAccent),
                                  ),
                                  Icon(Icons.notifications,
                                      size: 17, color: Colors.black54),
                                ],
                              )),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Notifications",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.group_work,
                                  size: 17, color: Colors.black54)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Pin to Shortcuts",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.announcement,
                                  size: 17, color: Colors.black54)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Find Support/Report Group",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 15, right: 15, top: 30, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.exit_to_app,
                                  size: 17, color: Colors.black54)),
                          Text.rich(
                            TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Leave Group",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w400)),

                                // can add more TextSpans here...
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.chevron_right,
                            size: 17, color: Colors.black54)),
                  ],
                ),
              ),
            ],
          );
        });
  }

  _pickBanner() async {
    bannerImage = await FilePicker.getFile(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'png', 'jpeg'],
    );
    print("bannerImage.length()");
    print(bannerImage.length());
    uploadBanner();
  }

  _pickLogo() async {
    logoImage = await FilePicker.getFile(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'png', 'jpeg'],
    );

    var bytes = logoImage.lengthSync();
    //var kb = bytes / 1024;
    var mb = bytes / (1024 * 1024);
    print("logoImage.length()");
    print(logoImage.length());
    print("mb");
    print(mb.toStringAsFixed(2));
    uploadLogo();
  }

  Future uploadBanner() async {
    setState(() {
      isBanner = true;
    });
    List<int> imageBytes = bannerImage.readAsBytesSync();
    banner = base64.encode(imageBytes);
    banner = 'data:image/png;base64,' + banner;

    var data = {'group_id': widget.id, 'type': "banner", 'image': banner};
    print(data);

    var res1 = await CallApi().postData1(data, 'upload/group');
    var body1 = json.decode(res1.body);
    print(body1);

    // setState(() {
    //   state1 = PhotoCrop.free;
    // });

    if (res1.statusCode == 200) {
      _showMessage("Group Banner uploaded successfully!");
    }
    setState(() {
      isBanner = false;
    });
  }

  Future uploadLogo() async {
    setState(() {
      isLogo = true;
    });
    List<int> imageBytes = logoImage.readAsBytesSync();
    logo = base64.encode(imageBytes);
    logo = 'data:image/png;base64,' + logo;

    var data = {'group_id': widget.id, 'type': "logo", 'image': logo};
    print(data);

    var res1 = await CallApi().postData1(data, 'upload/group');
    var body1 = json.decode(res1.body);
    print(body1);

    // setState(() {
    //   state1 = PhotoCrop.free;
    // });

    if (res1.statusCode == 200) {
      setState(() {
        isLogo = false;
      });
      _showMessage("Group Logo uploaded successfully!");
    }
  }

  _showMessage(msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: Colors.red.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }

  void followUnfollow(int number) async {
    //await Future.delayed(Duration(seconds: 3));
    var data = {
      'type': number == 1 ? "follow" : "unfollow",
      'user_id': groupDetails['admins']['user_id'],
    };

    print(data);
    var postresponse =
        await CallApi().postData1(data, 'group/follow/${widget.id}');
    print(postresponse);
    var postcontent = postresponse.body;
    print("postcontent");
    print(postcontent);

    // setState(() {
    //   groupList = posts;
    //   loading = false;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Group Details",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
              GestureDetector(
                onTap: () {
                  showBottomSheet(context);
                },
                child: Padding(
                  child: Icon(Icons.more_horiz),
                  padding: EdgeInsets.all(5),
                ),
              )
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/white.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: null,
          ),
          loading
              ? LoaderScreen()
              : Container(
                  margin: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
                  child: CustomScrollView(
                    physics: BouncingScrollPhysics(),
                    slivers: <Widget>[
                      SliverToBoxAdapter(
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              ////// <<<<< Cover Photo >>>>> //////
                              Stack(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 0, left: 0, right: 0),
                                    child: Container(
                                      height: 220,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: AssetImage(
                                                "assets/images/f4.jpg"),
                                            fit: BoxFit.cover,
                                          ),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(0),
                                              topRight: Radius.circular(0))),
                                      child: Container(
                                        height: 220,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: bannerImage != null
                                            ? Image.file(
                                                bannerImage,
                                                fit: BoxFit.cover,
                                              )
                                            : Container(
                                                child: CachedNetworkImage(
                                                  imageUrl:
                                                      "${groupDetails['banner']}",
                                                  placeholder: (context, url) =>
                                                      Center(
                                                          child: Text(
                                                              "Please Wait...")),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Image.asset(
                                                    "assets/images/placeholder_cover.jpg",
                                                    height: 40,
                                                    fit: BoxFit.cover,
                                                  ),
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                      height: 200,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(),
                                          Container(
                                            margin: EdgeInsets.only(right: 20),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Expanded(
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                      left: 20,
                                                    ),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Column(
                                                          children: <Widget>[
                                                            Container(
                                                              height: 80,
                                                              width: 80,
                                                              decoration:
                                                                  BoxDecoration(
                                                                      // color: Colors
                                                                      //     .white,
                                                                      image:
                                                                          DecorationImage(
                                                                        image: AssetImage(
                                                                            'assets/images/placeholder_cover.jpg'),
                                                                        fit: BoxFit
                                                                            .cover,
                                                                      ),
                                                                      border: Border.all(
                                                                          color: Colors
                                                                              .grey),
                                                                      borderRadius: BorderRadius.only(
                                                                          topLeft: Radius.circular(
                                                                              20),
                                                                          topRight: Radius.circular(
                                                                              20),
                                                                          bottomLeft: Radius.circular(isAdmin == false
                                                                              ? 20
                                                                              : 0),
                                                                          bottomRight: Radius.circular(isAdmin == false
                                                                              ? 20
                                                                              : 0))),
                                                              child: Container(
                                                                height: 80,
                                                                width: 80,
                                                                decoration: BoxDecoration(
                                                                    // image:
                                                                    //     DecorationImage(
                                                                    //   image: logoImage !=
                                                                    //           null
                                                                    //       ? FileImage(
                                                                    //           logoImage)
                                                                    //       : NetworkImage(
                                                                    //           '${groupDetails['comData']['logo']}'),
                                                                    //   fit: BoxFit
                                                                    //       .cover,
                                                                    // ),
                                                                    border: Border.all(color: Colors.grey),
                                                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20), bottomLeft: Radius.circular(isAdmin == false ? 20 : 0), bottomRight: Radius.circular(isAdmin == false ? 20 : 0))),
                                                                child: logoImage !=
                                                                        null
                                                                    ? Container(
                                                                        decoration: BoxDecoration(
                                                                            borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20), bottomLeft: Radius.circular(isAdmin == false ? 20 : 0), bottomRight: Radius.circular(isAdmin == false ? 20 : 0)),
                                                                            image: DecorationImage(
                                                                              image: FileImage(
                                                                                logoImage,
                                                                              ),
                                                                              fit: BoxFit.cover,
                                                                            )),
                                                                      )
                                                                    : Container(
                                                                        margin: EdgeInsets.only(
                                                                            left:
                                                                                0,
                                                                            top:
                                                                                0),
                                                                        height:
                                                                            50,
                                                                        // width: 50,
                                                                        //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                                        padding:
                                                                            EdgeInsets.all(0.0),
                                                                        child:
                                                                            ClipRRect(
                                                                          borderRadius:
                                                                              BorderRadius.circular(20),
                                                                          child: isLogo
                                                                              ? CircularProgressIndicator()
                                                                              : CachedNetworkImage(
                                                                                  imageUrl: "${groupDetails['logo']}",
                                                                                  placeholder: (context, url) => Center(
                                                                                      child: Text(
                                                                                    "Please Wait...",
                                                                                    textAlign: TextAlign.center,
                                                                                  )),
                                                                                  errorWidget: (context, url, error) => Image.asset("assets/images/placeholder_cover.jpg", fit: BoxFit.cover),
                                                                                  fit: BoxFit.cover,
                                                                                ),
                                                                        ),
                                                                        decoration: new BoxDecoration(
                                                                            shape:
                                                                                BoxShape.circle
                                                                            //borderRadius: BorderRadius.circular(100),
                                                                            ),
                                                                      ),
                                                              ),
                                                            ),
                                                            isAdmin == false
                                                                ? Container()
                                                                : GestureDetector(
                                                                    onTap: () {
                                                                      setState(
                                                                          () {
                                                                        _pickLogo();
                                                                      });
                                                                    },
                                                                    child: Container(
                                                                        width: 80,
                                                                        margin: EdgeInsets.only(left: 0),
                                                                        padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
                                                                        decoration: BoxDecoration(color: mainColor, borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20))),
                                                                        child: Container(
                                                                          child:
                                                                              Text(
                                                                            "Upload",
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 14,
                                                                              fontFamily: 'BebasNeue',
                                                                            ),
                                                                            textAlign:
                                                                                TextAlign.center,
                                                                          ),
                                                                        )),
                                                                  ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    setState(() {
                                                      if (isFollow == false) {
                                                        isFollow = true;
                                                        followUnfollow(1);
                                                      } else {
                                                        isFollow = false;
                                                        followUnfollow(2);
                                                      }
                                                    });
                                                  },
                                                  child: Container(
                                                      margin: EdgeInsets.only(
                                                          left: 0),
                                                      padding: EdgeInsets.only(
                                                          left: 10,
                                                          right: 15,
                                                          top: 5,
                                                          bottom: 5),
                                                      decoration: BoxDecoration(
                                                          color: isFollow ==
                                                                  false
                                                              ? mainColor
                                                              : Colors.grey
                                                                  .withOpacity(
                                                                      0.7),
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          15))),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Container(
                                                            child: Icon(
                                                                isFollow ==
                                                                        false
                                                                    ? Icons.send
                                                                    : Icons
                                                                        .remove,
                                                                color: Colors
                                                                    .white,
                                                                size: 14),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 3,
                                                                    left: 5),
                                                            child: Text(
                                                              isFollow == false
                                                                  ? "Follow"
                                                                  : "Unfollow",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 14,
                                                                fontFamily:
                                                                    'BebasNeue',
                                                              ),
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                            ),
                                                          ),
                                                        ],
                                                      )),
                                                ),
                                                // isAdmin == false
                                                //     ? Container()
                                                //     :
                                                isAdmin == false
                                                    ? Container()
                                                    : GestureDetector(
                                                        onTap: () {
                                                          setState(() {
                                                            _pickBanner();
                                                          });
                                                        },
                                                        child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 5,
                                                                    right: 5,
                                                                    top: 5,
                                                                    bottom: 5),
                                                            decoration: BoxDecoration(
                                                                color: isBanner
                                                                    ? Colors
                                                                        .white
                                                                    : mainColor,
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            15))),
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  child: isBanner
                                                                      ? CircularProgressIndicator()
                                                                      : Icon(
                                                                          Icons
                                                                              .image,
                                                                          color: Colors
                                                                              .white,
                                                                          size:
                                                                              14),
                                                                ),
                                                              ],
                                                            )),
                                                      ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ))
                                ],
                              ),

                              ////// <<<<< Group by >>>>> //////
                              Container(
                                padding:
                                    EdgeInsets.only(top: 0, left: 10, right: 0),
                                alignment: Alignment.centerLeft,
                                height: 30,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.3)),
                                child: Text(
                                  "Group by Sadek Hossain",
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 15,
                                      fontFamily: 'Oswald',
                                      fontWeight: FontWeight.w400),
                                ),
                              ),

                              ////// <<<<< About Button >>>>> //////
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                    margin:
                                        EdgeInsets.only(top: 15, bottom: 20),
                                    alignment: Alignment.centerLeft,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        // color: Colors.red
                                        ),
                                    child: Column(
                                      children: <Widget>[
                                        ////// <<<<< Group Title >>>>> //////
                                        GestureDetector(
                                          onTap: () {
                                            // Navigator.push(
                                            //     context,
                                            //     MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             GroupAboutPage()));
                                          },
                                          child: Container(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    groupDetails['name'],
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 23,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                                // Container(
                                                //     child: Icon(
                                                //   Icons.keyboard_arrow_right,
                                                //   color: Colors.black54,
                                                //   size: 27,
                                                // ))
                                              ],
                                            ),
                                          ),
                                        ),

                                        ////// <<<<< Group Members >>>>> //////
                                        Container(
                                          child: Text(
                                            memberList == null
                                                ? "Secret Group - 0 Memeber"
                                                : "Secret Group - ${memberList.length} Memeber",
                                            style: TextStyle(
                                                color: Colors.black45,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                      ],
                                    )),
                              ),
//////

                              memberList == null
                                  ? Container()
                                  : Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          ////// <<<<< Group Members Photo Button >>>>> //////
                                          GestureDetector(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          GroupMemberPage(widget.id, memberList.length, isAdmin)));
                                            },
                                            child: Container(
                                              width: memberList.length == 1
                                                  ? 40
                                                  : memberList.length == 2
                                                      ? 72
                                                      : memberList.length == 3
                                                          ? 104
                                                          : memberList.length ==
                                                                  4
                                                              ? 132
                                                              : 160,
                                              margin:
                                                  EdgeInsets.only(right: 10),
                                              alignment: Alignment.centerRight,
                                              // color: Colors.blue,
                                              child: Stack(
                                                children: <Widget>[
                                                  memberList == null
                                                      ? CircularProgressIndicator()
                                                      : memberList.length > 0
                                                          ? Container(
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              child: ClipOval(
                                                                child:
                                                                    Container(
                                                                        decoration: BoxDecoration(
                                                                            border:
                                                                                Border.all(color: Colors.grey[300], width: 1),
                                                                            shape: BoxShape.circle),
                                                                        child: Stack(
                                                                          children: <
                                                                              Widget>[
                                                                            CircleAvatar(
                                                                              backgroundColor: Colors.white,
                                                                              backgroundImage: AssetImage("assets/images/user.png"),
                                                                            ),
                                                                          ],
                                                                        )),
                                                              ),
                                                            )
                                                          : Container(),
                                                  memberList.length > 1
                                                      ? Positioned(
                                                          left: 30,
                                                          child: Container(
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: ClipOval(
                                                              child: Container(
                                                                  decoration: BoxDecoration(
                                                                      border: Border.all(
                                                                          color: Colors.grey[
                                                                              300],
                                                                          width:
                                                                              1),
                                                                      shape: BoxShape
                                                                          .circle),
                                                                  child: Stack(
                                                                    children: <
                                                                        Widget>[
                                                                      CircleAvatar(
                                                                        backgroundColor:
                                                                            Colors.white,
                                                                        backgroundImage:
                                                                            AssetImage("assets/images/user.png"),
                                                                      ),
                                                                    ],
                                                                  )),
                                                            ),
                                                          ),
                                                        )
                                                      : Container(),
                                                  memberList.length > 2
                                                      ? Positioned(
                                                          left: 60,
                                                          child: Container(
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: ClipOval(
                                                              child: Container(
                                                                  decoration: BoxDecoration(
                                                                      border: Border.all(
                                                                          color: Colors.grey.withOpacity(
                                                                              0.5),
                                                                          width:
                                                                              1),
                                                                      shape: BoxShape
                                                                          .circle),
                                                                  child: Stack(
                                                                    children: <
                                                                        Widget>[
                                                                      CircleAvatar(
                                                                        backgroundColor:
                                                                            Colors.white,
                                                                        backgroundImage:
                                                                            AssetImage("assets/images/user.png"),
                                                                      ),
                                                                    ],
                                                                  )),
                                                            ),
                                                          ),
                                                        )
                                                      : Container(),
                                                  memberList.length > 3
                                                      ? Positioned(
                                                          left: 90,
                                                          child: Container(
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: ClipOval(
                                                              child: Container(
                                                                  decoration: BoxDecoration(
                                                                      border: Border.all(
                                                                          color: Colors.grey.withOpacity(
                                                                              0.5),
                                                                          width:
                                                                              1),
                                                                      shape: BoxShape
                                                                          .circle),
                                                                  child: Stack(
                                                                    children: <
                                                                        Widget>[
                                                                      CircleAvatar(
                                                                        backgroundColor:
                                                                            Colors.white,
                                                                        backgroundImage:
                                                                            AssetImage("assets/images/user.png"),
                                                                      ),
                                                                    ],
                                                                  )),
                                                            ),
                                                          ),
                                                        )
                                                      : Container(),
                                                  memberList.length > 4
                                                      ? Positioned(
                                                          left: 120,
                                                          child: Container(
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: ClipOval(
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Image.asset(
                                                                    "assets/images/user.png",
                                                                    height: 40,
                                                                    width: 40,
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  ),
                                                                  memberList.length >
                                                                          5
                                                                      ? Positioned(
                                                                          child:
                                                                              ClipOval(
                                                                            child: Container(
                                                                                height: 40,
                                                                                width: 40,
                                                                                color: Colors.black.withOpacity(0.3),
                                                                                child: Icon(
                                                                                  Icons.more_horiz,
                                                                                  color: Colors.white,
                                                                                )),
                                                                          ),
                                                                        )
                                                                      : Container()
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      : Container(),
                                                ],
                                              ),
                                            ),
                                          ),

                                          ////// <<<<< Invite Members Button >>>>> //////

                                          GestureDetector(
                                            onTap: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        InviteGroupMemberPage(
                                                            widget.id)),
                                              );
                                            },
                                            child: Container(
                                                padding: EdgeInsets.only(
                                                    left: 10,
                                                    right: 15,
                                                    top: 10,
                                                    bottom: 10),
                                                //margin: EdgeInsets.only(left: 20, right: 20),
                                                decoration: BoxDecoration(
                                                    color: mainColor,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                15))),
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      child: Icon(Icons.add,
                                                          color: Colors.white,
                                                          size: 17),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 3),
                                                      child: Text(
                                                        "Add",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 15,
                                                          fontFamily:
                                                              'BebasNeue',
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                  ],
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),

                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: <Widget>[
                                    ////// <<<<< Profile >>>>> //////
                                    GestureDetector(
                                      onTap: () {
                                        // Navigator.push(
                                        //     context,
                                        //     MaterialPageRoute(
                                        //         builder: (context) =>
                                        //             //FriendsProfilePage("prabal23")),
                                        //             MyProfilePage(userData)));
                                      },
                                      child: Container(
                                        child: Stack(
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 0, left: 15),
                                              padding: EdgeInsets.all(1.0),
                                              child: CircleAvatar(
                                                radius: 21.0,
                                                backgroundColor:
                                                    Colors.transparent,
                                                backgroundImage: AssetImage(
                                                    'assets/images/prabal.jpg'),
                                              ),
                                              // decoration: new BoxDecoration(
                                              //   color: Colors.grey[300],
                                              //   shape: BoxShape.circle,
                                              // ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 45),
                                              padding: EdgeInsets.all(1.0),
                                              child: CircleAvatar(
                                                radius: 5.0,
                                                backgroundColor:
                                                    Colors.greenAccent,
                                              ),
                                              decoration: new BoxDecoration(
                                                color: Colors.greenAccent,
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),

                                    ////// <<<<< Status/Photo field >>>>> //////
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          // Navigator.push(
                                          //     context,
                                          //     MaterialPageRoute(
                                          //         builder: (context) =>
                                          //             CreatePost(userData, 1)));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              bottom: 10, top: 4),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            //mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  padding: EdgeInsets.only(
                                                      left: 5, right: 5),
                                                  margin: EdgeInsets.only(
                                                      left: 0,
                                                      right: 10,
                                                      top: 5),
                                                  // decoration: BoxDecoration(
                                                  //     color: Colors.grey[100],
                                                  //     border: Border.all(
                                                  //         color: Colors.grey[200],
                                                  //         width: 0.5),
                                                  //     borderRadius: BorderRadius.all(
                                                  //         Radius.circular(25))),
                                                  child: TextField(
                                                    enabled: false,
                                                    //controller: phoneController,
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: 'Oswald',
                                                    ),
                                                    decoration: InputDecoration(
                                                      hintText:
                                                          "Write something ${userData['firstName']}...",
                                                      hintStyle: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 15,
                                                          fontFamily: 'Oswald',
                                                          fontWeight:
                                                              FontWeight.w300),
                                                      //labelStyle: TextStyle(color: Colors.white70),
                                                      contentPadding:
                                                          EdgeInsets.fromLTRB(
                                                              10.0, 1, 20.0, 1),
                                                      border: InputBorder.none,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),

                                    Container(
                                      margin: EdgeInsets.only(right: 15),
                                      child: Stack(
                                        children: <Widget>[
                                          Container(
                                              padding: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                  color: mainColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(Icons.photo,
                                                  color: mainColor)),
                                          Container(
                                              padding: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                  color: Colors.white
                                                      .withOpacity(0.8),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(Icons.photo,
                                                  color: mainColor)),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              ///////////// <<<<< END >>>>> ////////////
                              Container(
                                  margin: EdgeInsets.only(
                                      left: 15, right: 15, top: 0, bottom: 0),
                                  child: Divider()),
                              SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                scrollDirection: Axis.horizontal,
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  //margin: EdgeInsets.only(bottom: 15),
                                  decoration: BoxDecoration(
                                      //border:Border.all(color:Colors.grey, width:0.3)
                                      ),
                                  padding: EdgeInsets.all(15),
                                  child: Wrap(
                                    children: <Widget>[
                                      !isAdmin
                                          ? Container()
                                          : GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            PendingPostsPage()));
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    left: 0,
                                                    right: 5,
                                                    top: 0,
                                                    bottom: 0),
                                                padding: EdgeInsets.all(7.0),
                                                decoration: new BoxDecoration(
                                                    color: Colors.grey
                                                        .withOpacity(0.2),
                                                    border: Border.all(
                                                        width: 0.5,
                                                        color: Colors.white),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                15))),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Text(
                                                            "Pending Posts",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontFamily:
                                                                    "Oswald",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300,
                                                                fontSize: 14),
                                                          ),
                                                          pendingPostCount ==
                                                                  null
                                                              ? Container()
                                                              : Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              5),
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              2.5),
                                                                  decoration: BoxDecoration(
                                                                      color:
                                                                          mainColor,
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              25)),
                                                                  child: Text(
                                                                    pendingPostCount ==
                                                                            null
                                                                        ? "0"
                                                                        : "$pendingPostCount",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontFamily:
                                                                            "Oswald",
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                        fontSize:
                                                                            9),
                                                                  ),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: 5,
                                            right: 5,
                                            top: 0,
                                            bottom: 0),
                                        padding: EdgeInsets.all(7.0),
                                        decoration: new BoxDecoration(
                                            color: Colors.grey.withOpacity(0.2),
                                            border: Border.all(
                                                width: 0.5,
                                                color: Colors.white),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(15))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.only(left: 0),
                                              child: Text(
                                                "Watch Party",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: "Oswald",
                                                    fontWeight: FontWeight.w300,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      GroupPhotosPage()));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 5,
                                              right: 5,
                                              top: 0,
                                              bottom: 0),
                                          padding: EdgeInsets.all(7.0),
                                          decoration: new BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.2),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(15))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 0),
                                                child: Text(
                                                  "Photos",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      EventsPage()));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 5,
                                              right: 5,
                                              top: 0,
                                              bottom: 0),
                                          padding: EdgeInsets.all(7.0),
                                          decoration: new BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.2),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(15))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 0),
                                                child: Text(
                                                  "Events",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      GroupAlbumPage()));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 5,
                                              right: 15,
                                              top: 0,
                                              bottom: 0),
                                          padding: EdgeInsets.all(7.0),
                                          decoration: new BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.2),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(15))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 0),
                                                child: Text(
                                                  "Albums",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              ////// <<<<< Divider 1 >>>>> //////
                              Container(
                                  width: 50,
                                  margin: EdgeInsets.only(
                                      top: 5, left: 25, right: 25, bottom: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      color: mainColor,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 1.0,
                                          color: mainColor,
                                          //offset: Offset(6.0, 7.0),
                                        ),
                                      ],
                                      border: Border.all(
                                          width: 0.5, color: mainColor))),
                              Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 15, left: 20),
                                  child: Text(
                                    "Discussion",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.normal),
                                  )),
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 30,
                                      margin: EdgeInsets.only(
                                          top: 10, left: 20, bottom: 10),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                          color: Colors.black54,
                                          boxShadow: [
                                            BoxShadow(
                                              blurRadius: 3.0,
                                              color: Colors.black54,
                                              //offset: Offset(6.0, 7.0),
                                            ),
                                          ],
                                          border: Border.all(
                                              width: 0.5,
                                              color: Colors.black54)),
                                    ),
                                  ],
                                ),
                              ),

                              Container(
                                child: Column(
                                    children: List.generate(3, (index) {
                                  return loading == false
                                      ? Column(
                                          children: <Widget>[
                                            Container(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Container(
                                                      //color: Colors.red,
                                                      margin: EdgeInsets.only(
                                                          left: 20,
                                                          right: 20,
                                                          top: 15),
                                                      padding: EdgeInsets.only(
                                                          right: 10),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          ////// <<<<< Picture start >>>>> //////
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 10),
                                                            //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                            padding:
                                                                EdgeInsets.all(
                                                                    1.0),
                                                            child: CircleAvatar(
                                                              radius: 20.0,
                                                              backgroundColor:
                                                                  Colors.white,
                                                              backgroundImage:
                                                                  AssetImage(
                                                                      'assets/images/prabal.jpg'),
                                                            ),
                                                            decoration:
                                                                new BoxDecoration(
                                                              color: Colors
                                                                      .grey[
                                                                  300], // border color
                                                              shape: BoxShape
                                                                  .circle,
                                                            ),
                                                          ),
                                                          ////// <<<<< Picture end >>>>> //////

                                                          Expanded(
                                                            child: Container(
                                                              child: Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: <
                                                                    Widget>[
                                                                  ////// <<<<< Name start >>>>> //////
                                                                  Container(
                                                                    child: Text(
                                                                      "${userData['firstName']} ${userData['lastName']}",
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15,
                                                                          color: Colors
                                                                              .black54,
                                                                          fontFamily:
                                                                              'Oswald',
                                                                          fontWeight:
                                                                              FontWeight.w500),
                                                                    ),
                                                                  ),
                                                                  ////// <<<<< Name start >>>>> //////

                                                                  ////// <<<<< Time start >>>>> //////
                                                                  Container(
                                                                    margin: EdgeInsets
                                                                        .only(
                                                                            top:
                                                                                3),
                                                                    child: Row(
                                                                      children: <
                                                                          Widget>[
                                                                        Text(
                                                                          "Apr 8, 20",
                                                                          maxLines:
                                                                              1,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                          style: TextStyle(
                                                                              fontFamily: 'Oswald',
                                                                              fontWeight: FontWeight.w400,
                                                                              fontSize: 11,
                                                                              color: Colors.black45),
                                                                        ),
                                                                        Container(
                                                                            margin:
                                                                                EdgeInsets.only(left: 5),
                                                                            child: Icon(
                                                                              Icons.public,
                                                                              color: Colors.black45,
                                                                              size: 12,
                                                                            ))
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  ////// <<<<< Time end >>>>> //////
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  ////// <<<<< More start >>>>> //////
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        _statusModalBottomSheet(
                                                            context);
                                                      });
                                                    },
                                                    child: Container(
                                                        padding:
                                                            EdgeInsets.all(5),
                                                        margin: EdgeInsets.only(
                                                            right: 15),
                                                        child: Icon(
                                                          Icons.more_horiz,
                                                          color: Colors.black45,
                                                        )),
                                                  ),
                                                  ////// <<<<< More end >>>>> //////
                                                ],
                                              ),
                                            ),
                                            Container(
                                                margin: EdgeInsets.only(
                                                    left: 20,
                                                    right: 20,
                                                    top: 20),
                                                child: index == 0
                                                    ? Container(
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                        child: Text(
                                                          "Honesty is the best policy",
                                                          textAlign:
                                                              TextAlign.justify,
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),
                                                      )
                                                    : index == 1
                                                        ? Container(
                                                            child: Column(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerLeft,
                                                                  child: Text(
                                                                    "Tour to nearest hill",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .justify,
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .black54,
                                                                        fontWeight:
                                                                            FontWeight.w400),
                                                                  ),
                                                                ),
                                                                Container(
                                                                    //color: Colors.red,
                                                                    height: 200,
                                                                    padding:
                                                                        const EdgeInsets.all(
                                                                            0.0),
                                                                    margin: EdgeInsets
                                                                        .only(
                                                                            top:
                                                                                10),
                                                                    decoration: BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        image: DecorationImage(
                                                                            image:
                                                                                AssetImage("assets/images/f7.jpg"),
                                                                            fit: BoxFit.cover)),
                                                                    child: null),
                                                              ],
                                                            ),
                                                          )
                                                        : Container(
                                                            child: Column(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerLeft,
                                                                  child: Text(
                                                                    "Bike and Car Riding",
                                                                    textAlign:
                                                                        TextAlign
                                                                            .justify,
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .black54,
                                                                        fontWeight:
                                                                            FontWeight.w400),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  child: Column(
                                                                    children: <
                                                                        Widget>[
                                                                      Container(
                                                                        child:
                                                                            Row(
                                                                          children: <
                                                                              Widget>[
                                                                            Expanded(
                                                                              child: Container(
                                                                                  //color: Colors.red,
                                                                                  height: 150,
                                                                                  //width: 150,
                                                                                  padding: const EdgeInsets.all(0.0),
                                                                                  margin: EdgeInsets.only(top: 10, right: 5),
                                                                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                  child: null),
                                                                            ),
                                                                            Expanded(
                                                                              child: Container(
                                                                                  //color: Colors.red,
                                                                                  height: 150,
                                                                                  // width: 150,
                                                                                  padding: const EdgeInsets.all(0.0),
                                                                                  margin: EdgeInsets.only(top: 10, left: 5),
                                                                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car3.jpeg"), fit: BoxFit.cover)),
                                                                                  child: null),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        child:
                                                                            Row(
                                                                          children: <
                                                                              Widget>[
                                                                            Expanded(
                                                                              child: Container(
                                                                                  //color: Colors.red,
                                                                                  height: 150,
                                                                                  //width: 150,
                                                                                  padding: const EdgeInsets.all(0.0),
                                                                                  margin: EdgeInsets.only(top: 10, right: 5),
                                                                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car6.jpg"), fit: BoxFit.cover)),
                                                                                  child: null),
                                                                            ),
                                                                            Expanded(
                                                                              child: Container(
                                                                                  //color: Colors.red,
                                                                                  height: 150,
                                                                                  // width: 150,
                                                                                  padding: const EdgeInsets.all(0.0),
                                                                                  margin: EdgeInsets.only(top: 10, left: 5),
                                                                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike3.jpeg"), fit: BoxFit.cover)),
                                                                                  child: Container(
                                                                                    height: 150,
                                                                                    // width: 150,
                                                                                    padding: const EdgeInsets.all(0.0),
                                                                                    margin: EdgeInsets.only(top: 0, left: 0),
                                                                                    decoration: BoxDecoration(
                                                                                      color: Colors.black.withOpacity(0.6),
                                                                                      borderRadius: BorderRadius.circular(5),
                                                                                    ),
                                                                                    child: Center(
                                                                                        child: Container(
                                                                                            child: Text(
                                                                                      "+5",
                                                                                      style: TextStyle(color: Colors.white, fontSize: 22),
                                                                                    ))),
                                                                                  )),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )),
                                            Container(
                                                margin: EdgeInsets.only(
                                                    left: 20,
                                                    right: 20,
                                                    bottom: 0,
                                                    top: 10),
                                                child: Divider(
                                                  color: Colors.grey[300],
                                                )),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 20, top: 0),
                                              padding: EdgeInsets.all(0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  ////// <<<<< Like start >>>>> //////
                                                  Container(
                                                    child: Row(
                                                      children: <Widget>[
                                                        Container(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  3.0),
                                                          child: Icon(
                                                            Icons
                                                                .favorite_border,
                                                            size: 20,
                                                            color:
                                                                Colors.black54,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 3),
                                                          child: Text("0",
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      'Oswald',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300,
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize:
                                                                      12)),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  ////// <<<<< Like end >>>>> //////

                                                  ////// <<<<< Comment start >>>>> //////
                                                  GestureDetector(
                                                    onTap: () {
                                                      // Navigator.push(
                                                      //     context,
                                                      //     MaterialPageRoute(
                                                      //         builder: (context) => CommentPage(
                                                      //             userData, index)));
                                                    },
                                                    child: Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 15),
                                                            padding:
                                                                EdgeInsets.all(
                                                                    3.0),
                                                            child: Icon(
                                                              Icons
                                                                  .chat_bubble_outline,
                                                              size: 20,
                                                              color: Colors
                                                                  .black54,
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 3),
                                                            child: Text("0",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Oswald',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300,
                                                                    color: Colors
                                                                        .black54,
                                                                    fontSize:
                                                                        12)),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  ////// <<<<< Comment end >>>>> //////

                                                  ////// <<<<< Share start >>>>> //////
                                                  // GestureDetector(
                                                  //   onTap: () {
                                                  //     _shareModalBottomSheet(
                                                  //         context, index, userData);
                                                  //   },
                                                  //   child: Container(
                                                  //     child: Row(
                                                  //       children: <Widget>[
                                                  //         Container(
                                                  //           margin:
                                                  //               EdgeInsets.only(left: 15),
                                                  //           padding: EdgeInsets.all(3.0),
                                                  //           child: Icon(
                                                  //             Icons.share,
                                                  //             size: 20,
                                                  //             color: Colors.black54,
                                                  //           ),
                                                  //         ),
                                                  //         Container(
                                                  //           margin:
                                                  //               EdgeInsets.only(left: 3),
                                                  //           child: Text("0",
                                                  //               style: TextStyle(
                                                  //                   fontFamily: 'Oswald',
                                                  //                   fontWeight:
                                                  //                       FontWeight.w300,
                                                  //                   color: Colors.black54,
                                                  //                   fontSize: 12)),
                                                  //         )
                                                  //       ],
                                                  //     ),
                                                  //   ),
                                                  // ),
                                                  ////// <<<<< Share end >>>>> //////
                                                ],
                                              ),
                                            ),
                                            Container(
                                                height: 10,
                                                decoration: BoxDecoration(
                                                    color: Colors.grey
                                                        .withOpacity(0.1),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5)),
                                                margin: EdgeInsets.only(
                                                    left: 0,
                                                    right: 0,
                                                    bottom: 0,
                                                    top: 7),
                                                child: null),
                                          ],
                                        )
                                      : Container(
                                          padding: EdgeInsets.only(
                                              top: 20, bottom: 10),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            //border: Border.all(width: 0.8, color: Colors.grey[300]),
                                            // boxShadow: [
                                            //   BoxShadow(
                                            //     blurRadius: 1.0,
                                            //     color: Colors.black38,
                                            //     //offset: Offset(6.0, 7.0),
                                            //   ),
                                            // ],
                                          ),
                                          margin: EdgeInsets.only(
                                              top: 5,
                                              bottom: 5,
                                              left: 10,
                                              right: 10),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Container(
                                                    //color: Colors.red,
                                                    margin: EdgeInsets.only(
                                                        left: 10,
                                                        right: 10,
                                                        top: 0),
                                                    padding: EdgeInsets.only(
                                                        right: 10),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 10),
                                                          //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                          padding:
                                                              EdgeInsets.all(
                                                                  1.0),
                                                          child: Shimmer
                                                              .fromColors(
                                                            baseColor: Colors
                                                                .grey[100],
                                                            highlightColor:
                                                                Colors
                                                                    .grey[200],
                                                            child: CircleAvatar(
                                                              radius: 20.0,
                                                              //backgroundColor: Colors.white,
                                                            ),
                                                          ),
                                                          decoration:
                                                              new BoxDecoration(
                                                            shape:
                                                                BoxShape.circle,
                                                          ),
                                                        ),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Shimmer.fromColors(
                                                              baseColor: Colors
                                                                  .grey[100],
                                                              highlightColor:
                                                                  Colors.grey[
                                                                      200],
                                                              child: Container(
                                                                width: 100,
                                                                height: 22,
                                                                child:
                                                                    Container(
                                                                  color: Colors
                                                                      .black,
                                                                ),
                                                              ),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(top: 3),
                                                              child: Shimmer
                                                                  .fromColors(
                                                                baseColor:
                                                                    Colors.grey[
                                                                        100],
                                                                highlightColor:
                                                                    Colors.grey[
                                                                        200],
                                                                child:
                                                                    Container(
                                                                  width: 50,
                                                                  height: 12,
                                                                  child:
                                                                      Container(
                                                                    color: Colors
                                                                        .black,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          left: 20,
                                                          right: 20,
                                                          top: 20,
                                                          bottom: 0),
                                                      child: Shimmer.fromColors(
                                                        baseColor:
                                                            Colors.grey[100],
                                                        highlightColor:
                                                            Colors.grey[200],
                                                        child: Container(
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          height: 10,
                                                          child: Container(
                                                            color: Colors.black,
                                                          ),
                                                        ),
                                                      )),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          left: 20,
                                                          right: 20,
                                                          top: 2,
                                                          bottom: 5),
                                                      child: Shimmer.fromColors(
                                                        baseColor:
                                                            Colors.grey[100],
                                                        highlightColor:
                                                            Colors.grey[200],
                                                        child: Container(
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width -
                                                              100,
                                                          height: 10,
                                                          child: Container(
                                                            color: Colors.black,
                                                          ),
                                                        ),
                                                      )),
                                                ],
                                              ),
                                              Container(
                                                  height: 10,
                                                  decoration: BoxDecoration(
                                                      color: Colors.grey
                                                          .withOpacity(0.1),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  margin: EdgeInsets.only(
                                                      left: 0,
                                                      right: 0,
                                                      bottom: 0,
                                                      top: 7),
                                                  child: null),
                                            ],
                                          ),
                                        );
                                })),
                              )
                            ],
                          ),
                        ),
                      ),
                      //Posts card
                      //CreatePostCard()
                    ],
                  ),
                ),
        ],
      ),
    );
  }

  void _statusModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                // Text('React to this post',
                //       style: TextStyle(fontWeight: FontWeight.normal)),
                new ListTile(
                  leading: new Icon(Icons.edit),
                  title: new Text('Edit',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => CreatePost()))
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  title: new Text('Delete',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.redAccent,
                          fontFamily: "Oswald")),
                  onTap: () => {Navigator.pop(context), _showDeleteDialog()},
                ),
              ],
            ),
          );
        });
  }

  Future<Null> _showDeleteDialog() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          "Want to delete the post?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _shareModalBottomSheet(context, int index, var userData) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Container(
                //height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.share,
                              color: Colors.black54,
                              size: 16,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 5),
                              child: Text(
                                "Share this post",
                                style: TextStyle(fontFamily: "Oswald"),
                              ),
                            ),
                          ],
                        )),
                    Divider(),
                    Container(
                      child: Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              width: 50,
                              height: 50,
                              margin: EdgeInsets.only(top: 0, left: 20),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/prabal.jpg"),
                                      fit: BoxFit.cover),
                                  border: Border.all(
                                      color: Colors.grey, width: 0.1),
                                  borderRadius: BorderRadius.circular(15)),
                            ),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                      alignment: Alignment.centerLeft,
                                      margin:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(
                                              "${userData['firstName']} ${userData['lastName']}",
                                              maxLines: 1,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14,
                                                  fontFamily: 'Oswald',
                                                  fontWeight:
                                                      FontWeight.normal),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            //_securityModalBottomSheet(context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 0, right: 5, left: 10),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 0.3,
                                                    color: Colors.black54),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Icon(
                                                    Icons.public,
                                                    size: 12,
                                                    color: Colors.black54,
                                                  ),
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Public",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                          fontFamily: "Oswald"),
                                                    )),
                                                Icon(
                                                  Icons.arrow_drop_down,
                                                  size: 25,
                                                  color: Colors.black54,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //height: 150,
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.only(
                          top: 20, left: 20, bottom: 5, right: 20),
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.only(
                            topLeft: Radius.circular(5.0),
                            topRight: Radius.circular(5.0),
                            bottomLeft: Radius.circular(5.0),
                            bottomRight: Radius.circular(5.0)),
                        //color: Colors.grey[100],
                        //border: Border.all(width: 0.2, color: Colors.grey)
                      ),
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(minHeight: 100.0, maxHeight: 100.0),
                        child: new SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          //reverse: true,
                          child: Container(
                            //height: 100,
                            child: new TextField(
                              maxLines: null,
                              autofocus: false,
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: 'Oswald',
                              ),
                              //controller: msgController,
                              decoration: InputDecoration(
                                alignLabelWithHint: true,
                                hintText: "Write something...",
                                hintStyle: TextStyle(
                                    color: Colors.black54,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w300),
                                contentPadding:
                                    EdgeInsets.fromLTRB(0.0, 10.0, 20.0, 10.0),
                                border: InputBorder.none,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  //sharePost = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              //snackBar(context);
                              //isSubmit == false ? statusShare(feeds.id) : null;
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 20, top: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.only(
                                          left: 20, right: 20, top: 0),
                                      decoration: BoxDecoration(
                                          color: mainColor,
                                          border: Border.all(
                                              color: Colors.grey, width: 0.5),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: Text(
                                        "Share",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontFamily: 'BebasNeue',
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      //height: MediaQuery.of(context).size.height / 2.05,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        //border: Border.all(width: 0.8, color: Colors.grey[300]),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 16.0,
                            color: Colors.grey[300],
                            //offset: Offset(3.0, 4.0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(
                          top: 5, bottom: 15, left: 10, right: 10),
                      child: Container(
                        //color: Colors.yellow,
                        margin:
                            EdgeInsets.only(left: 20, right: 20, bottom: 10),
                        padding: EdgeInsets.only(right: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< pic start >>>>> //////
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    padding: EdgeInsets.all(1.0),
                                    child: CircleAvatar(
                                      radius: 20.0,
                                      backgroundColor: Colors.white,
                                      backgroundImage:
                                          AssetImage("assets/images/man2.jpg"),
                                    ),
                                    decoration: new BoxDecoration(
                                      color: Colors.grey[300], // border color
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                  ////// <<<<< pic end >>>>> //////

                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ////// <<<<< Name & Interest start >>>>> //////
                                          Container(
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    "John Smith",
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        color: Colors.black,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                          ////// <<<<< Name & Interest end >>>>> //////

                                          ////// <<<<< time job start >>>>> //////
                                          Container(
                                            margin: EdgeInsets.only(top: 3),
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    "Aug 7 at 5:34 PM",
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 11,
                                                        color: Colors.black54),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 3),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Icon(
                                                          Icons.public,
                                                          size: 12,
                                                          color: Colors.black54,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          ////// <<<<< time job end >>>>> //////
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                                margin:
                                    EdgeInsets.only(left: 0, right: 0, top: 20),
                                child: index == 0
                                    ? Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Text(
                                          "Honesty is the best policy",
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              color: Colors.black54,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      )
                                    : index == 1
                                        ? Container(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                    "Tour to nearest hill",
                                                    textAlign:
                                                        TextAlign.justify,
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                                Container(
                                                    //color: Colors.red,
                                                    height: 200,
                                                    padding:
                                                        const EdgeInsets.all(
                                                            0.0),
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        image: DecorationImage(
                                                            image: AssetImage(
                                                                "assets/images/f7.jpg"),
                                                            fit: BoxFit.cover)),
                                                    child: null),
                                              ],
                                            ),
                                          )
                                        : Container(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                    "Bike and Car Riding",
                                                    textAlign:
                                                        TextAlign.justify,
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                                Container(
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 150,
                                                                  //width: 150,
                                                                  padding:
                                                                      const EdgeInsets.all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          right:
                                                                              5),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              "assets/images/bike1.jpg"),
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                  child: null),
                                                            ),
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 150,
                                                                  // width: 150,
                                                                  padding:
                                                                      const EdgeInsets.all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          left:
                                                                              5),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              "assets/images/car3.jpeg"),
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                  child: null),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 150,
                                                                  //width: 150,
                                                                  padding:
                                                                      const EdgeInsets.all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          right:
                                                                              5),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              "assets/images/car6.jpg"),
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                  child: null),
                                                            ),
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 150,
                                                                  // width: 150,
                                                                  padding:
                                                                      const EdgeInsets.all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          left:
                                                                              5),
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      image: DecorationImage(
                                                                          image: AssetImage(
                                                                              "assets/images/bike3.jpeg"),
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                  child:
                                                                      Container(
                                                                    height: 150,
                                                                    // width: 150,
                                                                    padding:
                                                                        const EdgeInsets.all(
                                                                            0.0),
                                                                    margin: EdgeInsets.only(
                                                                        top: 0,
                                                                        left:
                                                                            0),
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      color: Colors
                                                                          .black
                                                                          .withOpacity(
                                                                              0.6),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                    ),
                                                                    child: Center(
                                                                        child: Container(
                                                                            child: Text(
                                                                      "+5",
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              22),
                                                                    ))),
                                                                  )),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )),
                          ],
                        ),
                      ),
                    )
                  ],
                )),
          );
        });
  }
}
