import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:social_app_fb/MainScreens/GroupDetailsPage/groupDetailsPage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import '../../main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/ModelClass/GroupModel/GroupModel.dart';

class MyGroups extends StatefulWidget {
  @override
  _MyGroupsState createState() => _MyGroupsState();
}

class _MyGroupsState extends State<MyGroups> {
  TextEditingController searchController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController aboutController = new TextEditingController();
  String status = "Public", groupDate = "";
  int i = 0;
  var userData, groupData;
  bool isLoading = false;
  bool loading = false;
  List groupList = [];

  @override
  void initState() {
    setState(() {
      DateTime date1 = DateTime.now();

      groupDate = DateFormat("yyyy-MM-dd").format(date1);
    });
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);

      loadRequests();
    }
  }

  Future loadRequests() async {
    setState(() {
      loading = true;
    });

    var reqresponse = await CallApi().getData2('user/get/groups');
    var reqcontent = reqresponse.body;
    final req = json.decode(reqcontent);
    //var reqdata = GroupModel.fromJson(req);
    //print(reqdata);

    setState(() {
      groupData = req;

      for (int i = 0; i < groupData.length; i++) {
        groupList.add(
          {
            'id': groupData[i]['group']['id'],
            'pic': groupData[i]['group']['logo'],
            'name': groupData[i]['group']['name'],
            'members': 0,
          },
        );
      }
    });

    setState(() {
      loading = false;
    });
    print("groupData.length");
    print(groupData.length);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    margin:
                        EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
                    child: Column(
                      children: <Widget>[
                        ////// <<<<< Friend Number >>>>> //////
                        Container(
                          margin: EdgeInsets.only(top: 12, left: 20, bottom: 7),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(right: 0, bottom: 3),
                                  child: Icon(Icons.supervised_user_circle,
                                      size: 17, color: Colors.black54)),
                              Container(
                                  margin: EdgeInsets.only(bottom: 3),
                                  child: Text(
                                    " Groups ",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Colors.black45,
                                        fontSize: 13,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w400),
                                  )),
                              loading
                                  ? Container(
                                      height: 30,
                                      margin: EdgeInsets.only(bottom: 5),
                                      child:
                                          SpinKitCircle(color: Colors.black26))
                                  : Container(
                                      child: Text(
                                      groupData == null
                                          ? "0"
                                          : "${groupData.length}",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 18,
                                          fontFamily: 'Oswald',
                                          fontWeight: FontWeight.w400),
                                    )),
                            ],
                          ),
                        ),

                        ////// <<<<< Divider 5 >>>>> //////
                        Row(
                          children: <Widget>[
                            Container(
                              width: 40,
                              margin:
                                  EdgeInsets.only(top: 0, left: 20, bottom: 12),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0)),
                                  color: Colors.black54,
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 3.0,
                                      color: Colors.black54,
                                      //offset: Offset(6.0, 7.0),
                                    ),
                                  ],
                                  border: Border.all(
                                      width: 0.5, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  loading
                      ? LoaderScreen()
                      : Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Wrap(
                            children: List.generate(groupList.length, (index) {
                              return Column(
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  GroupDetailsPage(
                                                      groupList[index]['id'])));
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(
                                          left: 10, right: 10, top: 0),
                                      decoration: BoxDecoration(
                                          //border: Border.all(width: 0.4, color: Colors.grey),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: Colors.white),
                                      child: Stack(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                alignment: Alignment.center,
                                                margin: EdgeInsets.only(
                                                    top: 10,
                                                    left: 10,
                                                    bottom: 10),
                                                padding: EdgeInsets.all(1.0),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: Colors.grey
                                                              .withOpacity(0.5),
                                                          width: 2),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100)),
                                                  child: Container(
                                                    height: 40,
                                                    width: 40,
                                                    margin: EdgeInsets.only(
                                                        right: 0),
                                                    padding:
                                                        EdgeInsets.all(1.0),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100),
                                                      child: CachedNetworkImage(
                                                        imageUrl:
                                                            groupList[index]
                                                                ['pic'],
                                                        placeholder: (context,
                                                                url) =>
                                                            SpinKitRing(
                                                                color: Colors
                                                                    .grey),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Image.asset(
                                                          "assets/images/placeholder_cover.jpg",
                                                          fit: BoxFit.cover,
                                                          //height: 40,
                                                        ),
                                                        fit: BoxFit.cover,

                                                        // NetworkImage(
                                                        //     widget.friend[index].profilePic
                                                      ),
                                                    ),
                                                    decoration:
                                                        new BoxDecoration(
                                                      //color: Colors.grey[300],
                                                      shape: BoxShape.circle,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: Container(
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Container(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            10),
                                                                child: Text(
                                                                  groupList[
                                                                          index]
                                                                      ['name'],
                                                                  maxLines: 1,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .black54,
                                                                      fontFamily:
                                                                          "Oswald",
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      fontSize:
                                                                          15),
                                                                ),
                                                              ),
                                                              Container(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            10,
                                                                        top: 4),
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Text(
                                                                      "${groupList[index]['members']}+ Members",
                                                                      style: TextStyle(
                                                                          color:
                                                                              mainColor,
                                                                          fontFamily:
                                                                              "Oswald",
                                                                          fontWeight: FontWeight
                                                                              .w500,
                                                                          fontSize:
                                                                              11),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                          margin:
                                                              EdgeInsets.all(
                                                                  10),
                                                          child: Icon(
                                                            Icons.more_vert,
                                                            color:
                                                                Colors.black38,
                                                            size: 20,
                                                          )),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                      margin:
                                          EdgeInsets.only(left: 20, right: 20),
                                      child: Divider())
                                ],
                              );
                            }),
                          ),
                        )
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 15, top: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  _showCreateDialog();
                },
                child: Container(
                    //width: 30.0,
                    padding: EdgeInsets.all(8),
                    height: 30.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: mainColor),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.add,
                          color: Colors.white,
                          size: 16,
                        ),
                        Text(
                          "Create",
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        )
                      ],
                    )),
              ),
            ],
          ),
        )
      ],
    );
  }

  Future<Null> _showCreateDialog() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              content: Builder(
                builder: (context) {
                  // Get available height and width of the build area of this widget. Make a choice depending on the size.
                  var height = MediaQuery.of(context).size.height;
                  var width = MediaQuery.of(context).size.width;
                  return Container(
                    height: height - 270,
                    width: width - 100,
                    child: Center(
                      child: SingleChildScrollView(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10, left: 0),
                                  child: Text(
                                    "Group Name",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.normal),
                                  )),
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 30,
                                      margin: EdgeInsets.only(
                                          top: 10, left: 0, bottom: 10),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                          color: Colors.black,
                                          boxShadow: [
                                            BoxShadow(
                                              blurRadius: 3.0,
                                              color: Colors.black,
                                              //offset: Offset(6.0, 7.0),
                                            ),
                                          ],
                                          border: Border.all(
                                              width: 0.5, color: Colors.black)),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 10, top: 5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  //mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(5),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 0),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.5),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Icon(
                                                    Icons.people_outline,
                                                    color: Colors.black45,
                                                    size: 20)),
                                            Flexible(
                                              child: TextField(
                                                onChanged: (value) {},
                                                controller: nameController,
                                                keyboardType:
                                                    TextInputType.emailAddress,
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Oswald',
                                                ),
                                                decoration: InputDecoration(
                                                  hintText: "Type Group Name",
                                                  hintStyle: TextStyle(
                                                      color: Colors.black45,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                  //labelStyle: TextStyle(color: Colors.white70),
                                                  contentPadding:
                                                      EdgeInsets.fromLTRB(
                                                          10.0, 2.5, 20.0, 2.5),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10, left: 0),
                                  child: Text(
                                    "About Group",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.normal),
                                  )),
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 30,
                                      margin: EdgeInsets.only(
                                          top: 10, left: 0, bottom: 10),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                          color: Colors.black,
                                          boxShadow: [
                                            BoxShadow(
                                              blurRadius: 3.0,
                                              color: Colors.black,
                                              //offset: Offset(6.0, 7.0),
                                            ),
                                          ],
                                          border: Border.all(
                                              width: 0.5, color: Colors.black)),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 10, top: 5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  //mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(5),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 0),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.5),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                                margin: EdgeInsets.only(
                                                    left: 5, top: 5),
                                                child: Icon(Icons.info_outline,
                                                    color: Colors.black45,
                                                    size: 20)),
                                            Flexible(
                                              child: TextField(
                                                //maxLength: 300,
                                                maxLines: 6,
                                                onChanged: (value) {},
                                                controller: aboutController,
                                                keyboardType:
                                                    TextInputType.emailAddress,
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Oswald',
                                                ),
                                                decoration: InputDecoration(
                                                  hintText: "Type Something...",
                                                  hintStyle: TextStyle(
                                                      color: Colors.black45,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                  //labelStyle: TextStyle(color: Colors.white70),
                                                  contentPadding:
                                                      EdgeInsets.fromLTRB(
                                                          10.0, 2.5, 20.0, 2.5),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    i++;
                                    if (i % 2 == 0) {
                                      status = "Public";
                                    } else {
                                      status = "Private";
                                    }

                                    print(status);
                                  });
                                },
                                child: new Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: Row(
                                    children: <Widget>[
                                      Icon(
                                        status == "Private"
                                            ? Icons.lock_outline
                                            : Icons.public,
                                        size: 20,
                                        color: Colors.black45,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Text(status,
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.black45,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: "Oswald")),
                                        ),
                                      ),
                                      status != "Private"
                                          ? Icon(Icons.done, color: mainColor)
                                          : Icon(Icons.done,
                                              color: Colors.transparent),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Container(
                                            padding: EdgeInsets.all(10),
                                            margin: EdgeInsets.only(
                                                left: 0,
                                                right: 5,
                                                top: 20,
                                                bottom: 0),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                    color: Colors.grey,
                                                    width: 0.2),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            child: Text(
                                              "CANCEL",
                                              style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontFamily: 'BebasNeue',
                                              ),
                                              textAlign: TextAlign.center,
                                            )),
                                      ),
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          groupCreate();
                                        },
                                        child: Container(
                                            padding: EdgeInsets.all(10),
                                            margin: EdgeInsets.only(
                                                left: 5,
                                                right: 0,
                                                top: 20,
                                                bottom: 0),
                                            decoration: BoxDecoration(
                                                color: mainColor,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            child: Text(
                                              isLoading
                                                  ? "Please wait.."
                                                  : "CREATE",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontFamily: 'BebasNeue',
                                              ),
                                              textAlign: TextAlign.center,
                                            )),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            );
          },
        );
      },
    );
  }

  Future groupCreate() async {
    if (nameController.text.isEmpty) {
      _showMessage("Group Name shouldn't be empty!", 1);
    } else if (aboutController.text.isEmpty) {
      _showMessage("About shouldn't be empty!", 1);
    } else {
      setState(() {
        isLoading = true;
      });
      var data = {
        'name': nameController.text,
        'description': aboutController.text,
        'privacy': status,
        'creator_id': userData['id'],
      };

      print(data);

      var res = await CallApi().postData1(data, 'group/create/new');
      var body = json.decode(res.body);
      print(body);

      if (res.statusCode == 200) {
        DateTime dateTime = DateTime.parse(body['created_at']);
        String date = DateFormat.yMMMd().format(dateTime);

        setState(() {
          groupList.add(
            {
              'id': body['id'],
              'pic': body['logo'],
              'name': body['name'],
              'members': 0,
            },
          );
        });
        Navigator.pop(context);
        // Navigator.push(
        //     context, MaterialPageRoute(builder: (context) => AlbumPage()));
      } else {
        _showMessage("Something went wrong!", 1);
      }

      setState(() {
        isLoading = false;
      });
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
