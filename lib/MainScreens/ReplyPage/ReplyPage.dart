import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Lists/ReplyList/ReplyList.dart';
import 'package:social_app_fb/MainScreens/CommentPage/CommentPage.dart';
import 'package:social_app_fb/ModelClass/ReplyModel/ReplyModel.dart';
import 'package:social_app_fb/main.dart';

class ReplyPage extends StatefulWidget {
  final commentList;
  final index;
  final userData;
  final statusID;

  ReplyPage(this.commentList, this.index, this.userData, this.statusID);

  @override
  _ReplyPageState createState() => _ReplyPageState();
}

class _ReplyPageState extends State<ReplyPage> {
  TextEditingController repEditor = new TextEditingController();
  TextEditingController editingController = new TextEditingController();
  String reply = "", date = "";
  bool loading = false;
  var repList;
  List replyList = [];

  @override
  void initState() {
    loadReply();
    super.initState();
  }

  Future loadReply() async {
    //await Future.delayed(Duration(seconds: 3));

    setState(() {
      loading = true;
    });
    var represponse = await CallApi().getData(
        'statusCommentReply/show?status_id=${widget.statusID}&comment_id=${widget.commentList['id']}');
    var postcontent = represponse.body;
    final reply = json.decode(postcontent);
    var repdata = ReplyModel.fromJson(reply);

    setState(() {
      repList = repdata;
      loading = false;
    });
    for (int i = 0; i < repList.statusReply.length; i++) {
      replyList.add({
        'id': "${repList.statusReply[i].id}",
        'replyTxt': repList.statusReply[i].replyText,
        'uId': repList.statusReply[i].user.id,
        'pic': repList.statusReply[i].user.profilePic,
        'fName': repList.statusReply[i].user.firstName,
        'lName': repList.statusReply[i].user.lastName,
        'created': repList.statusReply[i].createdAt,
        'myLike': repList.statusReply[i].like,
        'totalLike': repList.statusReply[i].meta.totalLikesCount,
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Reply",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          replyList.length == 0
              ? Center(
                  child: Container(
                    child: Text(
                      "No replies",
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.black,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                )
              : SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Container(
                    margin: EdgeInsets.only(bottom: 50),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(
                              left: 20, right: 20, bottom: 0, top: 20),
                          padding: EdgeInsets.only(right: 0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              ////// <<<<< pic start >>>>> //////
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                padding: EdgeInsets.all(1.0),
                                child: CircleAvatar(
                                  radius: 15.0,
                                  backgroundColor: Colors.white,
                                  backgroundImage:
                                      AssetImage("assets/images/prabal.jpg"),
                                ),
                                decoration: new BoxDecoration(
                                  color: Colors.grey[300],
                                  shape: BoxShape.circle,
                                ),
                              ),
                              ////// <<<<< pic end >>>>> //////

                              Expanded(
                                child: Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      ////// <<<<< Name start >>>>> //////
                                      Container(
                                        child: Text(
                                          "${widget.commentList['fName']} ${widget.commentList['lName']}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: mainColor,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),

                                      ////// <<<<< Name end >>>>> //////
                                      Container(
                                          margin: EdgeInsets.only(
                                            left: 0,
                                            right: 20,
                                            top: 3,
                                          ),
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              "${widget.commentList['comment']}",
                                              textAlign: TextAlign.justify,
                                              style: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.black,
                                                  fontFamily: "Oswald",
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                              children:
                                  List.generate(replyList.length, (index) {
                            DateTime dateTime =
                                DateTime.parse(replyList[index]['created']);
                            date = DateFormat.yMMMd().format(dateTime);
                            return Container(
                              padding: EdgeInsets.only(top: 0, bottom: 5),
                              margin: EdgeInsets.only(
                                  top: 0, bottom: 0, left: 20, right: 10),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(right: 0),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                ////// <<<<< Name start >>>>> //////
                                                Container(
                                                  //color: Colors.yellow,
                                                  margin: EdgeInsets.only(
                                                      left: 40,
                                                      right: 0,
                                                      bottom: 0,
                                                      top: 20),
                                                  padding: EdgeInsets.only(
                                                      right: 10),
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      ////// <<<<< pic start >>>>> //////
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            right: 10),
                                                        //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                        padding:
                                                            EdgeInsets.all(1.0),
                                                        child: CircleAvatar(
                                                          radius: 12.0,
                                                          backgroundColor:
                                                              Colors.white,
                                                          backgroundImage:
                                                              AssetImage(
                                                                  "assets/images/prabal.jpg"),
                                                        ),
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: Colors.grey[
                                                              300], // border color
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                      ),
                                                      ////// <<<<< pic end >>>>> //////

                                                      Expanded(
                                                        child: Container(
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              ////// <<<<< Name start >>>>> //////
                                                              Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                    child:
                                                                        Container(
                                                                      child:
                                                                          Text(
                                                                        "${replyList[index]['fName']} ${replyList[index]['lName']}",
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                13,
                                                                            color:
                                                                                mainColor,
                                                                            fontFamily:
                                                                                'Oswald',
                                                                            fontWeight:
                                                                                FontWeight.bold),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  widget.userData[
                                                                              'id'] !=
                                                                          replyList[index]
                                                                              [
                                                                              'uId']
                                                                      ? Container()
                                                                      : GestureDetector(
                                                                          onTap:
                                                                              () {
                                                                            setState(() {
                                                                              _statusModalBottomSheet(context, index, widget.userData, replyList[index]);
                                                                              reply = replyList[index]['replyTxt'];
                                                                              editingController.text = replyList[index]['replyTxt'];
                                                                            });
                                                                          },
                                                                          child: Container(
                                                                              margin: EdgeInsets.only(right: 15),
                                                                              // color: Colors.blue,
                                                                              child: Icon(
                                                                                Icons.more_horiz,
                                                                                color: Colors.black54,
                                                                              )),
                                                                        ),
                                                                ],
                                                              ),

                                                              ////// <<<<< Name end >>>>> //////
                                                              Container(
                                                                  margin:
                                                                      EdgeInsets
                                                                          .only(
                                                                    left: 0,
                                                                    right: 0,
                                                                    top: 5,
                                                                  ),
                                                                  child: Column(
                                                                    children: <
                                                                        Widget>[
                                                                      ////// <<<<< Comment start >>>>> //////
                                                                      Container(
                                                                        margin: EdgeInsets.only(
                                                                            bottom:
                                                                                0),
                                                                        alignment:
                                                                            Alignment.centerLeft,
                                                                        child:
                                                                            Container(
                                                                          child:
                                                                              Text(
                                                                            "${replyList[index]['replyTxt']}",
                                                                            textAlign:
                                                                                TextAlign.justify,
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                            maxLines:
                                                                                1,
                                                                            style: TextStyle(
                                                                                color: Colors.black,
                                                                                fontSize: 12,
                                                                                fontFamily: "Oswald",
                                                                                fontWeight: FontWeight.w400),
                                                                          ),
                                                                        ),
                                                                      ),

                                                                      ////// <<<<< Comment end >>>>> //////
                                                                    ],
                                                                  )),
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left: 0,
                                                                        top: 5),
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .start,
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              0),
                                                                      child: Text(
                                                                          date,
                                                                          style: TextStyle(
                                                                              fontFamily: 'Oswald',
                                                                              fontWeight: FontWeight.w300,
                                                                              color: Colors.black54,
                                                                              fontSize: 10)),
                                                                    ),
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              15),
                                                                      child:
                                                                          Row(
                                                                        children: <
                                                                            Widget>[
                                                                          Container(
                                                                            padding:
                                                                                EdgeInsets.all(3.0),
                                                                            child:
                                                                                GestureDetector(
                                                                              onTap: () {
                                                                                if (replyList[index]['myLike'] != null) {
                                                                                  setState(() {
                                                                                    replyList[index]['myLike'] = null;
                                                                                    replyList[index]['totalLike']--;
                                                                                  });
                                                                                  likeUnlike(index, 2, replyList[index]['id']);
                                                                                } else {
                                                                                  setState(() {
                                                                                    replyList[index]['myLike'] = 1;
                                                                                    replyList[index]['totalLike']++;
                                                                                  });
                                                                                  likeUnlike(index, 1, replyList[index]['id']);
                                                                                }
                                                                              },
                                                                              child: Icon(
                                                                                replyList[index]['myLike'] == null ? Icons.favorite_border : Icons.favorite,
                                                                                size: 16,
                                                                                color: replyList[index]['myLike'] == null ? Colors.black54 : Colors.redAccent,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            margin:
                                                                                EdgeInsets.only(left: 3),
                                                                            child:
                                                                                Text("${replyList[index]['totalLike']}", style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w300, color: Colors.black54, fontSize: 10)),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              Divider()
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          })),
                        )
                      ],
                    ),
                  ),
                ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  color: Colors.white,
                  child: Row(
                    children: <Widget>[
                      ////// <<<<< Reply Box Text Field >>>>> //////
                      Flexible(
                        child: Container(
                          //height: 100,
                          padding: EdgeInsets.all(0),
                          margin: EdgeInsets.only(
                              top: 5, left: 10, bottom: 5, right: 10),
                          decoration: BoxDecoration(
                              // borderRadius:
                              //     BorderRadius.all(Radius.circular(100.0)),
                              borderRadius: new BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  topRight: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                  bottomRight: Radius.circular(20.0)),
                              color: Colors.grey[100],
                              border: Border.all(
                                  width: 0.5,
                                  color: Colors.black.withOpacity(0.2))),
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: new ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxHeight: 120.0,
                                  ),
                                  child: new SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    reverse: true,
                                    child: new TextField(
                                      maxLines: null,
                                      autofocus: false,
                                      controller: repEditor,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Oswald',
                                      ),
                                      decoration: InputDecoration(
                                        hintText: "Type a reply here...",
                                        hintStyle: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 15,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300),
                                        contentPadding: EdgeInsets.fromLTRB(
                                            10.0, 10.0, 20.0, 10.0),
                                        border: InputBorder.none,
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          reply = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),

                      ////// <<<<< Comment Send Icon Button >>>>> //////
                      GestureDetector(
                        onTap: () {
                          addReply(widget.index);
                          setState(() {
                            repEditor.text = "";
                            replyList.add({
                              'id': null,
                              'replyTxt': reply,
                              'uId': widget.userData['id'],
                              'pic': widget.userData['profilePic'],
                              'fName': widget.userData['firstName'],
                              'lName': widget.userData['lastName'],
                              'created': widget.userData['created_at'],
                              'myLike': null,
                              'totalLike': 0,
                            });
                            //repComCount[widget.index]['count'] += 1;
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: back,
                              borderRadius: BorderRadius.circular(25)),
                          margin: EdgeInsets.only(right: 10),
                          padding: EdgeInsets.all(8),
                          child: Icon(
                            Icons.send,
                            color: mainColor,
                            size: 23,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void addReply(index) {
    if (repEditor.text.isEmpty) {
    } else {
      commentSend(index);
    }
  }

  Future commentSend(index) async {
    var data = {
      'reply_text': repEditor.text,
      'status_id': widget.statusID,
      'user_id': widget.userData['id'],
      'comment_id': widget.commentList['id']
    };

    print(data);

    var res = await CallApi().postData1(data, 'statusCommentReply/add');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      setState(() {
        repEditor.text = "";
        repComCount[index]['count'] += 1;
        replyList.removeAt(replyList.length - 1);
        replyList.add({
          'id': "${body['addStatusCmntreply']['id']}",
          'replyTxt': body['addStatusCmntreply']['reply_text'],
          'uId': body['addStatusCmntreply']['user']['id'],
          'pic': body['addStatusCmntreply']['user']['profilePic'],
          'fName': body['addStatusCmntreply']['user']['firstName'],
          'lName': body['addStatusCmntreply']['user']['lastName'],
          'created': body['addStatusCmntreply']['created_at'],
          'myLike': null,
          'totalLike': 0,
        });
      });
    } else if (body['success'] == false) {
      repEditor.text = "";
      repComCount.removeAt(replyList.length - 1);
      replyList.removeAt(replyList.length - 1);
      repComCount[widget.index]['count'] -= 1;
    }

    setState(() {
      repEditor.text = "";
    });

    //loadComments();
  }

  void _statusModalBottomSheet(context, int index, var userData, var reply) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                // Text('React to this post',
                //       style: TextStyle(fontWeight: FontWeight.normal)),
                new ListTile(
                  leading: new Icon(Icons.edit),
                  title: new Text('Edit',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    _showEditDialog(reply, index),
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => FeedStatusEditPost(
                    //             feed, widget.userData, index)))
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  title: new Text('Delete',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.redAccent,
                          fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    _showDeleteDialog("${reply['id']}", index),
                  },
                ),
              ],
            ),
          );
        });
  }

  Future<Null> _showEditDialog(feed, index) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      //height: 100,
                      padding: EdgeInsets.all(0),
                      margin:
                          EdgeInsets.only(top: 5, left: 0, bottom: 5, right: 0),
                      decoration: BoxDecoration(
                          // borderRadius:
                          //     BorderRadius.all(Radius.circular(100.0)),
                          borderRadius: new BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0),
                              bottomLeft: Radius.circular(20.0),
                              bottomRight: Radius.circular(20.0)),
                          color: Colors.grey[100],
                          border: Border.all(
                              width: 0.5,
                              color: Colors.black.withOpacity(0.2))),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: new ConstrainedBox(
                              constraints: BoxConstraints(
                                maxHeight: 120.0,
                              ),
                              child: new SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                reverse: true,
                                child: new TextField(
                                  maxLines: null,
                                  autofocus: false,
                                  controller: editingController,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Oswald',
                                  ),
                                  decoration: InputDecoration(
                                    hintText: "Type a comment here...",
                                    hintStyle: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 15,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w300),
                                    contentPadding: EdgeInsets.fromLTRB(
                                        10.0, 10.0, 20.0, 10.0),
                                    border: InputBorder.none,
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      reply = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.of(context).pop();
                                });
                              },
                              child: Container(
                                  padding: EdgeInsets.all(10),
                                  margin: EdgeInsets.only(
                                      left: 0, right: 10, top: 20, bottom: 0),
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.5),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(100))),
                                  child: Text(
                                    "Cancel",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: 'BebasNeue',
                                    ),
                                    textAlign: TextAlign.center,
                                  )),
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.of(context).pop();
                                  editReply(
                                      replyList[index]['id'],
                                      replyList[index]['uId'],
                                      replyList[index],
                                      index);
                                  replyList[index]['replyTxt'] = reply;
                                });
                              },
                              child: Container(
                                  padding: EdgeInsets.all(10),
                                  margin: EdgeInsets.only(
                                      left: 10, right: 0, top: 20, bottom: 0),
                                  decoration: BoxDecoration(
                                      color: mainColor,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(100))),
                                  child: Text(
                                    "OK",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: 'BebasNeue',
                                    ),
                                    textAlign: TextAlign.center,
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<Null> _showDeleteDialog(String id, int index) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    // Container(
                    //     decoration: BoxDecoration(
                    //         border: Border.all(color: header, width: 1.5),
                    //         borderRadius: BorderRadius.circular(100),
                    //         color: Colors.white),
                    //     child: Icon(
                    //       Icons.done,
                    //       color: header,
                    //       size: 50,
                    //     )),
                    Container(
                        margin: EdgeInsets.only(top: 12),
                        child: Text(
                          "Want to delete the post?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                                deleteReply(id, index);
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void editReply(id, uID, replyList, index) {
    if (editingController.text.isEmpty) {
    } else {
      replyEdit(id, uID, replyList, index);
    }
  }

  Future replyEdit(id, uID, replyList, index) async {
    var data = {
      'reply_text': editingController.text,
      'status_id': widget.statusID,
      'comment_id': widget.commentList['id'],
    };

    print(data);

    var res = await CallApi().postData4(data, 'statusCommentReply/edit?id=$id');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      editingController.text = "";
    } else {
      editingController.text = "";
    }

    //loadComments();
  }

  Future deleteReply(id, int index) async {
    var data = {'id': id};

    print(data);

    var res = await CallApi().postData1(data, 'statusCommentReply/delete');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
      setState(() {
        //isDel.add("$id");
        if (repComCount[widget.index]['count'] != 0) {
          repComCount[widget.index]['count'] -= 1;
        }
        replyList.removeAt(index);
      });
    } else {
      _showMessage("Something went wrong!", 1);
    }
    // Navigator.pushReplacement(
    //     context, MaterialPageRoute(builder: (_) => FeedPage()));
  }

  Future likeUnlike(index, status, id) async {
    var data = {
      'user_id': widget.userData['id'],
      'status_id': widget.statusID,
      'type': status,
      'comment_id': widget.commentList['id'],
      'reply_id': id,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/reply/like');

    print(res.body);

    if (res.statusCode == 200) {
      //Navigator.pop(context);

    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
