import 'package:flutter/material.dart';

class EventUpcomingPage extends StatefulWidget {
  @override
  _EventUpcomingPageState createState() => _EventUpcomingPageState();
}

class _EventUpcomingPageState extends State<EventUpcomingPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: Column(
          children: List.generate(5, (index) {
        return Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 0, bottom: 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  // boxShadow: [
                  //   BoxShadow(
                  //     blurRadius: 1.0,
                  //     color: Colors.black26,
                  //   ),
                  // ],
                ),
                margin:
                    EdgeInsets.only(top: 2.5, bottom: 2.5, left: 0, right: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 20, right: 20, top: 0),
                        padding: EdgeInsets.only(right: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ////// <<<<< Profile picture >>>>> //////
                            Stack(
                              children: <Widget>[
                                ////// <<<<< Picture >>>>> //////
                                Container(
                                  height: 45,
                                  width: 45,
                                  margin: EdgeInsets.only(right: 10),
                                  padding: EdgeInsets.all(1.0),
                                  decoration: new BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(10),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/f7.jpg'),
                                          fit: BoxFit.cover)),
                                ),
                              ],
                            ),

                            ////// <<<<< User Name >>>>> //////
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Tech Fusion",
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black54,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(height: 3),
                                  Text(
                                    "Apr 8, 2020",
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 13,
                                        color: Colors.black54,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w400),
                                  ),
                                  SizedBox(height: 2),

                                  ////// <<<<< Mutual Friends >>>>> //////
                                  Container(
                                    margin: EdgeInsets.only(top: 3),
                                    child: Text(
                                      "Leading University, Sylhet",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontFamily: 'Oswald',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 11,
                                          color: Colors.black45),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    ////// <<<<< Message Button >>>>> //////
                    GestureDetector(
                      onTap: () {
                        //showBottomSheet(context);
                      },
                      child: Container(
                          margin: EdgeInsets.only(right: 15),
                          padding: EdgeInsets.only(
                              left: 10, right: 10, top: 10, bottom: 10),
                          child: Icon(
                            Icons.star_border,
                            color: Colors.black38,
                            size: 22,
                          )),
                    ),
                  ],
                ),
              ),
              Divider()
            ],
          ),
        );
      })),
    );
  }
}
