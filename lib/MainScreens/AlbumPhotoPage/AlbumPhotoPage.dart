import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:multi_media_picker/multi_media_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/main.dart';

class AlbumPhotoPage extends StatefulWidget {
  final albumList;
  AlbumPhotoPage(this.albumList);

  @override
  _AlbumPhotoPageState createState() => _AlbumPhotoPageState();
}

class _AlbumPhotoPageState extends State<AlbumPhotoPage> {
  String albumDate = "", currentDate = "";
  List allImages = [];
  List<File> resultList;
  var img, userData, body1;
  bool isImageLoading = false;
  DateTime now = DateTime.now();

  @override
  void initState() {
    _getUserInfo();
    setState(() {
      DateTime date1 = DateTime.parse("${widget.albumList['date']}");

      albumDate = DateFormat.yMMMd().format(date1);
      currentDate = DateFormat("yyyy-MM-dd").format(now);
    });
    print(widget.albumList['allPic'].length);
    super.initState();
  }

  initMultiPickUp() async {
    setState(() {
      allImages = [];
      //isImageLoading = true;
    });
    //List<File> resultList;
    resultList = await MultiMediaPicker.pickImages(
        source: ImageSource.gallery, maxHeight: 480, maxWidth: 640);
    setState(() {
      //resultList = imgs;
      if (resultList != null) {
        //isSubmit = true;
        isImageLoading = false;
      }
      uploadImages(resultList);
    });
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Wrap(
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                        //color: Colors.red,
                        height: 190,
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.all(0.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(0),
                          child: CachedNetworkImage(
                            imageUrl: widget.albumList['pic'],
                            placeholder: (context, url) => SpinKitCircle(
                                color: Colors.grey.withOpacity(0.5)),
                            errorWidget: (context, url, error) => Image.asset(
                              "assets/images/placeholder_cover.jpg",
                              fit: BoxFit.cover,
                              //height: 40,
                            ),
                            fit: BoxFit.cover,
                          ),
                        )),
                    Container(
                        color: Colors.black.withOpacity(0.2),
                        height: 190,
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.all(0.0),
                        child: null),
                    Container(
                        alignment: Alignment.topLeft,
                        margin: EdgeInsets.only(top: 25, left: 5),
                        child: BackButton(
                          color: Colors.white,
                        ))
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(right: 20),
                        child: Text(
                          "${widget.albumList['name']}",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 0),
                      child: Text(
                        albumDate,
                        style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: Colors.black45),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.info_outline,
                      color: mainColor,
                      size: 15,
                    ),
                    Expanded(
                      child: Container(
                        child: Text(
                          " ${widget.albumList['desc']}",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Colors.black54),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Divider(),
              GestureDetector(
                onTap: initMultiPickUp,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  margin:
                      EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.add_to_photos,
                        color: mainColor,
                        size: 16,
                      ),
                      Container(
                        child: Text(
                          " Add photos",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: mainColor),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              resultList == null
                  ? Container()
                  : SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        child: Row(
                            children: List.generate(resultList.length, (index) {
                          return Container(
                              height: 130,
                              width: 130,
                              padding: const EdgeInsets.all(0.0),
                              margin: EdgeInsets.only(
                                  right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                              child: Stack(
                                children: <Widget>[
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      height: 130,
                                      width: 130,
                                      child: Image.file(
                                        resultList[index],
                                        fit: BoxFit.cover,
                                        //height: 40,
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        resultList.removeAt(index);
                                        //images = img;
                                        print(resultList.length);
                                      });
                                    },
                                    child: Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.black87.withOpacity(0.5),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Icon(
                                          Icons.close,
                                          color: Colors.white,
                                        )),
                                  )
                                ],
                              ));
                        })),
                      ),
                    ),
              resultList == null
                  ? Container()
                  : GestureDetector(
                      onTap: () {
                        albumUpload();
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.centerRight,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              "Upload",
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: mainColor),
                            ),
                            Icon(Icons.chevron_right,
                                size: 17, color: mainColor),
                            isImageLoading == false
                                ? Container()
                                : Container(
                                    height: 30,
                                    margin: EdgeInsets.only(bottom: 20),
                                    child:
                                        SpinKitCircle(color: Colors.black26)),
                          ],
                        ),
                      ),
                    ),
              resultList == null ? Container() : Divider(),
              Wrap(
                  children:
                      List.generate(widget.albumList['allPic'].length, (index) {
                return Container(
                    height: 130,
                    width: 130,
                    padding: const EdgeInsets.all(0.0),
                    margin: EdgeInsets.only(
                        right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: CachedNetworkImage(
                        imageUrl: widget.albumList['allPic'][index]['url'],
                        placeholder: (context, url) =>
                            SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                        errorWidget: (context, url, error) => Image.asset(
                          "assets/images/placeholder_cover.jpg",
                          fit: BoxFit.cover,
                          //height: 40,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ));
              })),
            ],
          )),
    );
  }

  uploadImages(images) async {
    if (images != null) {
      for (int i = 0; i < images.length; i++) {
        //File file = new File(resultList[i].toString());
        List<int> imageBytes = images[i].readAsBytesSync();
        String image = base64.encode(imageBytes);
        image = 'data:image/png;base64,' + image;
        var data3 = {'image': image};
        print(data3);
        var res1 = await CallApi().postData1(data3, 'album/upload/images');
        body1 = json.decode(res1.body);
        print("image success");
        print(body1);
        setState(() {
          allImages.add({'url': body1['url'], 'created_at': currentDate});
        });

        print(allImages);
      }
    } else {
      images = [];
    }
  }

  Future albumUpload() async {
    setState(() {
      isImageLoading = true;
    });
    var data = {
      'images': allImages,
      'user_id': userData['id'],
    };

    print(data);

    var res = await CallApi()
        .postData1(data, 'profile/album/upload/${widget.albumList['id']}');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      setState(() {
        widget.albumList['allPic'].add(body1['url']);
      });
      //Navigator.pop(context);
      // Navigator.push(
      //     context, MaterialPageRoute(builder: (context) => AlbumPage()));
    } else {
      _showMessage("Something went wrong!", 1);
    }

    setState(() {
      isImageLoading = false;
    });
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
