import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:video_player/video_player.dart';

import '../../main.dart';

class VideoPage extends StatefulWidget {
  @override
  _VideoPageState createState() => _VideoPageState();
}

class _VideoPageState extends State<VideoPage> {
  VideoPlayerController _controller;
  VideoPlayerController _controller1;
  VideoPlayerController _controller2;
  VideoPlayerController _controller3;
  Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    _controller = VideoPlayerController.network(
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    );
    _initializeVideoPlayerFuture = _controller.initialize();

    _controller.setLooping(true);

    _controller1 = VideoPlayerController.network(
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    );
    _initializeVideoPlayerFuture = _controller1.initialize();

    _controller1.setLooping(true);

    _controller2 = VideoPlayerController.network(
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    );
    _initializeVideoPlayerFuture = _controller2.initialize();

    _controller2.setLooping(true);

    _controller3 = VideoPlayerController.network(
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    );
    _initializeVideoPlayerFuture = _controller3.initialize();

    _controller3.setLooping(true);

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _controller1.dispose();
    _controller2.dispose();
    _controller3.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      alignment: Alignment.topCenter,
      child: Wrap(children: <Widget>[
        Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                  height: 130,
                  width: 130,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage('assets/images/white.jpg'),
                          fit: BoxFit.cover)),
                ),
                Container(
                  margin: EdgeInsets.only(
                      right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                  height: 130,
                  width: 130,
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.3),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Icon(Icons.add),
                ),
              ],
            ),
            Container(
              width: 120,
              child: Text(
                "Add Video",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    fontSize: 13,
                    color: Colors.black54,
                    fontFamily: 'Oswald',
                    fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
        Column(
          children: <Widget>[
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 130,
                      height: 130,
                      child: FutureBuilder(
                        future: _initializeVideoPlayerFuture,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            return AspectRatio(
                              aspectRatio: _controller.value.aspectRatio,
                              child: VideoPlayer(_controller),
                            );
                          } else {
                            return Center(
                              child: SpinKitCircle(
                                color: Colors.grey.withOpacity(0.3),
                              ),
                            );
                          }
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          if (_controller.value.isPlaying) {
                            _controller.pause();
                          } else {
                            _controller.play();
                          }
                        });
                      },
                      child: Container(
                          width: 130,
                          height: 130,
                          child: Icon(
                              _controller.value.isPlaying
                                  ? Icons.pause
                                  : Icons.play_arrow,
                              size: 40,
                              color: _controller.value.isPlaying
                                  ? Colors.transparent
                                  : Colors.white)),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: 120,
              child: Text(
                "Car travelling last week",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    fontSize: 13,
                    color: mainColor,
                    fontFamily: 'Oswald',
                    fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
        Column(
          children: <Widget>[
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 130,
                      height: 130,
                      child: FutureBuilder(
                        future: _initializeVideoPlayerFuture,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            return AspectRatio(
                              aspectRatio: _controller1.value.aspectRatio,
                              child: VideoPlayer(_controller1),
                            );
                          } else {
                            return Center(
                              child: SpinKitCircle(
                                color: Colors.grey.withOpacity(0.3),
                              ),
                            );
                          }
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          if (_controller1.value.isPlaying) {
                            _controller1.pause();
                          } else {
                            _controller1.play();
                          }
                        });
                      },
                      child: Container(
                          width: 130,
                          height: 130,
                          child: Icon(
                              _controller1.value.isPlaying
                                  ? Icons.pause
                                  : Icons.play_arrow,
                              size: 40,
                              color: _controller1.value.isPlaying
                                  ? Colors.transparent
                                  : Colors.white)),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: 120,
              child: Text(
                "Car travelling 2",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    fontSize: 13,
                    color: mainColor,
                    fontFamily: 'Oswald',
                    fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
        Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  right: 2.5, left: 2.5, bottom: 2.5, top: 15.5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 130,
                      height: 130,
                      child: FutureBuilder(
                        future: _initializeVideoPlayerFuture,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            return AspectRatio(
                              aspectRatio: _controller2.value.aspectRatio,
                              child: VideoPlayer(_controller2),
                            );
                          } else {
                            return Center(
                              child: SpinKitCircle(
                                color: Colors.grey.withOpacity(0.3),
                              ),
                            );
                          }
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          if (_controller2.value.isPlaying) {
                            _controller2.pause();
                          } else {
                            _controller2.play();
                          }
                        });
                      },
                      child: Container(
                          width: 130,
                          height: 130,
                          child: Icon(
                              _controller2.value.isPlaying
                                  ? Icons.pause
                                  : Icons.play_arrow,
                              size: 40,
                              color: _controller2.value.isPlaying
                                  ? Colors.transparent
                                  : Colors.white)),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: 120,
              child: Text(
                "Car travelling 3",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    fontSize: 13,
                    color: mainColor,
                    fontFamily: 'Oswald',
                    fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
        Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  right: 2.5, left: 2.5, bottom: 2.5, top: 15.5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 130,
                      height: 130,
                      child: FutureBuilder(
                        future: _initializeVideoPlayerFuture,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            return AspectRatio(
                              aspectRatio: _controller3.value.aspectRatio,
                              child: VideoPlayer(_controller3),
                            );
                          } else {
                            return Center(
                              child: SpinKitCircle(
                                color: Colors.grey.withOpacity(0.3),
                              ),
                            );
                          }
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          if (_controller3.value.isPlaying) {
                            _controller3.pause();
                          } else {
                            _controller3.play();
                          }
                        });
                      },
                      child: Container(
                          width: 130,
                          height: 130,
                          child: Icon(
                              _controller3.value.isPlaying
                                  ? Icons.pause
                                  : Icons.play_arrow,
                              size: 40,
                              color: _controller3.value.isPlaying
                                  ? Colors.transparent
                                  : Colors.white)),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: 120,
              child: Text(
                "Bike Riding",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    fontSize: 13,
                    color: mainColor,
                    fontFamily: 'Oswald',
                    fontWeight: FontWeight.w400),
              ),
            ),
          ],
        ),
      ]),
    );
  }
}
