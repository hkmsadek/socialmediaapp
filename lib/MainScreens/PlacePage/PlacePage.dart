import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:social_app_fb/API/api.dart';

import '../../main.dart';

class PlacePage extends StatefulWidget {
  @override
  _PlacePageState createState() => _PlacePageState();
}

class _PlacePageState extends State<PlacePage> {
  bool isCity = false;
  bool isHome = false;
  bool isPlace = false;
  TextEditingController cityController = new TextEditingController();
  TextEditingController desCityController = new TextEditingController();
  TextEditingController homeController = new TextEditingController();
  TextEditingController desHomeController = new TextEditingController();
  TextEditingController placeController = new TextEditingController();
  TextEditingController desOtherController = new TextEditingController();

  TextEditingController editplaceController = new TextEditingController();
  TextEditingController editdesOtherController = new TextEditingController();
  String city = "Current City", home = "Hometown";
  DateTime selectedCurrentCityDate = DateTime.now();
  DateTime selectedHometownDate = DateTime.now();
  DateTime selectedMovedDate = DateTime.now();
  DateTime selectedEditMovedDate = DateTime.now();
  DateTime selectedLeaveDate = DateTime.now();
  DateTime selectedEditLeaveDate = DateTime.now();
  String cityDate = "Moving Date", cityDateServer = "";
  String homeDate = "Moving Date", homeDateServer = "";
  String placeMovedDate = "Moving Date", placeMovedDateServer = "";
  String placeleaveDate = "Leaving Date", placeleaveDateServer = "";
  String editplaceMovedDate = "Moving Date", editplaceMovedDateServer = "";
  String editplaceleaveDate = "Leaving Date", editplaceleaveDateServer = "";

  @override
  void initState() {
    setState(() {
      cityDate = DateFormat.yMMMd().format(selectedCurrentCityDate);
      homeDate = DateFormat.yMMMd().format(selectedHometownDate);
      // placeMovedDate = DateFormat.yMMMd().format(selectedMovedDate);
      // placeleaveDate = DateFormat.yMMMd().format(selectedLeaveDate);

      city = locationCur[0]['city'];
      cityController.text = locationCur[0]['city'];

      home = locationHome[0]['homeTown'];
      homeController.text = locationHome[0]['homeTown'];
    });
    super.initState();
  }

  Future<Null> _selectCityDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedCurrentCityDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedCurrentCityDate)
      setState(() {
        selectedCurrentCityDate = picked;
        cityDate = DateFormat.yMMMd().format(selectedCurrentCityDate);
        cityDateServer =
            DateFormat('yyyy-MM-dd').format(selectedCurrentCityDate);

        print("cityDate");
        print(cityDate);
      });
  }

  Future<Null> _selectHomeDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedHometownDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedHometownDate)
      setState(() {
        selectedHometownDate = picked;
        homeDate = DateFormat.yMMMd().format(selectedHometownDate);
        homeDateServer = DateFormat('yyyy-MM-dd').format(selectedHometownDate);

        print("homeDate");
        print(homeDate);
      });
  }

  Future<Null> _selectMovedDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedMovedDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedMovedDate)
      setState(() {
        selectedMovedDate = picked;
        placeMovedDate = DateFormat.yMMMd().format(selectedMovedDate);
        placeMovedDateServer =
            DateFormat('yyyy-MM-dd').format(selectedMovedDate);

        print("placeMovedDate");
        print(placeMovedDate);
      });
  }

  Future<Null> _selectEditMovedDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEditMovedDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedEditMovedDate)
      setState(() {
        selectedEditMovedDate = picked;
        editplaceMovedDate = DateFormat.yMMMd().format(selectedEditMovedDate);
        editplaceMovedDateServer =
            DateFormat('yyyy-MM-dd').format(selectedEditMovedDate);

        print("editplaceMovedDate");
        print(editplaceMovedDate);
      });
  }

  Future<Null> _selectLeaveDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedLeaveDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedLeaveDate)
      setState(() {
        selectedLeaveDate = picked;
        placeleaveDate = DateFormat.yMMMd().format(selectedLeaveDate);
        placeleaveDateServer =
            DateFormat('yyyy-MM-dd').format(selectedLeaveDate);

        print("placeleaveDate");
        print(placeleaveDate);
      });
  }

  Future<Null> _selectEditLeaveDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEditLeaveDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedEditLeaveDate)
      setState(() {
        selectedEditLeaveDate = picked;
        editplaceleaveDate = DateFormat.yMMMd().format(selectedEditLeaveDate);
        editplaceleaveDateServer =
            DateFormat('yyyy-MM-dd').format(selectedEditLeaveDate);

        print("editplaceleaveDate");
        print(editplaceleaveDate);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: <Widget>[
            Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 15, left: 20),
                child: Text(
                  "Current City and Hometown",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 17,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.normal),
                )),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 30,
                    margin: EdgeInsets.only(top: 10, left: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        color: Colors.black54,
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 3.0,
                            color: Colors.black54,
                            //offset: Offset(6.0, 7.0),
                          ),
                        ],
                        border: Border.all(width: 0.5, color: Colors.black54)),
                  ),
                ],
              ),
            ),
            // isCity
            //     ? Container()
            //     : GestureDetector(
            //         onTap: () {
            //           setState(() {
            //             isCity = true;
            //           });
            //         },
            //         child: Container(
            //           margin: EdgeInsets.only(left: 20, right: 15, top: 15),
            //           child: Row(
            //             children: <Widget>[
            //               Container(
            //                   margin: EdgeInsets.only(right: 10),
            //                   child:
            //                       Icon(Icons.add, size: 17, color: mainColor)),
            //               Expanded(
            //                 child: Text.rich(
            //                   TextSpan(
            //                     children: <TextSpan>[
            //                       TextSpan(
            //                           text: "Edit your city",
            //                           style: TextStyle(
            //                               color: mainColor,
            //                               fontSize: 14,
            //                               fontFamily: "Oswald",
            //                               fontWeight: FontWeight.w400)),
            //                       // can add more TextSpans here...
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 15, bottom: 0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    margin: EdgeInsets.only(
                        top: 2.5, bottom: 2.5, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      padding: EdgeInsets.all(1.0),
                                      child: CircleAvatar(
                                        radius: 20.0,
                                        backgroundColor: Colors.white,
                                        backgroundImage: AssetImage(
                                            'assets/images/user.png'),
                                      ),
                                      decoration: new BoxDecoration(
                                        color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                city == null
                                                    ? "Select City"
                                                    : "$city",
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color:
                                                        city == "Current City"
                                                            ? Colors.black54
                                                            : mainColor,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        city == "Current City"
                                                            ? FontWeight.w500
                                                            : FontWeight.w400),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),

                                      ////// <<<<< Mutual Friends >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(top: 3),
                                        child: Text(
                                          "Current City",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 11,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                isCity
                                    ? Container()
                                    : Container(
                                        child: Row(
                                          children: <Widget>[
                                            GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  isCity = true;
                                                });
                                              },
                                              child: Container(
                                                  padding: EdgeInsets.all(5),
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(
                                                    Icons.edit,
                                                    color: Colors.black38,
                                                    size: 15,
                                                  )),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  city = "Current City";
                                                });
                                              },
                                              child: Container(
                                                  padding: EdgeInsets.all(5),
                                                  child: Icon(
                                                    Icons.close,
                                                    color: Colors.black38,
                                                    size: 15,
                                                  )),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  !isCity
                      ? Container()
                      : Container(
                          margin: EdgeInsets.only(bottom: 0, top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            //mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                            child: Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                padding: EdgeInsets.all(0),
                                                margin: EdgeInsets.only(
                                                    left: 20,
                                                    right: 20,
                                                    top: 5),
                                                decoration: BoxDecoration(
                                                    color: Colors.white
                                                        .withOpacity(0.7),
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        width: 0.2),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                5))),
                                                child: TextField(
                                                  controller: cityController,
                                                  keyboardType:
                                                      TextInputType.text,
                                                  style: TextStyle(
                                                    color: Colors.black87,
                                                    fontFamily: 'Oswald',
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: "Current City",
                                                    hintStyle: TextStyle(
                                                        color: Colors.black38,
                                                        fontSize: 15,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w300),
                                                    //labelStyle: TextStyle(color: Colors.white70),
                                                    contentPadding:
                                                        EdgeInsets.fromLTRB(
                                                            10.0, 0, 10.0, 0),
                                                    border: InputBorder.none,
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      child: GestureDetector(
                                        onTap: () {
                                          _selectCityDate(context);
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              bottom: 0, top: 5),
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              padding: EdgeInsets.only(
                                                  left: 10,
                                                  top: 15,
                                                  bottom: 15),
                                              margin: EdgeInsets.only(
                                                  left: 20, right: 20, top: 5),
                                              decoration: BoxDecoration(
                                                  color: Colors.white
                                                      .withOpacity(0.7),
                                                  border: Border.all(
                                                      color: Colors.grey,
                                                      width: 0.2),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                    cityDate,
                                                    style: TextStyle(
                                                        color: cityDate ==
                                                                "Moving Date"
                                                            ? Colors.black38
                                                            : Colors.black,
                                                        fontSize: 15,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w300),
                                                  ),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          right: 10),
                                                      child: Icon(
                                                        Icons.calendar_today,
                                                        color: Colors.black26,
                                                        size: 16,
                                                      ))
                                                ],
                                              )),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                            child: Container(
                                              margin: EdgeInsets.only(
                                                  bottom: 0, top: 5),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                //mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      padding:
                                                          EdgeInsets.all(0),
                                                      margin: EdgeInsets.only(
                                                          left: 20,
                                                          right: 20,
                                                          top: 5),
                                                      decoration: BoxDecoration(
                                                          color: Colors.white
                                                              .withOpacity(0.7),
                                                          border: Border.all(
                                                              color:
                                                                  Colors.grey,
                                                              width: 0.2),
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          5))),
                                                      child:
                                                          SingleChildScrollView(
                                                        child: TextField(
                                                          maxLines: null,
                                                          maxLength: 150,
                                                          controller:
                                                              desCityController,
                                                          keyboardType:
                                                              TextInputType
                                                                  .text,
                                                          style: TextStyle(
                                                            color:
                                                                Colors.black87,
                                                            fontFamily:
                                                                'Oswald',
                                                          ),
                                                          decoration:
                                                              InputDecoration(
                                                            hintText:
                                                                "Description",
                                                            hintStyle: TextStyle(
                                                                color: Colors
                                                                    .black38,
                                                                fontSize: 15,
                                                                fontFamily:
                                                                    'Oswald',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300),
                                                            //labelStyle: TextStyle(color: Colors.white70),
                                                            contentPadding:
                                                                EdgeInsets
                                                                    .fromLTRB(
                                                                        10.0,
                                                                        0,
                                                                        10.0,
                                                                        0),
                                                            border: InputBorder
                                                                .none,
                                                          ),
                                                        ),
                                                      )),
                                                ],
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                isCity = false;
                                              });
                                            },
                                            child: Container(
                                                margin: EdgeInsets.only(
                                                    top: 10, right: 10),
                                                padding: EdgeInsets.all(5),
                                                decoration: BoxDecoration(
                                                    color: Colors.grey
                                                        .withOpacity(0.2),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100)),
                                                child: Icon(
                                                  Icons.close,
                                                  size: 15,
                                                )),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                if (cityController.text != "") {
                                                  isCity = false;
                                                  city = cityController.text;
                                                  locationCur.clear();
                                                  locationCur.add({
                                                    'city': cityController.text,
                                                    'movedDate': cityDateServer,
                                                    'description':
                                                        desCityController.text,
                                                    'isEdit': false,
                                                  });
                                                  sendLocation();
                                                }

                                                // allInfo.add(
                                                //     {"info": workController.text, "status": 2});
                                              });
                                            },
                                            child: Container(
                                                margin: EdgeInsets.only(
                                                    top: 10, right: 10),
                                                padding: EdgeInsets.all(5),
                                                decoration: BoxDecoration(
                                                    color: mainColor
                                                        .withOpacity(0.7),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100)),
                                                child: Icon(
                                                  Icons.done,
                                                  size: 15,
                                                  color: Colors.white,
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                  !isCity
                      ? Container()
                      : Container(
                          margin: EdgeInsets.only(top: 10, right: 10, left: 20),
                          padding: EdgeInsets.all(5),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.public,
                                size: 15,
                                color: Colors.black45,
                              ),
                              SizedBox(width: 5),
                              Text("Public",
                                  style: TextStyle(
                                      color: mainColor,
                                      fontSize: 12,
                                      fontFamily: "Oswald",
                                      fontWeight: FontWeight.w300)),
                            ],
                          )),
                  Divider(),
                  Container(
                    padding: EdgeInsets.only(top: 0, bottom: 0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    margin: EdgeInsets.only(
                        top: 2.5, bottom: 2.5, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      padding: EdgeInsets.all(1.0),
                                      child: CircleAvatar(
                                        radius: 20.0,
                                        backgroundColor: Colors.white,
                                        backgroundImage: AssetImage(
                                            'assets/images/user.png'),
                                      ),
                                      decoration: new BoxDecoration(
                                        color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                home == null
                                                    ? "Select Hometown"
                                                    : "$home",
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: home == "Hometown"
                                                        ? Colors.black54
                                                        : mainColor,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        home == "Hometown"
                                                            ? FontWeight.w500
                                                            : FontWeight.w400),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),

                                      ////// <<<<< Mutual Friends >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(top: 3),
                                        child: Text(
                                          "Hometown",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 11,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                isHome
                                    ? Container()
                                    : Container(
                                        child: Row(
                                          children: <Widget>[
                                            GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  isHome = true;
                                                });
                                              },
                                              child: Container(
                                                  padding: EdgeInsets.all(5),
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(
                                                    Icons.edit,
                                                    color: Colors.black38,
                                                    size: 15,
                                                  )),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  home = "Hometown";
                                                });
                                              },
                                              child: Container(
                                                  padding: EdgeInsets.all(5),
                                                  child: Icon(
                                                    Icons.close,
                                                    color: Colors.black38,
                                                    size: 15,
                                                  )),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  !isHome
                      ? Container()
                      : Container(
                          margin: EdgeInsets.only(bottom: 0, top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            //mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.all(0),
                                  margin: EdgeInsets.only(
                                      left: 20, right: 20, top: 5),
                                  decoration: BoxDecoration(
                                      color: Colors.white.withOpacity(0.7),
                                      border: Border.all(
                                          color: Colors.grey, width: 0.2),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: TextField(
                                    controller: homeController,
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: 'Oswald',
                                    ),
                                    decoration: InputDecoration(
                                      hintText: "Hometown",
                                      hintStyle: TextStyle(
                                          color: Colors.black38,
                                          fontSize: 15,
                                          fontFamily: 'Oswald',
                                          fontWeight: FontWeight.w300),
                                      //labelStyle: TextStyle(color: Colors.white70),
                                      contentPadding:
                                          EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                                      border: InputBorder.none,
                                    ),
                                  )),
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Flexible(
                                      child: Container(
                                        margin:
                                            EdgeInsets.only(bottom: 0, top: 5),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          //mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                padding: EdgeInsets.all(0),
                                                margin: EdgeInsets.only(
                                                    left: 20,
                                                    right: 20,
                                                    top: 5),
                                                decoration: BoxDecoration(
                                                    color: Colors.white
                                                        .withOpacity(0.7),
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        width: 0.2),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                5))),
                                                child: SingleChildScrollView(
                                                  child: TextField(
                                                    maxLines: null,
                                                    maxLength: 150,
                                                    controller:
                                                        desHomeController,
                                                    keyboardType:
                                                        TextInputType.text,
                                                    style: TextStyle(
                                                      color: Colors.black87,
                                                      fontFamily: 'Oswald',
                                                    ),
                                                    decoration: InputDecoration(
                                                      hintText: "Description",
                                                      hintStyle: TextStyle(
                                                          color: Colors.black38,
                                                          fontSize: 15,
                                                          fontFamily: 'Oswald',
                                                          fontWeight:
                                                              FontWeight.w300),
                                                      //labelStyle: TextStyle(color: Colors.white70),
                                                      contentPadding:
                                                          EdgeInsets.fromLTRB(
                                                              10.0, 0, 10.0, 0),
                                                      border: InputBorder.none,
                                                    ),
                                                  ),
                                                )),
                                          ],
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          isHome = false;
                                        });
                                      },
                                      child: Container(
                                          margin: EdgeInsets.only(
                                              top: 10, right: 10),
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.2),
                                              borderRadius:
                                                  BorderRadius.circular(100)),
                                          child: Icon(
                                            Icons.close,
                                            size: 15,
                                          )),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          if (homeController.text != "") {
                                            isHome = false;
                                            home = homeController.text;
                                            locationHome.clear();
                                            locationHome.add({
                                              'homeTown': homeController.text,
                                              'description':
                                                  desHomeController.text,
                                              'isEdit': false,
                                            });
                                            sendLocation();
                                          }

                                          // allInfo.add(
                                          //     {"info": workController.text, "status": 2});
                                        });
                                      },
                                      child: Container(
                                          margin: EdgeInsets.only(
                                              top: 10, right: 10),
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                              color: mainColor.withOpacity(0.7),
                                              borderRadius:
                                                  BorderRadius.circular(100)),
                                          child: Icon(
                                            Icons.done,
                                            size: 15,
                                            color: Colors.white,
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                  !isHome
                      ? Container()
                      : Container(
                          margin: EdgeInsets.only(top: 10, right: 10, left: 20),
                          padding: EdgeInsets.all(5),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.public,
                                size: 15,
                                color: Colors.black45,
                              ),
                              SizedBox(width: 5),
                              Text("Public",
                                  style: TextStyle(
                                      color: mainColor,
                                      fontSize: 12,
                                      fontFamily: "Oswald",
                                      fontWeight: FontWeight.w300)),
                            ],
                          )),
                ],
              ),
            ),

            Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 35, left: 20),
                child: Text(
                  "Other places lived",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 17,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.normal),
                )),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 30,
                    margin: EdgeInsets.only(top: 10, left: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        color: Colors.black54,
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 3.0,
                            color: Colors.black54,
                            //offset: Offset(6.0, 7.0),
                          ),
                        ],
                        border: Border.all(width: 0.5, color: Colors.black54)),
                  ),
                ],
              ),
            ),
            isPlace
                ? Container()
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        isPlace = true;
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child:
                                  Icon(Icons.add, size: 17, color: mainColor)),
                          Expanded(
                            child: Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "Add other places you have been",
                                      style: TextStyle(
                                          color: mainColor,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),
                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Column(
                  children: List.generate(locationOther.length, (index) {
                String mvdate = "", lvDate = "", mvServer = "", lvServer = "";
                if (locationOther[index]['movedDate'] != "") {
                  DateTime dateTime1 =
                      DateTime.parse(locationOther[index]['movedDate']);
                  mvdate = DateFormat.yMMMd().format(dateTime1);
                  mvServer = DateFormat('yyyy-MM-dd').format(dateTime1);
                }

                if (locationOther[index]['leftDate'] != "") {
                  DateTime dateTime1 =
                      DateTime.parse(locationOther[index]['leftDate']);
                  lvDate = DateFormat.yMMMd().format(dateTime1);
                  lvServer = DateFormat('yyyy-MM-dd').format(dateTime1);
                }

                return Container(
                  padding: EdgeInsets.only(top: 0, bottom: 0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  margin:
                      EdgeInsets.only(top: 2.5, bottom: 2.5, left: 0, right: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 20, right: 20, top: 0),
                          padding: EdgeInsets.only(right: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              ////// <<<<< Profile picture >>>>> //////
                              Stack(
                                children: <Widget>[
                                  ////// <<<<< Picture >>>>> //////
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    padding: EdgeInsets.all(1.0),
                                    child: CircleAvatar(
                                      radius: 20.0,
                                      backgroundColor: Colors.white,
                                      backgroundImage:
                                          AssetImage('assets/images/user.png'),
                                    ),
                                    decoration: new BoxDecoration(
                                      color: Colors.grey[300],
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ],
                              ),

                              ////// <<<<< User Name >>>>> //////
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(
                                              "${locationOther[index]['city']}",
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: home == "Hometown"
                                                      ? Colors.black54
                                                      : mainColor,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: home == "Hometown"
                                                      ? FontWeight.w500
                                                      : FontWeight.w400),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                    ////// <<<<< Mutual Friends >>>>> //////
                                    mvdate == "" && lvDate == ""
                                        ? Container()
                                        : Container(
                                            margin: EdgeInsets.only(top: 3),
                                            child: Text(
                                              mvdate != "" && lvDate == ""
                                                  ? mvdate
                                                  : mvdate + " - " + lvDate,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 11,
                                                  color: Colors.black45),
                                            ),
                                          ),
                                  ],
                                ),
                              ),
                              isHome
                                  ? Container()
                                  : Container(
                                      child: Row(
                                        children: <Widget>[
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                if (locationOther[index]
                                                        ['movedDate'] !=
                                                    "") {
                                                  DateTime dateTime =
                                                      DateTime.parse(
                                                          locationOther[index]
                                                              ['movedDate']);
                                                  if (dateTime != null) {
                                                    editplaceMovedDate =
                                                        DateFormat.yMMMd()
                                                            .format(dateTime);
                                                    editplaceMovedDateServer =
                                                        DateFormat('yyyy-MM-dd')
                                                            .format(dateTime);
                                                  }
                                                } else {
                                                  editplaceMovedDate =
                                                      "Moving Date";
                                                }

                                                if (locationOther[index]
                                                        ['leftDate'] !=
                                                    "") {
                                                  DateTime dateTime1 =
                                                      DateTime.parse(
                                                          locationOther[index]
                                                              ['leftDate']);
                                                  if (dateTime1 != null) {
                                                    editplaceleaveDate =
                                                        DateFormat.yMMMd()
                                                            .format(dateTime1);
                                                    editplaceleaveDateServer =
                                                        DateFormat('yyyy-MM-dd')
                                                            .format(dateTime1);
                                                  }
                                                } else {
                                                  editplaceleaveDate =
                                                      "Leaving Date";
                                                }
                                                editplaceController.text =
                                                    locationOther[index]
                                                        ['city'];
                                                editdesOtherController.text =
                                                    locationOther[index]
                                                        ['description'];
                                              });
                                              showPlacesDialogBox(
                                                  locationOther[index], index);
                                            },
                                            child: Container(
                                                padding: EdgeInsets.all(5),
                                                margin:
                                                    EdgeInsets.only(right: 10),
                                                child: Icon(
                                                  Icons.edit,
                                                  color: Colors.black38,
                                                  size: 15,
                                                )),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                locationOther.removeAt(index);
                                                sendLocation();
                                              });
                                            },
                                            child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: Icon(
                                                  Icons.close,
                                                  color: Colors.black38,
                                                  size: 15,
                                                )),
                                          ),
                                        ],
                                      ),
                                    )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              })),
            ),
            !isPlace
                ? Container()
                : Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller: placeController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "Place Name",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: GestureDetector(
                            onTap: () {
                              _selectMovedDate(context);
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 0, top: 5),
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.only(
                                      left: 10, top: 15, bottom: 15),
                                  margin: EdgeInsets.only(
                                      left: 20, right: 20, top: 5),
                                  decoration: BoxDecoration(
                                      color: Colors.white.withOpacity(0.7),
                                      border: Border.all(
                                          color: Colors.grey, width: 0.2),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        placeMovedDate,
                                        style: TextStyle(
                                            color:
                                                placeMovedDate == "Moving Date"
                                                    ? Colors.black38
                                                    : Colors.black,
                                            fontSize: 15,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(right: 10),
                                          child: Icon(
                                            Icons.calendar_today,
                                            color: Colors.black26,
                                            size: 16,
                                          ))
                                    ],
                                  )),
                            ),
                          ),
                        ),
                        Container(
                          child: GestureDetector(
                            onTap: () {
                              _selectLeaveDate(context);
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 0, top: 5),
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.only(
                                      left: 10, top: 15, bottom: 15),
                                  margin: EdgeInsets.only(
                                      left: 20, right: 20, top: 5),
                                  decoration: BoxDecoration(
                                      color: Colors.white.withOpacity(0.7),
                                      border: Border.all(
                                          color: Colors.grey, width: 0.2),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        placeleaveDate,
                                        style: TextStyle(
                                            color:
                                                placeleaveDate == "Leaving Date"
                                                    ? Colors.black38
                                                    : Colors.black,
                                            fontSize: 15,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(right: 10),
                                          child: Icon(
                                            Icons.calendar_today,
                                            color: Colors.black26,
                                            size: 16,
                                          ))
                                    ],
                                  )),
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: SingleChildScrollView(
                                            child: TextField(
                                              maxLines: null,
                                              maxLength: 150,
                                              controller: desOtherController,
                                              keyboardType: TextInputType.text,
                                              style: TextStyle(
                                                color: Colors.black87,
                                                fontFamily: 'Oswald',
                                              ),
                                              decoration: InputDecoration(
                                                hintText: "Description",
                                                hintStyle: TextStyle(
                                                    color: Colors.black38,
                                                    fontSize: 15,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.w300),
                                                //labelStyle: TextStyle(color: Colors.white70),
                                                contentPadding:
                                                    EdgeInsets.fromLTRB(
                                                        10.0, 0, 10.0, 0),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isPlace = false;
                                  });
                                },
                                child: Container(
                                    margin: EdgeInsets.only(top: 10, right: 10),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: Colors.grey.withOpacity(0.2),
                                        borderRadius:
                                            BorderRadius.circular(100)),
                                    child: Icon(
                                      Icons.close,
                                      size: 15,
                                    )),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if (placeController.text != "") {
                                      isPlace = false;
                                      print(placeMovedDateServer);
                                      print(placeleaveDateServer);

                                      locationOther.add({
                                        'city': placeController.text,
                                        'movedDate': placeMovedDateServer,
                                        'leftDate': placeleaveDateServer,
                                        'description': desOtherController.text,
                                        'isEdit': false,
                                      });

                                      sendLocation();
                                      placeController.text = "";
                                      desOtherController.text = "";
                                      placeleaveDate = "Leaving Date";
                                      placeMovedDate = "Moving Date";
                                    }

                                    // allInfo.add(
                                    //     {"info": workController.text, "status": 2});
                                  });
                                },
                                child: Container(
                                    margin: EdgeInsets.only(top: 10, right: 10),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: mainColor.withOpacity(0.7),
                                        borderRadius:
                                            BorderRadius.circular(100)),
                                    child: Icon(
                                      Icons.done,
                                      size: 15,
                                      color: Colors.white,
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            !isPlace
                ? Container()
                : Container(
                    margin: EdgeInsets.only(top: 10, right: 10, left: 20),
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.public,
                          size: 15,
                          color: Colors.black45,
                        ),
                        SizedBox(width: 5),
                        Text("Public",
                            style: TextStyle(
                                color: mainColor,
                                fontSize: 12,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.w300)),
                      ],
                    )),
          ],
        ),
      ),
    );
  }

  Future<Null> showPlacesDialogBox(places, ind) async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        color: Colors.transparent,
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.only(top: 35),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Flexible(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              bottom: 0, top: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            //mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  padding: EdgeInsets.all(0),
                                                  margin: EdgeInsets.only(
                                                      left: 0,
                                                      right: 0,
                                                      top: 5),
                                                  decoration: BoxDecoration(
                                                      color: Colors.white
                                                          .withOpacity(0.7),
                                                      border: Border.all(
                                                          color: Colors.grey,
                                                          width: 0.2),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: TextField(
                                                    controller:
                                                        editplaceController,
                                                    keyboardType:
                                                        TextInputType.text,
                                                    style: TextStyle(
                                                      color: Colors.black87,
                                                      fontFamily: 'Oswald',
                                                    ),
                                                    decoration: InputDecoration(
                                                      hintText: "Place Name",
                                                      hintStyle: TextStyle(
                                                          color: Colors.black38,
                                                          fontSize: 15,
                                                          fontFamily: 'Oswald',
                                                          fontWeight:
                                                              FontWeight.w300),
                                                      //labelStyle: TextStyle(color: Colors.white70),
                                                      contentPadding:
                                                          EdgeInsets.fromLTRB(
                                                              10.0, 0, 10.0, 0),
                                                      border: InputBorder.none,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: GestureDetector(
                                    onTap: () {
                                      _selectEditMovedDate(context);
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(bottom: 0, top: 5),
                                      child: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.only(
                                              left: 10, top: 15, bottom: 15),
                                          margin: EdgeInsets.only(
                                              left: 0, right: 0, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                editplaceMovedDate,
                                                style: TextStyle(
                                                    color: editplaceMovedDate ==
                                                            "Moving Date"
                                                        ? Colors.black38
                                                        : Colors.black,
                                                    fontSize: 15,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(
                                                    Icons.calendar_today,
                                                    color: Colors.black26,
                                                    size: 16,
                                                  ))
                                            ],
                                          )),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: GestureDetector(
                                    onTap: () {
                                      _selectEditLeaveDate(context);
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(bottom: 0, top: 5),
                                      child: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.only(
                                              left: 10, top: 15, bottom: 15),
                                          margin: EdgeInsets.only(
                                              left: 0, right: 0, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                editplaceleaveDate,
                                                style: TextStyle(
                                                    color: editplaceleaveDate ==
                                                            "Leaving Date"
                                                        ? Colors.black38
                                                        : Colors.black,
                                                    fontSize: 15,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(
                                                    Icons.calendar_today,
                                                    color: Colors.black26,
                                                    size: 16,
                                                  ))
                                            ],
                                          )),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Flexible(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              bottom: 0, top: 5),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            //mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  padding: EdgeInsets.all(0),
                                                  margin: EdgeInsets.only(
                                                      left: 0,
                                                      right: 0,
                                                      top: 5),
                                                  decoration: BoxDecoration(
                                                      color: Colors.white
                                                          .withOpacity(0.7),
                                                      border: Border.all(
                                                          color: Colors.grey,
                                                          width: 0.2),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: SingleChildScrollView(
                                                    child: TextField(
                                                      maxLines: null,
                                                      maxLength: 150,
                                                      controller:
                                                          editdesOtherController,
                                                      keyboardType:
                                                          TextInputType.text,
                                                      style: TextStyle(
                                                        color: Colors.black87,
                                                        fontFamily: 'Oswald',
                                                      ),
                                                      decoration:
                                                          InputDecoration(
                                                        hintText: "Description",
                                                        hintStyle: TextStyle(
                                                            color:
                                                                Colors.black38,
                                                            fontSize: 15,
                                                            fontFamily:
                                                                'Oswald',
                                                            fontWeight:
                                                                FontWeight
                                                                    .w300),
                                                        //labelStyle: TextStyle(color: Colors.white70),
                                                        contentPadding:
                                                            EdgeInsets.fromLTRB(
                                                                10.0,
                                                                0,
                                                                10.0,
                                                                0),
                                                        border:
                                                            InputBorder.none,
                                                      ),
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.only(
                                                  left: 0,
                                                  right: 10,
                                                  top: 20,
                                                  bottom: 0),
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.3),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Text(
                                                "Cancel",
                                                style: TextStyle(
                                                  color: Colors.black45,
                                                  fontSize: 12,
                                                  fontFamily: 'BebasNeue',
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                            setState(() {
                                              print(places['city']);
                                              print(editplaceController.text);
                                              places['city'] =
                                                  editplaceController.text;
                                              places['movedDate'] =
                                                  editplaceMovedDateServer;
                                              places['leftDate'] =
                                                  editplaceleaveDateServer;
                                              places['description'] =
                                                  editdesOtherController.text;
                                              places['isEdit'] = true;
                                              print(places);
                                              print(locationOther);

                                              sendLocation();
                                              editplaceController.text = "";
                                              editdesOtherController.text = "";
                                              editplaceleaveDate =
                                                  "Leaving Date";
                                              editplaceMovedDate =
                                                  "Moving Date";
                                            });
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.only(
                                                  left: 10,
                                                  right: 0,
                                                  top: 20,
                                                  bottom: 0),
                                              decoration: BoxDecoration(
                                                  color: mainColor,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Text(
                                                "Edit",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontFamily: 'BebasNeue',
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        });
  }

  Future<void> sendLocation() async {
    var data = {
      'currentCity': locationCur[0],
      'homeTown': locationHome[0],
      'otherPlaces': locationOther,
    };

    print(data);

    var res = await CallApi().postData1(data, 'profile/place');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      setState(() {
        placeController.text = "";
        desOtherController.text = "";
        placeleaveDate = "Leaving Date";
        placeMovedDate = "Moving Date";
      });
      _showMessage("Success!", 2);
    } else {
      _showMessage(body['message'], 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
