import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/ConfirmEmailPage/ConfirmEmailPage.dart';

import '../../main.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController usernameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  TextEditingController conPassController = new TextEditingController();
  String birthDate = "Birth Date", birthDateServer = "";
  String _radioValue; //Initial definition of radio button value
  String gender = "";
  String username = "";
  DateTime selectedDate = DateTime.now();
  bool isSpace = false;
  bool isSubmit = false;
  bool isUserAvailable = false;
  bool isContainer = false;

  @override
  void initState() {
    setState(() {
      birthDate = DateFormat.yMMMd().format(selectedDate);
      print("birthDate");
      print(birthDate);
    });
    super.initState();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        birthDate = DateFormat.yMMMd().format(selectedDate);
        birthDateServer = DateFormat('yyyy-MM-dd').format(selectedDate);

        print("birthDate");
        print(birthDate);
      });
  }

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'male':
          gender = value;
          break;
        case 'female':
          gender = value;
          break;
        default:
          gender = null;
      }
      debugPrint(gender); //Debug the choice in console
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: 0, bottom: 10, left: 30),
                  padding: EdgeInsets.only(top: 1, bottom: 1),
                  child: Text(
                    "Register",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 22,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w500),
                  )),
              Container(
                margin: EdgeInsets.only(bottom: 5, top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: firstNameController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Oswald',
                          ),
                          decoration: InputDecoration(
                            hintText: "First Name",
                            hintStyle: TextStyle(
                                color: Colors.black38,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w300),
                            //labelStyle: TextStyle(color: Colors.white70),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            border: InputBorder.none,
                          ),
                        )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 5, top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: lastNameController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Oswald',
                          ),
                          decoration: InputDecoration(
                            hintText: "Last Name",
                            hintStyle: TextStyle(
                                color: Colors.black38,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w300),
                            //labelStyle: TextStyle(color: Colors.white70),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            border: InputBorder.none,
                          ),
                        )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 0, top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: usernameController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Oswald',
                          ),
                          decoration: InputDecoration(
                            hintText: "Username",
                            hintStyle: TextStyle(
                                color: Colors.black38,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w300),
                            //labelStyle: TextStyle(color: Colors.white70),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            border: InputBorder.none,
                          ),
                          onChanged: (value) {
                            setState(() {
                              username = value;
                              if (username == "") {
                                isContainer = false;
                                isUserAvailable = false;
                              } else {
                                isContainer = true;
                                if (username.contains(" ")) {
                                  isSpace = true;
                                  isUserAvailable = false;
                                } else {
                                  isSpace = false;
                                  checkUserName(username);
                                }
                              }
                            });
                          },
                        )),
                  ],
                ),
              ),
              !isContainer
                  ? Container()
                  : Container(
                      margin: EdgeInsets.only(bottom: 5, top: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                  left: 10, top: 15, bottom: 15),
                              margin:
                                  EdgeInsets.only(left: 30, right: 30, top: 0),
                              decoration: BoxDecoration(
                                  color: isUserAvailable
                                      ? Colors.green.withOpacity(0.1)
                                      : Colors.red.withOpacity(0.1),
                                  border: Border.all(
                                      color: isUserAvailable
                                          ? Colors.greenAccent
                                          : Colors.redAccent,
                                      width: 0.5),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      margin: EdgeInsets.only(left: 5),
                                      padding: EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          color: isUserAvailable
                                              ? Colors.greenAccent
                                              : Colors.redAccent,
                                          borderRadius:
                                              BorderRadius.circular(25)),
                                      child: Icon(
                                        isUserAvailable
                                            ? Icons.done
                                            : Icons.close,
                                        size: 12,
                                        color: Colors.white,
                                      )),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Text(
                                        isSpace
                                            ? "Username cannot contain blank spaces!"
                                            : isUserAvailable
                                                ? "This username is available!"
                                                : "This username is not available",
                                        style: TextStyle(
                                            color: Colors.black38,
                                            fontSize: 15,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        isSpace = false;
                                      });
                                    },
                                    child: Container(
                                        margin: EdgeInsets.only(
                                            right: 10, left: 5, bottom: 5),
                                        child: Icon(
                                          Icons.close,
                                          size: 18,
                                          color: Colors.grey,
                                        )),
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ),
              Container(
                margin: EdgeInsets.only(bottom: 5, top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: emailController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Oswald',
                          ),
                          decoration: InputDecoration(
                            hintText: "Email",
                            hintStyle: TextStyle(
                                color: Colors.black38,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w300),
                            //labelStyle: TextStyle(color: Colors.white70),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            border: InputBorder.none,
                          ),
                        )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 5, top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: passController,
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Oswald',
                          ),
                          decoration: InputDecoration(
                            hintText: "Password",
                            hintStyle: TextStyle(
                                color: Colors.black38,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w300),
                            //labelStyle: TextStyle(color: Colors.white70),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            border: InputBorder.none,
                          ),
                        )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 5, top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: conPassController,
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Oswald',
                          ),
                          decoration: InputDecoration(
                            hintText: "Confirm Password",
                            hintStyle: TextStyle(
                                color: Colors.black38,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w300),
                            //labelStyle: TextStyle(color: Colors.white70),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            border: InputBorder.none,
                          ),
                        )),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  _selectDate(context);
                },
                child: Container(
                  margin: EdgeInsets.only(bottom: 5, top: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding:
                              EdgeInsets.only(left: 10, top: 15, bottom: 15),
                          margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.7),
                              border:
                                  Border.all(color: Colors.grey, width: 0.2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: Text(
                            birthDate,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w400),
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 0, top: 15, bottom: 0),
                  margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                  child: Text(
                    "Gender",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400),
                  )),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 0),
                child: Row(
                  children: <Widget>[
                    Radio(
                      value: 'male',
                      groupValue: _radioValue,
                      onChanged: radioButtonChanges,
                    ),
                    Text(
                      "Male",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 15,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.w400),
                    ),
                    Radio(
                      value: 'female',
                      groupValue: _radioValue,
                      onChanged: radioButtonChanges,
                    ),
                    Text(
                      "Female",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 15,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  isSubmit ? null : signUp();
                },
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
                    margin: EdgeInsets.only(
                        left: 30, right: 30, top: 5, bottom: 20),
                    decoration: BoxDecoration(
                        color:
                            isSubmit ? Colors.grey.withOpacity(0.5) : mainColor,
                        border: Border.all(color: Colors.grey, width: 0.2),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Text(
                      isSubmit ? "Please wait..." : "Sign Up",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.w500),
                    )),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(
                            left: 30, right: 0, top: 5, bottom: 20),
                        child: Text(
                          "Already have an account? ",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 15,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w500),
                        )),
                    GestureDetector(
                      onTap: () {
                        _selectDate(context);
                      },
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(
                              left: 0, right: 30, top: 5, bottom: 20),
                          child: Text(
                            "Sign in",
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w500),
                          )),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Future<void> checkUserName(String username) async {
    if (username != "") {
      var data = {'userName': username};
      var res = await CallApi().postData(data, 'user/verify-unique/userName');
      var body = json.decode(res.body);
      print(body);

      setState(() {
        //isSpace = true;
        if (res.statusCode == 200) {
          isUserAvailable = true;
        } else {
          isUserAvailable = false;
        }

        print(isUserAvailable);
      });
    } else {
      isSpace = false;
      isUserAvailable = false;
    }
  }

  Future<void> signUp() async {
    if (firstNameController.text.isEmpty) {
      _showMessage("First Name cannot be blank!", 1);
    } else if (lastNameController.text.isEmpty) {
      _showMessage("Last Name cannot be blank!", 1);
    } else if (usernameController.text.isEmpty) {
      _showMessage("Username cannot be blank!", 1);
    } else if (emailController.text.isEmpty) {
      _showMessage("Email cannot be blank!", 1);
    } else if (passController.text.isEmpty) {
      _showMessage("Password cannot be blank!", 1);
    } else if (conPassController.text.isEmpty) {
      _showMessage("Confirm password cannot be blank!", 1);
    } else if (birthDate == "Birth Date") {
      _showMessage("Select a birth date!", 1);
    } else if (gender == "") {
      _showMessage("Select a Gender!", 1);
    } else if (passController.text != conPassController.text) {
      _showMessage("Password doesn't match!", 1);
    } else {
      setState(() {
        isSubmit = true;
      });

      var data = {
        'firstName': firstNameController.text,
        'lastName': lastNameController.text,
        'userName': usernameController.text,
        'email': emailController.text,
        'birthDay': birthDateServer,
        'gender': gender,
        'password': passController.text,
        'deviceAppToken': deviceToken,
      };

      print(data);

      var res = await CallApi().postData(data, 'user/register');
      var body = json.decode(res.body);
      print(body);

      if (res.statusCode == 201) {
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('token', body['accessToken']);
        localStorage.setString('user', json.encode(body['user']));

        _showMessage("Registration successful!", 2);
        _showMessage("You are now logged in!", 2);

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ConfirmEmailPage(emailController.text)));
      } else {
        _showMessage("Something went wrong!", 1);
      }
      setState(() {
        isSubmit = false;
      });
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
