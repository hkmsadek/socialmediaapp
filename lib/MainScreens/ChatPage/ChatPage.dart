import 'dart:async';
import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Cards/ChatListCard/chatListCard.dart';
import 'package:social_app_fb/Cards/ChatStoryCard/chatStorycard.dart';
import 'package:social_app_fb/ModelClass/ChatListModel/chatListModel.dart';

import '../../main.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  SharedPreferences sharedPreferences;
  String theme = "";
  Timer _timer;
  int _start = 3;
  bool loading = true;
  String lMsg = "";
  bool isSeen = false;
  var chatLists, body, userData;
  List chats = [];
  int conId = 0;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    _getUserInfo();
    lastMsg.clear();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        setState(() {
          lastMessages = message['data']['msg'];
        });
      },
      //onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        //_navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        //_navigateToItemDetail(message);
      },
    );
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');

    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
    }
    loadChatList();
  }

  Future loadChatList() async {
    var key = 'chat-list-user';
    await _getChatListData(key);

    setState(() {
      loading = true;
    });
    var response = await CallApi().getData2('messenger/conversation/get/list');
    body = json.decode(response.body);

    if (response.statusCode == 200) {
      _chatListState();

      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString(key, json.encode(body));
    }
  }

  Future _getChatListData(key) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var localbestProductsData = localStorage.getString(key);
    if (localbestProductsData != null) {
      body = json.decode(localbestProductsData);
      _chatListState();
    }
  }

  void _chatListState() {
    var chats = ChatListModel.fromJson(body);
    if (!mounted) return;
    setState(() {
      chatLists = chats.lists;
      loading = false;
      lastMsg = [];
      lastSeen = [];

      for (int i = 0; i < chatLists.length; i++) {
        print(chatLists[i].seen);
        lastMsg.add(chatLists[i].message);
        if (chatLists[i].messageSender == userData['id']) {
          isSeen = false;
        } else if (chatLists[i].messageSender != userData['id']) {
          if (chatLists[i].seen == 0) {
            isSeen = false;
          } else {
            isSeen = true;
          }
        }
        lastSeen.add(isSeen);
      }

      print("lastMsg");
      print(lastMsg);
      print("lastSeen");
      print(lastSeen);
    });

    // print(productsData);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Chat",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/white.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: null,
          ),
          Container(
            margin: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
            child: CustomScrollView(
              physics: BouncingScrollPhysics(),
              slivers: <Widget>[
                SliverToBoxAdapter(
                  child: Column(
                    children: <Widget>[
                      Container(
                        //color: Colors.white,
                        margin: EdgeInsets.only(top: 5),
                        padding: EdgeInsets.only(top: 0, bottom: 0),
                        decoration: BoxDecoration(
                          //color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          //border: Border.all(width: 0.5, color: Colors.grey[400]),
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 10, top: 4),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  //mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(10),
                                        margin: EdgeInsets.only(
                                            left: 10, right: 10, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.4),
                                            border: Border.all(
                                                color: Colors.black
                                                    .withOpacity(0.4),
                                                width: 0.5),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(25))),
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                                child: Icon(Icons.search,
                                                    color: Colors.black45,
                                                    size: 17)),
                                            Flexible(
                                              child: TextField(
                                                //controller: phoneController,
                                                style: TextStyle(
                                                  color: Colors.black45,
                                                  fontFamily: 'Oswald',
                                                ),
                                                decoration: InputDecoration(
                                                  hintText: "Search",
                                                  hintStyle: TextStyle(
                                                      color: Colors.black45,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                  //labelStyle: TextStyle(color: Colors.white70),
                                                  contentPadding:
                                                      EdgeInsets.fromLTRB(
                                                          10.0, 1, 20.0, 1),
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 0, right: 0, top: 5, bottom: 0),
                        //color: back_new,
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(left: 0),
                        height: 95,
                        child: new ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) =>
                              ChatStoryCard(index),
                          itemCount: 6,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                      )
                    ],
                  ),
                ),
                loading == false
                    ? SliverList(
                        delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                          return ChatListCard(
                              userData, chatLists[index], index);
                        }, childCount: chatLists.length),
                      )
                    : SliverList(
                        delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                          return Container(
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              //border: Border.all(width: 0.8, color: Colors.grey[300]),
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 1.0,
                                  color: Colors.black38,
                                  //offset: Offset(6.0, 7.0),
                                ),
                              ],
                            ),
                            margin: EdgeInsets.only(
                                top: 2.5, bottom: 2.5, left: 10, right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    //color: Colors.red,
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 0),
                                    padding: EdgeInsets.only(right: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(right: 10),
                                          //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                          padding: EdgeInsets.all(1.0),
                                          child: Shimmer.fromColors(
                                            baseColor: Colors.grey[200],
                                            highlightColor: Colors.grey[200],
                                            child: CircleAvatar(
                                              radius: 20.0,
                                              backgroundColor: Colors.white,
                                            ),
                                          ),
                                          decoration: new BoxDecoration(
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Shimmer.fromColors(
                                                baseColor: Colors.grey[200],
                                                highlightColor:
                                                    Colors.grey[200],
                                                child: Container(
                                                  width: 150,
                                                  height: 22,
                                                  child: Container(
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top: 3),
                                                child: Shimmer.fromColors(
                                                  baseColor: Colors.grey[200],
                                                  highlightColor:
                                                      Colors.grey[200],
                                                  child: Container(
                                                    width: 90,
                                                    height: 12,
                                                    child: Container(
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }, childCount: 2),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
