import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:social_app_fb/MainScreens/SigninPage/SigninPage.dart';
import 'package:social_app_fb/MainScreens/SignupPage/SignupPage.dart';

import '../../main.dart';

class LoginRegisterPage extends StatefulWidget {
  @override
  _LoginRegisterPageState createState() => _LoginRegisterPageState();
}

class _LoginRegisterPageState extends State<LoginRegisterPage> {
  bool isLoginReg = true;

  Future<bool> _onWillPop() async {
    return (SystemChannels.platform.invokeMethod('SystemNavigator.pop')) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            actions: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        if (isLoginReg == false) {
                          isLoginReg = true;
                        }
                      });
                    },
                    child: Container(
                        width: 80,
                        height: 27,
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        decoration: BoxDecoration(
                            color:
                                isLoginReg == true ? mainColor : Colors.white,
                            border: Border.all(color: Colors.grey, width: 0.5)),
                        padding: EdgeInsets.only(top: 1, bottom: 1),
                        child: Text(
                          "Log in",
                          style: TextStyle(
                              fontFamily: 'Oswald',
                              color: isLoginReg == true
                                  ? Colors.white
                                  : Colors.black),
                        )),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        if (isLoginReg == true) {
                          isLoginReg = false;
                        }
                      });
                    },
                    child: Container(
                        width: 80,
                        height: 27,
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(top: 10, bottom: 10, right: 30),
                        decoration: BoxDecoration(
                            color:
                                isLoginReg == false ? mainColor : Colors.white,
                            border: Border.all(color: Colors.grey, width: 0.5)),
                        padding: EdgeInsets.only(top: 1, bottom: 1),
                        child: Text(
                          "Register",
                          style: TextStyle(
                              fontFamily: 'Oswald',
                              color: isLoginReg == false
                                  ? Colors.white
                                  : Colors.black),
                        )),
                  ),
                ],
              )
            ],
          ),
          body: isLoginReg ? SigninPage() : SignupPage()),
    );
  }
}
