import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';
import 'package:social_app_fb/MainScreens/ReplyPage/ReplyPage.dart';
import 'package:social_app_fb/ModelClass/CommentModel/CommentModel.dart';
import 'package:social_app_fb/main.dart';

List repComCount = [];

class CommentPage extends StatefulWidget {
  final userData;
  final index;
  final feedList;
  CommentPage(this.userData, this.index, this.feedList);
  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  var comList;
  var userData;
  bool loading = true, isSubmit = false;
  TextEditingController comEditor = new TextEditingController();
  TextEditingController comEditorCont = new TextEditingController();
  TextEditingController editingController = new TextEditingController();
  ScrollController scrollController = new ScrollController();
  String com = "", totalLike = "", date = "";
  List commentList = [];

  @override
  void initState() {
    setState(() {
      repComCount = [];
      userData = widget.userData;
    });
    loadComments();
    super.initState();
  }

  Future loadComments() async {
    //await Future.delayed(Duration(seconds: 3));

    setState(() {
      loading = true;
    });
    var comresponse = await CallApi()
        .getData('statusComment/show?status_id=${widget.feedList.id}');
    var postcontent = comresponse.body;
    final comments = json.decode(postcontent);
    var comdata = CommentModel.fromJson(comments);

    setState(() {
      comList = comdata;
      loading = false;
      totalLike = "1";

      for (int i = 0; i < comList.statusComment.length; i++) {
        repComCount.add({'count': comList.statusComment[i].meta.totalRepliesCount});

        commentList.add({
          'id': comList.statusComment[i].id,
          'comment': comList.statusComment[i].comment,
          'uId': comList.statusComment[i].user.id,
          'pic': comList.statusComment[i].user.profilePic,
          'fName': comList.statusComment[i].user.firstName,
          'lName': comList.statusComment[i].user.lastName,
          'created': comList.statusComment[i].createdAt,
          'myLike': comList.statusComment[i].like,
          'totalLike': comList.statusComment[i].meta.totalLikesCount,
          'totalReply': comList.statusComment[i].meta.totalRepliesCount,
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Comments",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          loading
              ? LoaderScreen()
              : commentList.length == 0
                  ? Center(
                      child: Container(
                        child: Text(
                          "No Comments",
                          style: TextStyle(
                              fontSize: 13,
                              color: Colors.black,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    )
                  : SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                          margin: EdgeInsets.only(bottom: 50),
                          child: Container(
                            child: Column(
                                children:
                                    List.generate(commentList.length, (index) {
                              DateTime dateTime =
                                  DateTime.parse(commentList[index]['created']);
                              date = DateFormat.yMMMd().format(dateTime);
                              return Container(
                                padding: EdgeInsets.only(top: 0, bottom: 5),
                                margin: EdgeInsets.only(
                                    top: 0, bottom: 0, left: 20, right: 10),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                          top: 10),
                                      padding: EdgeInsets.only(right: 0),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ////// <<<<< pic start >>>>> //////
                                          Container(
                                            margin: EdgeInsets.only(right: 10),
                                            padding: EdgeInsets.all(1.0),
                                            child: CircleAvatar(
                                              radius: 15.0,
                                              backgroundColor: Colors.white,
                                              backgroundImage: AssetImage(
                                                  "assets/images/prabal.jpg"),
                                            ),
                                            decoration: new BoxDecoration(
                                              color: Colors.grey[300],
                                              shape: BoxShape.circle,
                                            ),
                                          ),
                                          ////// <<<<< pic end >>>>> //////

                                          Expanded(
                                            child: Container(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  ////// <<<<< Name start >>>>> //////
                                                  Container(
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Container(
                                                            child: Text(
                                                              "${commentList[index]['fName']} ${commentList[index]['lName']}",
                                                              maxLines: 1,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              style: TextStyle(
                                                                  fontSize: 15,
                                                                  color:
                                                                      mainColor,
                                                                  fontFamily:
                                                                      'Oswald',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            children: <Widget>[
                                                              userData['id'] !=
                                                                      commentList[
                                                                              index]
                                                                          [
                                                                          'uId']
                                                                  ? Container(
                                                                      margin: EdgeInsets
                                                                          .only(
                                                                              top: 10),
                                                                    )
                                                                  : GestureDetector(
                                                                      onTap:
                                                                          () {
                                                                        setState(
                                                                            () {
                                                                          _statusModalBottomSheet(
                                                                              context,
                                                                              index,
                                                                              userData,
                                                                              commentList[index]);
                                                                          editingController.text =
                                                                              commentList[index]['comment'];
                                                                        });
                                                                      },
                                                                      child: Container(
                                                                          margin: EdgeInsets.only(right: 15),
                                                                          // color: Colors.blue,
                                                                          child: Icon(
                                                                            Icons.more_horiz,
                                                                            color:
                                                                                Colors.black54,
                                                                          )),
                                                                    ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  ////// <<<<< Name end >>>>> //////
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                        left: 0,
                                                        right: 20,
                                                        top: 3,
                                                      ),
                                                      child: Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Container(
                                                          child: Text(
                                                            "${commentList[index]['comment']}",
                                                            textAlign: TextAlign
                                                                .justify,
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                color: Colors
                                                                    .black,
                                                                fontFamily:
                                                                    "OSwald",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400),
                                                          ),
                                                        ),
                                                      )),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 0, top: 5),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 0),
                                                          child: Text(date,
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      'Oswald',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300,
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize:
                                                                      12)),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 15),
                                                          child: Row(
                                                            children: <Widget>[
                                                              Container(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            3.0),
                                                                child:
                                                                    GestureDetector(
                                                                  onTap: () {
                                                                    if (commentList[index]
                                                                            [
                                                                            'myLike'] !=
                                                                        null) {
                                                                      setState(
                                                                          () {
                                                                        commentList[index]['myLike'] =
                                                                            null;
                                                                        print(commentList[index]
                                                                            [
                                                                            'totalLike']);
                                                                        commentList[index]
                                                                            [
                                                                            'totalLike'] -= 1;
                                                                        print(commentList[index]
                                                                            [
                                                                            'totalLike']);
                                                                      });
                                                                      likeUnlike(
                                                                          index,
                                                                          2,
                                                                          commentList[index]
                                                                              [
                                                                              'id']);
                                                                    } else {
                                                                      setState(
                                                                          () {
                                                                        commentList[index]
                                                                            [
                                                                            'myLike'] = 1;
                                                                        print(commentList[index]
                                                                            [
                                                                            'totalLike']);
                                                                        commentList[index]
                                                                            [
                                                                            'totalLike'] += 1;
                                                                        print(commentList[index]
                                                                            [
                                                                            'totalLike']);
                                                                      });
                                                                      likeUnlike(
                                                                          index,
                                                                          1,
                                                                          commentList[index]
                                                                              [
                                                                              'id']);
                                                                    }
                                                                  },
                                                                  child: Icon(
                                                                    commentList[index]['myLike'] ==
                                                                            null
                                                                        ? Icons
                                                                            .favorite_border
                                                                        : Icons
                                                                            .favorite,
                                                                    size: 20,
                                                                    color: commentList[index]['myLike'] ==
                                                                            null
                                                                        ? Colors
                                                                            .black54
                                                                        : Colors
                                                                            .redAccent,
                                                                  ),
                                                                ),
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            3),
                                                                child: Text(
                                                                    "${commentList[index]['totalLike']}",
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'Oswald',
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w300,
                                                                        color: Colors
                                                                            .black54,
                                                                        fontSize:
                                                                            12)),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder: (context) => ReplyPage(
                                                                        commentList[
                                                                            index],
                                                                        index,
                                                                        widget
                                                                            .userData,
                                                                        widget
                                                                            .feedList
                                                                            .id)));
                                                          },
                                                          child: Container(
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              15),
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              3.0),
                                                                  child: Icon(
                                                                    Icons
                                                                        .chat_bubble_outline,
                                                                    size: 20,
                                                                    color: Colors
                                                                        .black54,
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              3),
                                                                  child: Text(
                                                                      repComCount == null
                                                                          ? "0"
                                                                          : "${repComCount[index]['count']}",
                                                                      style: TextStyle(
                                                                          fontFamily:
                                                                              'Oswald',
                                                                          fontWeight: FontWeight
                                                                              .w300,
                                                                          color: Colors
                                                                              .black54,
                                                                          fontSize:
                                                                              12)),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  repComCount[index]['count'] ==
                                                          0
                                                      ? Container()
                                                      : GestureDetector(
                                                          onTap: () {
                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder: (context) => ReplyPage(
                                                                        commentList[
                                                                            index],
                                                                        index,
                                                                        widget
                                                                            .userData,
                                                                        widget
                                                                            .feedList
                                                                            .id)));
                                                          },
                                                          child: Container(
                                                            width:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                    "View replies...",
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'Oswald',
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                        color: Colors
                                                                            .black54,
                                                                        fontSize:
                                                                            12)),
                                                              ],
                                                            ),
                                                          ),
                                                        ),

                                                  Divider()
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            })),
                          ))),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  color: Colors.white,
                  child: Row(
                    children: <Widget>[
                      ////// <<<<< Comment Box Text Field >>>>> //////
                      Flexible(
                        child: Container(
                          //height: 100,
                          padding: EdgeInsets.all(0),
                          margin: EdgeInsets.only(
                              top: 5, left: 10, bottom: 5, right: 10),
                          decoration: BoxDecoration(
                              // borderRadius:
                              //     BorderRadius.all(Radius.circular(100.0)),
                              borderRadius: new BorderRadius.only(
                                  topLeft: Radius.circular(5.0),
                                  topRight: Radius.circular(5.0),
                                  bottomLeft: Radius.circular(5.0),
                                  bottomRight: Radius.circular(5.0)),
                              color: Colors.grey[100],
                              border: Border.all(
                                  width: 0.5,
                                  color: Colors.black.withOpacity(0.2))),
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: new ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxHeight: 120.0,
                                  ),
                                  child: new SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    reverse: true,
                                    child: new TextField(
                                      maxLines: null,
                                      autofocus: false,
                                      controller: comEditor,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Oswald',
                                      ),
                                      decoration: InputDecoration(
                                        hintText: "Type a comment here...",
                                        hintStyle: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 15,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300),
                                        contentPadding: EdgeInsets.fromLTRB(
                                            10.0, 10.0, 20.0, 10.0),
                                        border: InputBorder.none,
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          com = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),

                      ////// <<<<< Comment Send Icon Button >>>>> //////
                      GestureDetector(
                        onTap: () {
                          addComment(widget.index);
                          setState(() {
                            comEditor.text = "";
                            commentList.add({
                              'id': null,
                              'comment': com,
                              'uId': userData['id'],
                              'pic': userData['profilePic'],
                              'fName': userData['firstName'],
                              'lName': userData['lastName'],
                              'created': userData['created_at'],
                              'myLike': null,
                              'totalLike': 0,
                              'totalReply': 0,
                            });

                            //postComCount[widget.index]['count'] += 1;
                            repComCount.add({'count': 0});

                            //print(commentList);
                          });

                          //postReplyList.add({'count': 0});
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: back,
                              borderRadius: BorderRadius.circular(25)),
                          margin: EdgeInsets.only(right: 10),
                          padding: EdgeInsets.all(8),
                          child: Icon(
                            Icons.send,
                            color: mainColor,
                            size: 23,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void addComment(index) {
    if (comEditor.text.isEmpty) {
    } else {
      commentSend(index);
    }
  }

  Future commentSend(index) async {
    setState(() {
      isSubmit = true;
    });

    var data = {
      'comment': comEditor.text,
      'status_id': widget.feedList.id,
      'user_id': widget.userData['id'],
    };

    print(data);

    var res = await CallApi().postData1(data, 'statusComment/add');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      if (commentList.length != 0 && repComCount.length != 0) {}
      commentList.removeAt(commentList.length - 1);
      repComCount.removeAt(repComCount.length - 1);
      comEditor.text = "";
      setState(() {
        postComCount[widget.index]['count'] += 1;
        commentList.add({
          'id': body['addStatusCmnt']['id'],
          'comment': body['addStatusCmnt']['comment'],
          'uId': body['addStatusCmnt']['user']['id'],
          'pic': body['addStatusCmnt']['user']['profilePic'],
          'fName': body['addStatusCmnt']['user']['firstName'],
          'lName': body['addStatusCmnt']['user']['lastName'],
          'created': body['addStatusCmnt']['created_at'],
          'myLike': null,
          'totalLike': 0,
          'totalReply': 0,
        });
      });
      repComCount.add({'count': 0});
    } else {
      comEditor.text = "";
      repComCount.removeAt(commentList.length - 1);
      commentList.removeAt(commentList.length - 1);
      postComCount[index]['count'] -= 1;
    }

    setState(() {
      isSubmit = false;
    });

    //loadComments();
  }

  void editComment(id, index) {
    if (editingController.text.isEmpty) {
    } else {
      commentEdit(id, index);
    }
  }

  Future commentEdit(id, index) async {
    setState(() {
      isSubmit = true;
    });

    var data = {
      'comment': editingController.text,
      'status_id': widget.feedList.id,
      'user_id': widget.userData['id'],
    };

    print(data);

    var res = await CallApi().postData4(data, 'statusComment/edit?id=$id');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      if (commentList.length != 0 && repComCount.length != 0) {}
      editingController.text = "";
    } else {
      editingController.text = "";
    }

    setState(() {
      isSubmit = false;
    });

    //loadComments();
  }

  void _statusModalBottomSheet(context, int index, var userData, var feed) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                // Text('React to this post',
                //       style: TextStyle(fontWeight: FontWeight.normal)),
                new ListTile(
                  leading: new Icon(Icons.edit),
                  title: new Text('Edit',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    _showEditDialog(feed, index),
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  title: new Text('Delete',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.redAccent,
                          fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    _showDeleteDialog(feed['id'], index),
                  },
                ),
              ],
            ),
          );
        });
  }

  Future<Null> _showDeleteDialog(int id, index) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    // Container(
                    //     decoration: BoxDecoration(
                    //         border: Border.all(color: header, width: 1.5),
                    //         borderRadius: BorderRadius.circular(100),
                    //         color: Colors.white),
                    //     child: Icon(
                    //       Icons.done,
                    //       color: header,
                    //       size: 50,
                    //     )),
                    Container(
                        margin: EdgeInsets.only(top: 12),
                        child: Text(
                          "Want to delete the post?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                                deleteStatus(id, index);
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<Null> _showEditDialog(feed, index) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      //height: 100,
                      padding: EdgeInsets.all(0),
                      margin:
                          EdgeInsets.only(top: 5, left: 0, bottom: 5, right: 0),
                      decoration: BoxDecoration(
                          // borderRadius:
                          //     BorderRadius.all(Radius.circular(100.0)),
                          borderRadius: new BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0)),
                          color: Colors.grey[100],
                          border: Border.all(
                              width: 0.5,
                              color: Colors.black.withOpacity(0.2))),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: new ConstrainedBox(
                              constraints: BoxConstraints(
                                maxHeight: 120.0,
                              ),
                              child: new SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                reverse: true,
                                child: new TextField(
                                  maxLines: null,
                                  autofocus: false,
                                  controller: editingController,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Oswald',
                                  ),
                                  decoration: InputDecoration(
                                    hintText: "Type a comment here...",
                                    hintStyle: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 15,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w300),
                                    contentPadding: EdgeInsets.fromLTRB(
                                        10.0, 10.0, 20.0, 10.0),
                                    border: InputBorder.none,
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      //comment = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.of(context).pop();
                                });
                              },
                              child: Container(
                                  padding: EdgeInsets.all(10),
                                  margin: EdgeInsets.only(
                                      left: 0, right: 10, top: 10, bottom: 0),
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.5),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  child: Text(
                                    "Cancel",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: 'BebasNeue',
                                    ),
                                    textAlign: TextAlign.center,
                                  )),
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.of(context).pop();
                                  editComment(commentList[index]['id'], index);
                                  commentList[index]['comment'] =
                                      editingController.text;
                                });
                              },
                              child: Container(
                                  padding: EdgeInsets.all(10),
                                  margin: EdgeInsets.only(
                                      left: 10, right: 0, top: 10, bottom: 0),
                                  decoration: BoxDecoration(
                                      color: mainColor,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  child: Text(
                                    "OK",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: 'BebasNeue',
                                    ),
                                    textAlign: TextAlign.center,
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future deleteStatus(int id, index) async {
    var data = {'id': id};

    print(data);

    var res = await CallApi().postData1(data, 'statusComment/delete');
    var body = json.decode(res.body);

    print(body);

    if (body == 1) {
      setState(() {
        postComCount[widget.index]['count'] -= 1;
        //isDeleted.add("$id");
        repComCount.removeAt(widget.index);
        commentList.removeAt(index);
      });
    } else {
      _showMessage("Something went wrong!", 1);
    }
    // Navigator.pushReplacement(
    //     context, MaterialPageRoute(builder: (_) => FeedPage()));
  }

  Future likeUnlike(index, status, id) async {
    var data = {
      'user_id': userData['id'],
      'status_id': widget.feedList.id,
      'type': status,
      'comment_id': id,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/comment/like');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      //Navigator.pop(context);

    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
