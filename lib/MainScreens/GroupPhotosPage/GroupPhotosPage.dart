import 'package:flutter/material.dart';
import 'package:social_app_fb/main.dart';

class GroupPhotosPage extends StatefulWidget {
  @override
  _GroupPhotosPageState createState() => _GroupPhotosPageState();
}

class _GroupPhotosPageState extends State<GroupPhotosPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Photos",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.only(top: 5),
          color: Colors.white,
          alignment: Alignment.topCenter,
          child: Wrap(children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                          right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
                      height: 130,
                      width: 130,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              image: AssetImage('assets/images/white.jpg'),
                              fit: BoxFit.cover)),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
                      height: 130,
                      width: 130,
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.3),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.add),
                          Container(
                            width: 120,
                            margin: EdgeInsets.only(top: 5),
                            child: Text(
                              "Add Photo",
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.black54,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/car1.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/car2.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/car3.jpeg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/bike1.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/bike2.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/bike3.jpeg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/bike4.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/car4.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/bike5.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/bike6.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/car6.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/car5.jpg'),
                      fit: BoxFit.cover)),
            ),
            Container(
              margin:
                  EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
              height: 130,
              width: 130,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/car7.jpg'),
                      fit: BoxFit.cover)),
            ),
          ]),
        ),
      ),
    );
  }
}
