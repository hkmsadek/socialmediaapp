import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/ConfirmEmailPage/ConfirmEmailPage.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/ForgetPassPage/ForgetPassPage.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';
import 'package:web_socket_channel/io.dart';

import '../../main.dart';

class SigninPage extends StatefulWidget {
  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  TextEditingController tokenController = new TextEditingController();
  bool isSubmit = false;

  @override
  void initState() {
    setState(() {
      tokenController.text = deviceToken;
    });

    print(deviceToken);
    print(tokenController.text);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 0, bottom: 10, left: 30),
                    padding: EdgeInsets.only(top: 1, bottom: 1),
                    child: Text(
                      "Login",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 22,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.w500),
                    )),
                Container(
                  margin: EdgeInsets.only(bottom: 5, top: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(0),
                          margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.7),
                              border:
                                  Border.all(color: Colors.grey, width: 0.2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: TextField(
                            controller: emailController,
                            keyboardType: TextInputType.text,
                            style: TextStyle(
                              color: Colors.black87,
                              fontFamily: 'Oswald',
                            ),
                            decoration: InputDecoration(
                              hintText: "Email",
                              hintStyle: TextStyle(
                                  color: Colors.black38,
                                  fontSize: 15,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w300),
                              //labelStyle: TextStyle(color: Colors.white70),
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                              border: InputBorder.none,
                            ),
                          )),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 5, top: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(0),
                          margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.7),
                              border:
                                  Border.all(color: Colors.grey, width: 0.2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: TextField(
                            controller: passController,
                            keyboardType: TextInputType.text,
                            obscureText: true,
                            style: TextStyle(
                              color: Colors.black87,
                              fontFamily: 'Oswald',
                            ),
                            decoration: InputDecoration(
                              hintText: "Password",
                              hintStyle: TextStyle(
                                  color: Colors.black38,
                                  fontSize: 15,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w300),
                              //labelStyle: TextStyle(color: Colors.white70),
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                              border: InputBorder.none,
                            ),
                          )),
                    ],
                  ),
                ),
                // Container(
                //   margin: EdgeInsets.only(bottom: 5, top: 5),
                //   child: Column(
                //     crossAxisAlignment: CrossAxisAlignment.center,
                //     //mainAxisAlignment: MainAxisAlignment.center,
                //     children: <Widget>[
                //       Container(
                //           width: MediaQuery.of(context).size.width,
                //           padding: EdgeInsets.all(0),
                //           margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                //           decoration: BoxDecoration(
                //               color: Colors.white.withOpacity(0.7),
                //               border:
                //                   Border.all(color: Colors.grey, width: 0.2),
                //               borderRadius:
                //                   BorderRadius.all(Radius.circular(5))),
                //           child: TextField(
                //             controller: tokenController,
                //             keyboardType: TextInputType.text,
                //             style: TextStyle(
                //               color: Colors.black87,
                //               fontFamily: 'Oswald',
                //             ),
                //             decoration: InputDecoration(
                //               hintText: "Token",
                //               hintStyle: TextStyle(
                //                   color: Colors.black38,
                //                   fontSize: 15,
                //                   fontFamily: 'Oswald',
                //                   fontWeight: FontWeight.w300),
                //               //labelStyle: TextStyle(color: Colors.white70),
                //               contentPadding:
                //                   EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                //               border: InputBorder.none,
                //             ),
                //           )),
                //     ],
                //   ),
                // ),
                GestureDetector(
                  onTap: () {
                    isSubmit ? null : signIn();
                  },
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
                      margin: EdgeInsets.only(
                          left: 30, right: 30, top: 5, bottom: 20),
                      decoration: BoxDecoration(
                          color: isSubmit
                              ? Colors.grey.withOpacity(0.5)
                              : mainColor,
                          border: Border.all(color: Colors.grey, width: 0.2),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      child: Text(
                        isSubmit ? "Please wait..." : "Sign In",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.w500),
                      )),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ForgetPassPage()));
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(5),
                            margin: EdgeInsets.only(
                                left: 30, right: 30, top: 0, bottom: 20),
                            child: Text(
                              "Forget Password",
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: 15,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w500),
                            )),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    "or",
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              tokenController.text = deviceToken;
                            });
                          },
                          child: Container(
                              alignment: Alignment.center,
                              padding:
                                  EdgeInsets.only(left: 0, top: 15, bottom: 15),
                              margin: EdgeInsets.only(
                                  left: 30, right: 5, top: 5, bottom: 20),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey, width: 0.2),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    "assets/images/google.png",
                                    height: 20,
                                    width: 20,
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    "Google",
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              )),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              tokenController.text = deviceToken;
                            });
                          },
                          child: Container(
                              alignment: Alignment.center,
                              padding:
                                  EdgeInsets.only(left: 0, top: 15, bottom: 15),
                              margin: EdgeInsets.only(
                                  left: 5, right: 30, top: 5, bottom: 20),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey, width: 0.2),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        image: DecorationImage(
                                            image: AssetImage(
                                          "assets/images/fb.png",
                                        ))),
                                    // child: Image.asset(
                                    //   "assets/images/fb.png",
                                    //   height: 20,
                                    //   width: 20,
                                    // ),
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    "Facebook",
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }

  Future<void> signIn() async {
    if (emailController.text.isEmpty) {
      _showMessage("Email cannot be blank!", 1);
    } else if (passController.text.isEmpty) {
      _showMessage("Password cannot be blank!", 1);
    } else {
      setState(() {
        isSubmit = true;
        selectedPage = 0;
      });

      var data = {
        'email': emailController.text,
        'password': passController.text,
        'deviceAppToken': deviceToken,
      };

      print(data);

      var res = await CallApi().postData(data, 'user/singin');
      var body = json.decode(res.body);
      print(body);

      if (res.statusCode == 200) {
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('token', body['token']);
        localStorage.setString('user', json.encode(body['user']));

        _showMessage("You are now logged in!", 2);

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      } else {
        _showMessage(body['message'], 1);
      }
    }
    setState(() {
      isSubmit = false;
    });
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
