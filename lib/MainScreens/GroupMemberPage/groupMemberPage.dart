import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Cards/AllMembersCard/allMembersCard.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import '../../main.dart';

class GroupMemberPage extends StatefulWidget {
  final id;
  final number;
  final isAdmin;
  GroupMemberPage(this.id, this.number, this.isAdmin);

  @override
  _GroupMemberPageState createState() => _GroupMemberPageState();
}

class _GroupMemberPageState extends State<GroupMemberPage> {
  SharedPreferences sharedPreferences;
  String theme = "";
  Timer _timer;
  int _start = 3;
  bool loading = true;
  var memberList, userData;
  List allAdmin = [];
  List allMember = [];

  @override
  void initState() {
    super.initState();
    _getUserInfo();
    loadMemberList();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
    }
  }

  Future loadMemberList() async {
    setState(() {
      loading = true;
    });
    var reqresponse =
        await CallApi().getData('group/members?group_id=${widget.id}');
    var reqcontent = reqresponse.body;
    final req = json.decode(reqcontent);
    //var memdata = GroupMemberModel.fromJson(req);

    setState(() {
      memberList = req["members"];
      for (int i = 0; i < memberList.length; i++) {
        if (memberList[i]['user_role'] == "Super Admin" ||
            memberList[i]['user_role'] == "Admin") {
          allAdmin.add({
            'id': memberList[i]['id'],
            'uID': memberList[i]['members']['id'],
            'fname': memberList[i]['members']['firstName'],
            'lname': memberList[i]['members']['lastName'],
            'pic': memberList[i]['members']['profilePic'],
            'role': memberList[i]['user_role'],
          });
        } else {
          allMember.add({
            'id': memberList[i]['id'],
            'uID': memberList[i]['members']['id'],
            'fname': memberList[i]['members']['firstName'],
            'lname': memberList[i]['members']['lastName'],
            'pic': memberList[i]['members']['profilePic'],
            'role': memberList[i]['user_role'],
          });
        }
      }
      loading = false;
    });
    print(allAdmin.length);
    print(allMember.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Group Members",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/white.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: null,
          ),
          Container(
              margin: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    ////// <<<<< Friend Number >>>>> //////
                    Container(
                      margin: EdgeInsets.only(top: 12, left: 20, bottom: 7),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                              child: Text(
                            "${allAdmin.length}",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 18,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w400),
                          )),
                          Container(
                              margin: EdgeInsets.only(bottom: 3),
                              child: Text(
                                allAdmin.length == 1 ? " admin" : " admins",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.black45,
                                    fontSize: 13,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w400),
                              )),
                        ],
                      ),
                    ),

                    ////// <<<<< Divider 5 >>>>> //////
                    Row(
                      children: <Widget>[
                        Container(
                          width: 30,
                          margin: EdgeInsets.only(top: 0, left: 20, bottom: 12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0)),
                              color: Colors.black54,
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 3.0,
                                  color: Colors.black54,
                                  //offset: Offset(6.0, 7.0),
                                ),
                              ],
                              border: Border.all(
                                  width: 0.5, color: Colors.black54)),
                        ),
                      ],
                    ),
                    ////// <<<<< All Members Card >>>>> //////
                    Container(
                      child: loading
                          ? LoaderScreen()
                          : Column(
                              children: List.generate(allAdmin.length, (index) {
                              return Column(
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      // Navigator.push(
                                      //     context,
                                      //     MaterialPageRoute(
                                      //         builder: (context) => FriendProfileNewPage()));
                                    },

                                    ////// <<<<< Main Data >>>>> //////
                                    child: Container(
                                      padding:
                                          EdgeInsets.only(top: 10, bottom: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(15),
                                        // boxShadow: [
                                        //   BoxShadow(
                                        //     blurRadius: 1.0,
                                        //     color: Colors.black38,
                                        //   ),
                                        // ],
                                      ),
                                      margin: EdgeInsets.only(
                                          top: 0, bottom: 0, left: 0, right: 0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                            child: Container(
                                              margin: EdgeInsets.only(
                                                  left: 20, right: 20, top: 0),
                                              padding:
                                                  EdgeInsets.only(right: 10),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  ////// <<<<< Profile picture >>>>> //////

                                                  Container(
                                                    height: 45,
                                                    width: 45,
                                                    margin: EdgeInsets.only(
                                                        right: 10),
                                                    padding:
                                                        EdgeInsets.all(1.0),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100),
                                                      child: CachedNetworkImage(
                                                        imageUrl:
                                                            allAdmin[index]
                                                                ['pic'],
                                                        placeholder: (context,
                                                                url) =>
                                                            Center(
                                                                child: Text(
                                                                    "wait...")),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Image.asset(
                                                          "assets/images/user.png",
                                                          //height: 40,
                                                        ),
                                                        fit: BoxFit.cover,

                                                        // NetworkImage(
                                                        //     widget.friend[index].profilePic
                                                      ),
                                                    ),
                                                    decoration:
                                                        new BoxDecoration(
                                                      //color: Colors.grey[300],
                                                      shape: BoxShape.circle,
                                                    ),
                                                  ),

                                                  ////// <<<<< User Name >>>>> //////
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Text(
                                                          "${allAdmin[index]['fname']} ${allAdmin[index]['lname']}",
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              color: Colors
                                                                  .black54,
                                                              fontFamily:
                                                                  'Oswald',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),

                                                        ////// <<<<< Mutual Friends >>>>> //////
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 3),
                                                          child: Text(
                                                            "${allAdmin[index]['role']}",
                                                            maxLines: 1,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    'Oswald',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 11,
                                                                color: Colors
                                                                    .black45),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),

                                          ////// <<<<< Message Button >>>>> //////
                                          Row(
                                            children: <Widget>[
                                              GestureDetector(
                                                onTap: () {
                                                  // Navigator.push(
                                                  //     context,
                                                  //     MaterialPageRoute(
                                                  //         builder: (context) => ChattingPage()));
                                                },
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 0),
                                                    padding: EdgeInsets.only(
                                                        left: 10,
                                                        right: 10,
                                                        top: 5,
                                                        bottom: 5),
                                                    decoration: BoxDecoration(
                                                        color: mainColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                        border: Border.all(
                                                            color: mainColor,
                                                            width: 0.5)),
                                                    child: Text("Message",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'Oswald',
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: Colors.white,
                                                            fontSize: 12))),
                                              ),
                                              widget.isAdmin
                                                  ? GestureDetector(
                                                      onTap: () {
                                                        showBottomSheet(
                                                            context,
                                                            index,
                                                            1,
                                                            allAdmin[index]);
                                                      },
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(15.0),
                                                        child: Icon(
                                                            Icons.more_vert,
                                                            color: Colors.grey,
                                                            size: 18),
                                                      ),
                                                    )
                                                  : Container()
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Divider()
                                ],
                              );
                            })),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 12, left: 20, bottom: 7),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                              child: Text(
                            "${allMember.length}",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 18,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w400),
                          )),
                          Container(
                              margin: EdgeInsets.only(bottom: 3),
                              child: Text(
                                allMember.length == 1 ? " member" : " members",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.black45,
                                    fontSize: 13,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w400),
                              )),
                        ],
                      ),
                    ),

                    ////// <<<<< Divider 5 >>>>> //////
                    Row(
                      children: <Widget>[
                        Container(
                          width: 30,
                          margin: EdgeInsets.only(top: 0, left: 20, bottom: 12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0)),
                              color: Colors.black54,
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 3.0,
                                  color: Colors.black54,
                                  //offset: Offset(6.0, 7.0),
                                ),
                              ],
                              border: Border.all(
                                  width: 0.5, color: Colors.black54)),
                        ),
                      ],
                    ),
                    ////// <<<<< All Members Card >>>>> //////
                    Container(
                      child: loading
                          ? LoaderScreen()
                          : Column(
                              children:
                                  List.generate(allMember.length, (index) {
                              return Column(
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      // Navigator.push(
                                      //     context,
                                      //     MaterialPageRoute(
                                      //         builder: (context) => FriendProfileNewPage()));
                                    },

                                    ////// <<<<< Main Data >>>>> //////
                                    child: Container(
                                      padding:
                                          EdgeInsets.only(top: 10, bottom: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(15),
                                        // boxShadow: [
                                        //   BoxShadow(
                                        //     blurRadius: 1.0,
                                        //     color: Colors.black38,
                                        //   ),
                                        // ],
                                      ),
                                      margin: EdgeInsets.only(
                                          top: 0, bottom: 0, left: 0, right: 0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                            child: Container(
                                              margin: EdgeInsets.only(
                                                  left: 20, right: 20, top: 0),
                                              padding:
                                                  EdgeInsets.only(right: 10),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  ////// <<<<< Profile picture >>>>> //////
                                                  Container(
                                                    height: 45,
                                                    width: 45,
                                                    margin: EdgeInsets.only(
                                                        right: 10),
                                                    padding:
                                                        EdgeInsets.all(1.0),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100),
                                                      child: CachedNetworkImage(
                                                        imageUrl:
                                                            allMember[index]
                                                                ['pic'],
                                                        placeholder: (context,
                                                                url) =>
                                                            Center(
                                                                child: Text(
                                                                    "wait...")),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Image.asset(
                                                          "assets/images/user.png",
                                                          //height: 40,
                                                        ),
                                                        fit: BoxFit.cover,

                                                        // NetworkImage(
                                                        //     widget.friend[index].profilePic
                                                      ),
                                                    ),
                                                    decoration:
                                                        new BoxDecoration(
                                                      //color: Colors.grey[300],
                                                      shape: BoxShape.circle,
                                                    ),
                                                  ),

                                                  ////// <<<<< User Name >>>>> //////
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Text(
                                                          "${allMember[index]['fname']} ${allMember[index]['lname']}",
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              color: Colors
                                                                  .black54,
                                                              fontFamily:
                                                                  'Oswald',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),

                                                        ////// <<<<< Mutual Friends >>>>> //////
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 3),
                                                          child: Text(
                                                            "${allMember[index]['role']}",
                                                            maxLines: 1,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    'Oswald',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 11,
                                                                color: Colors
                                                                    .black45),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),

                                          ////// <<<<< Message Button >>>>> //////
                                          Row(
                                            children: <Widget>[
                                              GestureDetector(
                                                onTap: () {
                                                  // Navigator.push(
                                                  //     context,
                                                  //     MaterialPageRoute(
                                                  //         builder: (context) => ChattingPage()));
                                                },
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        right: 0),
                                                    padding: EdgeInsets.only(
                                                        left: 10,
                                                        right: 10,
                                                        top: 5,
                                                        bottom: 5),
                                                    decoration: BoxDecoration(
                                                        color: mainColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                        border: Border.all(
                                                            color: mainColor,
                                                            width: 0.5)),
                                                    child: Text("Message",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'Oswald',
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: Colors.white,
                                                            fontSize: 12))),
                                              ),
                                              widget.isAdmin
                                                  ? GestureDetector(
                                                      onTap: () {
                                                        showBottomSheet(
                                                            context,
                                                            index,
                                                            2,
                                                            allMember[index]);
                                                      },
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(15.0),
                                                        child: Icon(
                                                            Icons.more_vert,
                                                            color: Colors.grey,
                                                            size: 18),
                                                      ),
                                                    )
                                                  : Container()
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Divider()
                                ],
                              );
                            })),
                    )
                  ],
                ),
              )),
        ],
      ),
    );
  }

  void showBottomSheet(context, index, num, datas) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return Wrap(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  if (num == 1) {
                    if (allAdmin[index]['role'] == "Super Admin" ||
                        allAdmin[index]['role'] == "Admin") {
                      _showAdminRemoveDialog(index, num, datas);
                    } else {
                      _showAdminAddDialog(index, num, datas);
                    }
                  } else {
                    _showAdminAddDialog(index, num, datas);
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(left: 15, right: 15, top: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(
                                    num == 1
                                        ? allAdmin[index]['role'] ==
                                                    "Super Admin" ||
                                                allAdmin[index]['role'] ==
                                                    "Admin"
                                            ? Icons.cancel
                                            : Icons.account_box
                                        : Icons.account_box,
                                    size: 17,
                                    color: Colors.black54)),
                            Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: num == 1
                                          ? allAdmin[index]['role'] ==
                                                      "Super Admin" ||
                                                  allAdmin[index]['role'] ==
                                                      "Admin"
                                              ? "Remove as Admin"
                                              : "Make Admin"
                                          : "Make Admin",
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),

                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(Icons.chevron_right,
                              size: 17, color: Colors.black54)),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  _showRemoveDialog(index, num);
                },
                child: Container(
                  margin:
                      EdgeInsets.only(left: 15, right: 15, top: 30, bottom: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(Icons.close,
                                    size: 17, color: Colors.redAccent)),
                            Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "Remove",
                                      style: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),

                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(Icons.chevron_right,
                              size: 17, color: Colors.black54)),
                    ],
                  ),
                ),
              ),
            ],
          );
        });
  }

  Future<Null> _showRemoveDialog(index, num) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 0),
                        child: Text(
                          "Do you want to remove from group?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.2),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                              removeMemberAdmin(index, num);
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future removeMemberAdmin(index, num) async {
    var data = {
      'group_id': widget.id,
      'user_id': userData['id'],
      'member_id': num == 1 ? allAdmin[index]['uID'] : allMember[index]['uID'],
    };
    var delresponse = await CallApi().postData1(data, 'group/member/remove');
    var delcontent = delresponse.body;

    if (delresponse.statusCode == 200) {
      setState(() {
        num == 1 ? allAdmin.removeAt(index) : allMember.removeAt(index);
      });
    }

    print(delcontent);
  }

  Future<Null> _showAdminRemoveDialog(index, num, datas) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 0),
                        child: Text(
                          "Do you want to remove as admin?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.2),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                              removeAdmin(index, num, datas);
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future removeAdmin(index, num, datas) async {
    var data = {
      'group_id': widget.id,
      'user_id': userData['id'],
      'member_id': num == 1 ? allAdmin[index]['uID'] : allMember[index]['uID'],
    };
    var delresponse = await CallApi().postData1(data, 'group/admin/remove');
    var delcontent = delresponse.body;

    if (delresponse.statusCode == 200) {
      print("datas");
      print(datas);
      setState(() {
        allAdmin.removeAt(index);
        allMember.add({
          'id': datas['id'],
          'uID': datas['uID'],
          'fname': datas['fname'],
          'lname': datas['lname'],
          'pic': datas['pic'],
          'role': "Member",
        });
      });
    }

    print(delcontent);
  }

  Future<Null> _showAdminAddDialog(index, num, datas) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 0),
                        child: Text(
                          "Do you want to make admin?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.2),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                              makeAdmin(index, num, datas);
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future makeAdmin(index, num, datas) async {
    var data = {
      'group_id': widget.id,
      'user_id': userData['id'],
      'member_id': num == 1 ? allAdmin[index]['uID'] : allMember[index]['uID'],
    };
    var delresponse = await CallApi().postData1(data, 'group/user/make/admin');
    var delcontent = delresponse.body;

    if (delresponse.statusCode == 200) {
      setState(() {
        allMember.removeAt(index);
        allAdmin.add({
          'id': datas['id'],
          'uID': datas['uID'],
          'fname': datas['fname'],
          'lname': datas['lname'],
          'pic': datas['pic'],
          'role': "Admin",
        });
      });
    }

    print(delcontent);
  }
}
