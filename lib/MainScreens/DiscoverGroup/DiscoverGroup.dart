import 'package:flutter/material.dart';
import 'package:social_app_fb/MainScreens/GroupDetailsPage/groupDetailsPage.dart';

import '../../main.dart';

class DiscoverGroup extends StatefulWidget {
  @override
  _DiscoverGroupState createState() => _DiscoverGroupState();
}

class _DiscoverGroupState extends State<DiscoverGroup> {
  TextEditingController searchController = new TextEditingController();

  List groupList = [
    {
      'pic': 'assets/images/f7.jpg',
      'name': 'Travel Bangladesh',
      'members': '30',
    },
    {
      'pic': 'assets/images/bike1.jpg',
      'name': 'Bike Riding Community',
      'members': '50',
    },
    {
      'pic': 'assets/images/friend.jpg',
      'name': 'Friends',
      'members': '50',
    },
    {
      'pic': 'assets/images/car1.jpg',
      'name': 'Car Driving',
      'members': '40',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5, top: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          margin: EdgeInsets.only(left: 20, right: 20, top: 5),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.7),
                              border:
                                  Border.all(color: Colors.grey, width: 0.2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.search,
                                color: Colors.black26,
                                size: 16,
                              ),
                              Flexible(
                                child: Container(
                                  child: TextField(
                                    controller: searchController,
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: 'Oswald',
                                    ),
                                    decoration: InputDecoration(
                                      hintText: "Search group...",
                                      hintStyle: TextStyle(
                                          color: Colors.black38,
                                          fontSize: 15,
                                          fontFamily: 'Oswald',
                                          fontWeight: FontWeight.w300),
                                      //labelStyle: TextStyle(color: Colors.white70),
                                      contentPadding:
                                          EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(5),
                                child: Icon(
                                  Icons.arrow_forward,
                                  color: mainColor.withOpacity(0.6),
                                  size: 20,
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Wrap(
                    children: List.generate(groupList.length, (index) {
                      return Column(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (context) =>
                              //             GroupDetailsPage()));
                            },
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 20, right: 20, top: 0),
                              decoration: BoxDecoration(
                                  //border: Border.all(width: 0.4, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white),
                              child: Stack(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(
                                            top: 10, left: 10, bottom: 10),
                                        padding: EdgeInsets.all(1.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.grey
                                                      .withOpacity(0.8),
                                                  width: 2),
                                              borderRadius:
                                                  BorderRadius.circular(100)),
                                          child: CircleAvatar(
                                            radius: 20.0,
                                            backgroundColor: Colors.transparent,
                                            backgroundImage: AssetImage(
                                                groupList[index]['pic']),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Expanded(
                                                child: Container(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(
                                                          groupList[index]
                                                              ['name'],
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              fontSize: 15),
                                                        ),
                                                      ),
                                                      Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        margin: EdgeInsets.only(
                                                            left: 10, top: 4),
                                                        child: Row(
                                                          children: <Widget>[
                                                            Text(
                                                              "${groupList[index]['members']}+ Members",
                                                              style: TextStyle(
                                                                  color:
                                                                      mainColor,
                                                                  fontFamily:
                                                                      "Oswald",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize: 11),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                  margin: EdgeInsets.all(10),
                                                  child: Icon(
                                                    Icons.more_vert,
                                                    color: Colors.black38,
                                                    size: 20,
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 20, right: 20),
                              child: Divider())
                        ],
                      );
                    }),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
