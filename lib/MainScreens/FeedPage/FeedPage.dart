import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Loader/FeedLoader/FeedLoader.dart';
import 'package:social_app_fb/MainScreens/CommentPage/CommentPage.dart';
import 'package:social_app_fb/MainScreens/CreatePostPage/CreatePostPage.dart';
import 'package:social_app_fb/MainScreens/EditPostPage/EditPostPage.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/MainScreens/LoginRegisterPage/LoginRegisterPage.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';
import 'package:social_app_fb/MainScreens/StatusDetailsPage/StatusDetailsPage.dart';
import 'package:social_app_fb/MainScreens/StoryAddPage/StoryAddPage.dart';
import 'package:social_app_fb/MainScreens/StoryPage/StoryPage.dart';
import 'package:social_app_fb/ModelClass/FeedModel/FeedModel.dart';
import 'package:social_app_fb/main.dart';
// import 'package:step_progress_indicator/step_progress_indicator.dart';

List postComCount = [];

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  var userData, postData, postList, strList;
  bool _isLoading = true;
  bool loading = false;
  Timer _timer;
  int _start = 3, lastID = 1;
  int no, likeNum = 0;
  int l = 0;
  int like = 0, likeStore = 0, cgn = 0;
  String date = "", sharePost = "";
  ScrollController _controller = new ScrollController();
  TextEditingController shareController = new TextEditingController();

  List storyList = [];

  @override
  void initState() {
    setState(() {
      postComCount.clear();
      feedList.clear();
    });
    _getUserInfo();
    //timerCheck();

    //controller for to go bottom top of the list
    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels == 0) {
          setState(() {
            print("top");
          });
        }
        // you are at top position

        else {
          setState(() {
            print("bottom");
            loadPosts(lastID); //api will be call at the bottom at the list

            print("lastID 1");
            print(lastID);
          });
        }
        // you are at bottom position
      }
    });
    super.initState();
  }

  Future loadStory() async {
    var storyresponse = await CallApi().getData2('story/get/feed/recent');

    setState(() {
      var storycontent = storyresponse.body;
      final stories = json.decode(storycontent);
      setState(() {
        strList = stories;

        for (int i = 0; i < strList.length; i++) {
          storyList.add({
            'id': '${strList[i]['id']}',
            'text': '${strList[i]['text']}',
            'storyPic': '${strList[i]['image']}',
            'profilePic': '${strList[i]['profilePic']}',
            'name': '${strList[i]['firstName']} ${strList[i]['lastName']}',
            'privacy': '${strList[i]['privacy']}',
            'created': '${strList[i]['created_at']}',
          });
        }
      });
    });
  }

  Future loadPosts(int number) async {
    var postresponse =
        await CallApi().getData('feed/get/allFeed?status_id=$number');

    setState(() {
      var postcontent = postresponse.body;
      final posts = json.decode(postcontent);
      var postdata = FeedModel.fromJson(posts);
      postList = postdata;

      for (int i = 0; i < postList.feed.length; i++) {
        //print(postList.feed[i].id);

        feedList.add(postList.feed[i]);
        //print(feedList[i].meta.totalCommentsCount);
        postComCount.add({'count': postList.feed[i].meta.totalCommentsCount});
        lastID = postList.feed[i].id;
        //print(lastID);

        if (postList.feed.length >= 4) {
          postList.feed[2].isAdd = 1;
        }

        print(postList.feed[i].isAdd);
      }

      // print(feedList);
      // print(postComCount);
    });

    setState(() {
      _isLoading = false;
    });
    //print(page1);
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
    loadStory();
    loadPosts(1);
  }

  Future<void> logout() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove("token");
    localStorage.remove("user");
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginRegisterPage()),
    );
  }

  void _statusModalBottomSheet(context, index, feedList) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                // Text('React to this post',
                //       style: TextStyle(fontWeight: FontWeight.normal)),
                feedList.activityText != ""
                    ? new ListTile(
                        leading: new Icon(Icons.edit),
                        title: new Text('Edit',
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontFamily: "Oswald")),
                        onTap: () => {
                          Navigator.pop(context),
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      EditPostPage(feedList, userData)))
                        },
                      )
                    : Container(),
                new ListTile(
                  leading: new Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  title: new Text('Delete',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.redAccent,
                          fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    _showDeleteDialog(index, feedList)
                  },
                ),
              ],
            ),
          );
        });
  }

  Future<Null> _showDeleteDialog(index, feedList) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          "Want to delete the post?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                              statusDelete(index, feedList);
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future statusDelete(index, feedLists) async {
    var data = {
      'user_id': userData['id'],
      'status_id': feedLists.id,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/delete');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      //Navigator.pop(context);
      setState(() {
        feedList.removeAt(index);
      });
      return _showMessage("Deleted Successfully!", 2);
    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }

  Future likeUnlike(index, status, id) async {
    var data = {
      'user_id': userData['id'],
      'status_id': id,
      'type': status,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/add/like');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      //Navigator.pop(context);

    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: userData == null || postComCount == null
          ? LoaderScreen()
          : SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    CreatePostPage(userData, 1)));
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 0, left: 5, right: 5),
                        padding: EdgeInsets.only(top: 0, bottom: 0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          // boxShadow: [
                          //   BoxShadow(
                          //     blurRadius: 0.2,
                          //     color: Colors.black38,
                          //     //offset: Offset(6.0, 7.0),
                          //   ),
                          // ],
                        ),
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              // StepProgressIndicator(
                              //   totalSteps: 10,
                              //   currentStep: cgn,
                              //   selectedColor: Colors.orange,
                              //   unselectedColor: Colors.purple,
                              //   onTap: (value) {
                              //     return () {
                              //       setState(() {
                              //         cgn = value;
                              //       });
                              //     };
                              //   },
                              // ),
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    ////// <<<<< Profile >>>>> //////
                                    GestureDetector(
                                      onTap: () {
                                        // Navigator.push(
                                        //     context,
                                        //     MaterialPageRoute(
                                        //         builder: (context) =>
                                        //             //FriendsProfilePage("prabal23")),
                                        //             MyProfilePage(userData)));
                                      },
                                      child: Container(
                                        child: Stack(
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 0, left: 15),
                                              padding: EdgeInsets.all(1.0),
                                              child: CircleAvatar(
                                                radius: 21.0,
                                                backgroundColor:
                                                    Colors.transparent,
                                                backgroundImage: AssetImage(
                                                    'assets/images/user.png'),
                                              ),
                                              // decoration: new BoxDecoration(
                                              //   color: Colors.grey[300],
                                              //   shape: BoxShape.circle,
                                              // ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 45),
                                              padding: EdgeInsets.all(1.0),
                                              child: CircleAvatar(
                                                radius: 5.0,
                                                backgroundColor:
                                                    Colors.greenAccent,
                                              ),
                                              decoration: new BoxDecoration(
                                                color: Colors.greenAccent,
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),

                                    ////// <<<<< Status/Photo field >>>>> //////
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          // Navigator.push(
                                          //     context,
                                          //     MaterialPageRoute(
                                          //         builder: (context) =>
                                          //             CreatePost(userData, 1)));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              bottom: 10, top: 4),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            //mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  padding: EdgeInsets.only(
                                                      left: 5, right: 5),
                                                  margin: EdgeInsets.only(
                                                      left: 0,
                                                      right: 10,
                                                      top: 5),
                                                  // decoration: BoxDecoration(
                                                  //     color: Colors.grey[100],
                                                  //     border: Border.all(
                                                  //         color: Colors.grey[200],
                                                  //         width: 0.5),
                                                  //     borderRadius: BorderRadius.all(
                                                  //         Radius.circular(25))),
                                                  child: TextField(
                                                    enabled: false,
                                                    //controller: phoneController,
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: 'Oswald',
                                                    ),
                                                    decoration: InputDecoration(
                                                      hintText:
                                                          "What's in your mind? ${userData['firstName']}",
                                                      hintStyle: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 15,
                                                          fontFamily: 'Oswald',
                                                          fontWeight:
                                                              FontWeight.w300),
                                                      //labelStyle: TextStyle(color: Colors.white70),
                                                      contentPadding:
                                                          EdgeInsets.fromLTRB(
                                                              10.0, 1, 20.0, 1),
                                                      border: InputBorder.none,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),

                                    Container(
                                      margin: EdgeInsets.only(right: 15),
                                      child: Stack(
                                        children: <Widget>[
                                          Container(
                                              padding: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                  color: mainColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(Icons.photo,
                                                  color: mainColor)),
                                          Container(
                                              padding: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                  color: Colors.white
                                                      .withOpacity(0.8),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(Icons.photo,
                                                  color: mainColor)),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                  width: 50,
                                  margin: EdgeInsets.only(
                                      top: 10, left: 25, right: 25, bottom: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      color: mainColor,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 1.0,
                                          color: mainColor,
                                          //offset: Offset(6.0, 7.0),
                                        ),
                                      ],
                                      border: Border.all(
                                          width: 0.5, color: mainColor))),
                              SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                scrollDirection: storyList.length == 0
                                    ? Axis.vertical
                                    : storyList.length > 2
                                        ? Axis.horizontal
                                        : Axis.vertical,
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 15),
                                  decoration: BoxDecoration(
                                      //border:Border.all(color:Colors.grey, width:0.3)
                                      ),
                                  child: Row(
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      StoryAddPage()));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 0,
                                              right: 0,
                                              top: 5,
                                              bottom: 0),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              // color: Colors.grey
                                              //     .withOpacity(0.1),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                height: 130,
                                                width: 100,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                    image: DecorationImage(
                                                        image: AssetImage(
                                                            'assets/images/user.png'),
                                                        fit: BoxFit.cover)),
                                                child: Stack(
                                                  children: <Widget>[
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(15),
                                                          color:
                                                              Colors.black45),
                                                    ),
                                                    Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        Container(
                                                          child: Row(
                                                            children: <Widget>[
                                                              Container(
                                                                //width: 25,
                                                                decoration: BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            100),
                                                                    color: Colors
                                                                        .black54),
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        top: 10,
                                                                        left:
                                                                            10),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            5.0),
                                                                child: Icon(
                                                                    Icons.add,
                                                                    color: Colors
                                                                        .white),
                                                                // ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Container(
                                                          alignment: Alignment
                                                              .bottomCenter,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              Expanded(
                                                                child:
                                                                    Container(
                                                                  margin:
                                                                      EdgeInsets
                                                                          .all(
                                                                              10),
                                                                  child: Text(
                                                                    "Add to your story",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontFamily:
                                                                            "Oswald",
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                        fontSize:
                                                                            12),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Wrap(
                                          children: List.generate(
                                              storyList.length, (index) {
                                            return GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            StoryPage(storyList[
                                                                index])));
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    left: 0,
                                                    right: 0,
                                                    top: 5,
                                                    bottom: 0),
                                                padding: EdgeInsets.all(5.0),
                                                decoration: new BoxDecoration(
                                                    // color: Colors.grey
                                                    //     .withOpacity(0.1),
                                                    border: Border.all(
                                                        width: 0.5,
                                                        color: Colors.white),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                5))),
                                                child: Column(
                                                  children: <Widget>[
                                                    Container(
                                                      height: 130,
                                                      width: 100,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                      ),
                                                      child: Stack(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 130,
                                                            width: 100,
                                                            child: storyList[
                                                                            index]
                                                                        [
                                                                        'text'] !=
                                                                    ""
                                                                ? Container(
                                                                    decoration: BoxDecoration(
                                                                        color:
                                                                            mainColor,
                                                                        borderRadius:
                                                                            BorderRadius.circular(15)),
                                                                    child:
                                                                        Center(
                                                                      child:
                                                                          Container(
                                                                        child:
                                                                            Text(
                                                                          storyList[index]
                                                                              [
                                                                              'text'],
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 8),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  )
                                                                : ClipRRect(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            15),
                                                                    child:
                                                                        CachedNetworkImage(
                                                                      imageUrl:
                                                                          storyList[index]
                                                                              [
                                                                              'storyPic'],
                                                                      placeholder: (context,
                                                                              url) =>
                                                                          Center(
                                                                              child: Text("wait...")),
                                                                      errorWidget: (context,
                                                                              url,
                                                                              error) =>
                                                                          Image
                                                                              .asset(
                                                                        "assets/images/placeholder_cover.jpg",
                                                                        fit: BoxFit
                                                                            .cover,
                                                                        //height: 40,
                                                                      ),
                                                                      fit: BoxFit
                                                                          .cover,

                                                                      // NetworkImage(
                                                                      //     widget.friend[index].profilePic
                                                                    ),
                                                                  ),
                                                          ),
                                                          Container(
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            15),
                                                                color: storyList[index]
                                                                            [
                                                                            'text'] !=
                                                                        ""
                                                                    ? Colors
                                                                        .black12
                                                                    : Colors
                                                                        .black45),
                                                          ),
                                                          Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: <Widget>[
                                                              Container(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        top: 10,
                                                                        left:
                                                                            10),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            1.0),
                                                                child:
                                                                    Container(
                                                                  height: 35,
                                                                  width: 35,
                                                                  decoration: BoxDecoration(
                                                                      border: Border.all(
                                                                          color: mainColor.withOpacity(
                                                                              0.8),
                                                                          width:
                                                                              3),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              100)),
                                                                  child:
                                                                      Container(
                                                                    decoration: BoxDecoration(
                                                                        color: Colors
                                                                            .white,
                                                                        borderRadius:
                                                                            BorderRadius.circular(100)),
                                                                    child:
                                                                        ClipRRect(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              100),
                                                                      child:
                                                                          CachedNetworkImage(
                                                                        imageUrl:
                                                                            storyList[index]['profilePic'],
                                                                        placeholder:
                                                                            (context, url) =>
                                                                                Center(child: SpinKitCircle(color: Colors.grey.withOpacity(0.5))),
                                                                        errorWidget: (context,
                                                                                url,
                                                                                error) =>
                                                                            Image.asset(
                                                                          "assets/images/user.png",
                                                                          //height: 40,
                                                                        ),
                                                                        fit: BoxFit
                                                                            .cover,

                                                                        // NetworkImage(
                                                                        //     widget.friend[index].profilePic
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                // decoration: new BoxDecoration(
                                                                //   color: Colors.grey[300],
                                                                //   shape: BoxShape.circle,
                                                                // ),
                                                              ),
                                                              Container(
                                                                alignment: Alignment
                                                                    .bottomCenter,
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: <
                                                                      Widget>[
                                                                    Expanded(
                                                                      child:
                                                                          Container(
                                                                        margin:
                                                                            EdgeInsets.all(10),
                                                                        child:
                                                                            Text(
                                                                          storyList[index]
                                                                              [
                                                                              'name'],
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontFamily: "Oswald",
                                                                              fontWeight: FontWeight.w400,
                                                                              fontSize: 12),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                        width: 50,
                        margin: EdgeInsets.only(
                            top: 0, left: 25, right: 25, bottom: 0),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            color: mainColor,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 1.0,
                                color: mainColor,
                                //offset: Offset(6.0, 7.0),
                              ),
                            ],
                            border: Border.all(width: 0.5, color: mainColor))),
                    Container(
                      height: MediaQuery.of(context).size.height - 260,
                      margin: EdgeInsets.only(top: 20, left: 10, right: 10),
                      child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          controller: _controller, //controller for detecting top bottom
                          itemCount: feedList.length,
                          itemBuilder: (context, index) {
                            DateTime dateTime =
                                DateTime.parse(feedList[index].createdAt);
                            date = DateFormat.yMMMd().format(dateTime);
                            List img = [];
                            List img1 = [];

                            if (feedList[index].data.images != null) {
                              for (int i = 0;
                                  i < feedList[index].data.images.length;
                                  i++) {
                                img.add({
                                  'id': feedList[index].data.images[i].id,
                                  'url': feedList[index].data.images[i].url,
                                });
                              }
                            }

                            if (feedList[index].shareData != null) {
                              if (feedList[index].shareData.data.images !=
                                  null) {
                                for (int i = 0;
                                    i <
                                        feedList[index]
                                            .shareData
                                            .data
                                            .images
                                            .length;
                                    i++) {
                                  img1.add({
                                    'id': feedList[index]
                                        .shareData
                                        .data
                                        .images[i]
                                        .id,
                                    'url': feedList[index]
                                        .shareData
                                        .data
                                        .images[i]
                                        .url,
                                  });
                                }
                              }
                            }

                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => StatusDetailsPage(
                                            index,
                                            userData,
                                            feedList[index].id)));
                              },
                              child: Container(
                                child: _isLoading == false
                                    ? Container(
                                        padding:
                                            EdgeInsets.only(top: 0, bottom: 0),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          //border: Border.all(width: 0.8, color: Colors.grey[300]),
                                          // boxShadow: [
                                          //   BoxShadow(
                                          //     blurRadius: 0.0,
                                          //     color: Colors.black26,
                                          //     //offset: Offset(6.0, 7.0),
                                          //   ),
                                          // ],
                                        ),
                                        margin: EdgeInsets.only(
                                            top: 5,
                                            bottom: 5,
                                            left: 0,
                                            right: 0),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Container(
                                                      //color: Colors.red,
                                                      margin: EdgeInsets.only(
                                                          left: 20,
                                                          right: 20,
                                                          top: 15),
                                                      padding: EdgeInsets.only(
                                                          right: 10),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          ////// <<<<< Picture start >>>>> //////
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 10),
                                                            //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                            padding:
                                                                EdgeInsets.all(
                                                                    1.0),
                                                            child: CircleAvatar(
                                                              radius: 20.0,
                                                              backgroundColor:
                                                                  Colors.white,
                                                              backgroundImage:
                                                                  AssetImage(
                                                                      'assets/images/user.png'),
                                                            ),
                                                            decoration:
                                                                new BoxDecoration(
                                                              color: Colors
                                                                      .grey[
                                                                  300], // border color
                                                              shape: BoxShape
                                                                  .circle,
                                                            ),
                                                          ),
                                                          ////// <<<<< Picture end >>>>> //////

                                                          Expanded(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: <
                                                                  Widget>[
                                                                ////// <<<<< Name start >>>>> //////

                                                                Container(
                                                                  child: Row(
                                                                    children: <
                                                                        Widget>[
                                                                      Text(
                                                                        feedList[index].activityType == "Page-Share" || feedList[index].activityType == "Share"
                                                                            ? "${feedList[index].feedUser.firstName} ${feedList[index].feedUser.lastName}"
                                                                            : feedList[index].activityType == "Page-Status"
                                                                                ? "${feedList[index].pageData.name}"
                                                                                : "${feedList[index].data.statusUserName}",
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                15,
                                                                            color: Colors
                                                                                .black54,
                                                                            fontFamily:
                                                                                'Oswald',
                                                                            fontWeight:
                                                                                FontWeight.w500),
                                                                      ),
                                                                      feedList[index].activityType == "User-Cover-Pic" ||
                                                                              feedList[index].activityType == "User-Profile-Pic"
                                                                          ? Expanded(
                                                                              child: Text(
                                                                                feedList[index].activityType == "User-Cover-Pic" ? " changed his cover photo" : " changed his profile picture",
                                                                                style: TextStyle(fontSize: 15, color: Colors.black54, fontFamily: 'Oswald', fontWeight: FontWeight.w300),
                                                                              ),
                                                                            )
                                                                          : Container(),
                                                                    ],
                                                                  ),
                                                                ),
                                                                ////// <<<<< Name start >>>>> //////

                                                                ////// <<<<< Time start >>>>> //////
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              3),
                                                                  child: Row(
                                                                    children: <
                                                                        Widget>[
                                                                      Text(
                                                                        date,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                        style: TextStyle(
                                                                            fontFamily:
                                                                                'Oswald',
                                                                            fontWeight: FontWeight
                                                                                .w400,
                                                                            fontSize:
                                                                                11,
                                                                            color:
                                                                                Colors.black45),
                                                                      ),
                                                                      Container(
                                                                          margin: EdgeInsets.only(
                                                                              left:
                                                                                  5),
                                                                          child:
                                                                              Icon(
                                                                            Icons.public,
                                                                            color:
                                                                                Colors.black45,
                                                                            size:
                                                                                12,
                                                                          ))
                                                                    ],
                                                                  ),
                                                                ),
                                                                ////// <<<<< Time end >>>>> //////
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  ////// <<<<< More start >>>>> //////
                                                  feedList[index].userId !=
                                                          userData['id']
                                                      ? Container()
                                                      : GestureDetector(
                                                          onTap: () {
                                                            setState(() {
                                                              _statusModalBottomSheet(
                                                                  context,
                                                                  index,
                                                                  feedList[
                                                                      index]);
                                                            });
                                                          },
                                                          child: Container(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(5),
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          15),
                                                              child: Icon(
                                                                Icons
                                                                    .more_horiz,
                                                                color: Colors
                                                                    .black45,
                                                              )),
                                                        ),
                                                  ////// <<<<< More end >>>>> //////
                                                ],
                                              ),
                                            ),

                                            ////// <<<<< Post start >>>>> //////

                                            Container(
                                                margin: EdgeInsets.only(
                                                    left: 20,
                                                    right: 20,
                                                    top: 20),
                                                child: Container(
                                                  child: Column(
                                                    children: <Widget>[
                                                      feedList[index].activityText ==
                                                                  "" ||
                                                              feedList[index]
                                                                      .activityText ==
                                                                  null
                                                          ? Container()
                                                          : Container(
                                                              width:
                                                                  MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                              child: Text(
                                                                "${feedList[index].activityText}",
                                                                textAlign:
                                                                    TextAlign
                                                                        .justify,
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black87,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              ),
                                                            ),
                                                      feedList[index].activityType !=
                                                                  "Share" &&
                                                              feedList[index]
                                                                      .activityType !=
                                                                  "Page-Share"
                                                          ? Container()
                                                          : Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      top: 10),
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  border: Border.all(
                                                                      color: Colors
                                                                          .grey,
                                                                      width:
                                                                          0.3)),
                                                              child: Column(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      children: <
                                                                          Widget>[
                                                                        Expanded(
                                                                          child:
                                                                              Container(
                                                                            //color: Colors.red,
                                                                            margin: EdgeInsets.only(
                                                                                left: 15,
                                                                                right: 15,
                                                                                top: 15),
                                                                            padding:
                                                                                EdgeInsets.only(right: 10),
                                                                            child:
                                                                                Row(
                                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: <Widget>[
                                                                                ////// <<<<< Picture start >>>>> //////
                                                                                Container(
                                                                                  margin: EdgeInsets.only(right: 10),
                                                                                  //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                                                  padding: EdgeInsets.all(1.0),
                                                                                  child: CircleAvatar(
                                                                                    radius: 20.0,
                                                                                    backgroundColor: Colors.white,
                                                                                    backgroundImage: AssetImage('assets/images/user.png'),
                                                                                  ),
                                                                                  decoration: new BoxDecoration(
                                                                                    color: Colors.grey[300], // border color
                                                                                    shape: BoxShape.circle,
                                                                                  ),
                                                                                ),
                                                                                ////// <<<<< Picture end >>>>> //////

                                                                                Expanded(
                                                                                  child: Column(
                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                                    children: <Widget>[
                                                                                      ////// <<<<< Name start >>>>> //////

                                                                                      Text(
                                                                                        feedList[index].activityType == "Page-Share" ? "${feedList[index].shareData.pageData.name}" : "${feedList[index].shareData.data.statusUserName}",
                                                                                        maxLines: 1,
                                                                                        overflow: TextOverflow.ellipsis,
                                                                                        style: TextStyle(fontSize: 15, color: Colors.black54, fontFamily: 'Oswald', fontWeight: FontWeight.w500),
                                                                                      ),
                                                                                      ////// <<<<< Name start >>>>> //////

                                                                                      ////// <<<<< Time start >>>>> //////
                                                                                      Container(
                                                                                        margin: EdgeInsets.only(top: 3),
                                                                                        child: Row(
                                                                                          children: <Widget>[
                                                                                            Text(
                                                                                              date,
                                                                                              maxLines: 1,
                                                                                              overflow: TextOverflow.ellipsis,
                                                                                              style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w400, fontSize: 11, color: Colors.black45),
                                                                                            ),
                                                                                            Container(
                                                                                                margin: EdgeInsets.only(left: 5),
                                                                                                child: Icon(
                                                                                                  Icons.public,
                                                                                                  color: Colors.black45,
                                                                                                  size: 12,
                                                                                                ))
                                                                                          ],
                                                                                        ),
                                                                                      ),
                                                                                      ////// <<<<< Time end >>>>> //////
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  feedList[index]
                                                                              .shareData
                                                                              .activityText ==
                                                                          ""
                                                                      ? Container()
                                                                      : Container(
                                                                          width: MediaQuery.of(context)
                                                                              .size
                                                                              .width,
                                                                          margin:
                                                                              EdgeInsets.all(15),
                                                                          child:
                                                                              Text(
                                                                            "${feedList[index].shareData.activityText}",
                                                                            textAlign:
                                                                                TextAlign.justify,
                                                                            style:
                                                                                TextStyle(color: Colors.black87, fontWeight: FontWeight.w400),
                                                                          ),
                                                                        ),
                                                                  img1.length ==
                                                                          0
                                                                      ? Container()
                                                                      : Container(
                                                                          margin:
                                                                              EdgeInsets.only(left: 20),
                                                                          child:
                                                                              Column(
                                                                            children: <Widget>[
                                                                              img1.length == 1
                                                                                  ? Row(
                                                                                      children: <Widget>[
                                                                                        Expanded(
                                                                                          child: Container(
                                                                                              //color: Colors.red,
                                                                                              height: 200,
                                                                                              //width: 150,
                                                                                              padding: const EdgeInsets.all(0.0),
                                                                                              margin: EdgeInsets.only(top: 10, right: 5, left: 0),
                                                                                              //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                              child: ClipRRect(
                                                                                                borderRadius: BorderRadius.circular(5),
                                                                                                child: CachedNetworkImage(
                                                                                                  imageUrl: img1[0]['url'],
                                                                                                  placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                  errorWidget: (context, url, error) => Image.asset(
                                                                                                    "assets/images/placeholder_cover.jpg",
                                                                                                    fit: BoxFit.cover,
                                                                                                    //height: 40,
                                                                                                  ),
                                                                                                  fit: BoxFit.cover,
                                                                                                ),
                                                                                              )),
                                                                                        ),
                                                                                      ],
                                                                                    )
                                                                                  : Column(
                                                                                      children: <Widget>[
                                                                                        Container(
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Expanded(
                                                                                                child: Container(
                                                                                                    //color: Colors.red,
                                                                                                    height: 150,
                                                                                                    //width: 150,
                                                                                                    padding: const EdgeInsets.all(0.0),
                                                                                                    margin: EdgeInsets.only(top: 10, right: 5),
                                                                                                    //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                                    child: ClipRRect(
                                                                                                      borderRadius: BorderRadius.circular(5),
                                                                                                      child: CachedNetworkImage(
                                                                                                        imageUrl: img1[0]['url'],
                                                                                                        placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                        errorWidget: (context, url, error) => Image.asset(
                                                                                                          "assets/images/placeholder_cover.jpg",
                                                                                                          fit: BoxFit.cover,
                                                                                                          //height: 40,
                                                                                                        ),
                                                                                                        fit: BoxFit.cover,
                                                                                                      ),
                                                                                                    )),
                                                                                              ),
                                                                                              Expanded(
                                                                                                child: Container(
                                                                                                    //color: Colors.red,
                                                                                                    height: 150,
                                                                                                    // width: 150,
                                                                                                    padding: const EdgeInsets.all(0.0),
                                                                                                    margin: EdgeInsets.only(top: 10, left: 5),
                                                                                                    //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car3.jpeg"), fit: BoxFit.cover)),
                                                                                                    child: ClipRRect(
                                                                                                      borderRadius: BorderRadius.circular(5),
                                                                                                      child: CachedNetworkImage(
                                                                                                        imageUrl: img1[1]['url'],
                                                                                                        placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                        errorWidget: (context, url, error) => Image.asset(
                                                                                                          "assets/images/placeholder_cover.jpg",
                                                                                                          fit: BoxFit.cover,
                                                                                                          //height: 40,
                                                                                                        ),
                                                                                                        fit: BoxFit.cover,
                                                                                                      ),
                                                                                                    )),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        img1.length == 2
                                                                                            ? Container()
                                                                                            : Container(
                                                                                                child: Row(
                                                                                                  children: <Widget>[
                                                                                                    Expanded(
                                                                                                      child: Container(
                                                                                                          //color: Colors.red,
                                                                                                          height: img1.length == 3 ? 200 : 150,
                                                                                                          //width: 150,
                                                                                                          padding: const EdgeInsets.all(0.0),
                                                                                                          margin: EdgeInsets.only(top: 10, right: img.length == 3 ? 0 : 5),
                                                                                                          //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car6.jpg"), fit: BoxFit.cover)),
                                                                                                          child: ClipRRect(
                                                                                                            borderRadius: BorderRadius.circular(5),
                                                                                                            child: CachedNetworkImage(
                                                                                                              imageUrl: img1[2]['url'],
                                                                                                              placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                              errorWidget: (context, url, error) => Image.asset(
                                                                                                                "assets/images/placeholder_cover.jpg",
                                                                                                                fit: BoxFit.cover,
                                                                                                                //height: 40,
                                                                                                              ),
                                                                                                              fit: BoxFit.cover,
                                                                                                            ),
                                                                                                          )),
                                                                                                    ),
                                                                                                    img1.length < 4
                                                                                                        ? Container()
                                                                                                        : Expanded(
                                                                                                            child: Container(
                                                                                                                //color: Colors.red,
                                                                                                                height: 150,
                                                                                                                // width: 150,
                                                                                                                padding: const EdgeInsets.all(0.0),
                                                                                                                margin: EdgeInsets.only(top: 10, left: 5),
                                                                                                                decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/placeholder_cover.jpg"), fit: BoxFit.cover)),
                                                                                                                child: Container(
                                                                                                                  child: Stack(
                                                                                                                    children: <Widget>[
                                                                                                                      Container(
                                                                                                                        height: 150,
                                                                                                                        child: ClipRRect(
                                                                                                                          borderRadius: BorderRadius.circular(5),
                                                                                                                          child: CachedNetworkImage(
                                                                                                                            imageUrl: img1[3]['url'],
                                                                                                                            placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                                            errorWidget: (context, url, error) => Image.asset(
                                                                                                                              "assets/images/placeholder_cover.jpg",
                                                                                                                              fit: BoxFit.cover,
                                                                                                                              //height: 40,
                                                                                                                            ),
                                                                                                                            fit: BoxFit.cover,
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                      Container(
                                                                                                                        height: 150,
                                                                                                                        // width: 150,
                                                                                                                        padding: const EdgeInsets.all(0.0),
                                                                                                                        margin: EdgeInsets.only(top: 0, left: 0),
                                                                                                                        decoration: BoxDecoration(
                                                                                                                          color: img1.length > 4 ? Colors.black.withOpacity(0.6) : Colors.transparent,
                                                                                                                          borderRadius: BorderRadius.circular(5),
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                      img1.length > 4
                                                                                                                          ? Center(
                                                                                                                              child: Container(
                                                                                                                                  child: Text(
                                                                                                                              "+${img1.length - 3}",
                                                                                                                              style: TextStyle(color: Colors.white, fontSize: 22),
                                                                                                                            )))
                                                                                                                          : Container(),
                                                                                                                    ],
                                                                                                                  ),
                                                                                                                )),
                                                                                                          ),
                                                                                                  ],
                                                                                                ),
                                                                                              ),
                                                                                      ],
                                                                                    ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                ],
                                                              ),
                                                            ),
                                                      img.length == 0
                                                          ? Container()
                                                          : Container(
                                                              child: Column(
                                                                children: <
                                                                    Widget>[
                                                                  img.length ==
                                                                          1
                                                                      ? Row(
                                                                          children: <
                                                                              Widget>[
                                                                            Expanded(
                                                                              child: Container(
                                                                                  //color: Colors.red,
                                                                                  height: 200,
                                                                                  //width: 150,
                                                                                  padding: const EdgeInsets.all(0.0),
                                                                                  margin: EdgeInsets.only(top: 10, right: 5),
                                                                                  //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                  child: ClipRRect(
                                                                                    borderRadius: BorderRadius.circular(5),
                                                                                    child: CachedNetworkImage(
                                                                                      imageUrl: img[0]['url'],
                                                                                      placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                      errorWidget: (context, url, error) => Image.asset(
                                                                                        "assets/images/placeholder_cover.jpg",
                                                                                        fit: BoxFit.cover,
                                                                                        //height: 40,
                                                                                      ),
                                                                                      fit: BoxFit.cover,
                                                                                    ),
                                                                                  )),
                                                                            ),
                                                                          ],
                                                                        )
                                                                      : Column(
                                                                          children: <
                                                                              Widget>[
                                                                            Container(
                                                                              child: Row(
                                                                                children: <Widget>[
                                                                                  Expanded(
                                                                                    child: Container(
                                                                                        //color: Colors.red,
                                                                                        height: 150,
                                                                                        //width: 150,
                                                                                        padding: const EdgeInsets.all(0.0),
                                                                                        margin: EdgeInsets.only(top: 10, right: 5),
                                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                                        child: ClipRRect(
                                                                                          borderRadius: BorderRadius.circular(5),
                                                                                          child: CachedNetworkImage(
                                                                                            imageUrl: img[0]['url'],
                                                                                            placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                            errorWidget: (context, url, error) => Image.asset(
                                                                                              "assets/images/placeholder_cover.jpg",
                                                                                              fit: BoxFit.cover,
                                                                                              //height: 40,
                                                                                            ),
                                                                                            fit: BoxFit.cover,
                                                                                          ),
                                                                                        )),
                                                                                  ),
                                                                                  Expanded(
                                                                                    child: Container(
                                                                                        //color: Colors.red,
                                                                                        height: 150,
                                                                                        // width: 150,
                                                                                        padding: const EdgeInsets.all(0.0),
                                                                                        margin: EdgeInsets.only(top: 10, left: 5),
                                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car3.jpeg"), fit: BoxFit.cover)),
                                                                                        child: ClipRRect(
                                                                                          borderRadius: BorderRadius.circular(5),
                                                                                          child: CachedNetworkImage(
                                                                                            imageUrl: img[1]['url'],
                                                                                            placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                            errorWidget: (context, url, error) => Image.asset(
                                                                                              "assets/images/placeholder_cover.jpg",
                                                                                              fit: BoxFit.cover,
                                                                                              //height: 40,
                                                                                            ),
                                                                                            fit: BoxFit.cover,
                                                                                          ),
                                                                                        )),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                            img.length == 2
                                                                                ? Container()
                                                                                : Container(
                                                                                    child: Row(
                                                                                      children: <Widget>[
                                                                                        Expanded(
                                                                                          child: Container(
                                                                                              //color: Colors.red,
                                                                                              height: img.length == 3 ? 200 : 150,
                                                                                              //width: 150,
                                                                                              padding: const EdgeInsets.all(0.0),
                                                                                              margin: EdgeInsets.only(top: 10, right: img.length == 3 ? 0 : 5),
                                                                                              //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car6.jpg"), fit: BoxFit.cover)),
                                                                                              child: ClipRRect(
                                                                                                borderRadius: BorderRadius.circular(5),
                                                                                                child: CachedNetworkImage(
                                                                                                  imageUrl: img[2]['url'],
                                                                                                  placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                  errorWidget: (context, url, error) => Image.asset(
                                                                                                    "assets/images/placeholder_cover.jpg",
                                                                                                    fit: BoxFit.cover,
                                                                                                    //height: 40,
                                                                                                  ),
                                                                                                  fit: BoxFit.cover,
                                                                                                ),
                                                                                              )),
                                                                                        ),
                                                                                        img.length < 4
                                                                                            ? Container()
                                                                                            : Expanded(
                                                                                                child: Container(
                                                                                                    //color: Colors.red,
                                                                                                    height: 150,
                                                                                                    // width: 150,
                                                                                                    padding: const EdgeInsets.all(0.0),
                                                                                                    margin: EdgeInsets.only(top: 10, left: 5),
                                                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/placeholder_cover.jpg"), fit: BoxFit.cover)),
                                                                                                    child: Container(
                                                                                                      child: Stack(
                                                                                                        children: <Widget>[
                                                                                                          Container(
                                                                                                            height: 150,
                                                                                                            child: ClipRRect(
                                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                                              child: CachedNetworkImage(
                                                                                                                imageUrl: img[3]['url'],
                                                                                                                placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                                errorWidget: (context, url, error) => Image.asset(
                                                                                                                  "assets/images/placeholder_cover.jpg",
                                                                                                                  fit: BoxFit.cover,
                                                                                                                  //height: 40,
                                                                                                                ),
                                                                                                                fit: BoxFit.cover,
                                                                                                              ),
                                                                                                            ),
                                                                                                          ),
                                                                                                          Container(
                                                                                                            height: 150,
                                                                                                            // width: 150,
                                                                                                            padding: const EdgeInsets.all(0.0),
                                                                                                            margin: EdgeInsets.only(top: 0, left: 0),
                                                                                                            decoration: BoxDecoration(
                                                                                                              color: img.length > 4 ? Colors.black.withOpacity(0.6) : Colors.transparent,
                                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                                            ),
                                                                                                          ),
                                                                                                          img.length > 4
                                                                                                              ? Center(
                                                                                                                  child: Container(
                                                                                                                      child: Text(
                                                                                                                  "+${img.length - 3}",
                                                                                                                  style: TextStyle(color: Colors.white, fontSize: 22),
                                                                                                                )))
                                                                                                              : Container(),
                                                                                                        ],
                                                                                                      ),
                                                                                                    )),
                                                                                              ),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                          ],
                                                                        ),
                                                                ],
                                                              ),
                                                            ),
                                                    ],
                                                  ),
                                                )),
                                            ////// <<<<< Post end >>>>> //////
                                            Container(
                                                margin: EdgeInsets.only(
                                                    left: 20,
                                                    right: 20,
                                                    bottom: 0,
                                                    top: 10),
                                                child: Divider(
                                                  color: Colors.grey[300],
                                                )),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 20, top: 0),
                                              padding: EdgeInsets.all(0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  ////// <<<<< Like start >>>>> //////
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        if (feedList[index]
                                                                .like ==
                                                            null) {
                                                          feedList[index]
                                                              .meta
                                                              .totalLikesCount++;
                                                          feedList[index].like =
                                                              1;
                                                          likeUnlike(
                                                              index,
                                                              1,
                                                              feedList[index]
                                                                  .id);
                                                        } else {
                                                          feedList[index]
                                                              .meta
                                                              .totalLikesCount--;
                                                          feedList[index].like =
                                                              null;
                                                          likeUnlike(
                                                              index,
                                                              2,
                                                              feedList[index]
                                                                  .id);
                                                        }
                                                      });
                                                    },
                                                    child: Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Container(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    3.0),
                                                            child: Icon(
                                                              feedList[index]
                                                                          .like !=
                                                                      null
                                                                  ? Icons
                                                                      .favorite
                                                                  : Icons
                                                                      .favorite_border,
                                                              size: 20,
                                                              color: feedList[index]
                                                                          .like !=
                                                                      null
                                                                  ? Colors
                                                                      .redAccent
                                                                  : Colors
                                                                      .black54,
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 3),
                                                            child: Text(
                                                                "${feedList[index].meta.totalLikesCount}",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Oswald',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300,
                                                                    color: Colors
                                                                        .black54,
                                                                    fontSize:
                                                                        12)),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  ////// <<<<< Like end >>>>> //////

                                                  ////// <<<<< Comment start >>>>> //////
                                                  GestureDetector(
                                                    onTap: () {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  CommentPage(
                                                                      userData,
                                                                      index,
                                                                      feedList[
                                                                          index])));
                                                    },
                                                    child: Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 15),
                                                            padding:
                                                                EdgeInsets.all(
                                                                    3.0),
                                                            child: Icon(
                                                              Icons
                                                                  .chat_bubble_outline,
                                                              size: 20,
                                                              color: Colors
                                                                  .black54,
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 3),
                                                            child: Text(
                                                                "${postComCount[index]['count']}",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Oswald',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300,
                                                                    color: Colors
                                                                        .black54,
                                                                    fontSize:
                                                                        12)),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  ////// <<<<< Comment end >>>>> //////

                                                  ////// <<<<< Share start >>>>> //////
                                                  GestureDetector(
                                                    onTap: () {
                                                      _shareModalBottomSheet(
                                                          context,
                                                          index,
                                                          userData,
                                                          feedList[index]);
                                                    },
                                                    child: Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 15),
                                                            padding:
                                                                EdgeInsets.all(
                                                                    3.0),
                                                            child: Icon(
                                                              Icons.share,
                                                              size: 20,
                                                              color: Colors
                                                                  .black54,
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 3),
                                                            child: Text(
                                                                "${feedList[index].meta.totalSharesCount}",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Oswald',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300,
                                                                    color: Colors
                                                                        .black54,
                                                                    fontSize:
                                                                        12)),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  ////// <<<<< Share end >>>>> //////
                                                ],
                                              ),
                                            ),
                                            postList.feed[index].isAdd != 1
                                                ? Container()
                                                : Container(
                                                    height: 10,
                                                    decoration: BoxDecoration(
                                                        color: Colors.grey
                                                            .withOpacity(0.1),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5)),
                                                    margin: EdgeInsets.only(
                                                        left: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                        top: 7),
                                                    child: null),
                                            postList.feed[index].isAdd != 1
                                                ? Container()
                                                : Column(
                                                    children: <Widget>[
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: <Widget>[
                                                          Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      top: 10,
                                                                      left: 20),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                "Suggested Posts (2 Posts)",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        12,
                                                                    color: Colors
                                                                        .black38),
                                                              )),
                                                          Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      top: 10,
                                                                      right:
                                                                          20),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                "More >",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        12,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    color:
                                                                        mainColor),
                                                              )),
                                                        ],
                                                      ),
                                                      Scrollbar(
                                                        child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  bottom: 10),
                                                          child:
                                                              SingleChildScrollView(
                                                            scrollDirection:
                                                                Axis.horizontal,
                                                            physics:
                                                                BouncingScrollPhysics(),
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Column(
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                      width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width,
                                                                      child:
                                                                          Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.spaceBetween,
                                                                        children: <
                                                                            Widget>[
                                                                          Expanded(
                                                                            child:
                                                                                Container(
                                                                              //color: Colors.red,
                                                                              margin: EdgeInsets.only(left: 20, right: 20, top: 15),
                                                                              padding: EdgeInsets.only(right: 10),
                                                                              child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                                children: <Widget>[
                                                                                  ////// <<<<< Picture start >>>>> //////
                                                                                  Container(
                                                                                    margin: EdgeInsets.only(right: 10),
                                                                                    //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                                                    padding: EdgeInsets.all(1.0),
                                                                                    child: CircleAvatar(
                                                                                      radius: 20.0,
                                                                                      backgroundColor: Colors.white,
                                                                                      backgroundImage: AssetImage('assets/images/cover.jpg'),
                                                                                    ),
                                                                                    decoration: new BoxDecoration(
                                                                                      color: Colors.grey[300], // border color
                                                                                      shape: BoxShape.circle,
                                                                                    ),
                                                                                  ),
                                                                                  ////// <<<<< Picture end >>>>> //////

                                                                                  Expanded(
                                                                                    child: Column(
                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                      children: <Widget>[
                                                                                        ////// <<<<< Ad Start >>>>> //////

                                                                                        Container(
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Text(
                                                                                                "Foodpanda",
                                                                                                maxLines: 1,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(fontSize: 15, color: Colors.black54, fontFamily: 'Oswald', fontWeight: FontWeight.w500),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        ////// <<<<< Ad End >>>>> //////

                                                                                        ////// <<<<< Time start >>>>> //////
                                                                                        Container(
                                                                                          margin: EdgeInsets.only(top: 3),
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Text(
                                                                                                date,
                                                                                                maxLines: 1,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w400, fontSize: 11, color: Colors.black45),
                                                                                              ),
                                                                                              Container(
                                                                                                  margin: EdgeInsets.only(left: 5),
                                                                                                  child: Icon(
                                                                                                    Icons.public,
                                                                                                    color: Colors.black45,
                                                                                                    size: 12,
                                                                                                  ))
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        ////// <<<<< Time end >>>>> //////
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          ////// <<<<< More start >>>>> //////
                                                                          feedList[index].userId != userData['id']
                                                                              ? Container()
                                                                              : GestureDetector(
                                                                                  onTap: () {
                                                                                    setState(() {
                                                                                      _statusModalBottomSheet(context, index, feedList[index]);
                                                                                    });
                                                                                  },
                                                                                  child: Container(
                                                                                      padding: EdgeInsets.all(5),
                                                                                      margin: EdgeInsets.only(right: 15),
                                                                                      child: Icon(
                                                                                        Icons.more_horiz,
                                                                                        color: Colors.black45,
                                                                                      )),
                                                                                ),
                                                                          ////// <<<<< More end >>>>> //////
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                        //color: Colors.red,
                                                                        height:
                                                                            200,
                                                                        width: MediaQuery.of(context).size.width -
                                                                            50,
                                                                        padding:
                                                                            const EdgeInsets.all(
                                                                                0.0),
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                10,
                                                                            right:
                                                                                5,
                                                                            left:
                                                                                0),
                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                        child:
                                                                            ClipRRect(
                                                                          borderRadius:
                                                                              BorderRadius.circular(5),
                                                                          child:
                                                                              CachedNetworkImage(
                                                                            imageUrl:
                                                                                'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',
                                                                            placeholder: (context, url) =>
                                                                                SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                            errorWidget: (context, url, error) =>
                                                                                Image.asset(
                                                                              "assets/images/placeholder_cover.jpg",
                                                                              fit: BoxFit.cover,
                                                                              //height: 40,
                                                                            ),
                                                                            fit:
                                                                                BoxFit.cover,
                                                                          ),
                                                                        )),
                                                                    Container(
                                                                      width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width,
                                                                      child:
                                                                          Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.spaceBetween,
                                                                        children: <
                                                                            Widget>[
                                                                          Expanded(
                                                                            child:
                                                                                Container(
                                                                              //color: Colors.red,
                                                                              margin: EdgeInsets.only(left: 20, right: 20, top: 15),
                                                                              padding: EdgeInsets.only(right: 10),
                                                                              child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                                children: <Widget>[
                                                                                  Expanded(
                                                                                    child: Column(
                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                      children: <Widget>[
                                                                                        ////// <<<<< Ad Start >>>>> //////

                                                                                        Container(
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Text(
                                                                                                "30% Discount",
                                                                                                maxLines: 1,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(fontSize: 14, color: mainColor, fontFamily: 'Oswald', fontWeight: FontWeight.w600),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        ////// <<<<< Ad End >>>>> //////

                                                                                        ////// <<<<< Time start >>>>> //////
                                                                                        Container(
                                                                                          margin: EdgeInsets.only(top: 3),
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Text(
                                                                                                "Offer ends Oct 23, 2020",
                                                                                                maxLines: 1,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w400, fontSize: 12, color: Colors.black54),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),

                                                                                        Container(
                                                                                          margin: EdgeInsets.only(top: 3, right: 25),
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Expanded(
                                                                                                child: Text(
                                                                                                  "A sales promotion is a tried and true way to ramp up your sales, acquire new customers, and take advantage of seasonal opportunities. Sales promotions are a short-term marketing tactic to create urgency and increase sales.",
                                                                                                  maxLines: 1,
                                                                                                  overflow: TextOverflow.ellipsis,
                                                                                                  style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w400, fontSize: 10, color: Colors.black45),
                                                                                                ),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        ////// <<<<< Time end >>>>> //////
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                  Container(
                                                                                      padding: EdgeInsets.all(5),
                                                                                      decoration: BoxDecoration(color: Colors.grey[300], borderRadius: BorderRadius.circular(5)),
                                                                                      child: Text(
                                                                                        "Visit Page",
                                                                                        style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w500, fontSize: 13, color: Colors.black54),
                                                                                      ))
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                Column(
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                      width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width,
                                                                      child:
                                                                          Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.spaceBetween,
                                                                        children: <
                                                                            Widget>[
                                                                          Expanded(
                                                                            child:
                                                                                Container(
                                                                              //color: Colors.red,
                                                                              margin: EdgeInsets.only(left: 20, right: 20, top: 15),
                                                                              padding: EdgeInsets.only(right: 10),
                                                                              child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                                children: <Widget>[
                                                                                  ////// <<<<< Picture start >>>>> //////
                                                                                  Container(
                                                                                    margin: EdgeInsets.only(right: 10),
                                                                                    //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                                                                    padding: EdgeInsets.all(1.0),
                                                                                    child: CircleAvatar(
                                                                                      radius: 20.0,
                                                                                      backgroundColor: Colors.white,
                                                                                      backgroundImage: AssetImage('assets/images/cover.jpg'),
                                                                                    ),
                                                                                    decoration: new BoxDecoration(
                                                                                      color: Colors.grey[300], // border color
                                                                                      shape: BoxShape.circle,
                                                                                    ),
                                                                                  ),
                                                                                  ////// <<<<< Picture end >>>>> //////

                                                                                  Expanded(
                                                                                    child: Column(
                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                      children: <Widget>[
                                                                                        ////// <<<<< Ad Start >>>>> //////

                                                                                        Container(
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Text(
                                                                                                "Pathao",
                                                                                                maxLines: 1,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(fontSize: 15, color: Colors.black54, fontFamily: 'Oswald', fontWeight: FontWeight.w500),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        ////// <<<<< Ad End >>>>> //////

                                                                                        ////// <<<<< Time start >>>>> //////
                                                                                        Container(
                                                                                          margin: EdgeInsets.only(top: 3),
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Text(
                                                                                                date,
                                                                                                maxLines: 1,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w400, fontSize: 11, color: Colors.black45),
                                                                                              ),
                                                                                              Container(
                                                                                                  margin: EdgeInsets.only(left: 5),
                                                                                                  child: Icon(
                                                                                                    Icons.public,
                                                                                                    color: Colors.black45,
                                                                                                    size: 12,
                                                                                                  ))
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        ////// <<<<< Time end >>>>> //////
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          ////// <<<<< More start >>>>> //////
                                                                          feedList[index].userId != userData['id']
                                                                              ? Container()
                                                                              : GestureDetector(
                                                                                  onTap: () {
                                                                                    setState(() {
                                                                                      _statusModalBottomSheet(context, index, feedList[index]);
                                                                                    });
                                                                                  },
                                                                                  child: Container(
                                                                                      padding: EdgeInsets.all(5),
                                                                                      margin: EdgeInsets.only(right: 15),
                                                                                      child: Icon(
                                                                                        Icons.more_horiz,
                                                                                        color: Colors.black45,
                                                                                      )),
                                                                                ),
                                                                          ////// <<<<< More end >>>>> //////
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                        //color: Colors.red,
                                                                        height:
                                                                            200,
                                                                        width: MediaQuery.of(context).size.width -
                                                                            50,
                                                                        padding:
                                                                            const EdgeInsets.all(
                                                                                0.0),
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                10,
                                                                            right:
                                                                                5,
                                                                            left:
                                                                                0),
                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                        child:
                                                                            ClipRRect(
                                                                          borderRadius:
                                                                              BorderRadius.circular(5),
                                                                          child:
                                                                              CachedNetworkImage(
                                                                            imageUrl:
                                                                                'https://homepages.cae.wisc.edu/~ece533/images/boat.png',
                                                                            placeholder: (context, url) =>
                                                                                SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                            errorWidget: (context, url, error) =>
                                                                                Image.asset(
                                                                              "assets/images/placeholder_cover.jpg",
                                                                              fit: BoxFit.cover,
                                                                              //height: 40,
                                                                            ),
                                                                            fit:
                                                                                BoxFit.cover,
                                                                          ),
                                                                        )),
                                                                    Container(
                                                                      width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width,
                                                                      child:
                                                                          Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.spaceBetween,
                                                                        children: <
                                                                            Widget>[
                                                                          Expanded(
                                                                            child:
                                                                                Container(
                                                                              //color: Colors.red,
                                                                              margin: EdgeInsets.only(left: 20, right: 20, top: 15),
                                                                              padding: EdgeInsets.only(right: 10),
                                                                              child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                                children: <Widget>[
                                                                                  Expanded(
                                                                                    child: Column(
                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                      children: <Widget>[
                                                                                        ////// <<<<< Ad Start >>>>> //////

                                                                                        Container(
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Text(
                                                                                                "60% off",
                                                                                                maxLines: 1,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(fontSize: 14, color: mainColor, fontFamily: 'Oswald', fontWeight: FontWeight.w600),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        ////// <<<<< Ad End >>>>> //////

                                                                                        ////// <<<<< Time start >>>>> //////
                                                                                        Container(
                                                                                          margin: EdgeInsets.only(top: 3),
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Text(
                                                                                                "Offer ends today",
                                                                                                maxLines: 1,
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w400, fontSize: 12, color: Colors.black54),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        Container(
                                                                                          margin: EdgeInsets.only(top: 3, right: 25),
                                                                                          child: Row(
                                                                                            children: <Widget>[
                                                                                              Expanded(
                                                                                                child: Text(
                                                                                                  "A sales promotion is a tried and true way to ramp up your sales, acquire new customers, and take advantage of seasonal opportunities. Sales promotions are a short-term marketing tactic to create urgency and increase sales.",
                                                                                                  maxLines: 1,
                                                                                                  overflow: TextOverflow.ellipsis,
                                                                                                  style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w400, fontSize: 10, color: Colors.black45),
                                                                                                ),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        ////// <<<<< Time end >>>>> //////
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                  Container(
                                                                                      padding: EdgeInsets.all(5),
                                                                                      decoration: BoxDecoration(color: Colors.grey[300], borderRadius: BorderRadius.circular(5)),
                                                                                      child: Text(
                                                                                        "Visit Page",
                                                                                        style: TextStyle(fontFamily: 'Oswald', fontWeight: FontWeight.w500, fontSize: 13, color: Colors.black54),
                                                                                      ))
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                            Container(
                                                height: 10,
                                                decoration: BoxDecoration(
                                                    color: Colors.grey
                                                        .withOpacity(0.1),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5)),
                                                margin: EdgeInsets.only(
                                                    left: 0,
                                                    right: 0,
                                                    bottom: 0,
                                                    top: 7),
                                                child: null),
                                          ],
                                        ),
                                      )
                                    : FeedLoader(),
                              ),
                            );
                          }),
                    ),
                    // Container(
                    //   margin: EdgeInsets.only(top: 20, left: 10, right: 10),
                    //   child: Column(
                    //       children: List.generate(feedList.length, (index) {
                    //     DateTime dateTime =
                    //         DateTime.parse(feedList[index].createdAt);
                    //     date = DateFormat.yMMMd().format(dateTime);
                    //     List img = [];

                    //     for (int i = 0;
                    //         i < feedList[index].data.images.length;
                    //         i++) {
                    //       img.add({
                    //         'id': feedList[index].data.images[i].id,
                    //         'url': feedList[index].data.images[i].url,
                    //       });
                    //     }
                    //     ;
                    //   })),
                    // )
                  ],
                ),
              ),
            ),
    );
  }

  void _shareModalBottomSheet(context, int index, var userData, feedList) {
    DateTime dateTime = DateTime.parse(feedList.createdAt);
    String date1 = DateFormat.yMMMd().format(dateTime);

    List img = [];

    for (int i = 0; i < feedList.data.images.length; i++) {
      img.add({
        'id': feedList.data.images[i].id,
        'url': feedList.data.images[i].url,
      });
    }
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Container(
                //height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.share,
                              color: Colors.black54,
                              size: 16,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 5),
                              child: Text(
                                "Share this post",
                                style: TextStyle(fontFamily: "Oswald"),
                              ),
                            ),
                          ],
                        )),
                    Divider(),
                    Container(
                      child: Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              width: 50,
                              height: 50,
                              margin: EdgeInsets.only(top: 0, left: 20),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/prabal.jpg"),
                                      fit: BoxFit.cover),
                                  border: Border.all(
                                      color: Colors.grey, width: 0.1),
                                  borderRadius: BorderRadius.circular(15)),
                            ),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                      alignment: Alignment.centerLeft,
                                      margin:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(
                                              "${userData['firstName']} ${userData['lastName']}",
                                              maxLines: 1,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14,
                                                  fontFamily: 'Oswald',
                                                  fontWeight:
                                                      FontWeight.normal),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            //_securityModalBottomSheet(context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 0, right: 5, left: 10),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 0.3,
                                                    color: Colors.black54),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Icon(
                                                    Icons.public,
                                                    size: 12,
                                                    color: Colors.black54,
                                                  ),
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Public",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                          fontFamily: "Oswald"),
                                                    )),
                                                Icon(
                                                  Icons.arrow_drop_down,
                                                  size: 25,
                                                  color: Colors.black54,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //height: 150,
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.only(
                          top: 20, left: 20, bottom: 5, right: 20),
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.only(
                            topLeft: Radius.circular(5.0),
                            topRight: Radius.circular(5.0),
                            bottomLeft: Radius.circular(5.0),
                            bottomRight: Radius.circular(5.0)),
                        //color: Colors.grey[100],
                        //border: Border.all(width: 0.2, color: Colors.grey)
                      ),
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(minHeight: 100.0, maxHeight: 100.0),
                        child: new SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          //reverse: true,
                          child: Container(
                            //height: 100,
                            child: new TextField(
                              maxLines: null,
                              autofocus: false,
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: 'Oswald',
                              ),
                              controller: shareController,
                              decoration: InputDecoration(
                                alignLabelWithHint: true,
                                hintText: "Write something...",
                                hintStyle: TextStyle(
                                    color: Colors.black54,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w300),
                                contentPadding:
                                    EdgeInsets.fromLTRB(0.0, 10.0, 20.0, 10.0),
                                border: InputBorder.none,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  sharePost = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              //snackBar(context);
                              loading == false
                                  ? statusShare(feedList, img)
                                  : null;
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 20, top: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.only(
                                          left: 20, right: 20, top: 0),
                                      decoration: BoxDecoration(
                                          color: mainColor,
                                          border: Border.all(
                                              color: Colors.grey, width: 0.5),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: Text(
                                        "Share",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontFamily: 'BebasNeue',
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      //height: MediaQuery.of(context).size.height / 2.05,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        //border: Border.all(width: 0.8, color: Colors.grey[300]),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 16.0,
                            color: Colors.grey[300],
                            //offset: Offset(3.0, 4.0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(
                          top: 5, bottom: 15, left: 10, right: 10),
                      child: Container(
                        //color: Colors.yellow,
                        margin:
                            EdgeInsets.only(left: 20, right: 20, bottom: 10),
                        padding: EdgeInsets.only(right: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< pic start >>>>> //////
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    padding: EdgeInsets.all(1.0),
                                    child: CircleAvatar(
                                      radius: 20.0,
                                      backgroundColor: Colors.white,
                                      backgroundImage: AssetImage(
                                          "assets/images/prabal.jpg"),
                                    ),
                                    decoration: new BoxDecoration(
                                      color: Colors.grey[300], // border color
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                  ////// <<<<< pic end >>>>> //////

                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ////// <<<<< Name & Interest start >>>>> //////
                                          Container(
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    feedList
                                                        .data.statusUserName,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        color: Colors.black,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                          ////// <<<<< Name & Interest end >>>>> //////

                                          ////// <<<<< time job start >>>>> //////
                                          Container(
                                            margin: EdgeInsets.only(top: 3),
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                  child: Text(
                                                    date1,
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 11,
                                                        color: Colors.black54),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 3),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Icon(
                                                          Icons.public,
                                                          size: 12,
                                                          color: Colors.black54,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          ////// <<<<< time job end >>>>> //////
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                                margin:
                                    EdgeInsets.only(left: 0, right: 0, top: 20),
                                child: Container(
                                  child: Column(
                                    children: <Widget>[
                                      feedList.activityText == ""
                                          ? Container()
                                          : Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: Text(
                                                "${feedList.activityText}",
                                                textAlign: TextAlign.justify,
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontWeight:
                                                        FontWeight.w400),
                                              ),
                                            ),
                                      img.length == 0
                                          ? Container()
                                          : Container(
                                              child: Column(
                                                children: <Widget>[
                                                  img.length == 1
                                                      ? Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child: Container(
                                                                  //color: Colors.red,
                                                                  height: 200,
                                                                  //width: 150,
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .all(
                                                                          0.0),
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10,
                                                                          right:
                                                                              0),
                                                                  //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                  child:
                                                                      ClipRRect(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(5),
                                                                    child:
                                                                        CachedNetworkImage(
                                                                      imageUrl:
                                                                          img[0]
                                                                              [
                                                                              'url'],
                                                                      placeholder: (context,
                                                                              url) =>
                                                                          SpinKitCircle(
                                                                              color: Colors.grey.withOpacity(0.5)),
                                                                      errorWidget: (context,
                                                                              url,
                                                                              error) =>
                                                                          Image
                                                                              .asset(
                                                                        "assets/images/placeholder_cover.jpg",
                                                                        fit: BoxFit
                                                                            .cover,
                                                                        //height: 40,
                                                                      ),
                                                                      fit: BoxFit
                                                                          .cover,
                                                                    ),
                                                                  )),
                                                            ),
                                                          ],
                                                        )
                                                      : Column(
                                                          children: <Widget>[
                                                            Container(
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                    child: Container(
                                                                        //color: Colors.red,
                                                                        height: 150,
                                                                        //width: 150,
                                                                        padding: const EdgeInsets.all(0.0),
                                                                        margin: EdgeInsets.only(top: 10, right: 5),
                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                                                        child: ClipRRect(
                                                                          borderRadius:
                                                                              BorderRadius.circular(5),
                                                                          child:
                                                                              CachedNetworkImage(
                                                                            imageUrl:
                                                                                img[0]['url'],
                                                                            placeholder: (context, url) =>
                                                                                SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                            errorWidget: (context, url, error) =>
                                                                                Image.asset(
                                                                              "assets/images/placeholder_cover.jpg",
                                                                              fit: BoxFit.cover,
                                                                              //height: 40,
                                                                            ),
                                                                            fit:
                                                                                BoxFit.cover,
                                                                          ),
                                                                        )),
                                                                  ),
                                                                  Expanded(
                                                                    child: Container(
                                                                        //color: Colors.red,
                                                                        height: 150,
                                                                        // width: 150,
                                                                        padding: const EdgeInsets.all(0.0),
                                                                        margin: EdgeInsets.only(top: 10, left: 5),
                                                                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car3.jpeg"), fit: BoxFit.cover)),
                                                                        child: ClipRRect(
                                                                          borderRadius:
                                                                              BorderRadius.circular(5),
                                                                          child:
                                                                              CachedNetworkImage(
                                                                            imageUrl:
                                                                                img[1]['url'],
                                                                            placeholder: (context, url) =>
                                                                                SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                            errorWidget: (context, url, error) =>
                                                                                Image.asset(
                                                                              "assets/images/placeholder_cover.jpg",
                                                                              fit: BoxFit.cover,
                                                                              //height: 40,
                                                                            ),
                                                                            fit:
                                                                                BoxFit.cover,
                                                                          ),
                                                                        )),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            img.length == 2
                                                                ? Container()
                                                                : Container(
                                                                    child: Row(
                                                                      children: <
                                                                          Widget>[
                                                                        Expanded(
                                                                          child: Container(
                                                                              //color: Colors.red,
                                                                              height: img.length == 3 ? 200 : 150,
                                                                              //width: 150,
                                                                              padding: const EdgeInsets.all(0.0),
                                                                              margin: EdgeInsets.only(top: 10, right: img.length == 3 ? 0 : 5),
                                                                              //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car6.jpg"), fit: BoxFit.cover)),
                                                                              child: ClipRRect(
                                                                                borderRadius: BorderRadius.circular(5),
                                                                                child: CachedNetworkImage(
                                                                                  imageUrl: img[2]['url'],
                                                                                  placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                  errorWidget: (context, url, error) => Image.asset(
                                                                                    "assets/images/placeholder_cover.jpg",
                                                                                    fit: BoxFit.cover,
                                                                                    //height: 40,
                                                                                  ),
                                                                                  fit: BoxFit.cover,
                                                                                ),
                                                                              )),
                                                                        ),
                                                                        img.length <
                                                                                4
                                                                            ? Container()
                                                                            : Expanded(
                                                                                child: Container(
                                                                                    //color: Colors.red,
                                                                                    height: 150,
                                                                                    // width: 150,
                                                                                    padding: const EdgeInsets.all(0.0),
                                                                                    margin: EdgeInsets.only(top: 10, left: 5),
                                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/placeholder_cover.jpg"), fit: BoxFit.cover)),
                                                                                    child: Container(
                                                                                      child: Stack(
                                                                                        children: <Widget>[
                                                                                          Container(
                                                                                            height: 150,
                                                                                            child: ClipRRect(
                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                              child: CachedNetworkImage(
                                                                                                imageUrl: img[3]['url'],
                                                                                                placeholder: (context, url) => SpinKitCircle(color: Colors.grey.withOpacity(0.5)),
                                                                                                errorWidget: (context, url, error) => Image.asset(
                                                                                                  "assets/images/placeholder_cover.jpg",
                                                                                                  fit: BoxFit.cover,
                                                                                                  //height: 40,
                                                                                                ),
                                                                                                fit: BoxFit.cover,
                                                                                              ),
                                                                                            ),
                                                                                          ),
                                                                                          Container(
                                                                                            height: 150,
                                                                                            // width: 150,
                                                                                            padding: const EdgeInsets.all(0.0),
                                                                                            margin: EdgeInsets.only(top: 0, left: 0),
                                                                                            decoration: BoxDecoration(
                                                                                              color: img.length > 4 ? Colors.black.withOpacity(0.6) : Colors.transparent,
                                                                                              borderRadius: BorderRadius.circular(5),
                                                                                            ),
                                                                                          ),
                                                                                          img.length > 4
                                                                                              ? Center(
                                                                                                  child: Container(
                                                                                                      child: Text(
                                                                                                  "+${img.length - 3}",
                                                                                                  style: TextStyle(color: Colors.white, fontSize: 22),
                                                                                                )))
                                                                                              : Container(),
                                                                                        ],
                                                                                      ),
                                                                                    )),
                                                                              ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                          ],
                                                        ),
                                                ],
                                              ),
                                            ),
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      ),
                    )
                  ],
                )),
          );
        });
  }

  Future statusShare(feedList, img) async {
    setState(() {
      loading = true;
    });
    var data = {
      'id': feedList.id,
      'user_id': feedList.userId,
      'activity_id': feedList.id,
      'activity_text': sharePost,
      'activity_type': "Share",
      "privacy": feedList.privacy,
      'data': {
        'status': feedList.activityText,
        'comments': [],
        'images': img,
        'statusUser_Name': feedList.data.statusUserName,
        'statusUser_userName': feedList.data.statusUserUserName,
        'statusUser_profilePic': feedList.data.statusUserProfilePic,
        'link': "",
        'link_meta': {},
      },
      'created_at': feedList.createdAt,
      'updated_at': feedList.createdAt,
    };

    print(data);

    var res = await CallApi().postData1(data, 'share/add');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 201) {
      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    } else {
      _showMessage("Something went wrong!", 1);
    }

    setState(() {
      loading = false;
    });
  }
}
