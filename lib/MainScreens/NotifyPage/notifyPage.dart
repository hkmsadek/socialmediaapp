import 'dart:convert';
import 'package:social_app_fb/API/api.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:social_app_fb/Loader/NotificationLoader/notifyLoader.dart';
import 'package:social_app_fb/MainScreens/StatusDetailsPage/StatusDetailsPage.dart';
import 'package:social_app_fb/ModelClass/NotifyModel/NotifyModel.dart';
import 'dart:async';
import '../../main.dart';

class NotifyPage extends StatefulWidget {
  @override
  NotifyPageState createState() => NotifyPageState();
}

class NotifyPageState extends State<NotifyPage> {
  SharedPreferences sharedPreferences;
  String theme = "", daysAgo = "";
  Timer _timer;
  int _start = 3;
  int lastNotifyID;
  bool loading = true;
  var notifyList, userData;
  ScrollController _controller = new ScrollController();
  List notificationList = [];

  @override
  void initState() {
    _getUserInfo();

    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels == 0) {
          setState(() {
            print("top");
          });
        }
        // you are at top position

        else {
          setState(() {
            print("bottom");
            loadNotification(lastNotifyID);

            print("lastNotifyID 1");
            print(lastNotifyID);
          });
        }
        // you are at bottom position
      }
    });
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    var user = json.decode(userJson);
    setState(() {
      userData = user;
      //_isLoaded = true;
    });
    loadNotification("");

    //print("${userData['shop_id']}");
  }

  Future loadNotification(number) async {
    //await Future.delayed(Duration(seconds: 3));
    var postresponse =
        await CallApi().getData('get/notificaiton?last_id=$number');
    print(postresponse);
    var postcontent = postresponse.body;
    final posts = json.decode(postcontent);
    var postdata = NotifyModel.fromJson(posts);

    setState(() {
      notifyList = postdata;
      for (int i = 0; i < notifyList.notifications.length; i++) {
        //print(notifyList.allNotification[i].id);
        notificationList.add(notifyList.notifications[i]);
        lastNotifyID = notifyList.notifications[i].id;
        print("lastNotifyID");
        print(lastNotifyID);
      }
    });
    print("lastNotifyID1");
    print(lastNotifyID);
    print("notifyList.notifications.length");
    print(notifyList.notifications.length);

    setState(() {
      loading = false;
    });
  }

  void makeSeen(id) async {
    var data = {
      'id': id,
    };

    //print(data);

    var res = await CallApi().postData1(data, 'post/specificNotifUpdateToSeen');
    var body = json.decode(res.body);
    print("body");
    print(body);

    if (res.statusCode == 200) {
      print("ok");
    } else {
      print("not ok");
    }
  }

  Future<void> pageRoute(result) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('stid', result);

    // Navigator.push(
    //     context, MaterialPageRoute(builder: (context) => StatusDetailsPage()));
  }

  Future<void> markAllRead() async {
    var allRead = await CallApi().postDataRead('post/allNotifUpdateToSeen');
    var body = json.decode(allRead.body);
    print(body);

    setState(() {
      for (int i = 0; i < notificationList.length; i++) {
        notificationList[i].seen = "1";
        print(notificationList[i].seen);
      }
    });
  }

  Future<Null> _showDeleteDialog(id, index) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          "Want to delete the notification?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                              deleteNotification(id, index);
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> deleteNotification(id, index) async {
    var data = {
      'id': id,
    };

    //print(data);

    var res = await CallApi().postData1(data, 'post/deleteNotification');
    var body = json.decode(res.body);
    print("body");
    print(body);

    if (res.statusCode == 200) {
      setState(() {
        notificationList.removeAt(index);
      });
    } else {
      print("not ok");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      ////// <<<<< Background color >>>>> //////
      backgroundColor: Colors.white,

      ////// <<<<< AppBar >>>>> //////
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ////// <<<<< Title >>>>> //////
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Notification",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              markAllRead();
            },
            child: Container(
              child: Center(
                  child: Container(
                      margin: EdgeInsets.only(right: 15),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.done_all,
                            color: mainColor,
                            size: 17,
                          ),
                          SizedBox(width: 5),
                          Text("Mark all read",
                              style: TextStyle(color: mainColor)),
                        ],
                      ))),
            ),
          )
        ],
      ),

      ////// <<<<< Body >>>>> //////
      body: Container(
          height: MediaQuery.of(context).size.height,
          child: loading
              ? Center(child: CircularProgressIndicator())
              : notificationList.length == 0
                  ? Center(
                      child: Container(
                        child: Text("No Notification Available!"),
                      ),
                    )
                  : Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          controller: _controller,
                          itemCount: notificationList.length,
                          itemBuilder: (context, index) {
                            List day = [];
                            for (int i = 0; i < notificationList.length; i++) {
                              DateTime date1 = DateTime.parse(
                                  "${notificationList[i].created_at}");

                              daysAgo = DateFormat.yMMMd().format(date1);
                              day.add(daysAgo);
                            }
                            return loading == false
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        String url =
                                            notificationList[index].url;
                                        int number = '/'.allMatches(url).length;
                                        String result = "";
                                        if (number > 1) {
                                          if (number == 3) {
                                            var urlCheck = url.split("/");
                                            print(urlCheck);
                                            String first = urlCheck[0];
                                            String second = urlCheck[1];
                                            String third = urlCheck[2];
                                            String fourth = urlCheck[3];

                                            print(third);
                                            print(fourth);

                                            // Navigator.push(
                                            //     context,
                                            //     MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             ProductDetailsPage(
                                            //                 fourth)));
                                          } else {
                                            var urlCheck = url.split("/");
                                            String first = urlCheck[1];
                                            String second = urlCheck[2];

                                            if (second.contains("?")) {
                                              result = second.substring(
                                                  0, second.indexOf('?'));
                                            } else {
                                              result = second;
                                            }

                                            print(first);
                                            print(result);

                                            if (first == "community") {
                                              // Navigator.push(
                                              //     context,
                                              //     MaterialPageRoute(
                                              //         builder: (context) =>
                                              //             GroupDetailsPage(
                                              //                 result)));
                                            } else if (first == "post") {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          StatusDetailsPage(
                                                              index,
                                                              userData,
                                                              result)));
                                            } else if (first == "profile") {
                                              if (userData['userName'] ==
                                                  result) {
                                                // Navigator.push(
                                                //     context,
                                                //     MaterialPageRoute(
                                                //         builder: (context) =>
                                                //             MyProfilePage(
                                                //                 userData)));
                                              } else {
                                                // Navigator.push(
                                                //     context,
                                                //     MaterialPageRoute(
                                                //         builder: (context) =>
                                                //             FriendsProfilePage(
                                                //                 result, 2)));
                                              }
                                            } else {
                                              pageRoute(result);
                                            }
                                          }
                                        } else {
                                          url = url.replaceAll("/", "");

                                          print(url);

                                          // Navigator.push(
                                          //     context,
                                          //     MaterialPageRoute(
                                          //         builder: (context) =>
                                          //             AllRequestPage()));
                                        }

                                        if (notifyNum != 0) {
                                          notifyNum--;
                                          if (notifyNum < 0) {
                                            notifyNum = 0;
                                          }
                                        }

                                        print(notifyNum);

                                        notificationList[index].seen = "1";

                                        makeSeen(notificationList[index].id);
                                      });
                                    },
                                    child: Container(
                                      padding:
                                          EdgeInsets.only(top: 0, bottom: 0),
                                      decoration: BoxDecoration(
                                        color:
                                            notificationList[index].seen == "0"
                                                ? Colors.grey.withOpacity(0.2)
                                                : Colors.white,
                                        borderRadius: BorderRadius.circular(15),
                                        boxShadow: [
                                          BoxShadow(
                                            blurRadius: 1.0,
                                            color: notificationList[index]
                                                        .seen ==
                                                    "0"
                                                ? Colors.grey.withOpacity(0.1)
                                                : Colors.black38
                                                    .withOpacity(0.3),
                                          ),
                                        ],
                                      ),
                                      margin: EdgeInsets.only(
                                          top: 2.5,
                                          bottom: 2.5,
                                          left: 10,
                                          right: 10),
                                      child: Container(
                                        margin: EdgeInsets.all(15),
                                        child: Row(
                                          children: <Widget>[
                                            Stack(
                                              children: <Widget>[
                                                ////// <<<<< React Icon along with picture >>>>> //////
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 0, top: 0),
                                                  padding: EdgeInsets.all(4.0),
                                                  decoration: new BoxDecoration(
                                                      color: Colors.white,
                                                      shape: BoxShape.circle,
                                                      border: Border.all(
                                                          width: 0.4,
                                                          color: mainColor)),
                                                  child: Icon(
                                                    Icons.notifications,
                                                    size: 15,
                                                    color: mainColor,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Expanded(
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text.rich(
                                                      TextSpan(
                                                        children: <TextSpan>[
                                                          ////// <<<<< Reacted for what >>>>> //////
                                                          TextSpan(
                                                              text:
                                                                  "${notificationList[index].notification_text}",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 15,
                                                                fontFamily:
                                                                    'Oswald',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                              )),
                                                        ],
                                                      ),
                                                    ),

                                                    ////// <<<<< Time >>>>> //////
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 5, left: 0),
                                                      child: Text(
                                                        "${day[index]}",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black45,
                                                            fontFamily:
                                                                'Oswald',
                                                            fontWeight:
                                                                FontWeight.w300,
                                                            fontSize: 12),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),

                                            ////// <<<<< More Icon >>>>> //////
                                            GestureDetector(
                                              onTap: () {
                                                _showDeleteDialog(
                                                    notificationList[index].id,
                                                    index);
                                              },
                                              child: Container(
                                                  padding: EdgeInsets.all(5),
                                                  margin: EdgeInsets.only(
                                                      left: 7, right: 0),
                                                  child: Icon(
                                                    Icons.delete,
                                                    color: Colors.black38,
                                                    size: 17,
                                                  )),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                : NotifyLoaderCard();
                          }),
                    )
          //NotificationCard(notifyList.allNotification, loading),
          ),
    );
  }
}
