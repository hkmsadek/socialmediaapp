import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/ContactInfoPage/ContactInfoPage.dart';
import 'package:social_app_fb/MainScreens/OverviewPage/OverviewPage.dart';
import 'package:social_app_fb/MainScreens/PlacePage/PlacePage.dart';
import 'package:social_app_fb/MainScreens/WorkEducationPage/WorkEducationPage.dart';
import 'package:social_app_fb/main.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');

    if (token != null) {
      getInitData(token);
    }
  }

  Future<void> getInitData(token) async {
    var res = await CallApi().getData2('initData');
    var body = json.decode(res.body);

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('user', json.encode(body['user']));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "About",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: DefaultTabController(
          length: 4,
          child: Scaffold(
            appBar: new AppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              //actions: <Widget>[],
              title: new TabBar(
                isScrollable: true,
                labelColor: mainColor,
                labelStyle:
                    TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                tabs: [
                  new Tab(text: "Overview"),
                  new Tab(text: "Contact and Basic Info"),
                  new Tab(text: "Work and Education"),
                  new Tab(text: "Places You've Lived")
                ],
                indicatorColor: mainColor,
                unselectedLabelColor: Colors.grey,
              ),
            ),
            body: TabBarView(children: <Widget>[
              SingleChildScrollView(
                  physics: BouncingScrollPhysics(), child: OverViewPage()),
              SingleChildScrollView(
                  physics: BouncingScrollPhysics(), child: ContactInfoPage()),
              WorkEducationPage(),
              PlacePage(),
            ]),
          )),
    );
  }
}
