import 'package:flutter/material.dart';
import 'package:social_app_fb/MainScreens/DiscoverGroup/DiscoverGroup.dart';
import 'package:social_app_fb/MainScreens/GroupDetailsPage/groupDetailsPage.dart';
import 'package:social_app_fb/MainScreens/MyGroups/MyGroups.dart';

import '../../main.dart';

class GroupPage extends StatefulWidget {
  @override
  _GroupPageState createState() => _GroupPageState();
}

class _GroupPageState extends State<GroupPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: new AppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              //actions: <Widget>[],
              title: new TabBar(
                isScrollable: false,
                labelColor: mainColor,
                labelStyle:
                    TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                tabs: [
                  new Tab(text: "My Groups"),
                  new Tab(text: "Discover"),
                ],
                indicatorColor: mainColor,
                unselectedLabelColor: Colors.grey,
              ),
            ),
            body: Container(
              color: Colors.white,
              child: TabBarView(children: <Widget>[
                SingleChildScrollView(
                    physics: BouncingScrollPhysics(), child: MyGroups()),
                SingleChildScrollView(
                    physics: BouncingScrollPhysics(), child: DiscoverGroup()),
              ]),
            ),
          )),
    );
  }
}
