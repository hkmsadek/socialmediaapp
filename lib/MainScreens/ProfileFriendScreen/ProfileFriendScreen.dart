import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
// import 'package:image_cropper/image_cropper.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Cards/MyProfileFeedCard/MyProfileFeedCard.dart';
import 'package:social_app_fb/MainScreens/AboutPage/AboutPage.dart';
import 'package:social_app_fb/MainScreens/CreatePostPage/CreatePostPage.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/FriendsPage/FriendsPage.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/MainScreens/PhotosPage/PhotoPage.dart';
import 'package:social_app_fb/ModelClass/FeedModel/FeedModel.dart';
import 'package:social_app_fb/ModelClass/InfoModel/InfoModel.dart';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:social_app_fb/main.dart';

List profileComCount = [];

class ProfileFriendScreen extends StatefulWidget {
  final userName;
  ProfileFriendScreen(this.userName);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

enum PhotoCrop {
  free,
  picked,
  cropped,
}

class _ProfilePageState extends State<ProfileFriendScreen> {
  SharedPreferences sharedPreferences;
  String theme = "";
  Timer _timer;
  int _start = 3;
  bool loading = true;
  PhotoCrop state;
  File bannerFile;
  File imageFile;
  var userData, basicInfo;
  bool timeline = true;
  bool about = false;
  bool friends = false;
  bool photos = false;
  var postData, postList;
  bool _isLoading = true;
  int lastID = 1;
  List feedList = [];
  ScrollController _controller = new ScrollController();

  @override
  void initState() {
    setState(() {
      profileComCount.clear();
      fwork.clear();
      feducation.clear();
      fschool.clear();
      fskill.clear();
      flocationCur.clear();
      flocationHome.clear();
      flocationOther.clear();
    });

    _getUserInfo();

    loadPosts(1);

    state = PhotoCrop.free;
    // WidgetsBinding.instance.addPostFrameCallback((_) async {
    //   await showDialogBox();
    // });
    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels == 0) {
          setState(() {
            print("top");
          });
        }
        // you are at top position

        else {
          setState(() {
            print("bottom");
            loadPosts(lastID);

            print("lastID 1");
            print(lastID);
          });
        }
        // you are at bottom position
      }
    });
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
      getBasicInfo(userData['userName']);
    }

    loadPosts(1);
  }

  Future loadPosts(int number) async {
    var postresponse =
        await CallApi().getData('feed/get/allFeed?status_id=$number');

    setState(() {
      var postcontent = postresponse.body;
      final posts = json.decode(postcontent);
      var postdata = FeedModel.fromJson(posts);
      postList = postdata;

      for (int i = 0; i < postList.feed.length; i++) {
        //print(postList.feed[i].id);

        profileFeedList.add(postList.feed[i]);
        print(profileFeedList[i].meta.totalCommentsCount);
        postComCount.add({'count': postList.feed[i].meta.totalCommentsCount});
        lastID = postList.feed[i].id;
        print(lastID);
      }

      print(feedList);
      print(postComCount);
    });
    getBasicInfo(widget.userName);

    setState(() {
      _isLoading = false;
    });

    //print(page1);
  }

  Future getBasicInfo(userName) async {
    //await Future.delayed(Duration(seconds: 3));
    var postresponse = await CallApi().getData2('profile/' + userName);
    var postcontent = postresponse.body;
    final body = json.decode(postcontent);
    var postdata = InfoModel.fromJson(body);

    setState(() {
      basicInfo = postdata;
    });

    if (basicInfo.user.abouts != null) {
      for (int i = 0; i < basicInfo.user.abouts.workData.work.length; i++) {
        setState(() {
          fwork.add({
            'company': basicInfo.user.abouts.workData.work[i].company,
            'position': basicInfo.user.abouts.workData.work[i].position,
            'city': basicInfo.user.abouts.workData.work[i].city,
            'joinDate': basicInfo.user.abouts.workData.work[i].joinDate,
            'leaveDate': basicInfo.user.abouts.workData.work[i].leaveDate,
            'currentlyWorking':
                basicInfo.user.abouts.workData.work[i].currentlyWorking,
            'description': basicInfo.user.abouts.workData.work[i].description,
            'privacy': basicInfo.user.abouts.workData.work[i].privacy,
            'isEdit': basicInfo.user.abouts.workData.work[i].isEdit,
          });
        });
      }

      for (int i = 0;
          i < basicInfo.user.abouts.educationData.collages.length;
          i++) {
        setState(() {
          feducation.add({
            'collage': basicInfo.user.abouts.educationData.collages[i].collage,
            'degree': basicInfo.user.abouts.educationData.collages[i].degree,
            'city': basicInfo.user.abouts.educationData.collages[i].city,
            'startDate':
                basicInfo.user.abouts.educationData.collages[i].startDate,
            'endDate': basicInfo.user.abouts.educationData.collages[i].endDate,
            'description':
                basicInfo.user.abouts.educationData.collages[i].description,
            'privacy': basicInfo.user.abouts.educationData.collages[i].privacy,
          });
        });
      }

      for (int i = 0;
          i < basicInfo.user.abouts.educationData.highSchools.length;
          i++) {
        setState(() {
          fschool.add({
            'collage':
                basicInfo.user.abouts.educationData.highSchools[i].collage,
            'city': basicInfo.user.abouts.educationData.highSchools[i].city,
            'startDate':
                basicInfo.user.abouts.educationData.highSchools[i].startDate,
            'endDate':
                basicInfo.user.abouts.educationData.highSchools[i].endDate,
            'description':
                basicInfo.user.abouts.educationData.highSchools[i].description,
            'privacy':
                basicInfo.user.abouts.educationData.highSchools[i].privacy,
          });
        });
      }

      flocationCur.add({
        'city': basicInfo.user.abouts.locationData.currentCity.city,
        'movedDate': basicInfo.user.abouts.locationData.currentCity.movedDate,
        'description':
            basicInfo.user.abouts.locationData.currentCity.description,
        'isEdit': basicInfo.user.abouts.locationData.currentCity.isEdit,
      });

      flocationHome.add({
        'homeTown': basicInfo.user.abouts.locationData.homeTown.homeTown,
        'description': basicInfo.user.abouts.locationData.homeTown.description,
        'isEdit': basicInfo.user.abouts.locationData.homeTown.isEdit,
      });

      for (int i = 0;
          i < basicInfo.user.abouts.locationData.otherPlaces.length;
          i++) {
        setState(() {
          flocationOther.add({
            'city': basicInfo.user.abouts.locationData.otherPlaces[i].city,
            'movedDate':
                basicInfo.user.abouts.locationData.otherPlaces[i].movedDate,
            'leftDate':
                basicInfo.user.abouts.locationData.otherPlaces[i].leftDate,
            'description':
                basicInfo.user.abouts.locationData.otherPlaces[i].description,
            'isEdit': basicInfo.user.abouts.locationData.otherPlaces[i].isEdit,
          });
        });
      }

      for (int i = 0;
          i < basicInfo.user.abouts.workData.proSkills.length;
          i++) {
        setState(() {
          fskill.add(basicInfo.user.abouts.workData.proSkills[i]);
        });
      }

      print("fskill");
      print(fskill);
    }
  }

  Future<Null> showDialogBox() async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            content: SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  Container(
                    color: Colors.transparent,
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(20),
                        margin: EdgeInsets.only(top: 35),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.only(top: 30),
                                child: Text(
                                  "Complete your profile info",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontFamily: 'Oswald',
                                      fontWeight: FontWeight.w400),
                                )),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: new LinearPercentIndicator(
                                animation: true,
                                animationDuration: 1000,
                                lineHeight: 15.0,
                                // leading: new Text("left content"),
                                trailing: Text(
                                  " 45.0%",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 10,
                                      fontFamily: 'Oswald',
                                      fontWeight: FontWeight.w400),
                                ),
                                backgroundColor: back,
                                percent: 0.45,
                                // center: Text("20.0%",style: TextStyle(
                                //     color: Colors.black,
                                //     fontSize: 10,
                                //     fontFamily: 'Oswald',
                                //     fontWeight: FontWeight.w400),),
                                //linearStrokeCap: LinearStrokeCap.butt,
                                progressColor: mainColor.withOpacity(0.7),
                              ),
                            ),
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          Navigator.of(context).pop();
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      AboutPage()));
                                        });
                                      },
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.only(
                                              left: 0,
                                              right: 0,
                                              top: 20,
                                              bottom: 0),
                                          decoration: BoxDecoration(
                                              color: mainColor,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Text(
                                            "Go to 'About' to complete the profile",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12,
                                              fontFamily: 'BebasNeue',
                                            ),
                                            textAlign: TextAlign.center,
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                    left: 0, top: 15, bottom: 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    FeedPage()));
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              "Skip to 'Feed'",
                                              style: TextStyle(
                                                  //decoration: TextDecoration.underline,
                                                  color: mainColor,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                            Icon(Icons.chevron_right,
                                                color: mainColor, size: 16)
                                          ],
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Container(
                                        child: Text(
                                          "Cancel",
                                          style: TextStyle(
                                              //decoration: TextDecoration.underline,
                                              color: Colors.black54,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.center,
                    //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                    padding: EdgeInsets.all(5.0),
                    child: CircleAvatar(
                      radius: 30.0,
                      backgroundColor: Colors.transparent,
                      backgroundImage: AssetImage('assets/images/prabal.jpg'),
                    ),
                    decoration: new BoxDecoration(
                      color: Colors.white, // border color
                      shape: BoxShape.circle,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future sendReq(req) async {
    var data = {
      'user_id': userData['id'],
      'request_receiver_id': req.id,
    };

    print(data);

    var res = await CallApi().postData1(data, 'friend/send/request');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
      loadPosts(1);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  Future acceptReq(req) async {
    var data = {
      'request_sender_id': req.request_sender_id,
    };

    print(data);

    var res =
        await CallApi().postData1(data, 'friend/request/accept/${req.id}');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
      loadPosts(1);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  Future deleteReq(req) async {
    var data = {
      'request_sender_id': req.request_sender_id,
    };

    print(data);

    var res =
        await CallApi().postData1(data, 'friend/request/cancel/${req.id}');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
      loadPosts(1);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  Future cancelReq(req) async {
    var data = {
      'request_sender_id': req.request_sender_id,
    };

    print(data);

    var res =
        await CallApi().postData1(data, 'friend/request/cancel/${req.id}');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
      loadPosts(1);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  Future unfriend(req) async {
    var data = {
      'request_sender_id': req.request_sender_id,
    };

    print(data);

    var res =
        await CallApi().postData1(data, 'friend/request/unfriend/${req.id}');
    var body = json.decode(res.body);

    print(body);

    if (res.statusCode == 200) {
      loadPosts(1);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black54),
          //automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          title: Container(
            margin: EdgeInsets.only(top: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 5, right: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      //color: Colors.black.withOpacity(0.5),
                    ),
                    child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Profile",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 20,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.normal),
                        )),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[],
        ),
        body: _isLoading
            ? LoaderScreen()
            : Stack(
                children: <Widget>[
                  Container(
                    child: CustomScrollView(
                      physics: BouncingScrollPhysics(),
                      slivers: <Widget>[
                        SliverToBoxAdapter(
                            child: SingleChildScrollView(
                          child: Container(
                              child: Column(
                            children: <Widget>[
                              SafeArea(
                                child: Stack(
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        bannerFile != null
                                            ? Container(
                                                height: 150,
                                                padding:
                                                    const EdgeInsets.all(0.0),
                                                decoration: BoxDecoration(
                                                    //color: header,

                                                    image: DecorationImage(
                                                        image: FileImage(
                                                            bannerFile),
                                                        fit: BoxFit.cover)),
                                                child: null)
                                            : Container(
                                                height: 150,
                                                padding:
                                                    const EdgeInsets.all(0.0),
                                                decoration: BoxDecoration(
                                                    //color: header,

                                                    image: DecorationImage(
                                                        image: AssetImage(
                                                            'assets/images/cover.jpg'),
                                                        fit: BoxFit.cover)),
                                                child: null),
                                        Container(
                                            height: 150,
                                            padding: const EdgeInsets.all(0.0),
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.black.withOpacity(0.2),
                                            ),
                                            child: null),
                                        Stack(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              margin: EdgeInsets.only(
                                                  right: 0, top: 110),
                                              alignment: Alignment.bottomCenter,
                                              child: Container(
                                                height: 50,
                                                padding:
                                                    const EdgeInsets.all(0.0),
                                                decoration: BoxDecoration(
                                                  color: Colors.black
                                                      .withOpacity(0.5),
                                                ),
                                              ),
                                            ),
                                            Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                padding: EdgeInsets.all(5),
                                                margin: EdgeInsets.only(
                                                    left: 125, top: 115),
                                                child: Text(
                                                  "${basicInfo.user.firstName} ${basicInfo.user.lastName}",
                                                  textAlign: TextAlign.start,
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: "Oswald",
                                                      color: Colors.white),
                                                )),
                                          ],
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                top: 155, left: 125),
                                            padding: EdgeInsets.all(5),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                GestureDetector(
                                                  onTap: () {
                                                    setState(() {
                                                      if (basicInfo.friend ==
                                                          null) {
                                                        sendReq(basicInfo.user);
                                                      } else if (basicInfo
                                                              .friend
                                                              .isFriend ==
                                                          "No") {
                                                        if (basicInfo.friend
                                                                .request_sender_id ==
                                                            userData['id']) {
                                                          cancelReq(
                                                              basicInfo.friend);
                                                        } else {
                                                          if (basicInfo.friend
                                                                  .request_receiver_id ==
                                                              userData['id']) {
                                                            acceptReq(basicInfo
                                                                .friend);
                                                          } else {
                                                            unfriend(basicInfo
                                                                .friend);
                                                          }
                                                        }
                                                      } else {
                                                        unfriend(
                                                            basicInfo.friend);
                                                      }
                                                    });
                                                  },
                                                  child: Container(
                                                    padding: EdgeInsets.all(5),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Icon(
                                                            basicInfo.friend ==
                                                                    null
                                                                ? Icons.add
                                                                : basicInfo.friend
                                                                            .isFriend ==
                                                                        "No"
                                                                    ? basicInfo.friend.request_sender_id ==
                                                                            userData[
                                                                                'id']
                                                                        ? Icons
                                                                            .close
                                                                        : basicInfo.friend.request_receiver_id == userData['id']
                                                                            ? Icons
                                                                                .done
                                                                            : Icons
                                                                                .person
                                                                    : Icons
                                                                        .person,
                                                            color: mainColor,
                                                            size: 15),
                                                        SizedBox(width: 5),
                                                        Text(
                                                          basicInfo.friend ==
                                                                  null
                                                              ? "Add as friend"
                                                              : basicInfo.friend
                                                                          .isFriend ==
                                                                      "No"
                                                                  ? basicInfo.friend
                                                                              .request_sender_id ==
                                                                          userData[
                                                                              'id']
                                                                      ? "Cancel Request"
                                                                      : basicInfo.friend.request_receiver_id ==
                                                                              userData['id']
                                                                          ? "Accept Request"
                                                                          : "Unfriend"
                                                                  : "Unfriend",
                                                          style: TextStyle(
                                                              color: mainColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 12,
                                                              fontFamily:
                                                                  "Oswald"),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                basicInfo.friend == null ||
                                                        (basicInfo.friend
                                                                    .request_sender_id ==
                                                                userData[
                                                                    'id'] &&
                                                            basicInfo.friend
                                                                    .isFriend ==
                                                                "Yes") ||
                                                        (basicInfo.friend
                                                                    .request_sender_id ==
                                                                userData[
                                                                    'id'] &&
                                                            basicInfo.friend
                                                                    .isFriend ==
                                                                "No") ||
                                                        (basicInfo.friend
                                                                    .request_receiver_id ==
                                                                userData[
                                                                    'id'] &&
                                                            basicInfo.friend
                                                                    .isFriend ==
                                                                "Yes")
                                                    ? Container()
                                                    : GestureDetector(
                                                        onTap: () {
                                                          deleteReq(
                                                              basicInfo.friend);
                                                        },
                                                        child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 10),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Icon(Icons.delete,
                                                                  color: Colors
                                                                      .red,
                                                                  size: 15),
                                                              SizedBox(
                                                                  width: 5),
                                                              Text(
                                                                "Delete Request",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .red,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontSize:
                                                                        12,
                                                                    fontFamily:
                                                                        "Oswald"),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                              ],
                                            )),
                                      ],
                                    ),
                                    Container(
                                      child: Stack(
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 20, top: 100),
                                            //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                            padding: EdgeInsets.all(5.0),
                                            child: CircleAvatar(
                                              radius: 45.0,
                                              backgroundColor:
                                                  Colors.transparent,
                                              backgroundImage: AssetImage(
                                                  'assets/images/prabal.jpg'),
                                            ),
                                            decoration: new BoxDecoration(
                                              color: Colors
                                                  .grey[300], // border color
                                              shape: BoxShape.circle,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          showDialogBox();
                                          // setState(() {
                                          //   timeline = true;
                                          //   about = false;
                                          //   friends = false;
                                          //   photos = false;
                                          // });
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 15, top: 15, right: 5),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              color: timeline
                                                  ? mainColor.withOpacity(0.8)
                                                  : back.withOpacity(0.3),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: timeline
                                                      ? mainColor
                                                      : Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.show_chart,
                                                color: timeline
                                                    ? Colors.white
                                                    : Colors.black,
                                                size: 15,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  "Timeline",
                                                  style: TextStyle(
                                                      color: timeline
                                                          ? Colors.white
                                                          : Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          // setState(() {
                                          //   timeline = false;
                                          //   about = true;
                                          //   friends = false;
                                          //   photos = false;
                                          // });
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      AboutPage()));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5, top: 15),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              color: about
                                                  ? mainColor.withOpacity(0.8)
                                                  : back.withOpacity(0.3),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: about
                                                      ? mainColor
                                                      : Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.info_outline,
                                                color: about
                                                    ? Colors.white
                                                    : Colors.black,
                                                size: 15,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  "About",
                                                  style: TextStyle(
                                                      color: about
                                                          ? Colors.white
                                                          : Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      FriendsPage(basicInfo
                                                          .user.userName)));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 15, top: 15),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              color: back.withOpacity(0.3),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.group,
                                                color: Colors.black,
                                                size: 17,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  "Friends",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      PhotoPage()));
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 15, top: 10, right: 5),
                                          padding: EdgeInsets.all(5.0),
                                          decoration: new BoxDecoration(
                                              color: back.withOpacity(0.3),
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.photo,
                                                color: Colors.black,
                                                size: 15,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  "Photos",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 5, right: 5, top: 10),
                                        padding: EdgeInsets.all(5.0),
                                        decoration: new BoxDecoration(
                                            color: back.withOpacity(0.3),
                                            border: Border.all(
                                                width: 0.5,
                                                color: Colors.white),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(
                                              Icons.event_available,
                                              color: Colors.black,
                                              size: 15,
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 5),
                                              child: Text(
                                                "Events",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: "Oswald",
                                                    fontWeight: FontWeight.w300,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 5, right: 15, top: 10),
                                        padding: EdgeInsets.all(5.0),
                                        decoration: new BoxDecoration(
                                            color: back.withOpacity(0.3),
                                            border: Border.all(
                                                width: 0.5,
                                                color: Colors.white),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(
                                              Icons.more_horiz,
                                              color: Colors.black,
                                              size: 17,
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 5),
                                              child: Text(
                                                "More",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: "Oswald",
                                                    fontWeight: FontWeight.w300,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              // Container(
                              //     width: 50,
                              //     margin: EdgeInsets.only(
                              //         top: 25, left: 25, right: 25, bottom: 0),
                              //     decoration: BoxDecoration(
                              //         borderRadius:
                              //             BorderRadius.all(Radius.circular(15.0)),
                              //         color: mainColor,
                              //         boxShadow: [
                              //           BoxShadow(
                              //             blurRadius: 1.0,
                              //             color: mainColor,
                              //             //offset: Offset(6.0, 7.0),
                              //           ),
                              //         ],
                              //         border:
                              //             Border.all(width: 0.5, color: mainColor))),

                              fwork.length == 0
                                  ? Container()
                                  : Container(
                                      margin: EdgeInsets.only(top: 20),
                                      child: Column(
                                          children: List.generate(fwork.length,
                                              (index) {
                                        return Container(
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(Icons.work,
                                                      size: 17,
                                                      color: Colors.black54)),
                                              Expanded(
                                                child: Text.rich(
                                                  TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text: fwork[
                                                                          index]
                                                                      [
                                                                      'currentlyWorking'] ==
                                                                  false
                                                              ? "Former ${fwork[index]['position']} at"
                                                              : "${fwork[index]['position']} at",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              " ${fwork[index]['company']}",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                      // can add more TextSpans here...
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })),
                                    ),

                              feducation.length == 0
                                  ? Container()
                                  : Container(
                                      child: Column(
                                          children: List.generate(
                                              feducation.length, (index) {
                                        return Container(
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(Icons.book,
                                                      size: 17,
                                                      color: Colors.black54)),
                                              Expanded(
                                                child: Text.rich(
                                                  TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text: "Studied ",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              "${feducation[index]['degree']} at",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              " ${feducation[index]['collage']}",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                      // can add more TextSpans here...
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })),
                                    ),
                              fschool.length == 0
                                  ? Container()
                                  : Container(
                                      child: Column(
                                          children: List.generate(
                                              fschool.length, (index) {
                                        return Container(
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(Icons.book,
                                                      size: 17,
                                                      color: Colors.black54)),
                                              Expanded(
                                                child: Text.rich(
                                                  TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text: "Went to",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              " ${fschool[index]['collage']}",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                      // can add more TextSpans here...
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })),
                                    ),
                              flocationCur == null
                                  ? Container()
                                  : Container(
                                      child: Column(
                                          children: List.generate(
                                              flocationCur.length, (index) {
                                        return Container(
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  child: Icon(Icons.location_on,
                                                      size: 17,
                                                      color: Colors.black54)),
                                              Expanded(
                                                child: Text.rich(
                                                  TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text: "Lives in ",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300)),
                                                      TextSpan(
                                                          text:
                                                              "${flocationCur[index]['city']}",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                      // can add more TextSpans here...
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })),
                                    ),

                              Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                //color: sub_white,
                                child: Container(
                                  //color: Colors.white,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                          width: 50,
                                          margin: EdgeInsets.only(
                                              top: 20,
                                              left: 25,
                                              right: 25,
                                              bottom: 10),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(15.0)),
                                              color: mainColor,
                                              boxShadow: [
                                                BoxShadow(
                                                  blurRadius: 1.0,
                                                  color: mainColor,
                                                  //offset: Offset(6.0, 7.0),
                                                ),
                                              ],
                                              border: Border.all(
                                                  width: 0.5,
                                                  color: mainColor))),
                                      Column(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.centerLeft,
                                              margin: EdgeInsets.only(
                                                  top: 15, left: 20),
                                              child: Text(
                                                "Mutual Friends",
                                                style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: 20,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.normal),
                                              )),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                width: 30,
                                                margin: EdgeInsets.only(
                                                    top: 10, left: 20),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                15.0)),
                                                    color: Colors.black54,
                                                    boxShadow: [
                                                      BoxShadow(
                                                        blurRadius: 3.0,
                                                        color: Colors.black54,
                                                        //offset: Offset(6.0, 7.0),
                                                      ),
                                                    ],
                                                    border: Border.all(
                                                        width: 0.5,
                                                        color: Colors.black54)),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  margin: EdgeInsets.only(
                                                      top: 12,
                                                      left: 20,
                                                      bottom: 0),
                                                  child: Text(
                                                    "3 friends",
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        color: Colors.black45,
                                                        fontSize: 13,
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  )),
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              FriendsPage(basicInfo.user.userName)));
                                                },
                                                child: Container(
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    margin: EdgeInsets.only(
                                                        top: 12,
                                                        right: 20,
                                                        bottom: 0),
                                                    child: Row(
                                                      children: <Widget>[
                                                        Text(
                                                          "See all",
                                                          textAlign:
                                                              TextAlign.start,
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black45,
                                                              fontSize: 13,
                                                              fontFamily:
                                                                  'Oswald',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 3),
                                                          child: Icon(
                                                              Icons
                                                                  .chevron_right,
                                                              color: Colors
                                                                  .black45,
                                                              size: 17),
                                                        )
                                                      ],
                                                    )),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                                top: 10, left: 10),
                                            child: Wrap(
                                              //mainAxisAlignment: MainAxisAlignment.start,
                                              children: List.generate(
                                                  loading ? 1 : 3, (int index) {
                                                return GestureDetector(
                                                  onTap: () {
                                                    // Navigator.push(
                                                    //   context,
                                                    //   MaterialPageRoute(
                                                    //       builder: (context) => HotelSearchPage()),
                                                    // );
                                                  },
                                                  child: Container(
                                                    child: Column(
                                                      children: <Widget>[
                                                        new Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10,
                                                                  right: 10,
                                                                  top: 5,
                                                                  bottom: 10),
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 0),
                                                          height: 50,
                                                          width: 50,
                                                          decoration:
                                                              BoxDecoration(
                                                            image:
                                                                DecorationImage(
                                                              image: index == 0
                                                                  ? AssetImage(
                                                                      'assets/images/man.png')
                                                                  : index == 1
                                                                      ? AssetImage(
                                                                          'assets/images/man4.jpg')
                                                                      : AssetImage(
                                                                          'assets/images/man2.png'),
                                                              fit: BoxFit.cover,
                                                            ),
                                                            borderRadius:
                                                                BorderRadius.all(
                                                                    Radius.circular(
                                                                        100.0)),
                                                            color: Colors.white,
                                                            boxShadow: [
                                                              BoxShadow(
                                                                blurRadius: 2.0,
                                                                color: Colors
                                                                    .black
                                                                    .withOpacity(
                                                                        .5),
                                                                //offset: Offset(6.0, 7.0),
                                                              ),
                                                            ],
                                                            // border: Border.all(width: 0.2, color: Colors.grey)
                                                          ),
                                                        ),
                                                        Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 0,
                                                                    left: 0,
                                                                    bottom: 5),
                                                            child: Text(
                                                              index == 0
                                                                  ? "John Louis"
                                                                  : index == 1
                                                                      ? "David King"
                                                                      : "Daniel Ryan",
                                                              maxLines: 2,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize: 10,
                                                                  fontFamily:
                                                                      'Oswald',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400),
                                                            )),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              }),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )),
                        )),
                        SliverToBoxAdapter(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  width: 50,
                                  margin: EdgeInsets.only(
                                      top: 10, left: 25, right: 25, bottom: 20),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      color: mainColor,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 1.0,
                                          color: mainColor,
                                          //offset: Offset(6.0, 7.0),
                                        ),
                                      ],
                                      border: Border.all(
                                          width: 0.5, color: mainColor))),
                            ],
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: Column(
                            children: <Widget>[
                              Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 0, left: 20),
                                  child: Text(
                                    "Post",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.normal),
                                  )),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: 30,
                                    margin: EdgeInsets.only(
                                        top: 10, left: 20, bottom: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(15.0)),
                                        color: Colors.black54,
                                        boxShadow: [
                                          BoxShadow(
                                            blurRadius: 3.0,
                                            color: Colors.black54,
                                            //offset: Offset(6.0, 7.0),
                                          ),
                                        ],
                                        border: Border.all(
                                            width: 0.5, color: Colors.black54)),
                                  ),
                                ],
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              CreatePostPage(userData, 2)));
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      top: 0, left: 5, right: 5),
                                  padding: EdgeInsets.only(top: 0, bottom: 0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    // boxShadow: [
                                    //   BoxShadow(
                                    //     blurRadius: 0.2,
                                    //     color: Colors.black38,
                                    //     //offset: Offset(6.0, 7.0),
                                    //   ),
                                    // ],
                                  ),
                                  child: Container(
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          child: Row(
                                            children: <Widget>[
                                              ////// <<<<< Profile >>>>> //////
                                              GestureDetector(
                                                onTap: () {
                                                  // Navigator.push(
                                                  //     context,
                                                  //     MaterialPageRoute(
                                                  //         builder: (context) =>
                                                  //             //FriendsProfilePage("prabal23")),
                                                  //             MyProfilePage(userData)));
                                                },
                                                child: Container(
                                                  child: Stack(
                                                    children: <Widget>[
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            right: 0, left: 15),
                                                        padding:
                                                            EdgeInsets.all(1.0),
                                                        child: CircleAvatar(
                                                          radius: 21.0,
                                                          backgroundColor:
                                                              Colors
                                                                  .transparent,
                                                          backgroundImage:
                                                              AssetImage(
                                                                  'assets/images/prabal.jpg'),
                                                        ),
                                                        // decoration: new BoxDecoration(
                                                        //   color: Colors.grey[300],
                                                        //   shape: BoxShape.circle,
                                                        // ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 45),
                                                        padding:
                                                            EdgeInsets.all(1.0),
                                                        child: CircleAvatar(
                                                          radius: 5.0,
                                                          backgroundColor:
                                                              Colors
                                                                  .greenAccent,
                                                        ),
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: Colors
                                                              .greenAccent,
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),

                                              ////// <<<<< Status/Photo field >>>>> //////
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 10, top: 4),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    //mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      Container(
                                                          width: MediaQuery.of(
                                                                  context)
                                                              .size
                                                              .width,
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 5,
                                                                  right: 5),
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 0,
                                                                  right: 10,
                                                                  top: 5),
                                                          // decoration: BoxDecoration(
                                                          //     color: Colors.grey[100],
                                                          //     border: Border.all(
                                                          //         color: Colors.grey[200],
                                                          //         width: 0.5),
                                                          //     borderRadius: BorderRadius.all(
                                                          //         Radius.circular(25))),
                                                          child: TextField(
                                                            enabled: false,
                                                            //controller: phoneController,
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  'Oswald',
                                                            ),
                                                            decoration:
                                                                InputDecoration(
                                                              hintText:
                                                                  "Post on ${basicInfo.user.firstName}'s timeline",
                                                              hintStyle: TextStyle(
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize: 15,
                                                                  fontFamily:
                                                                      'Oswald',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              //labelStyle: TextStyle(color: Colors.white70),
                                                              contentPadding:
                                                                  EdgeInsets
                                                                      .fromLTRB(
                                                                          10.0,
                                                                          1,
                                                                          20.0,
                                                                          1),
                                                              border:
                                                                  InputBorder
                                                                      .none,
                                                            ),
                                                          )),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(
                                                left: 15,
                                                right: 15,
                                                top: 0,
                                                bottom: 0),
                                            child: Divider()),
                                        SingleChildScrollView(
                                          physics: BouncingScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 15),
                                            decoration: BoxDecoration(
                                                //border:Border.all(color:Colors.grey, width:0.3)
                                                ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 15,
                                                      right: 5,
                                                      top: 5,
                                                      bottom: 0),
                                                  padding: EdgeInsets.all(5.0),
                                                  decoration: new BoxDecoration(
                                                      // color: Colors.grey
                                                      //     .withOpacity(0.1),
                                                      border: Border.all(
                                                          width: 0.5,
                                                          color: Colors.white),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.photo,
                                                        color: Colors.redAccent
                                                            .withOpacity(0.7),
                                                        size: 15,
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                          "Photo/Video",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 14),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                    height: 20,
                                                    child: VerticalDivider()),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 5,
                                                      right: 5,
                                                      top: 5,
                                                      bottom: 0),
                                                  padding: EdgeInsets.all(5.0),
                                                  decoration: new BoxDecoration(
                                                      // color: Colors.grey
                                                      //     .withOpacity(0.1),
                                                      border: Border.all(
                                                          width: 0.5,
                                                          color: Colors.white),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.mood,
                                                        color:
                                                            Color(0xFFECBF00),
                                                        size: 17,
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                          "Feeling/Activity",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 14),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                    height: 20,
                                                    child: VerticalDivider()),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 5,
                                                      right: 15,
                                                      top: 5,
                                                      bottom: 0),
                                                  padding: EdgeInsets.all(5.0),
                                                  decoration: new BoxDecoration(
                                                      // color: Colors.grey
                                                      //     .withOpacity(0.1),
                                                      border: Border.all(
                                                          width: 0.5,
                                                          color: Colors.white),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.more_horiz,
                                                        color: Colors.black,
                                                        size: 17,
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                          "More",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  "Oswald",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 14),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        // Container(
                                        //   child: Row(
                                        //     mainAxisAlignment: MainAxisAlignment.end,
                                        //     children: <Widget>[
                                        //       Container(
                                        //         margin: EdgeInsets.only(
                                        //             left: 15,
                                        //             top: 35,
                                        //             right: 15,
                                        //             bottom: 15),
                                        //         padding: EdgeInsets.all(5.0),
                                        //         decoration: new BoxDecoration(
                                        //             color: mainColor.withOpacity(0.8),
                                        //             border: Border.all(
                                        //                 width: 0.5, color: mainColor),
                                        //             borderRadius: BorderRadius.all(
                                        //                 Radius.circular(5))),
                                        //         child: Row(
                                        //           mainAxisAlignment:
                                        //               MainAxisAlignment.center,
                                        //           children: <Widget>[
                                        //             Container(
                                        //               margin: EdgeInsets.only(left: 5),
                                        //               child: Text(
                                        //                 "Create Post",
                                        //                 style: TextStyle(
                                        //                     color: Colors.white,
                                        //                     fontFamily: "Oswald",
                                        //                     fontWeight: FontWeight.w300,
                                        //                     fontSize: 14),
                                        //               ),
                                        //             ),
                                        //           ],
                                        //         ),
                                        //       ),
                                        //     ],
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                  width: 50,
                                  margin: EdgeInsets.only(
                                      top: 20, left: 25, right: 25, bottom: 0),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      color: mainColor,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 1.0,
                                          color: mainColor,
                                          //offset: Offset(6.0, 7.0),
                                        ),
                                      ],
                                      border: Border.all(
                                          width: 0.5, color: mainColor))),
                              Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 15, left: 20),
                                  child: Text(
                                    "Timeline",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.normal),
                                  )),
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: 30,
                                    margin: EdgeInsets.only(
                                        top: 10, left: 20, bottom: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(15.0)),
                                        color: Colors.black54,
                                        boxShadow: [
                                          BoxShadow(
                                            blurRadius: 3.0,
                                            color: Colors.black54,
                                            //offset: Offset(6.0, 7.0),
                                          ),
                                        ],
                                        border: Border.all(
                                            width: 0.5, color: Colors.black54)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                            return Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                child: MyProfileFeedCard(_isLoading, index,
                                    userData, profileFeedList[index]));
                          }, childCount: profileFeedList.length),
                        ),
                      ],
                    ),
                  ),
                ],
              ));
  }
}
