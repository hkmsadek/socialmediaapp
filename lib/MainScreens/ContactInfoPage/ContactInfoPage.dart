import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';

import '../../main.dart';

class ContactInfoPage extends StatefulWidget {
  @override
  _ContactInfoPageState createState() => _ContactInfoPageState();
}

class _ContactInfoPageState extends State<ContactInfoPage> {
  var userData;
  String birthDate = "N/A", mobile = "N/A", email = "N/A", birthDateServer = "";
  String fname = "", lname = "", nickName = "";
  DateTime selectedDate = DateTime.now();
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController fnameController = new TextEditingController();
  TextEditingController lnameController = new TextEditingController();
  TextEditingController nickNameController = new TextEditingController();
  bool isName = false;
  bool isBirthDay = false;
  bool isMobile = false;
  bool isEmail = false;
  DateTime selectedBirthDate = DateTime.now();

  @override
  void initState() {
    setState(() {
      birthDate = DateFormat.yMMMd().format(selectedBirthDate);
    });
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    print("userJson");
    print(userJson);
    if (userJson != null) {
      setState(() {
        var user = json.decode(userJson);
        userData = user;
        DateTime dateTime = DateTime.parse(userData['birthDay']);
        print(dateTime);
        birthDate = DateFormat.yMMMd().format(dateTime);
        birthDateServer = DateFormat('yyyy-MM-dd').format(dateTime);
        //birthDate = userData['birthDay'];
        email = userData['email'];
        emailController.text = userData['email'];
        fname = userData['firstName'];
        fnameController.text = userData['firstName'];
        lname = userData['lastName'];
        lnameController.text = userData['lastName'];
        nickName = userData['nickName'];
        if (nickName == null) {
          nickNameController.text = "";
        } else {
          nickNameController.text = nickName;
        }
        mobile = userData['phone'];
        phoneController.text = userData['phone'];
        print(userData);
      });
    }
  }

  Future<Null> _selectBirthDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedBirthDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedBirthDate)
      setState(() {
        selectedBirthDate = picked;
        birthDate = DateFormat.yMMMd().format(selectedBirthDate);
        birthDateServer = DateFormat('yyyy-MM-dd').format(selectedBirthDate);

        print("placbirthDateeleaveDate");
        print(birthDate);
      });
  }

  @override
  Widget build(BuildContext context) {
    return userData == null
        ? LoaderScreen()
        : Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      margin: EdgeInsets.only(
                          top: 2.5, bottom: 2.5, left: 0, right: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 20, right: 20, top: 0),
                              padding: EdgeInsets.only(right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< Profile picture >>>>> //////
                                  Stack(
                                    children: <Widget>[
                                      ////// <<<<< Picture >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(right: 10),
                                        padding: EdgeInsets.all(1.0),
                                        child: CircleAvatar(
                                          radius: 25.0,
                                          backgroundColor: Colors.white,
                                          backgroundImage: AssetImage(
                                              'assets/images/prabal.jpg'),
                                        ),
                                        decoration: new BoxDecoration(
                                          color: Colors.grey[300],
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                    ],
                                  ),

                                  ////// <<<<< User Name >>>>> //////
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          child: Text(
                                            fname + " " + lname,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black54,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              //isPlace = true;
                                            });
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                left: 0, right: 15, top: 5),
                                            child: Text.rich(
                                              TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                      text: "Nickname: ",
                                                      style: TextStyle(
                                                          color: Colors.black45,
                                                          fontSize: 14,
                                                          fontFamily: "Oswald",
                                                          fontWeight:
                                                              FontWeight.w400)),
                                                  TextSpan(
                                                      text: nickName == null
                                                          ? "N/A"
                                                          : nickName,
                                                      style: TextStyle(
                                                          color: mainColor,
                                                          fontSize: 14,
                                                          fontFamily: "Oswald",
                                                          fontWeight:
                                                              FontWeight.w400)),
                                                  // can add more TextSpans here...
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                if (isName == false) {
                                  isName = true;
                                } else {
                                  isName = false;
                                }
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.all(3),
                              // decoration: BoxDecoration(
                              //   borderRadius:BorderRadius.circular(5),
                              //   border:Border.all(color:mainColor, width:0.5)
                              // ),
                              margin:
                                  EdgeInsets.only(left: 0, right: 20, top: 7),
                              child: Icon(isName ? Icons.close : Icons.edit,
                                  size: 15, color: Colors.black54),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider()
                  ],
                ),
                !isName
                    ? Container()
                    : Container(
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 0, top: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  //mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Flexible(
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              padding: EdgeInsets.all(0),
                                              margin: EdgeInsets.only(
                                                  left: 20, right: 20, top: 5),
                                              decoration: BoxDecoration(
                                                  color: Colors.white
                                                      .withOpacity(0.7),
                                                  border: Border.all(
                                                      color: Colors.grey,
                                                      width: 0.2),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: TextField(
                                                controller: fnameController,
                                                keyboardType:
                                                    TextInputType.text,
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Oswald',
                                                ),
                                                decoration: InputDecoration(
                                                  hintText: "First Name",
                                                  hintStyle: TextStyle(
                                                      color: Colors.black38,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                  //labelStyle: TextStyle(color: Colors.white70),
                                                  contentPadding:
                                                      EdgeInsets.fromLTRB(
                                                          10.0, 0, 10.0, 0),
                                                  border: InputBorder.none,
                                                ),
                                              )),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                !isName
                    ? Container()
                    : Container(
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 0, top: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  //mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Flexible(
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              padding: EdgeInsets.all(0),
                                              margin: EdgeInsets.only(
                                                  left: 20, right: 20, top: 5),
                                              decoration: BoxDecoration(
                                                  color: Colors.white
                                                      .withOpacity(0.7),
                                                  border: Border.all(
                                                      color: Colors.grey,
                                                      width: 0.2),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: TextField(
                                                controller: lnameController,
                                                keyboardType:
                                                    TextInputType.text,
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Oswald',
                                                ),
                                                decoration: InputDecoration(
                                                  hintText: "Last Name",
                                                  hintStyle: TextStyle(
                                                      color: Colors.black38,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                  //labelStyle: TextStyle(color: Colors.white70),
                                                  contentPadding:
                                                      EdgeInsets.fromLTRB(
                                                          10.0, 0, 10.0, 0),
                                                  border: InputBorder.none,
                                                ),
                                              )),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isName = false;
                                            });
                                          },
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  top: 10, right: 10),
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.2),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(
                                                Icons.close,
                                                size: 15,
                                              )),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isName = false;
                                              fname = fnameController.text;
                                              lname = lnameController.text;
                                              sendName();
                                            });
                                          },
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  top: 10, right: 10),
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  color: mainColor
                                                      .withOpacity(0.7),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(
                                                Icons.done,
                                                size: 15,
                                                color: Colors.white,
                                              )),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                !isName
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(
                            top: 10, right: 10, left: 20, bottom: 0),
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.public,
                              size: 15,
                              color: Colors.black45,
                            ),
                            SizedBox(width: 5),
                            Text("Public",
                                style: TextStyle(
                                    color: mainColor,
                                    fontSize: 12,
                                    fontFamily: "Oswald",
                                    fontWeight: FontWeight.w300)),
                          ],
                        )),
                !isName
                    ? Container()
                    : Container(
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 0, top: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  //mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Flexible(
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              padding: EdgeInsets.all(0),
                                              margin: EdgeInsets.only(
                                                  left: 20, right: 20, top: 5),
                                              decoration: BoxDecoration(
                                                  color: Colors.white
                                                      .withOpacity(0.7),
                                                  border: Border.all(
                                                      color: Colors.grey,
                                                      width: 0.2),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: TextField(
                                                controller: nickNameController,
                                                keyboardType:
                                                    TextInputType.text,
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Oswald',
                                                ),
                                                decoration: InputDecoration(
                                                  hintText: "Nickname",
                                                  hintStyle: TextStyle(
                                                      color: Colors.black38,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                  //labelStyle: TextStyle(color: Colors.white70),
                                                  contentPadding:
                                                      EdgeInsets.fromLTRB(
                                                          10.0, 0, 10.0, 0),
                                                  border: InputBorder.none,
                                                ),
                                              )),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isName = false;
                                            });
                                          },
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  top: 10, right: 10),
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.2),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(
                                                Icons.close,
                                                size: 15,
                                              )),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isName = false;
                                              nickName =
                                                  nickNameController.text;
                                              sendNickname();
                                            });
                                          },
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  top: 10, right: 10),
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  color: mainColor
                                                      .withOpacity(0.7),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(
                                                Icons.done,
                                                size: 15,
                                                color: Colors.white,
                                              )),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                !isName
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(
                            top: 10, right: 10, left: 20, bottom: 20),
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.public,
                              size: 15,
                              color: Colors.black45,
                            ),
                            SizedBox(width: 5),
                            Text("Public",
                                style: TextStyle(
                                    color: mainColor,
                                    fontSize: 12,
                                    fontFamily: "Oswald",
                                    fontWeight: FontWeight.w300)),
                          ],
                        )),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 15, left: 20),
                    child: Text(
                      "Birthday",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 17,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.normal),
                    )),
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 30,
                        margin: EdgeInsets.only(top: 10, left: 20),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            color: Colors.black54,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 3.0,
                                color: Colors.black54,
                                //offset: Offset(6.0, 7.0),
                              ),
                            ],
                            border:
                                Border.all(width: 0.5, color: Colors.black54)),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(Icons.cake,
                                    size: 17, color: Colors.black54)),
                            Text(birthDate,
                                style: TextStyle(
                                    color: Colors.black54,
                                    fontSize: 14,
                                    fontFamily: "Oswald",
                                    fontWeight: FontWeight.w300)),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            if (isBirthDay == false) {
                              isBirthDay = true;
                            } else {
                              isBirthDay = false;
                            }
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(3),
                          // decoration: BoxDecoration(
                          //   borderRadius:BorderRadius.circular(5),
                          //   border:Border.all(color:mainColor, width:0.5)
                          // ),
                          margin: EdgeInsets.only(left: 0, right: 5, top: 0),
                          child: Icon(isBirthDay ? Icons.close : Icons.edit,
                              size: 15, color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                ),
                !isBirthDay
                    ? Container()
                    : Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                child: GestureDetector(
                                  onTap: () {
                                    _selectBirthDate(context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.only(
                                            left: 10, top: 15, bottom: 15),
                                        margin: EdgeInsets.only(
                                            left: 20, right: 20, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              birthDate,
                                              style: TextStyle(
                                                  color: birthDate == "N/A"
                                                      ? Colors.black38
                                                      : Colors.black,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                            ),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(right: 10),
                                                child: Icon(
                                                  Icons.calendar_today,
                                                  color: Colors.black26,
                                                  size: 16,
                                                ))
                                          ],
                                        )),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isBirthDay = false;
                                });
                              },
                              child: Container(
                                  margin: EdgeInsets.only(top: 10, right: 10),
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.2),
                                      borderRadius: BorderRadius.circular(100)),
                                  child: Icon(
                                    Icons.close,
                                    size: 15,
                                  )),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isBirthDay = false;
                                  sendRequest();
                                });
                              },
                              child: Container(
                                  margin: EdgeInsets.only(top: 10, right: 10),
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: mainColor.withOpacity(0.7),
                                      borderRadius: BorderRadius.circular(100)),
                                  child: Icon(
                                    Icons.done,
                                    size: 15,
                                    color: Colors.white,
                                  )),
                            ),
                          ],
                        ),
                      ),
                !isBirthDay
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(
                            top: 10, right: 10, left: 20, bottom: 20),
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.public,
                              size: 15,
                              color: Colors.black45,
                            ),
                            SizedBox(width: 5),
                            Text("Public",
                                style: TextStyle(
                                    color: mainColor,
                                    fontSize: 12,
                                    fontFamily: "Oswald",
                                    fontWeight: FontWeight.w300)),
                          ],
                        )),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 35, left: 20),
                    child: Text(
                      "Mobile Number",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 17,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.normal),
                    )),
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 30,
                        margin: EdgeInsets.only(top: 10, left: 20),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            color: Colors.black54,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 3.0,
                                color: Colors.black54,
                                //offset: Offset(6.0, 7.0),
                              ),
                            ],
                            border:
                                Border.all(width: 0.5, color: Colors.black54)),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(right: 10),
                                  child: Icon(Icons.phone_android,
                                      size: 17, color: Colors.black54)),
                              Container(
                                child: Text(mobile == null ? "N/A" : "$mobile",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontFamily: "Oswald",
                                        fontWeight: FontWeight.w300)),
                              ),
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            if (isMobile == false) {
                              isMobile = true;
                            } else {
                              isMobile = false;
                            }
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(3),
                          // decoration: BoxDecoration(
                          //   borderRadius:BorderRadius.circular(5),
                          //   border:Border.all(color:mainColor, width:0.5)
                          // ),
                          margin: EdgeInsets.only(left: 0, right: 5, top: 0),
                          child: Icon(isMobile ? Icons.close : Icons.edit,
                              size: 15, color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                ),
                isMobile == false
                    ? Container()
                    : Container(
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 0, top: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  //mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Flexible(
                                          child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              padding: EdgeInsets.all(0),
                                              margin: EdgeInsets.only(
                                                  left: 20, right: 20, top: 5),
                                              decoration: BoxDecoration(
                                                  color: Colors.white
                                                      .withOpacity(0.7),
                                                  border: Border.all(
                                                      color: Colors.grey,
                                                      width: 0.2),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: TextField(
                                                controller: phoneController,
                                                keyboardType:
                                                    TextInputType.text,
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Oswald',
                                                ),
                                                decoration: InputDecoration(
                                                  hintText: "Mobile Number",
                                                  hintStyle: TextStyle(
                                                      color: Colors.black38,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                  //labelStyle: TextStyle(color: Colors.white70),
                                                  contentPadding:
                                                      EdgeInsets.fromLTRB(
                                                          10.0, 0, 10.0, 0),
                                                  border: InputBorder.none,
                                                ),
                                              )),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isMobile = false;
                                            });
                                          },
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  top: 10, right: 10),
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.2),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(
                                                Icons.close,
                                                size: 15,
                                              )),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isMobile = false;
                                              mobile = phoneController.text;
                                              sendPhone();
                                            });
                                          },
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  top: 10, right: 10),
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  color: mainColor
                                                      .withOpacity(0.7),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100)),
                                              child: Icon(
                                                Icons.done,
                                                size: 15,
                                                color: Colors.white,
                                              )),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                !isMobile
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(
                            top: 10, right: 10, left: 20, bottom: 20),
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.public,
                              size: 15,
                              color: Colors.black45,
                            ),
                            SizedBox(width: 5),
                            Text("Public",
                                style: TextStyle(
                                    color: mainColor,
                                    fontSize: 12,
                                    fontFamily: "Oswald",
                                    fontWeight: FontWeight.w300)),
                          ],
                        )),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 35, left: 20),
                    child: Text(
                      "Email",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 17,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.normal),
                    )),
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 30,
                        margin: EdgeInsets.only(top: 10, left: 20),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            color: Colors.black54,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 3.0,
                                color: Colors.black54,
                                //offset: Offset(6.0, 7.0),
                              ),
                            ],
                            border:
                                Border.all(width: 0.5, color: Colors.black54)),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(right: 10),
                                  child: Icon(Icons.email,
                                      size: 17, color: Colors.black54)),
                              Expanded(
                                child: Container(
                                  child: Text(email,
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w300)),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            if (isEmail == false) {
                              isEmail = true;
                            } else {
                              isEmail = false;
                            }
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(3),
                          // decoration: BoxDecoration(
                          //   borderRadius:BorderRadius.circular(5),
                          //   border:Border.all(color:mainColor, width:0.5)
                          // ),
                          margin: EdgeInsets.only(left: 0, right: 5, top: 0),
                          child: Icon(isEmail ? Icons.close : Icons.edit,
                              size: 15, color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                ),
                isEmail == false
                    ? Container()
                    : Container(
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 0, top: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  //mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 20, right: 20, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller: emailController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "Email",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isEmail = false;
                                });
                              },
                              child: Container(
                                  margin: EdgeInsets.only(top: 10, right: 10),
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.2),
                                      borderRadius: BorderRadius.circular(100)),
                                  child: Icon(
                                    Icons.close,
                                    size: 15,
                                  )),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isEmail = false;
                                  email = emailController.text;
                                  //sendEducation();
                                });
                              },
                              child: Container(
                                  margin: EdgeInsets.only(top: 10, right: 10),
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: mainColor.withOpacity(0.7),
                                      borderRadius: BorderRadius.circular(100)),
                                  child: Icon(
                                    Icons.done,
                                    size: 15,
                                    color: Colors.white,
                                  )),
                            ),
                          ],
                        ),
                      ),
                !isEmail
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(
                            top: 10, right: 10, left: 20, bottom: 20),
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.public,
                              size: 15,
                              color: Colors.black45,
                            ),
                            SizedBox(width: 5),
                            Text("Public",
                                style: TextStyle(
                                    color: mainColor,
                                    fontSize: 12,
                                    fontFamily: "Oswald",
                                    fontWeight: FontWeight.w300)),
                          ],
                        )),
              ],
            ),
          );
  }

  Future<void> sendRequest() async {
    var data = {
      'birthDay': birthDateServer,
      'section': "birthday",
    };

    print(data);

    var res = await CallApi().postData1(data, 'profile/contact');
    // var body = json.decode(res.body);
    // print(body);

    if (res.statusCode == 200) {
      _showMessage("Success!", 2);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  Future<void> sendName() async {
    var data = {
      'firstName': fnameController.text,
      'lastName': lnameController.text,
      'section': "name",
    };

    print(data);

    var res = await CallApi().postData1(data, 'profile/contact');
    // var body = json.decode(res.body);
    // print(body);

    if (res.statusCode == 200) {
      _showMessage("Success!", 2);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  Future<void> sendNickname() async {
    var data = {
      'nickName': nickNameController.text,
      'section': "nickName",
    };

    print(data);

    var res = await CallApi().postData1(data, 'profile/contact');
    // var body = json.decode(res.body);
    // print(body);

    if (res.statusCode == 200) {
      _showMessage("Success!", 2);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  Future<void> sendPhone() async {
    var data = {
      'phone': phoneController.text,
      'section': "phone",
    };

    print(data);

    var res = await CallApi().postData1(data, 'profile/contact');
    // var body = json.decode(res.body);
    // print(body);

    if (res.statusCode == 200) {
      _showMessage("Success!", 2);
    } else {
      _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
