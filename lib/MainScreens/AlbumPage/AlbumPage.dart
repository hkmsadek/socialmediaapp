import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/AlbumCreatePage/AlbumCreatePage.dart';
import 'package:social_app_fb/MainScreens/AlbumPhotoPage/AlbumPhotoPage.dart';
import 'package:social_app_fb/main.dart';

class AlbumPage extends StatefulWidget {
  @override
  _AlbumPageState createState() => _AlbumPageState();
}

class _AlbumPageState extends State<AlbumPage> {
  var userData, body;
  bool loading = true;
  String albumDate = "";

  @override
  void initState() {
    DateTime date1 = DateTime.now();

    albumDate = DateFormat("yyyy-MM-dd").format(date1);
    setState(() {
      albumList.clear();
    });

    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');

    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
    }
    loadAlbumList();
  }

  Future loadAlbumList() async {
    setState(() {
      loading = true;
    });
    var response = await CallApi()
        .getData('profile/details/${userData['userName']}?tab=photos');
    body = json.decode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        //albumList = body;

        for (int i = 0; i < body['albums'].length; i++) {
          String pics = '${body['albums'][i]['albumImages']}';
          var chk = json.decode(pics);
          albumList.add(
            {
              'id': '${body['albums'][i]['id']}',
              'pic': '${chk[0]['url']}',
              'count': chk.length,
              'name': '${body['albums'][i]['albumName']}',
              'desc': '${body['albums'][i]['description']}',
              'allPic': chk,
              'date': '${body['albums'][i]['created_at']}',
            },
          );
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      alignment: Alignment.topCenter,
      child: Wrap(children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AlbumCreatePage()));
          },
          child: Container(
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                          right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              image: AssetImage('assets/images/white.jpg'),
                              fit: BoxFit.cover)),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          right: 8.5, left: 2.5, bottom: 2.5, top: 5.5),
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.3),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Create Album",
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.black54,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          SizedBox(width: 5),
                          Icon(
                            Icons.add_a_photo,
                            color: Colors.black54,
                            size: 17,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Wrap(
          children: List.generate(albumList.length, (index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            AlbumPhotoPage(albumList[index])));
              },
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                        //color: Colors.red,
                        height: 130,
                        width: 130,
                        padding: const EdgeInsets.all(0.0),
                        margin: EdgeInsets.only(
                            right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                        //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/car3.jpeg"), fit: BoxFit.cover)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: CachedNetworkImage(
                            imageUrl: albumList[index]['pic'],
                            placeholder: (context, url) => SpinKitCircle(
                                color: Colors.grey.withOpacity(0.5)),
                            errorWidget: (context, url, error) => Image.asset(
                              "assets/images/placeholder_cover.jpg",
                              fit: BoxFit.cover,
                              //height: 40,
                            ),
                            fit: BoxFit.cover,
                          ),
                        )),
                    Container(
                      width: 120,
                      child: Text(
                        albumList[index]['name'],
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 13,
                            color: mainColor,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Container(
                      width: 120,
                      margin: EdgeInsets.only(top: 3),
                      child: Text(
                        "${albumList[index]['count']} photos",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.w400,
                            fontSize: 11,
                            color: Colors.black45),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        ),
      ]),
    );
  }
}
