import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_rounded_progress_bar/flutter_rounded_progress_bar.dart';
// import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';
import 'package:intl/intl.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:social_app_fb/main.dart';
// import 'package:step_progress_indicator/step_progress_indicator.dart';

class StoryPage extends StatefulWidget {
  final strList;
  StoryPage(this.strList);

  @override
  _StoryPageState createState() => _StoryPageState();
}

class _StoryPageState extends State<StoryPage> {
  List storyList = [];
  int number = 0, init = 1, sec = 0, times = 0, num = 1;
  double percent = 0.0;
  bool _loading;
  double _progressValue;
  String name = "", date = "";

  @override
  void initState() {
    setState(() {
      name = "${widget.strList['name']}";
      DateTime dateTime = DateTime.parse(widget.strList['created']);
      date = DateFormat.yMMMd().format(dateTime);
      print(name);
      print(date);
      print(widget.strList['storyPic']);
      storyList.add(widget.strList);
    });
    _loading = false;
    _progressValue = 0.0;
    print("sec");
    print(sec);
    Timer timer = new Timer.periodic(new Duration(seconds: 5), (time) {
      times++;
      num++;
      percent += 20;
      if (times == storyList.length) {
        Navigator.pop(context);
      }
      Timer timer1 = new Timer.periodic(new Duration(seconds: 1), (time) {
        print('Something');
        time.cancel();
        setState(() {
          number++;
          init++;
          print(number);
          print(init);
          if (init > storyList.length) {
            number = storyList.length - 1;
            init = storyList.length;
          }
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.strList['text'] != "" ? mainColor : Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        //automaticallyImplyLeading: false,
        backgroundColor:
            widget.strList['text'] != "" ? mainColor : Colors.black,
        elevation: 0,
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.close)),
        title: Container(
          margin: EdgeInsets.only(top: 0, right: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            name,
                            style: TextStyle(
                                color: widget.strList['text'] != ""
                                    ? Colors.white
                                    : Colors.grey,
                                fontSize: 20,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.normal),
                          ),
                          Text(
                            date,
                            style: TextStyle(
                                color: widget.strList['text'] != ""
                                    ? Colors.white
                                    : Colors.grey,
                                fontSize: 12,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.normal),
                          ),
                        ],
                      )),
                ),
              ),
              GestureDetector(
                  onTap: () {
                    deleteStory();
                  },
                  child:
                      Container(child: Icon(Icons.delete, color: Colors.white)))
            ],
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          // Container(
          //   margin: EdgeInsets.only(left: 15, right: 15),
          //   child: StepProgressIndicator(
          //     totalSteps: storyList.length,
          //     currentStep: init,
          //     selectedColor: Colors.white,
          //     unselectedColor: Colors.transparent,
          //   ),
          // ),
          // Container(
          //   margin: EdgeInsets.only(left: 15, right: 15),
          //   height: 14,
          //   child: RoundedProgressBar(
          //       height: 2,
          //       milliseconds: 1000,
          //       percent: percent,
          //       style: RoundedProgressBarStyle(
          //           colorBackgroundIcon: Color(0xffc0392b),
          //           colorProgress: mainColor,
          //           colorProgressDark: Colors.blue,
          //           colorBorder: Colors.transparent,
          //           backgroundProgress: Color(0xff4a627a),
          //           borderWidth: 4,
          //           widthShadow: 6),
          //       //theme: RoundedProgressBarTheme.blue,
          //       borderRadius: BorderRadius.circular(10)),
          // ),
          Center(
            child: Container(
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      number++;
                      num++;
                      times++;
                      if (number == storyList.length) {
                        number = storyList.length;
                      }
                    });
                  },
                  child: widget.strList['text'] != ""
                      ? Center(
                          child: Container(
                              padding: EdgeInsets.all(0),
                              margin:
                                  EdgeInsets.only(left: 30, right: 30, top: 5),
                              decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: Text(
                                widget.strList['text'],
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              )),
                        )
                      : Container(
                          child: Center(
                              child: Stack(
                          children: <Widget>[
                            Container(
                                child: Center(
                                    child: CachedNetworkImage(
                              imageUrl: widget.strList['storyPic'],
                              placeholder: (context, url) =>
                                  Center(child: Text("wait...")),
                              errorWidget: (context, url, error) => Image.asset(
                                "assets/images/placeholder_cover.jpg",
                                //height: 40,
                              ),
                              fit: BoxFit.cover,

                              // NetworkImage(
                              //     widget.friend[index].profilePic
                            ))),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      number--;
                                      init--;
                                      num--;
                                      times--;
                                      if (number < 0) {
                                        number = 0;
                                        init = 1;
                                      }
                                    });
                                  },
                                  child: Container(
                                    height: MediaQuery.of(context).size.height,
                                    width: 50,
                                    color: Colors.transparent,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      number++;
                                      init++;
                                      num++;
                                      times++;
                                      print(init);
                                      print(storyList.length);

                                      if (times == storyList.length) {
                                        Navigator.pop(context);
                                      }

                                      if (init > storyList.length) {
                                        number = storyList.length - 1;
                                        init = storyList.length;
                                      }
                                    });
                                  },
                                  child: Container(
                                    height: MediaQuery.of(context).size.height,
                                    width: 50,
                                    color: Colors.transparent,
                                  ),
                                ),
                              ],
                            )
                          ],
                        )))),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(20),
            child: Text("$num/${storyList.length}",
                style: TextStyle(
                    color: widget.strList['text'] != ""
                        ? Colors.white
                        : Colors.grey,
                    fontWeight: FontWeight.bold)),
          ),
        ],
      ),
    );
  }

  Future<void> deleteStory() async {
    var res = await CallApi().postData5('story/delete/${widget.strList['id']}');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    }
  }
}
