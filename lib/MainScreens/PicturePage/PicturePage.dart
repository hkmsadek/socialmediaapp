import 'package:flutter/material.dart';

class PicturePage extends StatefulWidget {
  @override
  _PicturePageState createState() => _PicturePageState();
}

class _PicturePageState extends State<PicturePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      alignment: Alignment.topCenter,
      child: Wrap(children: <Widget>[
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/car1.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/car2.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/car3.jpeg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/bike1.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/bike2.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/bike3.jpeg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/bike4.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/car4.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/bike5.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/bike6.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/car6.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/car5.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          margin: EdgeInsets.only(right: 2.5, left: 2.5, bottom: 2.5, top: 2.5),
          height: 130,
          width: 130,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: AssetImage('assets/images/car7.jpg'),
                  fit: BoxFit.cover)),
        ),
      ]),
    );
  }
}
