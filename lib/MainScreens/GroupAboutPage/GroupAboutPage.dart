import 'package:flutter/material.dart';

class GroupAboutPage extends StatefulWidget {
  @override
  _GroupAboutPageState createState() => _GroupAboutPageState();
}

class _GroupAboutPageState extends State<GroupAboutPage> {
  int memberMargin = 10;
  double adminMargin = 10.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Flutter Developers",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: Container(
        margin: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(top: 10, left: 20),
                      child: Text(
                        "About",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 18,
                            fontFamily: "Oswald",
                            fontWeight: FontWeight.bold),
                      )),
                  Row(
                    children: <Widget>[
                      Container(
                        width: 30,
                        margin: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            color: Colors.black54,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 3.0,
                                color: Colors.black54,
                                //offset: Offset(6.0, 7.0),
                              ),
                            ],
                            border:
                                Border.all(width: 0.5, color: Colors.black54)),
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    margin:
                        EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //padding: EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.lock,
                                        color: Colors.black38,
                                        size: 20,
                                      ),
                                      decoration: new BoxDecoration(
                                        //color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Private",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w600),
                                      ),

                                      ////// <<<<< Mutual Friends >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(top: 3),
                                        child: Text(
                                          "Only members can see who's in the group and what they post",
                                          style: TextStyle(
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12.5,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    margin:
                        EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //padding: EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.visibility_off,
                                        color: Colors.black38,
                                        size: 20,
                                      ),
                                      decoration: new BoxDecoration(
                                        //color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Hidden",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w600),
                                      ),

                                      ////// <<<<< Mutual Friends >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(top: 3),
                                        child: Text(
                                          "Only members can find this group",
                                          style: TextStyle(
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12.5,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    margin:
                        EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //padding: EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.history,
                                        color: Colors.black38,
                                        size: 20,
                                      ),
                                      decoration: new BoxDecoration(
                                        //color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "View Group History",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w600),
                                      ),

                                      ////// <<<<< Mutual Friends >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(top: 3),
                                        child: Text(
                                          "Group created on Apr 8, 2020",
                                          style: TextStyle(
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12.5,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Column(
                    children: <Widget>[
                      Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(top: 10, left: 20),
                          child: Text(
                            "Members",
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 18,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.bold),
                          )),
                      Row(
                        children: <Widget>[
                          Container(
                            width: 30,
                            margin:
                                EdgeInsets.only(top: 10, left: 20, bottom: 10),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                                color: Colors.black54,
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 3.0,
                                    color: Colors.black54,
                                    //offset: Offset(6.0, 7.0),
                                  ),
                                ],
                                border: Border.all(
                                    width: 0.5, color: Colors.black54)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(left: 20, right: 10, top: 5),
                    child: Wrap(
                        children: List.generate(10, (index) {
                      return index + 1 == 10
                          ? Container(
                              height: 35,
                              width: 35,
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(color: Colors.grey, width: 1),
                                  shape: BoxShape.circle),
                              child: Stack(
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundColor: Colors.white,
                                    backgroundImage:
                                        AssetImage("assets/images/white.jpg"),
                                  ),
                                  CircleAvatar(
                                    backgroundColor:
                                        Colors.grey.withOpacity(0.6),
                                  ),
                                  Center(
                                      child: Container(
                                          child: Text("+4k",
                                              style: TextStyle(fontSize: 12))))
                                ],
                              ))
                          : Container(
                              height: 35,
                              width: 35,
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                  shape: BoxShape.circle),
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                backgroundImage:
                                    AssetImage("assets/images/prabal.jpg"),
                              ));
                    })),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 20, top: 10, right: 20),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Hkm, Prabal, Pranto and 20 other friends are members.",
                        style: TextStyle(color: Colors.black54, fontSize: 12),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(left: 20, right: 10, top: 15),
                    child: Wrap(
                        children: List.generate(3, (index) {
                      return index + 1 == 3
                          ? Container(
                              height: 35,
                              width: 35,
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(color: Colors.grey, width: 1),
                                  shape: BoxShape.circle),
                              child: Stack(
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundColor:
                                        Colors.grey.withOpacity(0.6),
                                  ),
                                  Center(
                                      child: Container(
                                          child: Text("+3",
                                              style: TextStyle(fontSize: 12))))
                                ],
                              ))
                          : Container(
                              height: 35,
                              width: 35,
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                  shape: BoxShape.circle),
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                backgroundImage:
                                    AssetImage("assets/images/user.jpg"),
                              ));
                    })),
                  ),
                  Container(
                      margin: EdgeInsets.only(
                          left: 20, top: 10, right: 20, bottom: 10),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Hkm and Appifylab are admins.",
                        style: TextStyle(color: Colors.black54, fontSize: 12),
                      )),
                  Divider(),
                  Column(
                    children: <Widget>[
                      Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(top: 10, left: 20),
                          child: Text(
                            "Group Activity",
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 18,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.bold),
                          )),
                      Row(
                        children: <Widget>[
                          Container(
                            width: 30,
                            margin:
                                EdgeInsets.only(top: 10, left: 20, bottom: 10),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                                color: Colors.black54,
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 3.0,
                                    color: Colors.black54,
                                    //offset: Offset(6.0, 7.0),
                                  ),
                                ],
                                border: Border.all(
                                    width: 0.5, color: Colors.black54)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    margin:
                        EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //padding: EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.insert_comment,
                                        color: Colors.black38,
                                        size: 20,
                                      ),
                                      decoration: new BoxDecoration(
                                        //color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "80 in the last 28 days",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    margin:
                        EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //padding: EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.person,
                                        color: Colors.black38,
                                        size: 20,
                                      ),
                                      decoration: new BoxDecoration(
                                        //color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "31 total members",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    margin:
                        EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //padding: EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.group,
                                        color: Colors.black38,
                                        size: 20,
                                      ),
                                      decoration: new BoxDecoration(
                                        //color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Created 1 year ago",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Column(
                    children: <Widget>[
                      Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(top: 10, left: 20),
                          child: Text(
                            "Group by this page",
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 18,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.bold),
                          )),
                      Row(
                        children: <Widget>[
                          Container(
                            width: 30,
                            margin:
                                EdgeInsets.only(top: 10, left: 20, bottom: 10),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                                color: Colors.black54,
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 3.0,
                                    color: Colors.black54,
                                    //offset: Offset(6.0, 7.0),
                                  ),
                                ],
                                border: Border.all(
                                    width: 0.5, color: Colors.black54)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    margin:
                        EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 0),
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                ////// <<<<< Profile picture >>>>> //////
                                Stack(
                                  children: <Widget>[
                                    ////// <<<<< Picture >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //padding: EdgeInsets.all(5.0),
                                      child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        backgroundImage: AssetImage(
                                            "assets/images/prabal.jpg"),
                                      ),
                                      decoration: new BoxDecoration(
                                        //color: Colors.grey[300],
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                  ],
                                ),

                                ////// <<<<< User Name >>>>> //////
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Flutter Developers",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.black54,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w600),
                                      ),

                                      ////// <<<<< Mutual Friends >>>>> //////
                                      Container(
                                        margin: EdgeInsets.only(top: 3),
                                        child: Text(
                                          "Product/Service - 1,078 liked this",
                                          style: TextStyle(
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12.5,
                                              color: Colors.black45),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
