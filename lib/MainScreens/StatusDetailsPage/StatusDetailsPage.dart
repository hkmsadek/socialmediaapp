import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/CommentPage/CommentPage.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/MainScreens/StatusCommentPage/StatusCommentPage.dart';
import 'package:social_app_fb/ModelClass/FeedDetailsModel/FeedDetailsModel.dart';
import 'package:social_app_fb/main.dart';

List postDetailsComCount = [];

class StatusDetailsPage extends StatefulWidget {
  final index;
  final userData;
  final id;
  StatusDetailsPage(this.index, this.userData, this.id);
  @override
  _StatusDetailsPageState createState() => _StatusDetailsPageState();
}

class _StatusDetailsPageState extends State<StatusDetailsPage> {
  var userData, postData, postList;
  bool _isLoading = true;
  bool noData = false;
  int _start = 3, lastID = 1;
  int no, likeNum = 0;
  int l = 0;
  int like = 0, likeStore = 0;
  String date = "";
  ScrollController _controller = new ScrollController();
  List img = [];

  @override
  void initState() {
    setState(() {
      postDetailsComCount.clear();
    });
    _getUserInfo();
    //timerCheck();
    print("widget.id");
    print(widget.id);

    super.initState();
  }

  Future loadPosts(int number) async {
    var postresponse = await CallApi().getData2('feed/singleFeed/${widget.id}');

    setState(() {
      var postcontent = postresponse.body;
      final posts = json.decode(postcontent);
      print("posts");
      print(posts['feed']);

      if (posts['feed']['data'] != null) {
        var postdata = FeedDetailsModel.fromJson(posts);
        postList = postdata;

        postDetailsComCount
            .add({'count': postList.feed.meta.totalCommentsCount});

        DateTime dateTime = DateTime.parse(postList.feed.createdAt);
        date = DateFormat.yMMMd().format(dateTime);

        for (int i = 0; i < postList.feed.data.images.length; i++) {
          img.add({
            'id': postList.feed.data.images[i].id,
            'url': postList.feed.data.images[i].url,
          });
        }

        print(postDetailsComCount);
        print(date);
      } else {
        noData = true;
      }
    });

    setState(() {
      _isLoading = false;
    });
    //print(page1);
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
    loadPosts(1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Post",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
      body: _isLoading
          ? LoaderScreen()
          : noData
              ? Center(
                  child: Container(
                    child: Text(
                      "No posts related to this!",
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.black,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                )
              : SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Container(
                    padding: EdgeInsets.only(top: 0, bottom: 0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      //border: Border.all(width: 0.8, color: Colors.grey[300]),
                      // boxShadow: [
                      //   BoxShadow(
                      //     blurRadius: 0.0,
                      //     color: Colors.black26,
                      //     //offset: Offset(6.0, 7.0),
                      //   ),
                      // ],
                    ),
                    margin:
                        EdgeInsets.only(top: 5, bottom: 5, left: 0, right: 0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                //color: Colors.red,
                                margin: EdgeInsets.only(
                                    left: 20, right: 20, top: 15),
                                padding: EdgeInsets.only(right: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    ////// <<<<< Picture start >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                      padding: EdgeInsets.all(1.0),
                                      child: CircleAvatar(
                                        radius: 20.0,
                                        backgroundColor: Colors.white,
                                        backgroundImage: AssetImage(
                                            'assets/images/prabal.jpg'),
                                      ),
                                      decoration: new BoxDecoration(
                                        color: Colors.grey[300], // border color
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                    ////// <<<<< Picture end >>>>> //////

                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        ////// <<<<< Name start >>>>> //////
                                        Text(
                                          postList.feed.data.statusUserName,
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.black54,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w500),
                                        ),
                                        ////// <<<<< Name start >>>>> //////

                                        ////// <<<<< Time start >>>>> //////
                                        Container(
                                          margin: EdgeInsets.only(top: 3),
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                date,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontFamily: 'Oswald',
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 11,
                                                    color: Colors.black45),
                                              ),
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Icon(
                                                    Icons.public,
                                                    color: Colors.black45,
                                                    size: 12,
                                                  ))
                                            ],
                                          ),
                                        ),
                                        ////// <<<<< Time end >>>>> //////
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              ////// <<<<< More start >>>>> //////
                              postList.feed.userId != userData['id']
                                  ? Container()
                                  : GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _statusModalBottomSheet(
                                              context, postList.feed.id);
                                        });
                                      },
                                      child: Container(
                                          padding: EdgeInsets.all(5),
                                          margin: EdgeInsets.only(right: 15),
                                          child: Icon(
                                            Icons.more_horiz,
                                            color: Colors.black45,
                                          )),
                                    ),
                              ////// <<<<< More end >>>>> //////
                            ],
                          ),
                        ),

                        ////// <<<<< Post start >>>>> //////
                        Container(
                            margin:
                                EdgeInsets.only(left: 20, right: 20, top: 20),
                            child: postList.feed.activityText == ""
                                ? Container()
                                : Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(
                                      postList.feed.activityText,
                                      textAlign: TextAlign.justify,
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  )),
                        ////// <<<<< Post end >>>>> //////
                        Container(
                            margin: EdgeInsets.only(
                                left: 20, right: 20, bottom: 0, top: 10),
                            child: Divider(
                              color: Colors.grey[300],
                            )),
                        Container(
                          margin: EdgeInsets.only(left: 20, top: 0),
                          padding: EdgeInsets.all(0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              ////// <<<<< Like start >>>>> //////
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if (postList.feed.like == null) {
                                      postList.feed.meta.totalLikesCount++;
                                      postList.feed.like = 1;
                                      likeUnlike(
                                          widget.index, 1, postList.feed.id);
                                    } else {
                                      postList.feed.meta.totalLikesCount--;
                                      postList.feed.like = null;
                                      likeUnlike(
                                          widget.index, 2, postList.feed.id);
                                    }
                                  });
                                },
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.all(3.0),
                                        child: Icon(
                                          postList.feed.like != null
                                              ? Icons.favorite
                                              : Icons.favorite_border,
                                          size: 20,
                                          color: postList.feed.like != null
                                              ? Colors.redAccent
                                              : Colors.black54,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3),
                                        child: Text(
                                            "${postList.feed.meta.totalLikesCount}",
                                            style: TextStyle(
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300,
                                                color: Colors.black54,
                                                fontSize: 12)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              ////// <<<<< Like end >>>>> //////

                              ////// <<<<< Comment start >>>>> //////
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              StatusCommentPage(
                                                  userData,
                                                  widget.index,
                                                  postList.feed)));
                                },
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(left: 15),
                                        padding: EdgeInsets.all(3.0),
                                        child: Icon(
                                          Icons.chat_bubble_outline,
                                          size: 20,
                                          color: Colors.black54,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3),
                                        child: Text(
                                            "${postDetailsComCount[0]['count']}",
                                            style: TextStyle(
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300,
                                                color: Colors.black54,
                                                fontSize: 12)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              ////// <<<<< Comment end >>>>> //////

                              ////// <<<<< Share start >>>>> //////
                              GestureDetector(
                                onTap: () {
                                  //_shareModalBottomSheet(context, userData);
                                },
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(left: 15),
                                        padding: EdgeInsets.all(3.0),
                                        child: Icon(
                                          Icons.share,
                                          size: 20,
                                          color: Colors.black54,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 3),
                                        child: Text(
                                            "${postList.feed.meta.totalSharesCount}",
                                            style: TextStyle(
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300,
                                                color: Colors.black54,
                                                fontSize: 12)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              ////// <<<<< Share end >>>>> //////
                            ],
                          ),
                        ),
                        Container(
                            height: 10,
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(5)),
                            margin: EdgeInsets.only(
                                left: 0, right: 0, bottom: 0, top: 7),
                            child: null),
                        img.length == 0
                            ? Container()
                            : Container(
                                child: Column(
                                    children:
                                        List.generate(img.length, (index) {
                                  return Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                            //color: Colors.red,
                                            height: 200,
                                            //width: 150,
                                            padding: const EdgeInsets.all(0.0),
                                            margin: EdgeInsets.only(
                                                top: 10, right: 5, left: 5),
                                            //decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), image: DecorationImage(image: AssetImage("assets/images/bike1.jpg"), fit: BoxFit.cover)),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: CachedNetworkImage(
                                                imageUrl: img[index]['url'],
                                                placeholder: (context, url) =>
                                                    SpinKitCircle(
                                                        color: Colors.grey
                                                            .withOpacity(0.5)),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Image.asset(
                                                  "assets/images/placeholder_cover.jpg",
                                                  fit: BoxFit.cover,
                                                  //height: 40,
                                                ),
                                                fit: BoxFit.cover,
                                              ),
                                            )),
                                      ),
                                    ],
                                  );
                                })),
                              ),
                      ],
                    ),
                  ),
                ),
    );
  }

  void _statusModalBottomSheet(context, id) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                // Text('React to this post',
                //       style: TextStyle(fontWeight: FontWeight.normal)),
                new ListTile(
                  leading: new Icon(Icons.edit),
                  title: new Text('Edit',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  onTap: () => {
                    Navigator.pop(context),
                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => CreatePost()))
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  title: new Text('Delete',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.redAccent,
                          fontFamily: "Oswald")),
                  onTap: () => {Navigator.pop(context), _showDeleteDialog(id)},
                ),
              ],
            ),
          );
        });
  }

  Future<Null> _showDeleteDialog(id) async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Text(
                          "Want to delete the post?",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.w400),
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                              statusDelete(widget.index, id);
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontFamily: 'BebasNeue',
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future statusDelete(index, id) async {
    var data = {
      'user_id': userData['id'],
      'status_id': id,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/delete');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      Navigator.pop(context);
      setState(() {
        feedList.removeAt(index);
      });

      return _showMessage("Deleted Successfully!", 2);
    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }

  Future likeUnlike(index, status, id) async {
    var data = {
      'user_id': userData['id'],
      'status_id': id,
      'type': status,
    };

    print(data);

    var res = await CallApi().postData1(data, 'status/add/like');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      //Navigator.pop(context);

    } else {
      return _showMessage("Something went wrong!", 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
