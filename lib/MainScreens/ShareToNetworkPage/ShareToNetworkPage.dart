import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:social_app_fb/main.dart';
import 'package:http/http.dart' as http;

class ShareToNetworkPage extends StatefulWidget {
  @override
  _ShareToNetworkPageState createState() => _ShareToNetworkPageState();
}

class _ShareToNetworkPageState extends State<ShareToNetworkPage> {
  int fb = 0, twitter = 0, instagram = 0, linkedin = 0;
  bool isLoggedIn = false;
  var profileData;

  void onLoginStatusChanged(bool isLoggedIn, {profileData}) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
      this.profileData = profileData;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Share To",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.exit_to_app,
              color: Colors.black54,
            ),
            onPressed: () {
              
            }
                
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.only(top: 10),
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (fb == 0) {
                      fb = 1;
                    } else {
                      fb = 0;
                    }
                  });
                },
                child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 0, top: 15, bottom: 15),
                    margin:
                        EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        //border: Border.all(color: Colors.grey, width: 0.2),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    image: DecorationImage(
                                        image: AssetImage(
                                      "assets/images/fb.png",
                                    ))),
                              ),
                              SizedBox(width: 10),
                              Text(
                                "Facebook",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Container(
                            decoration: BoxDecoration(
                                color: fb == 1 ? mainColor : Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    width: 0.3, color: Colors.black45)),
                            child: Icon(
                              Icons.done,
                              size: 17,
                              color: fb == 1 ? Colors.white : Colors.black54,
                            ))
                      ],
                    )),
              ),
              // SingleChildScrollView(
              //   child: Container(
              //     //height: 250,
              //     child: Center(
              //       child: isLoggedIn
              //           ? _displayUserData(profileData)
              //           : _displayLoginButton(),
              //     ),
              //   ),
              // ),
              Divider(),
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (twitter == 0) {
                      twitter = 1;
                    } else {
                      twitter = 0;
                    }
                  });
                },
                child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 0, top: 15, bottom: 15),
                    margin:
                        EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        //border: Border.all(color: Colors.grey, width: 0.2),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    image: DecorationImage(
                                        image: AssetImage(
                                      "assets/images/twitter.png",
                                    ))),
                              ),
                              SizedBox(width: 10),
                              Text(
                                "Twitter",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Container(
                            decoration: BoxDecoration(
                                color: twitter == 1 ? mainColor : Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    width: 0.3, color: Colors.black45)),
                            child: Icon(
                              Icons.done,
                              size: 17,
                              color:
                                  twitter == 1 ? Colors.white : Colors.black54,
                            ))
                      ],
                    )),
              ),
              Divider(),
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (linkedin == 0) {
                      linkedin = 1;
                    } else {
                      linkedin = 0;
                    }
                  });
                },
                child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 0, top: 15, bottom: 15),
                    margin:
                        EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        //border: Border.all(color: Colors.grey, width: 0.2),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    image: DecorationImage(
                                        image: AssetImage(
                                      "assets/images/linkedin.png",
                                    ))),
                              ),
                              SizedBox(width: 10),
                              Text(
                                "Linkedin",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Container(
                            decoration: BoxDecoration(
                                color: linkedin == 1 ? mainColor : Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    width: 0.3, color: Colors.black45)),
                            child: Icon(
                              Icons.done,
                              size: 17,
                              color:
                                  linkedin == 1 ? Colors.white : Colors.black54,
                            ))
                      ],
                    )),
              ),
              Divider(),
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (instagram == 0) {
                      instagram = 1;
                    } else {
                      instagram = 0;
                    }
                  });
                },
                child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 0, top: 15, bottom: 15),
                    margin:
                        EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        //border: Border.all(color: Colors.grey, width: 0.2),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    image: DecorationImage(
                                        image: AssetImage(
                                      "assets/images/instagram.png",
                                    ))),
                              ),
                              SizedBox(width: 10),
                              Text(
                                "Instagram",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Container(
                            decoration: BoxDecoration(
                                color:
                                    instagram == 1 ? mainColor : Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    width: 0.3, color: Colors.black45)),
                            child: Icon(
                              Icons.done,
                              size: 17,
                              color: instagram == 1
                                  ? Colors.white
                                  : Colors.black54,
                            ))
                      ],
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // void initiateFacebookLogin() async {
  //   var facebookLoginResult =
  //       await facebookLogin.logInWithReadPermissions(['email']);

  //   switch (facebookLoginResult.status) {
  //     case FacebookLoginStatus.error:
  //       onLoginStatusChanged(false);
  //       break;
  //     case FacebookLoginStatus.cancelledByUser:
  //       onLoginStatusChanged(false);
  //       break;
  //     case FacebookLoginStatus.loggedIn:
  //       var graphResponse = await http.get(
  //           'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult.accessToken.token}');

  //       var profile = json.decode(graphResponse.body);
  //       print(profile.toString());

  //       onLoginStatusChanged(true, profileData: profile);
  //       break;
  //   }
  // }

  // _displayUserData(profileData) {
  //   return Column(
  //     mainAxisSize: MainAxisSize.min,
  //     children: <Widget>[
  //       Container(
  //         height: 200.0,
  //         width: 200.0,
  //         decoration: BoxDecoration(
  //           shape: BoxShape.circle,
  //           image: DecorationImage(
  //             fit: BoxFit.fill,
  //             image: NetworkImage(
  //               profileData['picture']['data']['url'],
  //             ),
  //           ),
  //         ),
  //       ),
  //       SizedBox(height: 28.0),
  //       Text(
  //         "Logged in as: ${profileData['name']}",
  //         style: TextStyle(
  //           fontSize: 20.0,
  //         ),
  //       ),
  //     ],
  //   );
  // }

  // _displayLoginButton() {
  //   return RaisedButton(
  //     child: Text("Login with Facebook"),
  //     onPressed: () => initiateFacebookLogin(),
  //   );
  // }

  // _logout() async {
  //   await facebookLogin.logOut();
  //   onLoginStatusChanged(false);
  //   print("Logged out");
  // }
}
