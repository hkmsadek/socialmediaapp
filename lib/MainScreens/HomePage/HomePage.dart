import 'package:flutter/material.dart';
import 'package:social_app_fb/MainScreens/ChatPage/ChatPage.dart';
import 'package:social_app_fb/MainScreens/FeedPage/FeedPage.dart';
import 'package:social_app_fb/MainScreens/FriendRequestPage/FriendRequestPage.dart';
import 'package:social_app_fb/MainScreens/FriendsPage/FriendsPage.dart';
import 'package:social_app_fb/MainScreens/GroupPage/GroupPage.dart';
import 'package:social_app_fb/MainScreens/MorePage/MorePage.dart';
import 'package:social_app_fb/MainScreens/NotifyPage/notifyPage.dart';
import 'package:social_app_fb/MainScreens/ProfileScreen/ProfileScreen.dart';
import 'package:social_app_fb/MainScreens/SearchPage/searchPage.dart';

import '../../main.dart';

class HomePage extends StatefulWidget {
 @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int currentIndex = selectedPage;

  final pageOptions = [
    FeedPage(),
    FriendRequestPage(),
    //GroupPage(),
    GroupPage(),
    FriendRequestPage(),
    GroupPage(),
    MorePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Social App",
                        style: TextStyle(
                            color: mainColor,
                            fontSize: 17,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SearchPage()),
                  );

                  //_showMsg("Coming Soon");
                },
                child: Container(
                  decoration: BoxDecoration(
                      //color: sub_white,
                      borderRadius: BorderRadius.circular(15)),
                  margin: EdgeInsets.only(right: 10),
                  padding: EdgeInsets.all(8),
                  child: Icon(
                    Icons.search,
                    color: Colors.black45.withOpacity(0.4),
                    size: 18,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NotifyPage()),
                  );
                },
                child: Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                          //color: sub_white,
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(8),
                      child: Icon(
                        Icons.notifications,
                        color: Colors.black45.withOpacity(0.4),
                        size: 18,
                      ),
                    ),
                    notifyNum == 0
                        ? Container()
                        : Container(
                            padding: EdgeInsets.only(
                                top: 1, bottom: 1, right: 5, left: 5),
                            margin: EdgeInsets.only(right: 10, left: 20),
                            decoration: BoxDecoration(
                                color: mainColor,
                                borderRadius: BorderRadius.circular(15)),
                            child: Text(
                              "$notifyNum",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 10,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ChatPage()),
                  );
                  //_showMsg("Coming Soon");
                },
                child: Stack(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          //color: sub_white,
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(8),
                      child: Icon(
                        Icons.chat,
                        color: Colors.black45.withOpacity(0.4),
                        size: 18,
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(top: 1, bottom: 1, right: 5, left: 5),
                      margin: EdgeInsets.only(right: 0, left: 20),
                      decoration: BoxDecoration(
                          color: mainColor,
                          borderRadius: BorderRadius.circular(15)),
                      child: Text(
                        "5",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 10,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: pageOptions[currentIndex],
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Colors.white,
            primaryColor: mainColor,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: new TextStyle(color: Colors.grey[500]))),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: currentIndex,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home, size: 20), title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: new Stack(children: <Widget>[
                  frndNum != 0
                      ? Container(
                          margin: EdgeInsets.only(top: 5, right: 17),
                          child: new Icon(
                            Icons.group,
                            size: 20,
                          ),
                        )
                      : Container(
                          margin: EdgeInsets.only(top: 5, right: 0),
                          child: new Icon(
                            Icons.group,
                            size: 20,
                          ),
                        ),
                  frndNum == 0
                      ? Positioned(
                          right: 0,
                          child: Container(
                            padding: EdgeInsets.only(
                                top: 1, bottom: 1, right: 5, left: 5),
                            margin:
                                EdgeInsets.only(right: 0, left: 0, bottom: 10),
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(15)),
                          ),
                        )
                      : Positioned(
                          left: 9,
                          child: Container(
                            padding: EdgeInsets.only(
                                top: 1, bottom: 1, right: 5, left: 5),
                            margin:
                                EdgeInsets.only(right: 0, left: 0, bottom: 10),
                            decoration: BoxDecoration(
                                color: mainColor,
                                borderRadius: BorderRadius.circular(15)),
                            child: Text(
                              frndNum.toString(),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 9,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        )
                ]),
                title: SizedBox.shrink()),
            // BottomNavigationBarItem(
            //     icon: Icon(Icons.supervised_user_circle, size: 20),
            //     title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: Icon(Icons.supervised_user_circle, size: 20),
                title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart, size: 20),
                title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: Icon(Icons.flag, size: 20), title: SizedBox.shrink()),
            BottomNavigationBarItem(
                icon: Icon(Icons.menu, size: 20), title: SizedBox.shrink())
          ],
          onTap: (int _selectedPage) {
            setState(() {
              currentIndex = _selectedPage;
              selectedPage = _selectedPage;

              if (currentIndex == 1) {
                frndNum = 0;
              }
            });
            //print(selectedPage);
          },
        ),
      ),
    );
  }
}
