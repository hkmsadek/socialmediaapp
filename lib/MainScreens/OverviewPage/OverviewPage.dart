import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/main.dart';

class OverViewPage extends StatefulWidget {
  @override
  _OverViewPageState createState() => _OverViewPageState();
}

class _OverViewPageState extends State<OverViewPage> {
  var userData;
  String birthDate = "";
  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
        DateTime dateTime = DateTime.parse(userData['birthDay']);
        birthDate = DateFormat.yMMMd().format(dateTime);
        //birthDate = userData['birthDay'];
        print(birthDate);
      });
      print(userData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return userData == null
        ? LoaderScreen()
        : Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: <Widget>[
                Container(
                  child: Column(
                      children: List.generate(work.length, (int index) {
                    return Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          margin: EdgeInsets.only(
                              top: 2.5, bottom: 2.5, left: 0, right: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: 20, right: 20, top: 0),
                                  padding: EdgeInsets.only(right: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      ////// <<<<< Profile picture >>>>> //////
                                      Container(
                                          margin: EdgeInsets.only(right: 10),
                                          child: Icon(Icons.work,
                                              size: 17, color: Colors.black54)),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text.rich(
                                              TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                      text: work[index][
                                                                  'currentlyWorking'] ==
                                                              false
                                                          ? "Former ${work[index]['position']} at"
                                                          : "${work[index]['position']} at",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 14,
                                                          fontFamily: "Oswald",
                                                          fontWeight:
                                                              FontWeight.w400)),
                                                  TextSpan(
                                                      text:
                                                          " ${work[index]['company']}",
                                                      style: TextStyle(
                                                          color: mainColor,
                                                          fontSize: 14,
                                                          fontFamily: "Oswald",
                                                          fontWeight:
                                                              FontWeight.w500)),
                                                  // can add more TextSpans here...
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 5),
                                              child: Text(
                                                  "${work[index]['city']}",
                                                  style: TextStyle(
                                                      color: Colors.black54,
                                                      fontSize: 11,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w300)),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider()
                      ],
                    );
                  })),
                ),
                Container(
                  child: Column(
                      children: List.generate(education.length, (int index) {
                    return Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          margin: EdgeInsets.only(
                              top: 2.5, bottom: 2.5, left: 0, right: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: 20, right: 20, top: 0),
                                  padding: EdgeInsets.only(right: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      ////// <<<<< Profile picture >>>>> //////
                                      Container(
                                          margin: EdgeInsets.only(right: 10),
                                          child: Icon(Icons.book,
                                              size: 17, color: Colors.black54)),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              child: Text.rich(
                                                TextSpan(
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text: "Studied ",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black54,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                "Oswald",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400)),
                                                    TextSpan(
                                                        text:
                                                            "${education[index]['degree']} at",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black54,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                "Oswald",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400)),
                                                    TextSpan(
                                                        text:
                                                            " ${education[index]['collage']}",
                                                        style: TextStyle(
                                                            color: mainColor,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                "Oswald",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                    // can add more TextSpans here...
                                                  ],
                                                ),
                                              ),
                                            ),
                                            education[index]['city'] == ""
                                                ? Container()
                                                : Container(
                                                    margin:
                                                        EdgeInsets.only(top: 5),
                                                    child: Text(
                                                        "${education[index]['city']}",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black54,
                                                            fontSize: 11,
                                                            fontFamily:
                                                                "Oswald",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w300)),
                                                  ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider()
                      ],
                    );
                  })),
                ),
                Container(
                  child: Column(
                      children: List.generate(school.length, (int index) {
                    return Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          margin: EdgeInsets.only(
                              top: 2.5, bottom: 2.5, left: 0, right: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: 20, right: 20, top: 0),
                                  padding: EdgeInsets.only(right: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      ////// <<<<< Profile picture >>>>> //////
                                      Container(
                                          margin: EdgeInsets.only(right: 10),
                                          child: Icon(Icons.book,
                                              size: 17, color: Colors.black54)),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              child: Text.rich(
                                                TextSpan(
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text: "Went to",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black54,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                "Oswald",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400)),
                                                    TextSpan(
                                                        text:
                                                            " ${school[index]['collage']}",
                                                        style: TextStyle(
                                                            color: mainColor,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                "Oswald",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                    // can add more TextSpans here...
                                                  ],
                                                ),
                                              ),
                                            ),
                                            school[index]['city'] == ""
                                                ? Container()
                                                : Container(
                                                    margin:
                                                        EdgeInsets.only(top: 5),
                                                    child: Text(
                                                        "${school[index]['city']}",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black54,
                                                            fontSize: 11,
                                                            fontFamily:
                                                                "Oswald",
                                                            fontWeight:
                                                                FontWeight
                                                                    .w300)),
                                                  ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider()
                      ],
                    );
                  })),
                ),
                locationCur[0]['city'] == null
                    ? Container()
                    : Container(
                        child: Column(
                            children:
                                List.generate(locationCur.length, (int index) {
                          return Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                margin: EdgeInsets.only(
                                    top: 2.5, bottom: 2.5, left: 0, right: 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            left: 20, right: 20, top: 0),
                                        padding: EdgeInsets.only(right: 10),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            ////// <<<<< Profile picture >>>>> //////
                                            Container(
                                                margin:
                                                    EdgeInsets.only(right: 10),
                                                child: Icon(Icons.location_on,
                                                    size: 17,
                                                    color: Colors.black54)),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                    child: Text.rich(
                                                      TextSpan(
                                                        children: <TextSpan>[
                                                          TextSpan(
                                                              text: "Lives in ",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize: 14,
                                                                  fontFamily:
                                                                      "Oswald",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400)),
                                                          TextSpan(
                                                              text:
                                                                  "${locationCur[index]['city']}",
                                                              style: TextStyle(
                                                                  color:
                                                                      mainColor,
                                                                  fontSize: 14,
                                                                  fontFamily:
                                                                      "Oswald",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                          // can add more TextSpans here...
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Divider()
                            ],
                          );
                        })),
                      ),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 15, left: 20),
                    child: Text(
                      "Birthday",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 17,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.normal),
                    )),
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 30,
                        margin: EdgeInsets.only(top: 10, left: 20),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            color: Colors.black54,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 3.0,
                                color: Colors.black54,
                                //offset: Offset(6.0, 7.0),
                              ),
                            ],
                            border:
                                Border.all(width: 0.5, color: Colors.black54)),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                  child: Row(
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(Icons.cake,
                              size: 17, color: Colors.black54)),
                      Expanded(
                        child: Text.rich(
                          TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: birthDate,
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 14,
                                      fontFamily: "Oswald",
                                      fontWeight: FontWeight.w300)),
                              // can add more TextSpans here...
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 35, left: 20),
                    child: Text(
                      "Mobile Number",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 17,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.normal),
                    )),
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 30,
                        margin: EdgeInsets.only(top: 10, left: 20),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            color: Colors.black54,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 3.0,
                                color: Colors.black54,
                                //offset: Offset(6.0, 7.0),
                              ),
                            ],
                            border:
                                Border.all(width: 0.5, color: Colors.black54)),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                  child: Row(
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(Icons.phone_android,
                              size: 17, color: Colors.black54)),
                      Expanded(
                        child: Text.rich(
                          TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: userData['phone'] == null
                                      ? "N/A"
                                      : "${userData['phone']}",
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 14,
                                      fontFamily: "Oswald",
                                      fontWeight: FontWeight.w300)),
                              // can add more TextSpans here...
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 35, left: 20),
                    child: Text(
                      "Email",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 17,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.normal),
                    )),
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 30,
                        margin: EdgeInsets.only(top: 10, left: 20),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            color: Colors.black54,
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 3.0,
                                color: Colors.black54,
                                //offset: Offset(6.0, 7.0),
                              ),
                            ],
                            border:
                                Border.all(width: 0.5, color: Colors.black54)),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                  child: Row(
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(Icons.email,
                              size: 17, color: Colors.black54)),
                      Expanded(
                        child: Text.rich(
                          TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: userData['email'] == null
                                      ? "N/A"
                                      : "${userData['email']}",
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 14,
                                      fontFamily: "Oswald",
                                      fontWeight: FontWeight.w300)),
                              // can add more TextSpans here...
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
  }
}
