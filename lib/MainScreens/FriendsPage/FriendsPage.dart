import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Cards/AllFriendsCard/allFriendsCard.dart';
import 'package:social_app_fb/MainScreens/LoaderScreen/LoaderScreen.dart';
import 'package:social_app_fb/ModelClass/FriendsModel/FriendsModel.dart';

class FriendsPage extends StatefulWidget {
  final userName;
  FriendsPage(this.userName);

  @override
  _FriendsPageState createState() => _FriendsPageState();
}

class _FriendsPageState extends State<FriendsPage> {
  SharedPreferences sharedPreferences;
  String theme = "";
  Timer _timer;
  int _start = 3;
  bool loading = true;
  var friendList;

  @override
  void initState() {
    loadFriends();
    super.initState();
  }

  Future loadFriends() async {
    //await Future.delayed(Duration(seconds: 3));

    setState(() {
      loading = true;
    });
    var frndresponse = await CallApi()
        .getData('profile/details/${widget.userName}?tab=friends');
    var postcontent = frndresponse.body;
    final friends = json.decode(postcontent);
    var frnddata = FriendsModel.fromJson(friends);

    setState(() {
      friendList = frnddata;
      loading = false;

      print("friendList.friends.length");
      print(friendList.friends.length);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Friends",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/white.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: null,
          ),
          loading
              ? LoaderScreen()
              : Container(
                  margin: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
                  child: CustomScrollView(
                    slivers: <Widget>[
                      ////// <<<<< All Friend Option >>>>> //////
                      SliverToBoxAdapter(
                        child: Column(
                          children: <Widget>[
                            ////// <<<<< Friend Number >>>>> //////
                            Container(
                              margin:
                                  EdgeInsets.only(top: 12, left: 20, bottom: 7),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                      margin:
                                          EdgeInsets.only(right: 0, bottom: 3),
                                      child: Icon(Icons.group,
                                          size: 17, color: Colors.black54)),
                                  Container(
                                      margin: EdgeInsets.only(bottom: 3),
                                      child: Text(
                                        " All friends ",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontSize: 13,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w400),
                                      )),
                                  Container(
                                      child: Text(
                                    "${friendList.friends.length}",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 18,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w400),
                                  )),
                                ],
                              ),
                            ),

                            ////// <<<<< Divider 5 >>>>> //////
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 40,
                                  margin: EdgeInsets.only(
                                      top: 0, left: 20, bottom: 12),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      color: Colors.black54,
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 3.0,
                                          color: Colors.black54,
                                          //offset: Offset(6.0, 7.0),
                                        ),
                                      ],
                                      border: Border.all(
                                          width: 0.5, color: Colors.black54)),
                                ),
                              ],
                            ),
                            friendList.friends.length == 0
                                ? Row(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(left: 20),
                                        child: Text(
                                          "No Friends!",
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ],
                                  )
                                : Container()
                          ],
                        ),
                      ),

                      ////// <<<<< All Members Card >>>>> //////
                      AllFriendsCard(friendList.friends),
                    ],
                  ),
                ),
        ],
      ),
    );
  }
}
