import 'dart:convert';
import 'dart:io';
import 'package:custom_switch_button/custom_switch_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:multi_media_picker/multi_media_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/AlbumPage/AlbumPage.dart';

import '../../main.dart';

class AlbumCreatePage extends StatefulWidget {
  @override
  _AlbumCreatePageState createState() => _AlbumCreatePageState();
}

class _AlbumCreatePageState extends State<AlbumCreatePage> {
  bool isChecked = false;
  bool isImageLoading = false;
  String status = "Public", desc = "";
  String albumDate = "";
  List allImages = [];
  List images = [];
  List imagesBase64 = [];
  List<File> resultList;
  var userData;
  TextEditingController nameController = new TextEditingController();
  TextEditingController descController = new TextEditingController();

  @override
  void initState() {
    DateTime date1 = DateTime.now();

    albumDate = DateFormat("yyyy-MM-dd").format(date1);
    _getUserInfo();
    super.initState();
  }

  void _statusModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                  leading: new Icon(Icons.public),
                  title: new Text('Public',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  trailing: status == "Public"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "Public";
                    }),
                    Navigator.pop(context)
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.lock_outline),
                  title: new Text('Only Me',
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontFamily: "Oswald")),
                  trailing: status == "Only Me"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "Only Me";
                    }),
                    Navigator.pop(context)
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.group),
                  title: new Text('Friends',
                      style: TextStyle(fontWeight: FontWeight.normal)),
                  trailing: status == "3"
                      ? Icon(Icons.done, color: mainColor)
                      : Icon(Icons.done, color: Colors.transparent),
                  onTap: () => {
                    setState(() {
                      status = "3";
                    }),
                    Navigator.pop(context)
                  },
                ),
              ],
            ),
          );
        });
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
  }

  Future albumUpload() async {
    if (nameController.text.isEmpty) {
      _showMessage("Album Name shouldn't be empty!", 1);
    } else if (allImages.length == 0) {
      _showMessage("Minimum 1 picture need to be selected!", 1);
    } else {
      setState(() {
        isImageLoading = true;
      });
      var data = {
        'name': nameController.text,
        'description': desc,
        'privacy': status,
        'allImages': allImages,
        'user_id': userData['id'],
      };

      print(data);

      var res = await CallApi().postData1(data, 'profile/album/update');
      var body = json.decode(res.body);
      print(body);

      if (res.statusCode == 200) {
        setState(() {
          albumList.add(
            {
              'id': '${body['id']}',
              'pic': '${allImages[0]['url']}',
              'count': allImages.length,
              'name': '${nameController.text}',
              'desc': '$desc',
              'allPic': allImages,
              'date': albumDate,
            },
          );
        });
        Navigator.pop(context);
        // Navigator.push(
        //     context, MaterialPageRoute(builder: (context) => AlbumPage()));
      } else {
        _showMessage("Something went wrong!", 1);
      }

      setState(() {
        isImageLoading = false;
      });
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Create Album",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
              GestureDetector(
                onTap: () {
                  albumUpload();
                },
                child: Container(
                    child: Text("Create",
                        style: TextStyle(color: mainColor, fontSize: 15))),
              )
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          child: Column(children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 10, top: 20, left: 20, right: 20),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(left: 0, right: 0, top: 0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 3.0,
                        color: Colors.black.withOpacity(.2),
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(left: 5, top: 15),
                          child: Icon(
                            Icons.photo,
                            size: 19,
                            color: mainColor,
                          )),
                      Flexible(
                        child: ConstrainedBox(
                          constraints: BoxConstraints(maxHeight: 50),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: TextField(
                              maxLines: null,
                              controller: nameController,
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: 'Oswald',
                              ),
                              decoration: InputDecoration(
                                hintText: "Album Name",
                                hintStyle: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w300),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10, top: 0, left: 20, right: 20),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(left: 0, right: 0, top: 0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 3.0,
                        color: Colors.black.withOpacity(.2),
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(left: 5, top: 15),
                          child: Icon(
                            Icons.info,
                            size: 19,
                            color: mainColor,
                          )),
                      Flexible(
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                              maxHeight: 200.0, minHeight: 100.0),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: TextField(
                              maxLines: null,
                              controller: descController,
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: 'Oswald',
                              ),
                              decoration: InputDecoration(
                                hintText: "Album Description (Optional)",
                                hintStyle: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w300),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                                border: InputBorder.none,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  desc = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
            Divider(),
            GestureDetector(
              onTap: () {
                _statusModalBottomSheet(context);
              },
              child: Container(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                ),
                margin: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 20, right: 20, top: 0),
                        padding: EdgeInsets.only(right: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ////// <<<<< Profile picture >>>>> //////
                            Stack(
                              children: <Widget>[
                                ////// <<<<< Picture >>>>> //////
                                Container(
                                  margin: EdgeInsets.only(right: 10),
                                  //padding: EdgeInsets.all(5.0),
                                  child: Icon(
                                    status == "Public"
                                        ? Icons.public
                                        : status == "Only Me"
                                            ? Icons.lock_outline
                                            : Icons.group,
                                    color: Colors.black38,
                                    size: 20,
                                  ),
                                  decoration: new BoxDecoration(
                                    //color: Colors.grey[300],
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ],
                            ),

                            ////// <<<<< User Name >>>>> //////
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    status == "Public"
                                        ? "Public"
                                        : status == "Only Me"
                                            ? "Only Me"
                                            : "Friends",
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black54,
                                        fontFamily: 'Oswald',
                                        fontWeight: FontWeight.w600),
                                  ),

                                  ////// <<<<< Mutual Friends >>>>> //////
                                  Container(
                                    margin: EdgeInsets.only(top: 3),
                                    child: Text(
                                      "Anyone on or off Social App",
                                      style: TextStyle(
                                          fontFamily: 'Oswald',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12.5,
                                          color: Colors.black45),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 15, left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Add Pictures",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 15,
                              fontFamily: "Oswald",
                              fontWeight: FontWeight.bold),
                        ),
                        // GestureDetector(
                        //   onTap: () {
                        //     setState(() {
                        //       isChecked = !isChecked;
                        //     });
                        //   },
                        //   child: Container(
                        //     padding: EdgeInsets.only(right: 20),
                        //     child: CustomSwitchButton(
                        //       // buttonWidth: 40,
                        //       // buttonHeight: 20,
                        //       backgroundColor: !isChecked
                        //           ? Colors.grey.withOpacity(0.3)
                        //           : Colors.blue[100],
                        //       unCheckedColor: Colors.grey.withOpacity(0.7),
                        //       animationDuration: Duration(milliseconds: 400),
                        //       checkedColor: mainColor.withOpacity(0.7),
                        //       checked: isChecked,
                        //     ),
                        //   ),
                        // ),
                      ],
                    )),
                Row(
                  children: <Widget>[
                    Container(
                      width: 30,
                      margin: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          color: Colors.black54,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 3.0,
                              color: Colors.black54,
                              //offset: Offset(6.0, 7.0),
                            ),
                          ],
                          border:
                              Border.all(width: 0.5, color: Colors.black54)),
                    ),
                  ],
                ),
                !isChecked
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(left: 10),
                        padding: EdgeInsets.all(10),
                        child: Text(
                          "Choose Friends",
                          style: TextStyle(
                              color: Colors.black87,
                              fontSize: 14,
                              fontFamily: "Oswald",
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                Container(
                  margin: EdgeInsets.only(top: 0, left: 0, right: 10),
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: initMultiPickUp,
                        child: Container(
                          margin: EdgeInsets.only(top: 0, left: 10, right: 0),
                          padding: EdgeInsets.only(
                              left: 10, top: 5, right: 10, bottom: 10),
                          child: Text(
                            "Choose from gallery (min 1 picture)",
                            style: TextStyle(
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w400,
                                fontSize: 13,
                                color: Colors.black45),
                          ),
                        ),
                      ),
                      isImageLoading == false
                          ? Container()
                          : Container(
                              height: 30,
                              margin: EdgeInsets.only(bottom: 20),
                              child: SpinKitCircle(color: Colors.black26)),
                    ],
                  ),
                ),
                resultList == null
                    ? Container()
                    : Container(
                        //height: 100,
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        margin: EdgeInsets.only(
                            top: 0, bottom: 0, left: 15, right: 15),
                        child: Column(
                            children: List.generate(resultList.length, (index) {
                          return Container(
                            height: 220,
                            //width: 150,
                            alignment: Alignment.topRight,
                            decoration: BoxDecoration(
                                color: Colors.grey[300],
                                border:
                                    Border.all(color: Colors.grey, width: 0.3),
                                borderRadius: BorderRadius.circular(5),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: FileImage(resultList[index]))),
                            margin: EdgeInsets.only(
                                left: 5, right: 5, top: 5, bottom: 5),
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  resultList.removeAt(index);
                                  //images = img;
                                  print(resultList.length);
                                });
                              },
                              child: Container(
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: Colors.black87.withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Icon(
                                    Icons.close,
                                    color: Colors.white,
                                  )),
                            ),
                          );
                        })),
                      ),
              ],
            ),
          ]),
        ),
      ),
    );
  }

  initMultiPickUp() async {
    setState(() {
      allImages = [];
    });
    //List<File> resultList;
    resultList = await MultiMediaPicker.pickImages(
        source: ImageSource.gallery, maxHeight: 480, maxWidth: 640);
    setState(() {
      //resultList = imgs;
      if (resultList != null) {
        setState(() {
          isImageLoading = true;
        });
        uploadImages(resultList);
      }
    });
  }

  uploadImages(images) async {
    if (images != null) {
      for (int i = 0; i < images.length; i++) {
        //File file = new File(resultList[i].toString());
        List<int> imageBytes = images[i].readAsBytesSync();
        String image = base64.encode(imageBytes);
        image = 'data:image/png;base64,' + image;
        var data3 = {'image': image};
        print(data3);
        var res1 = await CallApi().postData1(data3, 'album/upload/images');
        var body1 = json.decode(res1.body);
        print("image success");
        print(body1);
        if (res1.statusCode == 200) {
          setState(() {
            isImageLoading = false;
            allImages.add({'url': body1['url'], 'created_at': albumDate});
          });
        }
      }
    } else {
      images = [];
    }
  }
}
