import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/Cards/ProfileCard/profileCard.dart';
import 'package:social_app_fb/MainScreens/LoginRegisterPage/LoginRegisterPage.dart';
import 'package:social_app_fb/MainScreens/ShareToNetworkPage/ShareToNetworkPage.dart';

class MorePage extends StatefulWidget {
  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  var userData;

  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
  }

  Future<void> logout() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove("token");
    localStorage.remove("user");
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginRegisterPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: userData == null
          ? Center(child: CircularProgressIndicator())
          : CustomScrollView(slivers: <Widget>[
              SliverToBoxAdapter(
                child: Container(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ////// <<<<< Profile Card >>>>> //////
                      ProfileCard(userData),

                      //// <<<<< Group Cards >>>>> //////
                      // GestureDetector(
                      //   onTap: () {
                      //     Navigator.push(
                      //         context,
                      //         MaterialPageRoute(
                      //             builder: (context) => GroupPage()));
                      //   },
                      //   child: profileContainer(
                      //       Icon(
                      //         Icons.group,
                      //         color: Colors.black45,
                      //         size: 20,
                      //       ),
                      //       "Groups"),
                      // ),

                      ////// <<<<< Interests Edit Cards >>>>> //////
                      GestureDetector(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) =>
                          //             ProfileInterestEditPage(userData)));
                          //_showMsg("Coming Soon");
                        },
                        child: profileContainer(
                            Icon(
                              Icons.book,
                              color: Colors.black45,
                              size: 20,
                            ),
                            "Pages"),
                      ),

                      GestureDetector(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) =>
                          //             BusinessInfoPage(userData)));
                          //_showMsg("Coming Soon");
                        },
                        child: profileContainer(
                            Icon(
                              Icons.event,
                              color: Colors.black45,
                              size: 20,
                            ),
                            "Events"),
                      ),

                      GestureDetector(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) =>
                          //             BusinessInfoPage(userData)));
                          //_showMsg("Coming Soon");
                        },
                        child: profileContainer(
                            Icon(
                              Icons.switch_video,
                              color: Colors.black45,
                              size: 20,
                            ),
                            "Videos"),
                      ),

                      ////// <<<<< Settings Cards >>>>> //////
                      GestureDetector(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => LearnMorePage()));
                          //_showMsg("Coming Soon");
                        },
                        child: profileContainer(
                            Icon(
                              Icons.label_important,
                              color: Colors.black45,
                              size: 20,
                            ),
                            "Saved"),
                      ),

                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ShareToNetworkPage()));
                          //_showMsg("Coming Soon");
                        },
                        child: profileContainer(
                            Icon(
                              Icons.share,
                              color: Colors.black45,
                              size: 20,
                            ),
                            "Share To"),
                      ),

                      ////// <<<<< Logout Cards >>>>> //////
                      GestureDetector(
                        onTap: () {
                          logout();
                        },
                        child: profileContainer(
                            Icon(
                              Icons.power_settings_new,
                              color: Colors.black45,
                              size: 20,
                            ),
                            "Logout"),
                      ),
                    ],
                  ),
                ),
              )
            ]),
    );
  }

  Container profileContainer(Icon icon, String text) {
    return Container(
      padding: EdgeInsets.only(top: 13, bottom: 13),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        //border: Border.all(width: 0.8, color: Colors.grey[300]),
        boxShadow: [
          BoxShadow(
            blurRadius: 5.0,
            color: Colors.grey[300],
            //offset: Offset(3.0, 4.0),
          ),
        ],
      ),
      margin: EdgeInsets.only(top: 2.5, bottom: 2.5, left: 0, right: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            //color: Colors.red,
            margin: EdgeInsets.only(left: 20, right: 20, top: 0),
            padding: EdgeInsets.only(right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(margin: EdgeInsets.only(right: 10), child: icon),
                Container(
                  child: Text(
                    text,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
          ),
          Container(
              padding: EdgeInsets.all(2),
              margin: EdgeInsets.only(right: 15),
              child: Icon(
                Icons.chevron_right,
                color: Colors.black45,
                size: 22,
              )),
        ],
      ),
    );
  }
}
