import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/HomePage/HomePage.dart';
import 'package:social_app_fb/main.dart';

class StoryAddPage extends StatefulWidget {
  @override
  _StoryAddPageState createState() => _StoryAddPageState();
}

class _StoryAddPageState extends State<StoryAddPage> {
  String text = "Write something...", image = "";
  TextEditingController storyController = new TextEditingController();
  bool isEdit = false;
  var userData;
  File storyImgFile;
  var imageFile;

  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        elevation: 0,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Add Story",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              onTap: () {
                _textModalBottomSheet(context);
              },
              child: Container(
                margin: EdgeInsets.only(top: 20, left: 20, right: 8),
                height: 150,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(15)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.text_fields,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Text("Text",
                            style:
                                TextStyle(color: Colors.white, fontSize: 17))),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                _pickImage();
              },
              child: Container(
                margin: EdgeInsets.only(top: 20, left: 8, right: 20),
                height: 150,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(15)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.photo,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Text("Image",
                            style:
                                TextStyle(color: Colors.white, fontSize: 17))),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _textModalBottomSheet(context) {
    showModalBottomSheet(
        backgroundColor: storyImgFile != null ? Colors.black26 : mainColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return StatefulBuilder(
            builder: (context, setState) {
              return SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height - 50,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                      alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.all(15),
                                      child: Icon(
                                        Icons.close,
                                        color: Colors.white,
                                      )),
                                ),
                                isEdit
                                    ? GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            isEdit = false;
                                          });
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(right: 15),
                                          padding: EdgeInsets.all(5),
                                          child: Text(
                                            "Done",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 13.5),
                                          ),
                                        ),
                                      )
                                    : Container(),
                              ],
                            ),
                            isEdit
                                ? Container()
                                : GestureDetector(
                                    onTap: () {
                                      addStory();
                                    },
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.all(10),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            margin: EdgeInsets.only(
                                                right: 10, bottom: 10),
                                            child: Row(
                                              children: <Widget>[
                                                Text(
                                                  "Add to story",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 13.5),
                                                ),
                                                Icon(Icons.chevron_right,
                                                    size: 14)
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                          ],
                        ),
                      ),
                      storyImgFile != null
                          ? Container(
                              height: 150,
                              padding: const EdgeInsets.all(0.0),
                              decoration: BoxDecoration(
                                  //color: header,

                                  image: DecorationImage(
                                      image: FileImage(storyImgFile),
                                      fit: BoxFit.cover)),
                              child: null)
                          : isEdit
                              ? Container(
                                  margin: EdgeInsets.only(top: 70),
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(
                                        maxHeight: 300.0, minHeight: 100.0),
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: TextField(
                                        maxLines: null,
                                        autofocus: true,
                                        textAlign: TextAlign.center,
                                        controller: storyController,
                                        cursorColor: Colors.white,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Oswald',
                                        ),
                                        decoration: InputDecoration(
                                          hintText: "",
                                          hintStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300),
                                          contentPadding: EdgeInsets.fromLTRB(
                                              10.0, 0.0, 10.0, 0.0),
                                          border: InputBorder.none,
                                        ),
                                        onChanged: (value) {
                                          setState(() {
                                            text = value;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                )
                              : GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isEdit = true;
                                    });
                                    //_showEditDialog();
                                  },
                                  child: Center(
                                    child: Container(
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 30, right: 30, top: 5),
                                        decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Text(
                                          text,
                                          style: TextStyle(
                                              color:
                                                  text == "Write something..."
                                                      ? Colors.white
                                                          .withOpacity(0.5)
                                                      : Colors.white,
                                              fontSize: 20),
                                        )),
                                  ),
                                ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  _pickImage() async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile != null) {
      setState(() {
        storyImgFile = imageFile;
      });
    }

    uploadStory();
  }

  Future uploadStory() async {
    List<int> imageBytes = storyImgFile.readAsBytesSync();
    String dp = base64.encode(imageBytes);
    dp = 'data:image/png;base64,' + dp;

    var data = {'image': dp};
    print(data);

    var res1 = await CallApi().postData1(data, 'album/upload/images');
    var body1 = json.decode(res1.body);
    print(body1);

    if (res1.statusCode == 200) {
      _textModalBottomSheet(context);
      setState(() {
        image = body1['url'];
      });
    } else {
      //_showMessage("Failed to upload cover picture!", 1);
    }
  }

  Future<void> addStory() async {
    if (text == "Write something...") {
    } else {
      var data = {
        'user_id': userData['id'],
        'text': storyController.text,
        'image': image,
        'privacy': 'Public',
        'firstName': userData['firstName'],
        'lastName': userData['lastName'],
        'userName': userData['userName'],
        'profilePic': userData['profilePic'],
      };

      print(data);

      var res = await CallApi().postData1(data, 'story/create');
      var body = json.decode(res.body);
      print(body);

      if (res.statusCode == 200) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      }
    }
  }

  Future<Null> _showEditDialog() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return new AlertDialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(maxHeight: 200.0, minHeight: 100.0),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: TextField(
                            maxLines: null,
                            autofocus: true,
                            textAlign: TextAlign.center,
                            controller: storyController,
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Oswald',
                            ),
                            decoration: InputDecoration(
                              hintText: "",
                              hintStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.w300),
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                              border: InputBorder.none,
                            ),
                            onChanged: (value) {
                              setState(() {
                                text = value;
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(5),
                                margin: EdgeInsets.only(
                                    left: 0, right: 5, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.5),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Icon(
                                  Icons.close,
                                  color: Colors.black54,
                                  size: 15,
                                )),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Container(
                                padding: EdgeInsets.all(5),
                                margin: EdgeInsets.only(
                                    left: 5, right: 0, top: 20, bottom: 0),
                                decoration: BoxDecoration(
                                    color: mainColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100))),
                                child: Icon(
                                  Icons.done,
                                  color: Colors.white,
                                  size: 15,
                                )),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
