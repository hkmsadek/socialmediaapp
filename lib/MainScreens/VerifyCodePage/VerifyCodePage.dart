import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/ResetPassPage/ResetPassPage.dart';

import '../../main.dart';

class VerifyCodePage extends StatefulWidget {
  @override
  _VerifyCodePageState createState() => _VerifyCodePageState();
}

class _VerifyCodePageState extends State<VerifyCodePage> {
  TextEditingController codeController = new TextEditingController();
  bool isSubmit = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Verify email",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 30, left: 30, right: 30),
                child: Text(
                  "A code has been sent to your email. Use that code here to reset your password",
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 5, top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: codeController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Oswald',
                          ),
                          decoration: InputDecoration(
                            hintText: "Code",
                            hintStyle: TextStyle(
                                color: Colors.black38,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w300),
                            //labelStyle: TextStyle(color: Colors.white70),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            border: InputBorder.none,
                          ),
                        )),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  isSubmit ? null : signIn();
                },
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
                    margin: EdgeInsets.only(
                        left: 30, right: 30, top: 5, bottom: 20),
                    decoration: BoxDecoration(
                        color:
                            isSubmit ? Colors.grey.withOpacity(0.5) : mainColor,
                        border: Border.all(color: Colors.grey, width: 0.2),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Text(
                      isSubmit ? "Please wait..." : "Send",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.w500),
                    )),
              ),
            ],
          )),
    );
  }

  Future<void> signIn() async {
    if (codeController.text.isEmpty) {
      _showMessage("Code cannot be blank!", 1);
    } else {
      setState(() {
        isSubmit = true;
      });

      var data = {
        'token': codeController.text,
      };

      print(data);

      var res =
          await CallApi().postUrlData(data, 'user/verify/passwordResetCode');
      print(res.body);

      if (res.statusCode == 200) {
        _showMessage("Code verified successfully!", 2);

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ResetPassPage(codeController.text)));
      } else {
        _showMessage("Something went wrong!", 1);
      }
    }
    setState(() {
      isSubmit = false;
    });
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
