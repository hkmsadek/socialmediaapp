import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/MainScreens/VerifyCodePage/VerifyCodePage.dart';

import '../../main.dart';

class ForgetPassPage extends StatefulWidget {
  @override
  _ForgetPassPageState createState() => _ForgetPassPageState();
}

class _ForgetPassPageState extends State<ForgetPassPage> {
  TextEditingController emailController = new TextEditingController();
  bool isSubmit = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Forget Password",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Text(
                  "Insert your email to reset your password",
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 5, top: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.only(left: 30, right: 30, top: 5),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            border: Border.all(color: Colors.grey, width: 0.2),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: emailController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            color: Colors.black87,
                            fontFamily: 'Oswald',
                          ),
                          decoration: InputDecoration(
                            hintText: "Email",
                            hintStyle: TextStyle(
                                color: Colors.black38,
                                fontSize: 15,
                                fontFamily: 'Oswald',
                                fontWeight: FontWeight.w300),
                            //labelStyle: TextStyle(color: Colors.white70),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                            border: InputBorder.none,
                          ),
                        )),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  isSubmit ? null : signIn();
                },
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 10, top: 15, bottom: 15),
                    margin: EdgeInsets.only(
                        left: 30, right: 30, top: 5, bottom: 20),
                    decoration: BoxDecoration(
                        color:
                            isSubmit ? Colors.grey.withOpacity(0.5) : mainColor,
                        border: Border.all(color: Colors.grey, width: 0.2),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Text(
                      isSubmit ? "Please wait..." : "Send",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.w500),
                    )),
              ),
            ],
          )),
    );
  }

  Future<void> signIn() async {
    if (emailController.text.isEmpty) {
      _showMessage("Email cannot be blank!", 1);
    } else {
      setState(() {
        isSubmit = true;
      });

      var data = {
        'email': emailController.text,
      };

      print(data);

      var res =
          await CallApi().postUrlData(data, 'user/send/passwordResetCode');
      //var body = json.decode(res.body);
      print(res.body);

      if (res.statusCode == 200) {
        _showMessage("Email sent successfully!", 2);

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => VerifyCodePage()));
      } else {
        _showMessage("Something went wrong!", 1);
      }
    }
    setState(() {
      isSubmit = false;
    });
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
