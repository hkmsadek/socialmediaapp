import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';

class PendingPostsPage extends StatefulWidget {
  @override
  _PendingPostsPageState createState() => _PendingPostsPageState();
}

class _PendingPostsPageState extends State<PendingPostsPage> {
  bool loading = true;
  var userData;
  SharedPreferences sharedPreferences;
  String theme = "";
  Timer _timer;
  int _start = 3;

  @override
  void initState() {
    _getUserInfo();
    sharedPrefcheck();
    timerCheck();
    super.initState();
  }

  void sharedPrefcheck() async {
    sharedPreferences = await SharedPreferences.getInstance();

    setState(() {
      theme = sharedPreferences.getString("theme");
    });
    //print(theme);
  }

  void timerCheck() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            timer.cancel();
            setState(() {
              loading = false;
            });
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    if (userJson != null) {
      var user = json.decode(userJson);
      setState(() {
        userData = user;
      });
      print(userData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Pending Posts",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          child: Column(
              children: List.generate(3, (index) {
            return loading == false
                ? Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                //color: Colors.red,
                                margin: EdgeInsets.only(
                                    left: 20, right: 20, top: 15),
                                padding: EdgeInsets.only(right: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    ////// <<<<< Picture start >>>>> //////
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                      padding: EdgeInsets.all(1.0),
                                      child: CircleAvatar(
                                        radius: 20.0,
                                        backgroundColor: Colors.white,
                                        backgroundImage: AssetImage(
                                            'assets/images/prabal.jpg'),
                                      ),
                                      decoration: new BoxDecoration(
                                        color: Colors.grey[300], // border color
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                    ////// <<<<< Picture end >>>>> //////

                                    Expanded(
                                      child: Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            ////// <<<<< Name start >>>>> //////
                                            Container(
                                              child: Text(
                                                "${userData['firstName']} ${userData['lastName']}",
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    color: Colors.black54,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                            ////// <<<<< Name start >>>>> //////

                                            ////// <<<<< Time start >>>>> //////
                                            Container(
                                              margin: EdgeInsets.only(top: 3),
                                              child: Row(
                                                children: <Widget>[
                                                  Text(
                                                    "Apr 8, 20",
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontFamily: 'Oswald',
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 11,
                                                        color: Colors.black45),
                                                  ),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          left: 5),
                                                      child: Icon(
                                                        Icons.public,
                                                        color: Colors.black45,
                                                        size: 12,
                                                      ))
                                                ],
                                              ),
                                            ),
                                            ////// <<<<< Time end >>>>> //////
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            ////// <<<<< More start >>>>> //////
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: Colors.grey.withOpacity(0.2)),
                                      padding: EdgeInsets.all(5),
                                      margin: EdgeInsets.only(right: 10),
                                      child: Icon(
                                        Icons.done,
                                        color: Colors.black45,
                                        size: 14,
                                      )),
                                  Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: Colors.grey.withOpacity(0.2)),
                                      padding: EdgeInsets.all(5),
                                      margin: EdgeInsets.only(right: 15),
                                      child: Icon(
                                        Icons.close,
                                        color: Colors.black45,
                                        size: 14,
                                      )),
                                ],
                              ),
                            ),
                            ////// <<<<< More end >>>>> //////
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 20, right: 20, top: 20),
                          child: index == 0
                              ? Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Text(
                                    "Honesty is the best policy",
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontWeight: FontWeight.w400),
                                  ),
                                )
                              : index == 1
                                  ? Container(
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              "Tour to nearest hill",
                                              textAlign: TextAlign.justify,
                                              style: TextStyle(
                                                  color: Colors.black54,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                          Container(
                                              //color: Colors.red,
                                              height: 200,
                                              padding:
                                                  const EdgeInsets.all(0.0),
                                              margin: EdgeInsets.only(top: 10),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          "assets/images/f7.jpg"),
                                                      fit: BoxFit.cover)),
                                              child: null),
                                        ],
                                      ),
                                    )
                                  : Container(
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              "Bike and Car Riding",
                                              textAlign: TextAlign.justify,
                                              style: TextStyle(
                                                  color: Colors.black54,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                          Container(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: Container(
                                                            //color: Colors.red,
                                                            height: 150,
                                                            //width: 150,
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0.0),
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    right: 5),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius.circular(
                                                                        5),
                                                                image: DecorationImage(
                                                                    image: AssetImage(
                                                                        "assets/images/bike1.jpg"),
                                                                    fit: BoxFit
                                                                        .cover)),
                                                            child: null),
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                            //color: Colors.red,
                                                            height: 150,
                                                            // width: 150,
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0.0),
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    left: 5),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius.circular(
                                                                        5),
                                                                image: DecorationImage(
                                                                    image: AssetImage(
                                                                        "assets/images/car3.jpeg"),
                                                                    fit: BoxFit
                                                                        .cover)),
                                                            child: null),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: Container(
                                                            //color: Colors.red,
                                                            height: 150,
                                                            //width: 150,
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0.0),
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    right: 5),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius.circular(
                                                                        5),
                                                                image: DecorationImage(
                                                                    image: AssetImage(
                                                                        "assets/images/car6.jpg"),
                                                                    fit: BoxFit
                                                                        .cover)),
                                                            child: null),
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                            //color: Colors.red,
                                                            height: 150,
                                                            // width: 150,
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0.0),
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    left: 5),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius.circular(
                                                                        5),
                                                                image: DecorationImage(
                                                                    image: AssetImage(
                                                                        "assets/images/bike3.jpeg"),
                                                                    fit: BoxFit
                                                                        .cover)),
                                                            child: Container(
                                                              height: 150,
                                                              // width: 150,
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(0.0),
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      top: 0,
                                                                      left: 0),
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: Colors
                                                                    .black
                                                                    .withOpacity(
                                                                        0.6),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                              ),
                                                              child: Center(
                                                                  child:
                                                                      Container(
                                                                          child:
                                                                              Text(
                                                                "+5",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        22),
                                                              ))),
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                      Container(
                          margin: EdgeInsets.only(
                              left: 20, right: 20, bottom: 0, top: 10),
                          child: Divider(
                            color: Colors.grey[300],
                          )),
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 0),
                        padding: EdgeInsets.all(0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            ////// <<<<< Like start >>>>> //////
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(3.0),
                                    child: Icon(
                                      Icons.favorite_border,
                                      size: 20,
                                      color: Colors.black54,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 3),
                                    child: Text("0",
                                        style: TextStyle(
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300,
                                            color: Colors.black54,
                                            fontSize: 12)),
                                  )
                                ],
                              ),
                            ),
                            ////// <<<<< Like end >>>>> //////

                            ////// <<<<< Comment start >>>>> //////
                            GestureDetector(
                              onTap: () {
                                // Navigator.push(
                                //     context,
                                //     MaterialPageRoute(
                                //         builder: (context) => CommentPage(
                                //             userData, index)));
                              },
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(left: 15),
                                      padding: EdgeInsets.all(3.0),
                                      child: Icon(
                                        Icons.chat_bubble_outline,
                                        size: 20,
                                        color: Colors.black54,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 3),
                                      child: Text("0",
                                          style: TextStyle(
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300,
                                              color: Colors.black54,
                                              fontSize: 12)),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            ////// <<<<< Comment end >>>>> //////

                            ////// <<<<< Share start >>>>> //////
                            // GestureDetector(
                            //   onTap: () {
                            //     _shareModalBottomSheet(
                            //         context, index, userData);
                            //   },
                            //   child: Container(
                            //     child: Row(
                            //       children: <Widget>[
                            //         Container(
                            //           margin:
                            //               EdgeInsets.only(left: 15),
                            //           padding: EdgeInsets.all(3.0),
                            //           child: Icon(
                            //             Icons.share,
                            //             size: 20,
                            //             color: Colors.black54,
                            //           ),
                            //         ),
                            //         Container(
                            //           margin:
                            //               EdgeInsets.only(left: 3),
                            //           child: Text("0",
                            //               style: TextStyle(
                            //                   fontFamily: 'Oswald',
                            //                   fontWeight:
                            //                       FontWeight.w300,
                            //                   color: Colors.black54,
                            //                   fontSize: 12)),
                            //         )
                            //       ],
                            //     ),
                            //   ),
                            // ),
                            ////// <<<<< Share end >>>>> //////
                          ],
                        ),
                      ),
                      Container(
                          height: 10,
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(5)),
                          margin: EdgeInsets.only(
                              left: 0, right: 0, bottom: 0, top: 7),
                          child: null),
                    ],
                  )
                : Container(
                    padding: EdgeInsets.only(top: 20, bottom: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      //border: Border.all(width: 0.8, color: Colors.grey[300]),
                      // boxShadow: [
                      //   BoxShadow(
                      //     blurRadius: 1.0,
                      //     color: Colors.black38,
                      //     //offset: Offset(6.0, 7.0),
                      //   ),
                      // ],
                    ),
                    margin:
                        EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              //color: Colors.red,
                              margin:
                                  EdgeInsets.only(left: 10, right: 10, top: 0),
                              padding: EdgeInsets.only(right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    //transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                                    padding: EdgeInsets.all(1.0),
                                    child: Shimmer.fromColors(
                                      baseColor: Colors.grey[100],
                                      highlightColor: Colors.grey[200],
                                      child: CircleAvatar(
                                        radius: 20.0,
                                        //backgroundColor: Colors.white,
                                      ),
                                    ),
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Shimmer.fromColors(
                                        baseColor: Colors.grey[100],
                                        highlightColor: Colors.grey[200],
                                        child: Container(
                                          width: 100,
                                          height: 22,
                                          child: Container(
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 3),
                                        child: Shimmer.fromColors(
                                          baseColor: Colors.grey[100],
                                          highlightColor: Colors.grey[200],
                                          child: Container(
                                            width: 50,
                                            height: 12,
                                            child: Container(
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(
                                    left: 20, right: 20, top: 20, bottom: 0),
                                child: Shimmer.fromColors(
                                  baseColor: Colors.grey[100],
                                  highlightColor: Colors.grey[200],
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 10,
                                    child: Container(
                                      color: Colors.black,
                                    ),
                                  ),
                                )),
                            Container(
                                margin: EdgeInsets.only(
                                    left: 20, right: 20, top: 2, bottom: 5),
                                child: Shimmer.fromColors(
                                  baseColor: Colors.grey[100],
                                  highlightColor: Colors.grey[200],
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width - 100,
                                    height: 10,
                                    child: Container(
                                      color: Colors.black,
                                    ),
                                  ),
                                )),
                          ],
                        ),
                        Container(
                            height: 10,
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(5)),
                            margin: EdgeInsets.only(
                                left: 0, right: 0, bottom: 0, top: 7),
                            child: null),
                      ],
                    ),
                  );
          })),
        ),
      ),
    );
  }
}
