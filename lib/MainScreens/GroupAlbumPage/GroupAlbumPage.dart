import 'package:flutter/material.dart';

import '../../main.dart';

class GroupAlbumPage extends StatefulWidget {
  @override
  _GroupAlbumPageState createState() => _GroupAlbumPageState();
}

class _GroupAlbumPageState extends State<GroupAlbumPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Group Album",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
          alignment: Alignment.topCenter,
          child: Wrap(children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                          right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                      height: 130,
                      width: 130,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              image: AssetImage('assets/images/white.jpg'),
                              fit: BoxFit.cover)),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                      height: 130,
                      width: 130,
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.3),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Icon(Icons.add),
                    ),
                  ],
                ),
                Container(
                  width: 120,
                  child: Text(
                    "Create Album",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontSize: 13,
                        color: Colors.black54,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                  height: 130,
                  width: 130,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage('assets/images/car1.jpg'),
                          fit: BoxFit.cover)),
                ),
                Container(
                  width: 120,
                  child: Text(
                    "Car travelling last week",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontSize: 13,
                        color: mainColor,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  width: 120,
                  margin: EdgeInsets.only(top: 3),
                  child: Text(
                    "9 photos",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400,
                        fontSize: 11,
                        color: Colors.black45),
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      right: 2.5, left: 2.5, bottom: 2.5, top: 5.5),
                  height: 130,
                  width: 130,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage('assets/images/car2.jpg'),
                          fit: BoxFit.cover)),
                ),
                Container(
                  width: 120,
                  child: Text(
                    "Car travelling 2",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontSize: 13,
                        color: mainColor,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  width: 120,
                  margin: EdgeInsets.only(top: 3),
                  child: Text(
                    "19 photos",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400,
                        fontSize: 11,
                        color: Colors.black45),
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      right: 2.5, left: 2.5, bottom: 2.5, top: 15.5),
                  height: 130,
                  width: 130,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage('assets/images/car3.jpeg'),
                          fit: BoxFit.cover)),
                ),
                Container(
                  width: 120,
                  child: Text(
                    "Car travelling 3",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontSize: 13,
                        color: mainColor,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  width: 120,
                  margin: EdgeInsets.only(top: 3),
                  child: Text(
                    "10 photos",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400,
                        fontSize: 11,
                        color: Colors.black45),
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      right: 2.5, left: 2.5, bottom: 2.5, top: 15.5),
                  height: 130,
                  width: 130,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage('assets/images/bike1.jpg'),
                          fit: BoxFit.cover)),
                ),
                Container(
                  width: 120,
                  child: Text(
                    "Bike Riding",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontSize: 13,
                        color: mainColor,
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  width: 120,
                  margin: EdgeInsets.only(top: 3),
                  child: Text(
                    "3 photos",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Oswald',
                        fontWeight: FontWeight.w400,
                        fontSize: 11,
                        color: Colors.black45),
                  ),
                ),
              ],
            ),
          ]),
        ),
      ),
    );
  }
}
