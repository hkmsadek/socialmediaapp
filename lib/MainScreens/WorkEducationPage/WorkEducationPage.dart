import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:social_app_fb/API/api.dart';

import '../../main.dart';

class WorkEducationPage extends StatefulWidget {
  @override
  _WorkEducationPageState createState() => _WorkEducationPageState();
}

class _WorkEducationPageState extends State<WorkEducationPage> {
  bool isWork = false;
  bool isSkill = false;
  bool isCollege = false;
  bool isSchool = false;
  TextEditingController comController = new TextEditingController();
  TextEditingController posController = new TextEditingController();
  TextEditingController cityController = new TextEditingController();
  TextEditingController desController = new TextEditingController();
  TextEditingController skillController = new TextEditingController();
  TextEditingController collegeController = new TextEditingController();
  TextEditingController degController = new TextEditingController();
  TextEditingController cityTownController = new TextEditingController();
  TextEditingController descController = new TextEditingController();
  TextEditingController schoolController = new TextEditingController();
  TextEditingController schoolcityTownController = new TextEditingController();
  TextEditingController schooldescController = new TextEditingController();

  TextEditingController editcomController = new TextEditingController();
  TextEditingController editposController = new TextEditingController();
  TextEditingController editcityController = new TextEditingController();
  TextEditingController editdesController = new TextEditingController();
  TextEditingController editSkillController = new TextEditingController();
  TextEditingController editcollegeController = new TextEditingController();
  TextEditingController editdegController = new TextEditingController();
  TextEditingController editcityTownController = new TextEditingController();
  TextEditingController editdescController = new TextEditingController();
  TextEditingController editschoolController = new TextEditingController();
  TextEditingController editschoolcityTownController =
      new TextEditingController();
  TextEditingController editschooldescController = new TextEditingController();

  bool isContinue = false;
  bool editisContinue = false;
  DateTime selectedJoinDate = DateTime.now();
  DateTime selectedLeaveDate = DateTime.now();
  DateTime selectedstartDate = DateTime.now();
  DateTime selectedEndDate = DateTime.now();
  String joinDate = "Joining Date", joinDateServer = "";
  String leaveDate = "Leaving Date", leaveDateServer = "";
  String editjoinDate = "Joining Date", editjoinDateServer = "";
  String editleaveDate = "Leaving Date", editleaveDateServer = "";
  String stDate = "Stating Date", stDateServer = "";
  String endDate = "Ending Date", endDateServer = "";
  String editstDate = "Stating Date", editstDateServer = "";
  String editendDate = "Ending Date", editendDateServer = "";
  String scstDate = "Stating Date", scstDateServer = "";
  String scendDate = "Ending Date", scendDateServer = "";
  String sceditstDate = "Stating Date", sceditstDateServer = "";
  String sceditendDate = "Ending Date", sceditendDateServer = "";

  @override
  void initState() {
    setState(() {
      joinDate = DateFormat.yMMMd().format(selectedJoinDate);
      editjoinDate = DateFormat.yMMMd().format(selectedJoinDate);
      leaveDate = DateFormat.yMMMd().format(selectedLeaveDate);
      editleaveDate = DateFormat.yMMMd().format(selectedLeaveDate);
      stDate = DateFormat.yMMMd().format(selectedstartDate);
      endDate = DateFormat.yMMMd().format(selectedEndDate);
      editstDate = DateFormat.yMMMd().format(selectedstartDate);
      editendDate = DateFormat.yMMMd().format(selectedEndDate);
      scstDate = DateFormat.yMMMd().format(selectedstartDate);
      scendDate = DateFormat.yMMMd().format(selectedEndDate);
      sceditstDate = DateFormat.yMMMd().format(selectedstartDate);
      sceditendDate = DateFormat.yMMMd().format(selectedEndDate);
    });
    super.initState();
  }

  Future<Null> _selectJoinDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedJoinDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedJoinDate)
      setState(() {
        selectedJoinDate = picked;
        joinDate = DateFormat.yMMMd().format(selectedJoinDate);
        joinDateServer = DateFormat('yyyy-MM-dd').format(selectedJoinDate);

        print("joinDate");
        print(joinDate);
      });
  }

  Future<Null> _selectEditJoinDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedJoinDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedJoinDate)
      setState(() {
        selectedJoinDate = picked;
        editjoinDate = DateFormat.yMMMd().format(selectedJoinDate);
        editjoinDateServer = DateFormat('yyyy-MM-dd').format(selectedJoinDate);

        print("editjoinDate");
        print(editjoinDate);
      });
  }

  Future<Null> _selectLeaveDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedLeaveDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedLeaveDate)
      setState(() {
        selectedLeaveDate = picked;
        leaveDate = DateFormat.yMMMd().format(selectedLeaveDate);
        leaveDateServer = DateFormat('yyyy-MM-dd').format(selectedLeaveDate);

        print("leaveDate");
        print(leaveDate);
      });
  }

  Future<Null> _selectEditLeaveDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedLeaveDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedLeaveDate)
      setState(() {
        selectedLeaveDate = picked;
        editleaveDate = DateFormat.yMMMd().format(selectedLeaveDate);
        editleaveDateServer =
            DateFormat('yyyy-MM-dd').format(selectedLeaveDate);

        print("editleaveDate");
        print(editleaveDate);
      });
  }

  Future<Null> _selectStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedstartDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedstartDate)
      setState(() {
        selectedstartDate = picked;
        stDate = DateFormat.yMMMd().format(selectedstartDate);
        stDateServer = DateFormat('yyyy-MM-dd').format(selectedstartDate);

        print("stDate");
        print(stDate);
      });
  }

  Future<Null> _selectScStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedstartDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedstartDate)
      setState(() {
        selectedstartDate = picked;
        scstDate = DateFormat.yMMMd().format(selectedstartDate);
        scstDateServer = DateFormat('yyyy-MM-dd').format(selectedstartDate);

        print("scstDate");
        print(scstDate);
      });
  }

  Future<Null> _selectEditStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedstartDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedstartDate)
      setState(() {
        selectedstartDate = picked;
        editstDate = DateFormat.yMMMd().format(selectedstartDate);
        editstDateServer = DateFormat('yyyy-MM-dd').format(selectedstartDate);

        print("editstDate");
        print(editstDate);
      });
  }

  Future<Null> _selectScEditStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedstartDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedstartDate)
      setState(() {
        selectedstartDate = picked;
        sceditstDate = DateFormat.yMMMd().format(selectedstartDate);
        sceditstDateServer = DateFormat('yyyy-MM-dd').format(selectedstartDate);

        print("sceditstDate");
        print(sceditstDate);
      });
  }

  Future<Null> _selectEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedEndDate)
      setState(() {
        selectedEndDate = picked;
        endDate = DateFormat.yMMMd().format(selectedEndDate);
        endDateServer = DateFormat('yyyy-MM-dd').format(selectedEndDate);

        print("endDate");
        print(endDate);
      });
  }

  Future<Null> _selectScEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedEndDate)
      setState(() {
        selectedEndDate = picked;
        scendDate = DateFormat.yMMMd().format(selectedEndDate);
        scendDateServer = DateFormat('yyyy-MM-dd').format(selectedEndDate);

        print("scendDate");
        print(scendDate);
      });
  }

  Future<Null> _selectEditEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedEndDate)
      setState(() {
        selectedEndDate = picked;
        editendDate = DateFormat.yMMMd().format(selectedEndDate);
        editendDateServer = DateFormat('yyyy-MM-dd').format(selectedEndDate);

        print("editendDate");
        print(editendDate);
      });
  }

  Future<Null> _selectScEditEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedEndDate)
      setState(() {
        selectedEndDate = picked;
        sceditendDate = DateFormat.yMMMd().format(selectedEndDate);
        sceditendDateServer = DateFormat('yyyy-MM-dd').format(selectedEndDate);

        print("sceditendDate");
        print(sceditendDate);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: <Widget>[
            Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 15, left: 20),
                child: Text(
                  "Work",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 17,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.normal),
                )),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 30,
                    margin: EdgeInsets.only(top: 10, left: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        color: Colors.black54,
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 3.0,
                            color: Colors.black54,
                            //offset: Offset(6.0, 7.0),
                          ),
                        ],
                        border: Border.all(width: 0.5, color: Colors.black54)),
                  ),
                ],
              ),
            ),
            isWork
                ? Container()
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        isWork = true;
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child:
                                  Icon(Icons.add, size: 17, color: mainColor)),
                          Expanded(
                            child: Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "Add a work place",
                                      style: TextStyle(
                                          color: mainColor,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),
                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Column(
                  children: List.generate(work.length, (int index) {
                return Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      margin: EdgeInsets.only(
                          top: 2.5, bottom: 2.5, left: 0, right: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 20, right: 10, top: 0),
                              padding: EdgeInsets.only(right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< Profile picture >>>>> //////
                                  Container(
                                      margin: EdgeInsets.only(right: 10),
                                      child: Icon(Icons.work,
                                          size: 17, color: Colors.black38)),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text.rich(
                                          TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: work[index][
                                                              'currentlyWorking'] ==
                                                          false
                                                      ? "Former ${work[index]['position']} at"
                                                      : "${work[index]['position']} at",
                                                  style: TextStyle(
                                                      color: Colors.black54,
                                                      fontSize: 14,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w400)),
                                              TextSpan(
                                                  text:
                                                      " ${work[index]['company']}",
                                                  style: TextStyle(
                                                      color: Colors.black54,
                                                      fontSize: 14,
                                                      fontFamily: "Oswald",
                                                      fontWeight:
                                                          FontWeight.w500)),
                                              // can add more TextSpans here...
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: Text("${work[index]['city']}",
                                              style: TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 11,
                                                  fontFamily: "Oswald",
                                                  fontWeight: FontWeight.w300)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (work[index]['joinDate'] !=
                                                  "") {
                                                DateTime dateTime =
                                                    DateTime.parse(work[index]
                                                        ['joinDate']);
                                                if (dateTime != null) {
                                                  editjoinDate =
                                                      DateFormat.yMMMd()
                                                          .format(dateTime);
                                                  editjoinDateServer =
                                                      DateFormat('yyyy-MM-dd')
                                                          .format(dateTime);
                                                }
                                              } else {
                                                editjoinDate = "Joining Date";
                                              }

                                              if (work[index]['leaveDate'] !=
                                                  "") {
                                                DateTime dateTime1 =
                                                    DateTime.parse(work[index]
                                                        ['leaveDate']);
                                                if (dateTime1 != null) {
                                                  editleaveDate =
                                                      DateFormat.yMMMd()
                                                          .format(dateTime1);
                                                  editleaveDateServer =
                                                      DateFormat('yyyy-MM-dd')
                                                          .format(dateTime1);
                                                } else {
                                                  editleaveDate =
                                                      work[index]['leaveDate'];
                                                }
                                              } else {
                                                editleaveDate = "Leaving Date";
                                              }
                                              editcomController.text =
                                                  work[index]['company'];
                                              editposController.text =
                                                  work[index]['position'];
                                              editcityController.text =
                                                  work[index]['city'];
                                              // joinDate =
                                              //     work[index]['joinDate'];
                                              // leaveDate =
                                              //     work[index]['leaveDate'];
                                              editisContinue = work[index]
                                                  ['currentlyWorking'];
                                              editdesController.text =
                                                  work[index]['description'];
                                            });
                                            showWorkDialogBox(
                                                work[index], index);
                                          },
                                          child: Container(
                                              margin:
                                                  EdgeInsets.only(right: 10),
                                              padding: EdgeInsets.all(5),
                                              child: Icon(Icons.edit,
                                                  color: Colors.black26,
                                                  size: 18)),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              work.removeAt(index);
                                            });
                                            sendWork();
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(5),
                                              child: Icon(Icons.close,
                                                  color: Colors.black26,
                                                  size: 18)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider()
                  ],
                );
              })),
            ),
            !isWork
                ? Container()
                : Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller: comController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "Company",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller: posController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "Position",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller: cityController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "City Town",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            _selectJoinDate(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: 0, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        left: 10, top: 15, bottom: 15),
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.7),
                                        border: Border.all(
                                            color: Colors.grey, width: 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          joinDate,
                                          style: TextStyle(
                                              color: joinDate == "Joining Date"
                                                  ? Colors.black38
                                                  : Colors.black,
                                              fontSize: 15,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.calendar_today,
                                              color: Colors.black26,
                                              size: 16,
                                            ))
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            if (isContinue == false) {
                              _selectLeaveDate(context);
                            }
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: 0, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        left: 10, top: 15, bottom: 15),
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.7),
                                        border: Border.all(
                                            color: Colors.grey, width: 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          leaveDate,
                                          style: TextStyle(
                                              color: isContinue
                                                  ? Colors.black38
                                                  : Colors.black,
                                              fontSize: 15,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.calendar_today,
                                              color: Colors.black26,
                                              size: 16,
                                            ))
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              if (isContinue == false) {
                                isContinue = true;
                                //leaveDate = "Leaving Date";
                              } else {
                                isContinue = false;
                              }
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 20, top: 10),
                            child: Row(
                              children: <Widget>[
                                Container(
                                    decoration: BoxDecoration(
                                        color: isContinue
                                            ? mainColor.withOpacity(0.7)
                                            : Colors.white,
                                        border: Border.all(
                                            color: Colors.black45, width: 0.5),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Icon(
                                      Icons.done,
                                      color: isContinue
                                          ? Colors.white
                                          : Colors.black45,
                                      size: 14,
                                    )),
                                Container(
                                  child: Text("  am currently working here",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w300)),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: SingleChildScrollView(
                                            child: TextField(
                                              maxLines: null,
                                              maxLength: 150,
                                              controller: desController,
                                              keyboardType: TextInputType.text,
                                              style: TextStyle(
                                                color: Colors.black87,
                                                fontFamily: 'Oswald',
                                              ),
                                              decoration: InputDecoration(
                                                hintText: "Description",
                                                hintStyle: TextStyle(
                                                    color: Colors.black38,
                                                    fontSize: 15,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.w300),
                                                //labelStyle: TextStyle(color: Colors.white70),
                                                contentPadding:
                                                    EdgeInsets.fromLTRB(
                                                        10.0, 0, 10.0, 0),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isWork = false;
                                  });
                                },
                                child: Container(
                                    margin: EdgeInsets.only(top: 10, right: 10),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: Colors.grey.withOpacity(0.2),
                                        borderRadius:
                                            BorderRadius.circular(100)),
                                    child: Icon(
                                      Icons.close,
                                      size: 15,
                                    )),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isWork = false;
                                    work.add({
                                      'company': comController.text,
                                      'position': posController.text,
                                      'city': cityController.text,
                                      'joinDate': joinDateServer,
                                      'leaveDate': leaveDate == "Leaving Date"
                                          ? ""
                                          : leaveDateServer,
                                      'currentlyWorking': isContinue,
                                      'description': descController.text,
                                      'privacy': "Public",
                                      'isEdit': false,
                                    });
                                  });

                                  sendWork();
                                },
                                child: Container(
                                    margin: EdgeInsets.only(top: 10, right: 10),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: mainColor.withOpacity(0.7),
                                        borderRadius:
                                            BorderRadius.circular(100)),
                                    child: Icon(
                                      Icons.done,
                                      size: 15,
                                      color: Colors.white,
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            !isWork
                ? Container()
                : Container(
                    margin: EdgeInsets.only(top: 10, right: 10, left: 20),
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.public,
                          size: 15,
                          color: Colors.black45,
                        ),
                        SizedBox(width: 5),
                        Text("Public",
                            style: TextStyle(
                                color: mainColor,
                                fontSize: 12,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.w300)),
                      ],
                    )),
            Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: isWork ? 35 : 15, left: 20),
                child: Text(
                  "Professional Skills",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 17,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.normal),
                )),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 30,
                    margin: EdgeInsets.only(top: 10, left: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        color: Colors.black54,
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 3.0,
                            color: Colors.black54,
                            //offset: Offset(6.0, 7.0),
                          ),
                        ],
                        border: Border.all(width: 0.5, color: Colors.black54)),
                  ),
                ],
              ),
            ),
            isSkill
                ? Container()
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        isSkill = true;
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 20, right: 15, top: 15),
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child:
                                  Icon(Icons.add, size: 17, color: mainColor)),
                          Expanded(
                            child: Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "Add a professional skill",
                                      style: TextStyle(
                                          color: mainColor,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),
                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
            Container(
              margin: EdgeInsets.only(top: skill.length == 0 ? 0 : 10),
              child: Column(
                  children: List.generate(skill.length, (int index) {
                return Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      margin: EdgeInsets.only(
                          top: 2.5, bottom: 2.5, left: 0, right: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 20, right: 10, top: 0),
                              padding: EdgeInsets.only(right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< User Name >>>>> //////
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: Row(
                                                  children: <Widget>[
                                                    Icon(Icons.label_important,
                                                        color: Colors.black38,
                                                        size: 18),
                                                    Expanded(
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5),
                                                        child: Text(
                                                          "${skill[index]}",
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color:
                                                                  Colors.black,
                                                              fontFamily:
                                                                  'Oswald',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                              child: Row(
                                                children: <Widget>[
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        editSkillController
                                                                .text =
                                                            skill[index];
                                                      });
                                                      showDialogBox(
                                                          skill[index], index);
                                                    },
                                                    child: Container(
                                                        margin: EdgeInsets.only(
                                                            right: 10),
                                                        padding:
                                                            EdgeInsets.all(5),
                                                        child: Icon(Icons.edit,
                                                            color:
                                                                Colors.black26,
                                                            size: 18)),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        skill.removeAt(index);
                                                      });
                                                      sendWork();
                                                    },
                                                    child: Container(
                                                        padding:
                                                            EdgeInsets.all(5),
                                                        child: Icon(Icons.close,
                                                            color:
                                                                Colors.black26,
                                                            size: 18)),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider()
                  ],
                );
              })),
            ),
            !isSkill
                ? Container()
                : Container(
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 0, top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.all(0),
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.7),
                                        border: Border.all(
                                            color: Colors.grey, width: 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: TextField(
                                      controller: skillController,
                                      keyboardType: TextInputType.text,
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontFamily: 'Oswald',
                                      ),
                                      decoration: InputDecoration(
                                        hintText: "Professional Skill",
                                        hintStyle: TextStyle(
                                            color: Colors.black38,
                                            fontSize: 15,
                                            fontFamily: 'Oswald',
                                            fontWeight: FontWeight.w300),
                                        //labelStyle: TextStyle(color: Colors.white70),
                                        contentPadding: EdgeInsets.fromLTRB(
                                            10.0, 0, 10.0, 0),
                                        border: InputBorder.none,
                                      ),
                                    )),
                              ],
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isSkill = false;
                            });
                          },
                          child: Container(
                              margin: EdgeInsets.only(top: 10, right: 10),
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: Colors.grey.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(100)),
                              child: Icon(
                                Icons.close,
                                size: 15,
                              )),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              if (skillController.text != "") {
                                isSkill = false;
                                skill.add(skillController.text);
                                skillController.text = "";
                              }
                              sendWork();

                              // allInfo.add(
                              //     {"info": workController.text, "status": 2});
                            });
                          },
                          child: Container(
                              margin: EdgeInsets.only(top: 10, right: 10),
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: mainColor.withOpacity(0.7),
                                  borderRadius: BorderRadius.circular(100)),
                              child: Icon(
                                Icons.done,
                                size: 15,
                                color: Colors.white,
                              )),
                        ),
                      ],
                    ),
                  ),
            !isSkill
                ? Container()
                : Container(
                    margin: EdgeInsets.only(top: 10, right: 10, left: 20),
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.public,
                          size: 15,
                          color: Colors.black45,
                        ),
                        SizedBox(width: 5),
                        Text("Public",
                            style: TextStyle(
                                color: mainColor,
                                fontSize: 12,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.w300)),
                      ],
                    )),
            Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 15, left: 20),
                child: Text(
                  "College",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 17,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.normal),
                )),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 30,
                    margin: EdgeInsets.only(top: 10, left: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        color: Colors.black54,
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 3.0,
                            color: Colors.black54,
                            //offset: Offset(6.0, 7.0),
                          ),
                        ],
                        border: Border.all(width: 0.5, color: Colors.black54)),
                  ),
                ],
              ),
            ),
            isCollege
                ? Container()
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        isCollege = true;
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          left: 20,
                          right: 15,
                          top: 15,
                          bottom: education.length == 0 ? 20 : 0),
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child:
                                  Icon(Icons.add, size: 17, color: mainColor)),
                          Expanded(
                            child: Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "Add a college",
                                      style: TextStyle(
                                          color: mainColor,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),
                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Column(
                  children: List.generate(education.length, (int index) {
                return Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      margin: EdgeInsets.only(
                          top: 2.5, bottom: 2.5, left: 0, right: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 20, right: 10, top: 0),
                              padding: EdgeInsets.only(right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< Profile picture >>>>> //////
                                  Container(
                                      margin: EdgeInsets.only(right: 10),
                                      child: Icon(Icons.book,
                                          size: 17, color: Colors.black38)),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          child: Text.rich(
                                            TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                    text: "Studied ",
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 14,
                                                        fontFamily: "Oswald",
                                                        fontWeight:
                                                            FontWeight.w400)),
                                                TextSpan(
                                                    text:
                                                        "${education[index]['degree']} at",
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 14,
                                                        fontFamily: "Oswald",
                                                        fontWeight:
                                                            FontWeight.w400)),
                                                TextSpan(
                                                    text:
                                                        " ${education[index]['collage']}",
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 14,
                                                        fontFamily: "Oswald",
                                                        fontWeight:
                                                            FontWeight.w500)),
                                                // can add more TextSpans here...
                                              ],
                                            ),
                                          ),
                                        ),
                                        education[index]['city'] == ""
                                            ? Container()
                                            : Container(
                                                margin: EdgeInsets.only(top: 5),
                                                child: Text(
                                                    "${education[index]['city']}",
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 11,
                                                        fontFamily: "Oswald",
                                                        fontWeight:
                                                            FontWeight.w300)),
                                              ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (education[index]
                                                      ['startDate'] !=
                                                  "") {
                                                DateTime dateTime =
                                                    DateTime.parse(
                                                        education[index]
                                                            ['startDate']);
                                                if (dateTime != null) {
                                                  editstDate =
                                                      DateFormat.yMMMd()
                                                          .format(dateTime);
                                                  editstDateServer =
                                                      DateFormat('yyyy-MM-dd')
                                                          .format(dateTime);
                                                }
                                              } else {
                                                editstDate = "Start Date";
                                              }

                                              if (education[index]['endDate'] !=
                                                  "") {
                                                DateTime dateTime1 =
                                                    DateTime.parse(
                                                        education[index]
                                                            ['endDate']);
                                                if (dateTime1 != null) {
                                                  editendDate =
                                                      DateFormat.yMMMd()
                                                          .format(dateTime1);
                                                  editendDateServer =
                                                      DateFormat('yyyy-MM-dd')
                                                          .format(dateTime1);
                                                }
                                              } else {
                                                editendDate = "End Date";
                                              }
                                              editcollegeController.text =
                                                  education[index]['collage'];
                                              editdegController.text =
                                                  education[index]['degree'];
                                              editcityTownController.text =
                                                  education[index]['city'];
                                              editdescController.text =
                                                  education[index]
                                                      ['description'];
                                            });
                                            showEducationDialogBox(
                                                education, index);
                                          },
                                          child: Container(
                                              margin:
                                                  EdgeInsets.only(right: 10),
                                              padding: EdgeInsets.all(5),
                                              child: Icon(Icons.edit,
                                                  color: Colors.black26,
                                                  size: 18)),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              education.removeAt(index);
                                            });
                                            sendEducation();
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(5),
                                              child: Icon(Icons.close,
                                                  color: Colors.black26,
                                                  size: 18)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider()
                  ],
                );
              })),
            ),
            !isCollege
                ? Container()
                : Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller: collegeController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "College",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller: degController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "Degree/Subject",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller: cityTownController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "City Town",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            _selectStartDate(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: 0, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        left: 10, top: 15, bottom: 15),
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.7),
                                        border: Border.all(
                                            color: Colors.grey, width: 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          stDate,
                                          style: TextStyle(
                                              color: stDate == "Start Date"
                                                  ? Colors.black38
                                                  : Colors.black,
                                              fontSize: 15,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.calendar_today,
                                              color: Colors.black26,
                                              size: 16,
                                            ))
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            _selectEndDate(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: 0, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        left: 10, top: 15, bottom: 15),
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.7),
                                        border: Border.all(
                                            color: Colors.grey, width: 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          endDate,
                                          style: TextStyle(
                                              color: endDate == "End Date"
                                                  ? Colors.black38
                                                  : Colors.black,
                                              fontSize: 15,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.calendar_today,
                                              color: Colors.black26,
                                              size: 16,
                                            ))
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: SingleChildScrollView(
                                            child: TextField(
                                              maxLines: null,
                                              maxLength: 150,
                                              controller: descController,
                                              keyboardType: TextInputType.text,
                                              style: TextStyle(
                                                color: Colors.black87,
                                                fontFamily: 'Oswald',
                                              ),
                                              decoration: InputDecoration(
                                                hintText: "Description",
                                                hintStyle: TextStyle(
                                                    color: Colors.black38,
                                                    fontSize: 15,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.w300),
                                                //labelStyle: TextStyle(color: Colors.white70),
                                                contentPadding:
                                                    EdgeInsets.fromLTRB(
                                                        10.0, 0, 10.0, 0),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isCollege = false;
                                  });
                                },
                                child: Container(
                                    margin: EdgeInsets.only(top: 10, right: 10),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: Colors.grey.withOpacity(0.2),
                                        borderRadius:
                                            BorderRadius.circular(100)),
                                    child: Icon(
                                      Icons.close,
                                      size: 15,
                                    )),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isCollege = false;
                                    education.add({
                                      'collage': collegeController.text,
                                      'degree': degController.text,
                                      'city': cityTownController.text,
                                      'startDate': stDateServer,
                                      'endDate': endDateServer,
                                      'description': descController.text,
                                      'privacy': "Public",
                                    });
                                    sendEducation();
                                  });
                                },
                                child: Container(
                                    margin: EdgeInsets.only(top: 10, right: 10),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: mainColor.withOpacity(0.7),
                                        borderRadius:
                                            BorderRadius.circular(100)),
                                    child: Icon(
                                      Icons.done,
                                      size: 15,
                                      color: Colors.white,
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            !isCollege
                ? Container()
                : Container(
                    margin: EdgeInsets.only(
                        top: 10, right: 10, left: 20, bottom: 20),
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.public,
                          size: 15,
                          color: Colors.black45,
                        ),
                        SizedBox(width: 5),
                        Text("Public",
                            style: TextStyle(
                                color: mainColor,
                                fontSize: 12,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.w300)),
                      ],
                    )),

            //////// school /\/////
            ///
            Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 15, left: 20),
                child: Text(
                  "High School",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 17,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.normal),
                )),
            Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 30,
                    margin: EdgeInsets.only(top: 10, left: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        color: Colors.black54,
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 3.0,
                            color: Colors.black54,
                            //offset: Offset(6.0, 7.0),
                          ),
                        ],
                        border: Border.all(width: 0.5, color: Colors.black54)),
                  ),
                ],
              ),
            ),
            isSchool
                ? Container()
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        isSchool = true;
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          left: 20,
                          right: 15,
                          top: 15,
                          bottom: school.length == 0 ? 20 : 0),
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child:
                                  Icon(Icons.add, size: 17, color: mainColor)),
                          Expanded(
                            child: Text.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "Add a high school",
                                      style: TextStyle(
                                          color: mainColor,
                                          fontSize: 14,
                                          fontFamily: "Oswald",
                                          fontWeight: FontWeight.w400)),
                                  // can add more TextSpans here...
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Column(
                  children: List.generate(school.length, (int index) {
                return Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      margin: EdgeInsets.only(
                          top: 2.5, bottom: 2.5, left: 0, right: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin:
                                  EdgeInsets.only(left: 20, right: 10, top: 0),
                              padding: EdgeInsets.only(right: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  ////// <<<<< Profile picture >>>>> //////
                                  Container(
                                      margin: EdgeInsets.only(right: 10),
                                      child: Icon(Icons.book,
                                          size: 17, color: Colors.black38)),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          child: Text.rich(
                                            TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                    text: "Went to",
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 14,
                                                        fontFamily: "Oswald",
                                                        fontWeight:
                                                            FontWeight.w400)),
                                                TextSpan(
                                                    text:
                                                        " ${school[index]['collage']}",
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 14,
                                                        fontFamily: "Oswald",
                                                        fontWeight:
                                                            FontWeight.w500)),
                                                // can add more TextSpans here...
                                              ],
                                            ),
                                          ),
                                        ),
                                        school[index]['city'] == ""
                                            ? Container()
                                            : Container(
                                                margin: EdgeInsets.only(top: 5),
                                                child: Text(
                                                    "${school[index]['city']}",
                                                    style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 11,
                                                        fontFamily: "Oswald",
                                                        fontWeight:
                                                            FontWeight.w300)),
                                              ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (school[index]['startDate'] !=
                                                  "") {
                                                DateTime dateTime =
                                                    DateTime.parse(school[index]
                                                        ['startDate']);
                                                if (dateTime != null) {
                                                  sceditstDate =
                                                      DateFormat.yMMMd()
                                                          .format(dateTime);
                                                  sceditstDateServer =
                                                      DateFormat('yyyy-MM-dd')
                                                          .format(dateTime);
                                                }
                                              } else {
                                                sceditstDate = "Start Date";
                                              }

                                              if (school[index]['endDate'] !=
                                                  "") {
                                                DateTime dateTime1 =
                                                    DateTime.parse(school[index]
                                                        ['endDate']);
                                                if (dateTime1 != null) {
                                                  sceditendDate =
                                                      DateFormat.yMMMd()
                                                          .format(dateTime1);
                                                  sceditendDateServer =
                                                      DateFormat('yyyy-MM-dd')
                                                          .format(dateTime1);
                                                }
                                              } else {
                                                sceditendDate = "End Date";
                                              }
                                              editschoolController.text =
                                                  school[index]['collage'];
                                              editschoolcityTownController
                                                  .text = school[index]['city'];
                                              editschooldescController.text =
                                                  school[index]['description'];
                                            });
                                            showSchoolDialogBox(school, index);
                                          },
                                          child: Container(
                                              margin:
                                                  EdgeInsets.only(right: 10),
                                              padding: EdgeInsets.all(5),
                                              child: Icon(Icons.edit,
                                                  color: Colors.black26,
                                                  size: 18)),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              school.removeAt(index);
                                            });
                                            sendSchool();
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(5),
                                              child: Icon(Icons.close,
                                                  color: Colors.black26,
                                                  size: 18)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider()
                  ],
                );
              })),
            ),
            !isSchool
                ? Container()
                : Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller: schoolController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "College",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: TextField(
                                            controller:
                                                schoolcityTownController,
                                            keyboardType: TextInputType.text,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Oswald',
                                            ),
                                            decoration: InputDecoration(
                                              hintText: "City Town",
                                              hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 15,
                                                  fontFamily: 'Oswald',
                                                  fontWeight: FontWeight.w300),
                                              //labelStyle: TextStyle(color: Colors.white70),
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      10.0, 0, 10.0, 0),
                                              border: InputBorder.none,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            _selectScStartDate(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: 0, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        left: 10, top: 15, bottom: 15),
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.7),
                                        border: Border.all(
                                            color: Colors.grey, width: 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          scstDate,
                                          style: TextStyle(
                                              color: scstDate == "Start Date"
                                                  ? Colors.black38
                                                  : Colors.black,
                                              fontSize: 15,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.calendar_today,
                                              color: Colors.black26,
                                              size: 16,
                                            ))
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            _selectScEndDate(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: 0, top: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        left: 10, top: 15, bottom: 15),
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.7),
                                        border: Border.all(
                                            color: Colors.grey, width: 0.2),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          scendDate,
                                          style: TextStyle(
                                              color: scendDate == "End Date"
                                                  ? Colors.black38
                                                  : Colors.black,
                                              fontSize: 15,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.calendar_today,
                                              color: Colors.black26,
                                              size: 16,
                                            ))
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    //mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(0),
                                          margin: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.white.withOpacity(0.7),
                                              border: Border.all(
                                                  color: Colors.grey,
                                                  width: 0.2),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: SingleChildScrollView(
                                            child: TextField(
                                              maxLines: null,
                                              maxLength: 150,
                                              controller: schooldescController,
                                              keyboardType: TextInputType.text,
                                              style: TextStyle(
                                                color: Colors.black87,
                                                fontFamily: 'Oswald',
                                              ),
                                              decoration: InputDecoration(
                                                hintText: "Description",
                                                hintStyle: TextStyle(
                                                    color: Colors.black38,
                                                    fontSize: 15,
                                                    fontFamily: 'Oswald',
                                                    fontWeight:
                                                        FontWeight.w300),
                                                //labelStyle: TextStyle(color: Colors.white70),
                                                contentPadding:
                                                    EdgeInsets.fromLTRB(
                                                        10.0, 0, 10.0, 0),
                                                border: InputBorder.none,
                                              ),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isSchool = false;
                                  });
                                },
                                child: Container(
                                    margin: EdgeInsets.only(top: 10, right: 10),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: Colors.grey.withOpacity(0.2),
                                        borderRadius:
                                            BorderRadius.circular(100)),
                                    child: Icon(
                                      Icons.close,
                                      size: 15,
                                    )),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isSchool = false;
                                    school.add({
                                      'collage': schoolController.text,
                                      'city': schoolcityTownController.text,
                                      'startDate': scstDateServer,
                                      'endDate': scendDateServer,
                                      'description': schooldescController.text,
                                      'privacy': "Public",
                                    });
                                    sendSchool();
                                  });
                                },
                                child: Container(
                                    margin: EdgeInsets.only(top: 10, right: 10),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: mainColor.withOpacity(0.7),
                                        borderRadius:
                                            BorderRadius.circular(100)),
                                    child: Icon(
                                      Icons.done,
                                      size: 15,
                                      color: Colors.white,
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            !isSchool
                ? Container()
                : Container(
                    margin: EdgeInsets.only(
                        top: 10, right: 10, left: 20, bottom: 20),
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.public,
                          size: 15,
                          color: Colors.black45,
                        ),
                        SizedBox(width: 5),
                        Text("Public",
                            style: TextStyle(
                                color: mainColor,
                                fontSize: 12,
                                fontFamily: "Oswald",
                                fontWeight: FontWeight.w300)),
                      ],
                    )),
          ],
        ),
      ),
    );
  }

  Future<Null> showDialogBox(skills, ind) async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            content: SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  Container(
                    color: Colors.transparent,
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(20),
                        margin: EdgeInsets.only(top: 35),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.only(top: 0),
                                child: Text(
                                  "Edit $skills",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontFamily: 'Oswald',
                                      fontWeight: FontWeight.w400),
                                )),
                            Container(
                              margin: EdgeInsets.only(bottom: 5, top: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.all(0),
                                      margin: EdgeInsets.only(
                                          left: 0, right: 0, top: 5),
                                      decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.7),
                                          border: Border.all(
                                              color: Colors.grey, width: 0.2),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: TextField(
                                        controller: editSkillController,
                                        keyboardType: TextInputType.text,
                                        style: TextStyle(
                                          color: Colors.black87,
                                          fontFamily: 'Oswald',
                                        ),
                                        decoration: InputDecoration(
                                          hintText: "Professional Skill",
                                          hintStyle: TextStyle(
                                              color: Colors.black38,
                                              fontSize: 15,
                                              fontFamily: 'Oswald',
                                              fontWeight: FontWeight.w300),
                                          //labelStyle: TextStyle(color: Colors.white70),
                                          contentPadding: EdgeInsets.fromLTRB(
                                              10.0, 0, 10.0, 0),
                                          border: InputBorder.none,
                                        ),
                                      )),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          Navigator.of(context).pop();
                                        });
                                      },
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.only(
                                              left: 0,
                                              right: 10,
                                              top: 20,
                                              bottom: 0),
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.3),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Text(
                                            "Cancel",
                                            style: TextStyle(
                                              color: Colors.black45,
                                              fontSize: 12,
                                              fontFamily: 'BebasNeue',
                                            ),
                                            textAlign: TextAlign.center,
                                          )),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          setState(() {
                                            skill[ind] =
                                                editSkillController.text;
                                          });
                                          print(skill);
                                          Navigator.of(context).pop();
                                          sendWork();
                                        });
                                      },
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.only(
                                              left: 10,
                                              right: 0,
                                              top: 20,
                                              bottom: 0),
                                          decoration: BoxDecoration(
                                              color: mainColor,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Text(
                                            "Edit",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12,
                                              fontFamily: 'BebasNeue',
                                            ),
                                            textAlign: TextAlign.center,
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<Null> showWorkDialogBox(works, ind) async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        color: Colors.transparent,
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.only(top: 35),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 10),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller: editcomController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "Company",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller: editposController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "Position",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller: editcityController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "City Town",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    _selectEditJoinDate(context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.only(
                                            left: 10, top: 15, bottom: 15),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  editjoinDate,
                                                  style: TextStyle(
                                                      color: editjoinDate ==
                                                              "Joining Date"
                                                          ? Colors.black38
                                                          : Colors.black,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                ),
                                              ),
                                            ),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(right: 10),
                                                child: Icon(
                                                  Icons.calendar_today,
                                                  color: Colors.black26,
                                                  size: 16,
                                                ))
                                          ],
                                        )),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    if (editisContinue == false) {
                                      _selectEditLeaveDate(context);
                                    }
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      //mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            padding: EdgeInsets.only(
                                                left: 10, top: 15, bottom: 15),
                                            margin: EdgeInsets.only(
                                                left: 0, right: 0, top: 5),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                                    .withOpacity(0.7),
                                                border: Border.all(
                                                    color: Colors.grey,
                                                    width: 0.2),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  editleaveDate,
                                                  style: TextStyle(
                                                      color: isContinue
                                                          ? Colors.black38
                                                          : Colors.black,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        right: 10),
                                                    child: Icon(
                                                      Icons.calendar_today,
                                                      color: Colors.black26,
                                                      size: 16,
                                                    ))
                                              ],
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      if (editisContinue == false) {
                                        editisContinue = true;
                                        //leaveDate = "Leaving Date";
                                      } else {
                                        editisContinue = false;
                                      }
                                    });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(left: 0, top: 10),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                            decoration: BoxDecoration(
                                                color: editisContinue
                                                    ? mainColor.withOpacity(0.7)
                                                    : Colors.white,
                                                border: Border.all(
                                                    color: Colors.black45,
                                                    width: 0.5),
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            child: Icon(
                                              Icons.done,
                                              color: editisContinue
                                                  ? Colors.white
                                                  : Colors.black45,
                                              size: 14,
                                            )),
                                        Container(
                                          child: Text(
                                              "  am currently working here",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                  fontFamily: "Oswald",
                                                  fontWeight: FontWeight.w300)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.all(0),
                                      margin: EdgeInsets.only(
                                          left: 0, right: 0, top: 5),
                                      decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.7),
                                          border: Border.all(
                                              color: Colors.grey, width: 0.2),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: SingleChildScrollView(
                                        child: TextField(
                                          maxLines: null,
                                          maxLength: 150,
                                          controller: editdesController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "Description",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      )),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              editcomController.text = "";
                                              editposController.text = "";
                                              editcityController.text = "";
                                              editjoinDate = joinDate;
                                              editleaveDate = leaveDate;
                                              editisContinue = false;
                                              editdesController.text = "";
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.only(
                                                  left: 0,
                                                  right: 10,
                                                  top: 20,
                                                  bottom: 0),
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.3),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Text(
                                                "Cancel",
                                                style: TextStyle(
                                                  color: Colors.black45,
                                                  fontSize: 12,
                                                  fontFamily: 'BebasNeue',
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                            setState(() {
                                              print(works['company']);
                                              print(editcomController.text);
                                              works['company'] =
                                                  editcomController.text;
                                              works['position'] =
                                                  editposController.text;
                                              works['city'] =
                                                  editcityController.text;
                                              works['joinDate'] =
                                                  editjoinDateServer;
                                              works['leaveDate'] =
                                                  editisContinue
                                                      ? ""
                                                      : editleaveDateServer;
                                              works['currentlyWorking'] =
                                                  editisContinue;
                                              works['description'] =
                                                  editdesController.text;
                                              works['privacy'] = "Public";
                                              works['isEdit'] = true;
                                              print(work);

                                              sendWork();
                                              editcomController.text = "";
                                              editposController.text = "";
                                              editcityController.text = "";
                                              editjoinDate = joinDate;
                                              editleaveDate = leaveDate;
                                              editisContinue = false;
                                              editdesController.text = "";
                                            });
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.only(
                                                  left: 10,
                                                  right: 0,
                                                  top: 20,
                                                  bottom: 0),
                                              decoration: BoxDecoration(
                                                  color: mainColor,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Text(
                                                "Edit",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontFamily: 'BebasNeue',
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        });
  }

  Future<Null> showEducationDialogBox(edu, ind) async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        color: Colors.transparent,
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.only(top: 35),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 10),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller: editcollegeController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "College",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller: editdegController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "Degree/Subject",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller: editcityTownController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "City Town",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    _selectEditStartDate(context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.only(
                                            left: 10, top: 15, bottom: 15),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  editstDate,
                                                  style: TextStyle(
                                                      color: editstDate ==
                                                              "Start Date"
                                                          ? Colors.black38
                                                          : Colors.black,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                ),
                                              ),
                                            ),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(right: 10),
                                                child: Icon(
                                                  Icons.calendar_today,
                                                  color: Colors.black26,
                                                  size: 16,
                                                ))
                                          ],
                                        )),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    _selectEditEndDate(context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      //mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            padding: EdgeInsets.only(
                                                left: 10, top: 15, bottom: 15),
                                            margin: EdgeInsets.only(
                                                left: 0, right: 0, top: 5),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                                    .withOpacity(0.7),
                                                border: Border.all(
                                                    color: Colors.grey,
                                                    width: 0.2),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  editendDate,
                                                  style: TextStyle(
                                                      color: editendDate ==
                                                              "End Date"
                                                          ? Colors.black38
                                                          : Colors.black,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        right: 10),
                                                    child: Icon(
                                                      Icons.calendar_today,
                                                      color: Colors.black26,
                                                      size: 16,
                                                    ))
                                              ],
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.all(0),
                                      margin: EdgeInsets.only(
                                          left: 0, right: 0, top: 5),
                                      decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.7),
                                          border: Border.all(
                                              color: Colors.grey, width: 0.2),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: SingleChildScrollView(
                                        child: TextField(
                                          maxLines: null,
                                          maxLength: 150,
                                          controller: editdescController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "Description",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      )),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              editcollegeController.text = "";
                                              editdegController.text = "";
                                              editcityTownController.text = "";
                                              editstDate = stDate;
                                              editendDate = endDate;
                                              editdescController.text = "";
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.only(
                                                  left: 0,
                                                  right: 10,
                                                  top: 20,
                                                  bottom: 0),
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.3),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Text(
                                                "Cancel",
                                                style: TextStyle(
                                                  color: Colors.black45,
                                                  fontSize: 12,
                                                  fontFamily: 'BebasNeue',
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                            setState(() {
                                              print(edu[ind]['collage']);
                                              print(editcollegeController.text);
                                              edu[ind]['collage'] =
                                                  editcollegeController.text;
                                              edu[ind]['degree'] =
                                                  editdegController.text;
                                              edu[ind]['city'] =
                                                  editcityTownController.text;
                                              edu[ind]['startDate'] =
                                                  editstDateServer;
                                              edu[ind]['endDate'] =
                                                  editendDateServer;
                                              edu[ind]['description'] =
                                                  editdescController.text;
                                              edu[ind]['privacy'] = "Public";
                                              print(edu[ind]);
                                              print(education);

                                              sendEducation();
                                              editcollegeController.text = "";
                                              editdegController.text = "";
                                              editcityTownController.text = "";
                                              editstDate = stDate;
                                              editendDate = endDate;
                                              editdescController.text = "";
                                            });
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.only(
                                                  left: 10,
                                                  right: 0,
                                                  top: 20,
                                                  bottom: 0),
                                              decoration: BoxDecoration(
                                                  color: mainColor,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Text(
                                                "Edit",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontFamily: 'BebasNeue',
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        });
  }

  Future<Null> showSchoolDialogBox(sch, ind) async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        color: Colors.transparent,
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.only(top: 35),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 10),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller: editschoolController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "College",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: TextField(
                                          controller:
                                              editschoolcityTownController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "City Town",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        )),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    _selectScEditStartDate(context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.only(
                                            left: 10, top: 15, bottom: 15),
                                        margin: EdgeInsets.only(
                                            left: 0, right: 0, top: 5),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            border: Border.all(
                                                color: Colors.grey, width: 0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  sceditstDate,
                                                  style: TextStyle(
                                                      color: sceditstDate ==
                                                              "Start Date"
                                                          ? Colors.black38
                                                          : Colors.black,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                ),
                                              ),
                                            ),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(right: 10),
                                                child: Icon(
                                                  Icons.calendar_today,
                                                  color: Colors.black26,
                                                  size: 16,
                                                ))
                                          ],
                                        )),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    _selectScEditEndDate(context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 0, top: 5),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      //mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            padding: EdgeInsets.only(
                                                left: 10, top: 15, bottom: 15),
                                            margin: EdgeInsets.only(
                                                left: 0, right: 0, top: 5),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                                    .withOpacity(0.7),
                                                border: Border.all(
                                                    color: Colors.grey,
                                                    width: 0.2),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  sceditendDate,
                                                  style: TextStyle(
                                                      color: sceditendDate ==
                                                              "End Date"
                                                          ? Colors.black38
                                                          : Colors.black,
                                                      fontSize: 15,
                                                      fontFamily: 'Oswald',
                                                      fontWeight:
                                                          FontWeight.w300),
                                                ),
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        right: 10),
                                                    child: Icon(
                                                      Icons.calendar_today,
                                                      color: Colors.black26,
                                                      size: 16,
                                                    ))
                                              ],
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 0, top: 5),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.all(0),
                                      margin: EdgeInsets.only(
                                          left: 0, right: 0, top: 5),
                                      decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.7),
                                          border: Border.all(
                                              color: Colors.grey, width: 0.2),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: SingleChildScrollView(
                                        child: TextField(
                                          maxLines: null,
                                          maxLength: 150,
                                          controller: editschooldescController,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontFamily: 'Oswald',
                                          ),
                                          decoration: InputDecoration(
                                            hintText: "Description",
                                            hintStyle: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 15,
                                                fontFamily: 'Oswald',
                                                fontWeight: FontWeight.w300),
                                            //labelStyle: TextStyle(color: Colors.white70),
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 0, 10.0, 0),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      )),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              editschoolController.text = "";
                                              editschoolcityTownController
                                                  .text = "";
                                              sceditstDate = scstDate;
                                              sceditendDate = scendDate;
                                              editschooldescController.text =
                                                  "";
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.only(
                                                  left: 0,
                                                  right: 10,
                                                  top: 20,
                                                  bottom: 0),
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.3),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Text(
                                                "Cancel",
                                                style: TextStyle(
                                                  color: Colors.black45,
                                                  fontSize: 12,
                                                  fontFamily: 'BebasNeue',
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                            setState(() {
                                              print(sch[ind]['collage']);
                                              print(editschoolController.text);
                                              sch[ind]['collage'] =
                                                  editschoolController.text;
                                              sch[ind]['city'] =
                                                  editschoolcityTownController
                                                      .text;
                                              sch[ind]['startDate'] =
                                                  sceditstDateServer;
                                              sch[ind]['endDate'] =
                                                  sceditendDateServer;
                                              sch[ind]['description'] =
                                                  editschooldescController.text;
                                              sch[ind]['privacy'] = "Public";
                                              print(sch[ind]);
                                              print(school);

                                              sendSchool();
                                              editschoolController.text = "";
                                              editschoolcityTownController
                                                  .text = "";
                                              sceditstDate = scstDate;
                                              sceditendDate = scendDate;
                                              editschooldescController.text =
                                                  "";
                                            });
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(10),
                                              margin: EdgeInsets.only(
                                                  left: 10,
                                                  right: 0,
                                                  top: 20,
                                                  bottom: 0),
                                              decoration: BoxDecoration(
                                                  color: mainColor,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Text(
                                                "Edit",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontFamily: 'BebasNeue',
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        });
  }

  Future<void> sendWork() async {
    var data = {
      'work': work,
      'pro_skills': skill,
    };

    print(data);

    var res = await CallApi().postData1(data, 'profile/work');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      setState(() {
        comController.text = "";
        posController.text = "";
        cityController.text = "";
        editjoinDate = joinDate;
        editleaveDate = leaveDate;
        isContinue = false;
        desController.text = "";
      });
      _showMessage("Success!", 2);
    } else {
      _showMessage(body['message'], 1);
    }
  }

  Future<void> sendEducation() async {
    var data = {
      'collages': education,
      'high_schools': school,
    };

    print(data);

    var res = await CallApi().postData1(data, 'profile/education');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      setState(() {
        collegeController.text = "";
        degController.text = "";
        cityTownController.text = "";
        descController.text = "";
      });
      _showMessage("Success!", 2);
    } else {
      _showMessage(body['message'], 1);
    }
  }

  Future<void> sendSchool() async {
    var data = {
      'collages': education,
      'high_schools': school,
    };

    print(data);

    var res = await CallApi().postData1(data, 'profile/education');
    var body = json.decode(res.body);
    print(body);

    if (res.statusCode == 200) {
      setState(() {
        schoolController.text = "";
        schoolcityTownController.text = "";
        schooldescController.text = "";
      });
      _showMessage("Success!", 2);
    } else {
      _showMessage(body['message'], 1);
    }
  }

  _showMessage(msg, numb) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        //timeInSecForIos: 1,
        backgroundColor: numb == 1
            ? Colors.red.withOpacity(0.9)
            : mainColor.withOpacity(0.9),
        textColor: Colors.white,
        fontSize: 13.0);
  }
}
