import 'dart:convert';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:social_app_fb/API/api.dart';
import 'package:social_app_fb/Cards/SearchCard/searchCard.dart';

import '../../main.dart';

class SearchPage extends StatefulWidget {
  @override
  SearchPageState createState() => new SearchPageState();
}

class SearchPageState extends State<SearchPage> {
  int _current = 0;
  int _isBack = 0;
  int activePage = 1;
  String result = '';
  bool _isChecked = false;
  bool isOpen = false;
  List<String> selectedCategory = [];
  var selectedCat;
  SharedPreferences sharedPreferences;
  String theme = "", productCategory = "", seller = "", sp = "";
  bool loading = false, isLoading = true;
  var searchList;
  TextEditingController src = new TextEditingController();
  TextEditingController controller = new TextEditingController();

  @override
  void initState() {
    searchResult("All");

    super.initState();
  }

  Future searchData(String result) async {
    setState(() {
      loading = true;
    });

    var data = {'search_text': result};
    var searchtresponse = await CallApi().postData1(data, 'search/people');
    var searchcontent = searchtresponse.body;
    var searchs = json.decode(searchcontent);

    setState(() {
      searchList = searchs;
    });
    setState(() {
      loading = false;
    });
    // print("searchList.searchResult.length");
    // print(searchList.searchResult.length);
  }

  Future searchResult(String value) async {
    setState(() {
      loading = true;
      result = value;
    });
    print(value);
    var data = {
      'search_text': value == "" ? "All" : value,
    };

    print(data);

    await Future.delayed(Duration(seconds: 1));

    var res = await CallApi().postData1(data, 'search/people');
    var body = json.decode(res.body);

    setState(() {
      searchList = body;
      loading = false;
    });
    print(searchList);
    //print(searchList['searchResult'].length);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.grey),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          titleSpacing: 0.0,
          title: Container(
            margin: EdgeInsets.only(top: 0),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 5), child: BackButton()),
                Expanded(
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      //padding: EdgeInsets.all(5),
                      margin: EdgeInsets.only(
                          left: 0, right: 10, top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          color: Colors.grey[100],
                          border: Border.all(color: Colors.grey, width: 0.2),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: TextField(
                              onChanged: (value) {
                                setState(() {
                                  searchResult(value);
                                });
                              },
                              controller: src,
                              autofocus: true,
                              keyboardType: TextInputType.emailAddress,
                              style: TextStyle(
                                color: Colors.black87,
                                fontFamily: 'Oswald',
                              ),
                              decoration: InputDecoration(
                                hintText: "Search",
                                hintStyle: TextStyle(
                                    color: Colors.black45,
                                    fontSize: 15,
                                    fontFamily: 'Oswald',
                                    fontWeight: FontWeight.w300),
                                //labelStyle: TextStyle(color: Colors.white70),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 0, 20.0, 0),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ],
                      )),
                ),
              ],
            ),
          ),
          actions: <Widget>[],
        ),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : Container(
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverToBoxAdapter(
                        child: Column(
                      children: <Widget>[
                        Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(top: 10, left: 20),
                            child: Text(
                              "Search",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontFamily: 'Oswald',
                                  fontWeight: FontWeight.normal),
                            )),
                        Row(
                          children: <Widget>[
                            Container(
                              width: 30,
                              margin: EdgeInsets.only(
                                  top: 10, left: 20, bottom: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15.0)),
                                  color: Colors.black,
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 3.0,
                                      color: Colors.black,
                                      //offset: Offset(6.0, 7.0),
                                    ),
                                  ],
                                  border: Border.all(
                                      width: 0.5, color: Colors.black)),
                            ),
                          ],
                        ),
                      ],
                    )),
                    SliverPadding(
                      padding: EdgeInsets.only(bottom: 15, top: 5, left: 20),
                      sliver: loading == true
                          ? SliverToBoxAdapter(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Please wait...",
                                    style: TextStyle(
                                        fontFamily: "Oswald",
                                        color: Colors.grey[600]),
                                  ),
                                ],
                              ),
                            )
                          : searchList == null
                              ? SliverToBoxAdapter()
                              : searchList['searchResult'].length == 0
                                  ? SliverPadding(
                                      padding: EdgeInsets.only(left: 0),
                                      sliver: SliverToBoxAdapter(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              "No result found!",
                                              style: TextStyle(
                                                  fontFamily: "Oswald",
                                                  color: Colors.grey[600]),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SliverPadding(
                                      padding: EdgeInsets.only(left: 0),
                                      sliver: SliverToBoxAdapter(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              searchList['searchResult']
                                                          .length ==
                                                      1
                                                  ? "${searchList['searchResult'].length} result found"
                                                  : "${searchList['searchResult'].length} results found",
                                              style: TextStyle(
                                                  fontFamily: "Oswald",
                                                  color: Colors.grey[600]),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                    ),
                    searchList == null
                        ? SliverToBoxAdapter()
                        : SliverList(
                            delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                              return SearchCard(
                                  searchList['searchResult'][index]);
                            }, childCount: searchList['searchResult'].length),
                          )
                  ],
                ),
              ));
  }
}
