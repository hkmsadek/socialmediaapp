import 'package:flutter/material.dart';
import 'package:social_app_fb/MainScreens/EventPastPage/EventPastPage.dart';
import 'package:social_app_fb/MainScreens/EventUpcomingPage/EventUpcomingPage.dart';
import 'package:social_app_fb/MainScreens/EventsAddPage/events_add.dart';

import '../../main.dart';

class EventsPage extends StatefulWidget {
  @override
  _EventsPageState createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.grey),
          //automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          title: Container(
            margin: EdgeInsets.only(top: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 5, right: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      //color: Colors.black.withOpacity(0.5),
                    ),
                    child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Events",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 20,
                              fontFamily: 'Oswald',
                              fontWeight: FontWeight.normal),
                        )),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EventAddPage()));
                  },
                  child: Padding(
                    child: Icon(Icons.add),
                    padding: EdgeInsets.all(5),
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[],
        ),
        body: DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: new AppBar(
                elevation: 0,
                automaticallyImplyLeading: false,
                backgroundColor: Colors.white,
                //actions: <Widget>[],
                title: new TabBar(
                  isScrollable: false,
                  labelColor: mainColor,
                  labelStyle:
                      TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                  tabs: [
                    new Tab(text: "Upcoming"),
                    new Tab(text: "Past"),
                  ],
                  indicatorColor: mainColor,
                  unselectedLabelColor: Colors.grey,
                ),
              ),
              body: TabBarView(children: <Widget>[
                SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: EventUpcomingPage()),
                SingleChildScrollView(
                    physics: BouncingScrollPhysics(), child: EventPastPage()),
              ]),
            )));
  }
}
