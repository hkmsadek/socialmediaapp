import 'package:flutter/material.dart';
import 'package:social_app_fb/MainScreens/AlbumPage/AlbumPage.dart';
import 'package:social_app_fb/MainScreens/PicturePage/PicturePage.dart';
import 'package:social_app_fb/MainScreens/VideoPage/VideoPage.dart';
import 'package:social_app_fb/main.dart';

class PhotoPage extends StatefulWidget {
  @override
  _PhotoPageState createState() => _PhotoPageState();
}

class _PhotoPageState extends State<PhotoPage> {
  var data = [
    new Tab(text: "PHOTOS"),
    new Tab(text: "ALBUMS"),
    new Tab(text: "VIDEOS")
  ];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black54),
        //automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Container(
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    //color: Colors.black.withOpacity(0.5),
                  ),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Photos",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.normal),
                      )),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[],
      ),
      body: DefaultTabController(
          length: data.length,
          child: Scaffold(
            appBar: new AppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              //actions: <Widget>[],
              title: new TabBar(
                isScrollable: false,
                labelColor: mainColor,
                labelStyle:
                    TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                tabs: data,
                indicatorColor: mainColor,
                unselectedLabelColor: Colors.grey,
              ),
            ),
            body: Container(
              color: Colors.white,
              child: TabBarView(children: <Widget>[
                SingleChildScrollView(
                    physics: BouncingScrollPhysics(), child: PicturePage()),
                SingleChildScrollView(
                    physics: BouncingScrollPhysics(), child: AlbumPage()),
                SingleChildScrollView(
                    physics: BouncingScrollPhysics(), child: VideoPage()),
              ]),
            ),
          )),
    );
  }
}
